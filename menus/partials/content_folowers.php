<div class="bg-inverse innerAll inner-2x">
    <div class="media margin-none">
        <div class="pull-left"><i class="fa fa-fw fa-eye fa-2x"></i></div>
        <div class="media-body">
            <div class="lead margin-none strong">5,550</div>
            <p class="margin-none">followers</p>
        </div>
    </div>
</div>