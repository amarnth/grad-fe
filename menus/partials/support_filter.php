<div class="innerAll">
    <ul>
        <li><a href=""><span class="fa fa-fw fa-circle-o text-success"></span> Answered</a></li>
        <li><a href=""><span class="fa fa-fw fa-circle-o text-primary"></span> Unanswered</a></li>
        <li><a href=""><span class="fa fa-fw fa-circle-o text-info"></span> Pending</a></li>
    </ul>
</div>