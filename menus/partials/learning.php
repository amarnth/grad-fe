<div class="media innerAll margin-none bg-inverse">
    <div class="pull-left">
        Subscription
        <div class="strong">25 days left</div>
    </div>
    <div class="media-body innerAll">
        <div class="progress progress-mini margin-none">
            <div class="progress-bar progress-bar-primary" style="width: 80%"></div>
        </div>
    </div>
</div>