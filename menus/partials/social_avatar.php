<?php

if ($this->loader->sidebar_type == 'discover'):?>

    <div class="profile-avatar innerT">
        <div class="text-center innerTB">
            <span class="display-block-inline animated fadeInDown"><img src="<?php echo getURL(); ?>images/people/100/16.jpg" class="img-responsive img-circle thumb" /></span>
            <div class="btn-avatar btn-group btn-group-xs">
                <a href="#" class="btn"><i class="fa fa-cog"></i></a>
            </div>
        </div>
    </div>

    <div class="innerAll">
        <h4 class="text-white margin-none"><i class="fa text-small fa-circle text-success pull-right"></i> MosaicPRO</h4>
        <p class="margin-none text-muted text-small">Premium Web Templates</p>
    </div>

    <div class="innerLR innerB center">
        <div class="btn-group-vertical btn-group-sm display-block margin-none">
            <a href="#" class="btn btn-inverse"><i class="fa fa-fw fa-check"></i> Friend</a>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->loader->sidebar_type == 'fusion') :?>

    <ul class="reset">
        <li class="btn-avatar btn-group dropdown dropdown-menu-light btn-group-xs">
            <a href="#" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
            <ul class="dropdown-menu pull-right">
                <li class="active"><a data-toggle="tab" href="#profile_avatar_small" class="not-animated">Small</a></li>
                <li><a data-toggle="tab" href="#profile_avatar_large" class="not-animated">Large</a></li>
            </ul>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="profile_avatar_small">
            <div class="innerL innerT innerB">
                <div class="media innerL">
                    <div class="animated bounceIn pull-left"><img src="<?php echo getURL(); ?>images/people/50/16.jpg" width="48" class="img-circle thumb" alt="people" /><i class="bg-success"></i></div>
                    <div class="media-body innerL half">
                        <h4 class="text-white">MosaicPro</h4>
                        <a href="#" class="btn btn-xs btn-inverse"><i class="fa fa-fw fa-check"></i> Friend</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="profile_avatar_large">
            <div class="profile-avatar innerT">
                <div class="text-center innerTB">
                    <span class="display-block-inline animated bounceIn"><img src="<?php echo getURL(); ?>images/people/100/16.jpg" class="img-responsive img-circle thumb" alt="people" /></span>
                </div>
            </div>
            <div class="innerAll">
                <h4 class="text-white margin-none"><i class="fa text-small fa-circle text-success pull-right"></i> MosaicPRO</h4>
                <p class="margin-none text-muted text-small">Premium Web Templates</p>
            </div>
            <div class="innerLR innerB center">
                <a href="#" class="btn btn-sm btn-block btn-inverse"><i class="fa fa-fw fa-check"></i> Friend</a>
            </div>
        </div>
    </div>
<?php endif;?>

