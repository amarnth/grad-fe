<div class="innerAll text-center">
    <div class="btn-group btn-block dropdown">
        <a href="#" data-toggle="dropdown" class="btn btn-block btn-inverse dropdown-toggle"><span class="caret"></span> Compose <i class="fa fa-fw fa-pencil text-primary"></i></a>
        <ul class="dropdown-menu pull-right">
            <li><a href="<?php echo getURL(array('email_compose')); ?>">Compose page</a></li>
            <li><a href="#modal-compose" data-toggle="modal">Compose modal</a></li>
        </ul>
    </div>
</div>