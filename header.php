<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceSimple sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceSimple sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceSimple sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceSimple sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if !IE]><!--><html class="paceSimple sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"><!-- <![endif]-->
<head>
    <title>Grad</title>
    <link rel="icon" href="assets/grad_assets/img/favicon.png" type="image/png" sizes="16x16">
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--
**********************************************************
In development, use the LESS files and the less.js compiler
instead of the minified CSS loaded by default.
**********************************************************
<link rel="stylesheet/less" href="assets/less/admin/module.admin.stylesheet-complete.less" />
    --> 
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/components/library/bootstrap/css/bootstrap.min.css" />
    <![endif]-->
    <?php
$skin = isset($_GET['skin']) && $_GET['skin'] !== 'style-default' ? $_GET['skin'] : false;
if ($skin)
echo '<link href="' . ASSETS_PATH . '/css/skins/module.' . $module . '.stylesheet-complete.skin.' . $skin . '.min.css" rel="stylesheet" />
        ';
else

// echo '<link href="' . ASSETS_PATH . '/css/' . $module . '/module.' . $module . '.stylesheet-complete.min.css" rel="stylesheet" />';
echo '<link href="' . ASSETS_PATH . '/css/skins/module.swift-shift.css" rel="stylesheet" />';

?> 


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <?php foreach ($scripts as $id =>
            $script)
{
$sections = !empty($script['sections']) && !empty($script['sections'][$page]);
$inPages = in_array($page, $script['pages']);
$inSections = !$sections ? false : in_array($section, $script['sections'][$page]); if ($script['header'] && ((!$sections && $inPages) || ($sections && $inSections)))
echo '
            <script src="' . $script['file'] . '"></script>
            ' . "\n\t";
} ?> 
            <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
        </head>
        <body class="menu-right">  <!-- menu-right-hidden -->
            <!-- Main Container Fluid -->
            <div class="container-fluid menu-hidden">
                <!-- Main Sidebar Menu -->
                <div id="menu" class="hidden-print hidden-xs sidebar-blue sidebar-brand-primary">
                    <div id="sidebar-fusion-wrapper">
                        <div id="brandWrapper">
                        
                            <a href="index.php?page=index" class="display-block-inline pull-left logo" style="width: 100%;">
                                <!-- <img src="assets/grad_assets/img/logo-icon.png" alt=""> -->
                                <img src="assets/grad_assets/img/logo.png" alt="" style="width: 65%; margin: 3px;">
                            </a>
                            
                            
                            <!-- <a href="index.php?page=index"><span class="text">Coral</span></a> -->
                            
                        </div>
                        <!-- <div id="logoWrapper">
                            <div id="logo">
                                <a href="index.php?page=index" class="btn btn-sm btn-inverse"><i class="fa fa-fw icon-home-fill-1"></i></a>
                                <a href="index.php?page=email" class="btn btn-sm btn-inverse"><i class="fa fa-fw fa-envelope"></i><span class="badge pull-right badge-primary">2</span></a>
                                <div id="toggleNavbarColor" data-toggle="navbar-color">
<a href="" class="not-animated color color-blue active"></a>
<a href="" class="not-animated color color-white"></a>
<a href="" class="not-animated color bg-primary"></a>
<a href="" class="not-animated color color-inverse"></a>
</div>
                                <div class="innerTB">
                                    <select name="" id="menu_switch" data-style="btn-inverse" class="selectpicker margin-none dropdown-menu-light" data-container="body">
                                        <option value="navigation_current_page">Admin Modules</option>
                                        <option value="navigation_components">Components</option>
                                        <option value="navigation_modules_front">Website Modules</option>
                                    </select>
                                </div>
                            </div>
                        </div> 
                        <ul class="menu list-unstyled" id="navigation_current_page">
                            <?php Menu::make($config["menu"]["admin"]["default"], $page, $module, "collapse", "default"); ?>
                        </ul>
                        <ul class="menu list-unstyled hide" id="navigation_components">
                            <?php Menu::make($config["menu"]["admin"]["components"], $page, $module, "collapse", "components"); ?>
                        </ul>
                        <ul class="menu list-unstyled hide" id="navigation_modules_front">
                            <?php Menu::make($config["menu"]["admin"]["front_modules"], $page, $module, "collapse", "front_modules"); ?>
                        </ul> -->


                        <?php 
                        
                        // echo ASSETS_PATH;
                        
                            include ASSETS_PATH."/grad_assets/classes/myClass.php";
                            $userinfo = new Util();
                            $userinfo->getUserMenu();
                        ?>
                                </div>
                            </div>
                            <!-- // Main Sidebar Menu END -->
                            <!-- Main Sidebar Menu -->
                            <div id="menu_kis" class="hidden-print sidebar-light">
                                <div>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="index.php?page=index" class="glyphicons globe"><i></i><span>Wall</span></a>
                                        </li>
                                        <!-- <li>
                                            <a href="index.php?page=realestate_listing_map" class="glyphicons home"><i></i> Estate</a>
                                        </li> -->
                                        
                                        <li>
                                            <a href="index.php?page=dashboard" class="glyphicons charts"><i></i> Dashboard</a>
                                        </li>
                                        <?php 
                                            // if($_SESSION['user_role'] != "student"){}
                                        ?>
                                        

                                        <li>
                                            <a href="index.php?page=events" class="glyphicons google_maps"><i></i> Events</a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=groups" class="glyphicons group"><i></i><span>Groups</span></a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=assignments" class="glyphicons notes_2"><i></i> Assignments</a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=noticeboard" class="glyphicons pushpin"><i></i> Noticeboard</a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=calendar" class="glyphicons calendar"><i></i> Calendar</a>
                                        </li>
                                        <li>    
                                            <a href="index.php?page=projects" class="glyphicons folder_closed"><i></i> Projects</a>
                                        </li>
                                        <li>    
                                            <a href="index.php?page=doubts" class="glyphicons circle_question_mark"><i></i> Doubts</a>
                                        </li>
                                        
                                        
                                         <?php 
                                            if("student" != $_SESSION['user_role']){
                                            ?>
                                                <li>    
                                                    <a href="index.php?page=attendance" class="glyphicons hand_up"><i></i> Attendance</a>
                                                </li>
                                            <?php
                                            }
                                        ?>

                                        <?php 
                                            if("admin" == $_SESSION['user_role']){
                                            ?>
                                                <li>
                                                    <a href="index.php?page=permissions" class="glyphicons warning_sign"><i></i> Permissions</a>
                                                </li>
                                            <?php
                                            }
                                        ?>
                                        
                                        
                                        

                                       <!--  <li>
                                            <a href="index.php?page=gallery_video" class="glyphicons picture"><i></i><span>Media</span></a>
                                        </li> 
                                        <li>
                                            <a href="index.php?page=tasks" class="glyphicons share_alt"><i></i><span>Tasks</span></a>
                                        </li>
                                       
                                        <li>
                                            <a href="index.php?page=medical_overview" class="glyphicons circle_plus"><i></i><span>Medical</span></a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=courses_2" class="glyphicons crown"><i></i> Learning</a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=finances" class="glyphicons calculator"><i></i> Finance</a>
                                        </li> 
                                        <li>
                                            <a href="index.php?page=shop_products" class="glyphicons shopping_cart"><i></i><span>Shop</span></a>
                                        </li>
                                        <li>
                                            <a href="index.php?page=survey" class="glyphicons turtle no-ajaxify"><i></i><span>Surveys</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="index.php?page=dashboard_analytics" class="glyphicons plus"><i></i><span>Other</span></a>
                                        </li>
                                        <li>
                                            <a href="?module=front&amp;page=index" class="glyphicons leather" target="_blank"><i></i><span>Website</span></a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                            <!-- // Main Sidebar Menu END -->
                            <!-- Content -->
                            <div id="content">
                                <div class="navbar hidden-print navbar-primary main" role="navigation">
                                    <div class="user-action user-action-btn-navbar pull-left border-right">
                                        <button class="btn btn-sm btn-navbar btn-primary btn-stroke"><i class="fa fa-bars fa-2x"></i></button>
                                    </div>
                                    <div class="col-md-3 padding-none visible-md visible-lg">
                                        <div class="input-group innerLR">
                                            <input type="text" class="form-control input-sm" placeholder="Search stories ...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <div class="user-action visible-xs user-action-btn-navbar pull-right">
                                        <button class="right-main-menu btn btn-sm btn-navbar-right btn-primary"><i class="fa fa-fw fa-arrow-right"></i><span class="menu-left-hidden-xs"> Menu</span></button>
                                    </div>
                                    <div class="user-action pull-right menu-right-hidden-xs menu-left-hidden-xs" style="min-width: 16.5%;">
                                        <div class="dropdown username hidden-xs pull-left">
                                            <a class="dropdown-toggle " data-toggle="dropdown" href="#">
                                                <?php
                                                    $account = new SocialAccount();
                                                    $account->getHeader();
                                                ?>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="index.php?page=index" class="glyphicons user"><i></i> Profile</a>
                                                </li>
                                                <li>
                                                    <a href="index.php?page=timeline" class="glyphicons list"><i></i>Timeline</a>
                                                </li>
                                                <li>
                                                    <a href="index.php?page=messages" class="glyphicons envelope"><i></i>Messages</a>
                                                </li>
                                                <li><a href="index.php?page=social_account">Settings</a></li>
                                                <li><a href="index.php?page=logout">Logout</a></li>
                                            </ul>
                                        </div>
                                        <div class="dropdown dropdown-icons padding-none">
                                            <a data-toggle="dropdown" href="#" class="btn btn-primary btn-circle dropdown-toggle"><i class="fa fa-user"></i> </a>
                                            <ul class="dropdown-menu">
                                                <li data-toggle="tooltip" data-title="Photo Gallery" data-placement="left" data-container="body">
                                                    <a href="index.php?page=gallery_photo"><i class="fa fa-camera"></i></a>
                                                </li>
                                                <li data-toggle="tooltip" data-title="Tasks" data-placement="left" data-container="body">
                                                    <a href="index.php?page=tasks"><i class="fa fa-code-fork"></i></a>
                                                </li>
                                                <li data-toggle="tooltip" data-title="Employees" data-placement="left" data-container="body">
                                                    <a href="index.php?page=employees"><i class="fa fa-group"></i></a>
                                                </li>
                                                <li data-toggle="tooltip" data-title="Contacts" data-placement="left" data-container="body">
                                                    <a href="index.php?page=contacts"><i class="fa fa-phone-square"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- <ul class="notifications pull-right hidden-xs">
                                        <li class="dropdown notif">
                                            <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="notif-block fa fa-comments-o"></i><span class="badge badge-primary">7</span></a>
                                            <ul class="dropdown-menu chat media-list"> -->
                                        
                                        <!-- notification -->    

                                               <?php
                                                   //$notification = new Notification();
                                                   //var_dump($notification->get());
                                               ?>

                                                <!-- <li class="media">
                                                    <a class="pull-left" href="#">
                                                        <img class="media-object thumb" src="assets/images/people/100/15.jpg" alt="50x50" width="50"/>
                                                    </a>
                                                    <div class="media-body">
                                                        <span class="label label-default pull-right">5 min</span> 
                                                        <h5 class="media-heading">
                                                            Adrian D.
                                                        </h5>
                                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                                    </div>
                                                </li>


                                                <li>
                                                    <a href="#" class="btn btn-primary"><i class="fa fa-list"></i> <span>View all messages</span></a>
                                                </li>


                                            </ul>
                                        </li>
                                    </ul> -->


                                    <div class="clearfix"></div>
                                </div>
                                <!-- // END navbar -->
                                <div id="menu-top">

                                    <ul class="main pull-right visible-lg">
                                        <li>
                                            <a href="">Close <i class="fa fa-fw fa-times"></i></a>
                                        </li>
                                    </ul>
                                    <ul class="colors pull-right hidden-xs">
                                        <li class="active">
                                            <a href="?skin=style-default" style="background-color: #eb6a5a" class="no-ajaxify"><span class="hide">style-default</span></a>
                                        </li>
                                        <li>
                                            <a href="?skin=green" style="background-color: #87c844" class="no-ajaxify"><span class="hide">green</span></a>
                                        </li>
                                        <li>
                                            <a href="?skin=coral" style="background-color: #7eccc2" class="no-ajaxify"><span class="hide">coral</span></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="" data-toggle="dropdown" class="dropdown-toggle">
<span class="color inverse"></span>
<span class="color danger"></span>
<span class="color success"></span>
<span class="color info"></span>
</a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="?skin=alizarin-crimson" class="no-ajaxify"><span class="color" style="background-color: #F06F6F"></span> alizarin-crimson</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=blue-gray" class="no-ajaxify"><span class="color" style="background-color: #7293CF"></span> blue-gray</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=brown" class="no-ajaxify"><span class="color" style="background-color: #d39174"></span> brown</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=purple-gray" class="no-ajaxify"><span class="color" style="background-color: #AF86B9"></span> purple-gray</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=purple-wine" class="no-ajaxify"><span class="color" style="background-color: #CC6788"></span> purple-wine</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=green-army" class="no-ajaxify"><span class="color" style="background-color: #9FB478"></span> green-army</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=black-and-white" class="no-ajaxify"><span class="color" style="background-color: #979797"></span> black-and-white</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=amazon" class="no-ajaxify"><span class="color" style="background-color: #8BC4B9"></span> amazon</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=amber" class="no-ajaxify"><span class="color" style="background-color: #CACA8A"></span> amber</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=android-green" class="no-ajaxify"><span class="color" style="background-color: #A9C784"></span> android-green</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=antique-brass" class="no-ajaxify"><span class="color" style="background-color: #B3998A"></span> antique-brass</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=antique-bronze" class="no-ajaxify"><span class="color" style="background-color: #8D8D6E"></span> antique-bronze</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=artichoke" class="no-ajaxify"><span class="color" style="background-color: #B0B69D"></span> artichoke</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=atomic-tangerine" class="no-ajaxify"><span class="color" style="background-color: #F19B69"></span> atomic-tangerine</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=bazaar" class="no-ajaxify"><span class="color" style="background-color: #98777B"></span> bazaar</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=bistre-brown" class="no-ajaxify"><span class="color" style="background-color: #C3A961"></span> bistre-brown</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=bittersweet" class="no-ajaxify"><span class="color" style="background-color: #d6725e"></span> bittersweet</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=blueberry" class="no-ajaxify"><span class="color" style="background-color: #7789D1"></span> blueberry</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=bud-green" class="no-ajaxify"><span class="color" style="background-color: #6fa362"></span> bud-green</a>
                                                </li>
                                                <li>
                                                    <a href="?skin=burnt-sienna" class="no-ajaxify"><span class="color" style="background-color: #E4968A"></span> burnt-sienna</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="layout-app">


<style>
    
    @media (max-width: 767px){
        #menu_kis.active {
          display: block !important;
          width: 70px;
        }

        .user-action-btn-navbar button.active{
            margin-right: 65px;
        }
    }


    #menu_kis > div > ul > li > a {
      font-size: 10px !important;
    }

</style>