<?php
include ('../classes/myClass.php');
	
	$assignment = new Assignments();
	$utilities = new Util();

	if($_POST['staff_assignment_id']){
		$assignment->getAsStaff($_POST['staff_assignment_id']);
	}


	if($_POST['student_assignment_id']){
		$assignment->getAsStudent($_POST['student_assignment_id']);
	}

	if($_POST['student_submit']){
		$assignment->setAsStudent($_POST['student_submit']);
	}

	if($_POST['create_new']){
		$assignment->createNewAssignment($_POST['create_new']);
	}

	if($_POST['overview_data']){

		if(isset($_POST['overview_data']['student'])){
			$assignment->getOverviewStudent($_POST['overview_data']['student']);
		}

		if(isset($_POST['overview_data']['staff'])){
			$assignment->getOverviewStaff($_POST['overview_data']['staff']);
		}
	}

	if($_POST['preview_modal_attachment']){
		$assignment->previewAttachment($_POST['preview_modal_attachment']);
	}


	if($_POST['upload_student_data']){
		$assignment->uploadStudentData($_POST['upload_student_data']);
	}

	


	// if($_POST['save_student_marks_local']){
	// 	$grad->saveAssignmentMarksLocal($_POST['save_student_marks']);
	// }



	if($_POST['save']){
		$grad->saveAssignmentMarks($_POST['save']);
	}


	if($_POST['publish']){
		$grad->publishAssignmentMarks($_POST['publish']);
	}

	if($_POST['create_data']){
		$grad->setNewAssignment($_POST['create_data']);
	}

	if($_POST['all']){
		$assignment->getAll($_POST['all']);
	}

	if($_POST['marks']){
		$grad->setAssignmentMarks($_POST['marks']);
	}

	if($_POST['post_answer']){
		$grad->setAssignmentAnswer($_POST['post_answer']);
	}





	if($_POST['remove']){
		$assignment->setAssignmentStatus($_POST['remove']);
	}


	if($_POST['department']){
		$utilities->getListDepartment();
	}

	if($_POST['individual']){
		$utilities->getListStudent();
	}





	

?>