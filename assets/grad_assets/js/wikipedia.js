$( document ).ready(function() {	
	$('.trigger').click(function(){
		  $('.wikipedia.modal').modal("show");
		  $('#wiki-tags').hide();
		   $('#wiki-desc').hide();
	});

	
	$("#searchfor").keyup(function(event){
	    if(event.keyCode == 13) {
	        var searchText = document.getElementById('searchfor').value;
			//alert(searchText);

			$('#wiki-tags').slideDown( 500 );

			$("#wiki-tags").append('<div class="ui active inverted dimmer"><div class="ui loader"></div></div><br><br><br><br>');

			$.ajax({
	            dataType: "jsonp",
	            url: "http://en.wikipedia.org/w/api.php?action=opensearch&search="+searchText,
	            //url: "http://en.wikipedia.org/w/api.php?action=opensearch&limit=20&search="+searchText,
	            success:function(data){
	            		
	            		$("#wiki-tags").empty();

	            		$("#wiki-desc").hide();

	            		console.log(data);

	            		var initalLength = Object.keys(data).length;
          				console.log(data[1]);

          				if(data[1]!=""){
          					$.each(data[1], function(k, v) {
	            				$("#wiki-tags").append("<a class='ui tag label' id="+k+">"+v+"</a>");
	            			});
          				} else{
          					$("#wiki-tags").append("<h3>Please Check your Spelling.</h3>");
          				}

	            		setTimeout(tagFunc, 1000);
	                }
	        });

	    }
	});


	function tagFunc() {
		$('.tag.label').click(function(){
			var searchTag = $(this).text();

			$('#wiki-desc').slideDown( 500 );

			$("#wiki-desc").append('<div class="ui active inverted dimmer"><div class="ui loader"></div></div><br><br><br><br>');

			$.ajax({
	            dataType: "jsonp",
	            url: "http://en.wikipedia.org/w/api.php?action=opensearch&search="+searchTag,
	            success:function(data){

	            		console.log(data);

	            		var initalLength = Object.keys(data).length;
          				console.log(data[2]);

          				$("#wiki-desc").empty();

          				$("#wiki-desc").append("<div class='ui top right attached label'><a target='_blank' href='"+data[3]+"'>Read More</a></div>");


          				if(data[2] != ""){
          					$("#wiki-desc").append("<h3>"+data[1]+"</h3>");
          					$("#wiki-desc").append("<p>"+data[2]+"</p>");
						} else {
							$("#wiki-desc").append("<h3></h3>");
						}

	                }
	        });
		});	
	}
});