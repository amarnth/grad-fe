
$("#staffSearch").keyup(function(){
	var searchText = $("#staffSearch").val();
	$(".staffCard").each(function(index){
		if($(this).attr("data-search")){
			if(!$(this).attr("data-search").match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
		}
	});
});


$("#studentSearch").keyup(function(){
	var searchText = $("#studentSearch").val();
	$(".studentCard").each(function(index){
		if($(this).attr("data-search")){
			if(!$(this).attr("data-search").match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
		}
	});
});