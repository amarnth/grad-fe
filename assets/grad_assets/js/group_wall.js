var MY_CONSTANT = "assets/grad_assets";

selectGroupMembers();

function selectGroupMembers(){

	$("#invite-members").select2({
	    placeholder: "Select Members",
	    allowClear: true
	});

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/multiselectController.php",
		data: {student: true},
		success: function(data){
			$('#invite-members').empty();
			$('#invite-members').append(data);
		}
	});	


	$('.invite-new-member').click(function(){
		var group_id = $('.group_post').attr("data-id");

		var selected_choices = new Array();
		
				$('#s2id_invite-members .selected-choices').each(function(index){
					selected_choices[index] = $(this).attr('data-id');
				});

			var assigned_to = selected_choices.toString();

			var data = {group_id: group_id, members: assigned_to};

			groupInvite(data);

	});

}


groupPosts();

function groupPosts(){

	$('.group_post').click(function(){

		var wall_message = $('#textareaWall').val();
		
		if(!wall_message){

			alert('Please Enter Post Value');

		} else {
			
			// alert(wall_message);
			
			var time_stamp = epochDate();

			//var yesday = 1426744634;

			var group_id = $(this).attr('data-id');

			var files = document.getElementById('uploadPostForm');

			var data = new FormData(files);
			

			var attachment = $('#newPostFile').val();

			var comments = "";

			var posted_on = humanTime(time_stamp);

			var is_media_available = false;
			
			// Session Values
			sessionHandler(function(output){

			  	var session = jQuery.parseJSON(output);

				  	var author_name = session.user_name;

					var author_id = session.user_id;

					var user_id = session.user_id;

					var message_author = session.user_name;
			
				var wall_id = author_id+"_"+time_stamp;
				
					var profile_picture = session.user_pic;

					var author_dept = session.user_dept;

					var college_id = session.college_id;
					
					var college_name = session.college_name;

					var data = {is_media_available: is_media_available, group_id: group_id, comments: comments, user_id: user_id, wall_id: wall_id, wall_message: wall_message, posted_on: posted_on, time_stamp: time_stamp, profile_picture: profile_picture, author_id: author_id, message_author: message_author};

					wallGroupPost(data);
			});

		}
		
	});


	$('.post_wall_file').click(function(){

		var wall_message = $('#file_post_wall').val();
		
		if(!wall_message){

			alert('Please Enter Post Value');

		} else {
			
			// alert(wall_message);
			
			var time_stamp = epochDate();

			//var yesday = 1426744634;

			var group_id = $(this).attr('data-id');

			var comments = "";

			var posted_on = humanTime(time_stamp);


			// Session Values
			sessionHandler(function(output){

			  	var session = jQuery.parseJSON(output);

				  	var author_name = session.user_name;

					var author_id = session.user_id;

					var user_id = session.user_id;

					var message_author = session.user_name;
			
				var wall_id = author_id+"_"+time_stamp;
				
					var profile_picture = session.user_pic;

					var author_dept = session.user_dept;

					var college_id = session.college_id;
					
					var college_name = session.college_name;



					var file_paths = new Array();
					
					var i=0;

					$('.ajax-file-upload-statusbar').each(function(index){
						var status = $(this).attr('style');
						// alert(status);
						if(status.indexOf("block") > -1){
							var name =  $(this).find('.ajax-file-upload-filename').text();
					    	name = name.split("). ")
					    	file_paths[i] = "files/"+college_id+"/groups_wall/"+author_id+"/"+name[1];
					    	i++;
						}				    
					});

					// alert(file_paths.toString());
					
					file_paths = file_paths.toString();

					//if(file_paths==""){
						//alert("Please attach");
					//}else{
						var is_media_available = true;

						var data = {is_media_available: is_media_available, group_id: group_id, comments: comments, user_id: user_id, wall_id: wall_id, wall_message: wall_message, posted_on: posted_on, time_stamp: time_stamp, profile_picture: profile_picture, author_id: author_id, message_author: message_author, path_to_files: file_paths};

						emptyPreviewFile();

						wallGroupPost(data);
					//}
			});

		}
		
	});
}


	function wallGroupPost(data){
		//

		$.ajax({
			type: "POST",
			url: MY_CONSTANT+"/controller/wallController.php",
			data: {group_post: data},
			success: function(data){
				$('#wallPosts').empty();
				$('#wallPosts').prepend(data);
				$('#textareaWall').val("");
				$('#newPostFile').val("");
				$('#newPostPreview').empty();
				groupComments();
			}
		})
	}


function groupInvite(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/wallController.php",
		data: {group_invite: data},
		success: function(data){
			console.log(data);
			//selectGroupMembers();
			// $('#wallPosts').empty();
			// $('#wallPosts').prepend(data);
			// $('#textareaWall').val("");
			// $('#newPostFile').val("");
			// $('#newPostPreview').empty();
			//groupComments();
		}
	})
}




function groupCommentPost(data){
				//
	var wall_id = data.wall_id;

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/commentController.php",
		data: data,
		success: function(data){
			$('#wallPosts').empty();
			//var id = "#wallComments_"+wall_id;
			//$(id).append(data);
			$('#wallPosts').append(data);
			groupComments();
			//alert(data);
		}
	})
}


groupComments();

function groupComments() {

	$('.newComment').keyup(function(event){
		if( event.which == 13 ) {
			
			var comment = $(this).val();

			var group_id = $('.group_post').attr('data-id');

			var wall_id = $(this).attr('id');

			var time_stamp = epochDate();

			var shared_on = humanTime(time_stamp);

			var stars = 0;

			var is_media_available = false;

			var file_path = "";


			// Session Values
			sessionHandler(function(output){

			  	var session = jQuery.parseJSON(output);

				  	var user_name = session.user_name;

					var author_id = session.user_id;

					var user_id = session.user_id;

					var message_author = session.user_name;
			
					var profile_pic = session.user_pic;

					var author_dept = session.user_dept;
					
					var college_name = session.college_name;

					var college_id = session.college_id;

						var comment_id = user_id+"_"+time_stamp;

						var data = {comment_id: comment_id, group_id: group_id, wall_id: wall_id, comment: comment, shared_on: shared_on, time_stamp: time_stamp, profile_pic: profile_pic, user_id: user_id, user_name: user_name, file_path: file_path, is_media_available: is_media_available, stars: stars};

				groupCommentPost(data);

				//commentPost(comment);
				$(this).val("");
			});
			//

		}
	});
}



$('.post_wall_file').hide();

fileHandler();

function fileHandler(){

	var settings = {
	    url: MY_CONSTANT+"/upload.php",
	    dragDrop:true,
	    fileName: "myfile",
	    allowedTypes:"*",	
	    returnType:"json",
		onSuccess:function(files,data,xhr)
	    {
			//alert(data);
			$('.post_wall_file').show();
			groupPosts();
	    },
	    showDelete:true,
	    deleteCallback: function(data,pd)
		{
		    for(var i=0;i<data.length;i++)
		    {
		        $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
		        function(resp, textStatus, jqXHR)
		        {
		            //Show Message  
		            $("#mulitplefileuploadStatus").append("<div>File Deleted</div>");      
		        });
		     }      
		    pd.statusbar.hide(); //You choice to hide/not.
		}
	}

	var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

}

