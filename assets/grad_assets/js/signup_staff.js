var MY_CONSTANT = "assets/grad_assets";

// today button
$('#dob').bdatepicker({
	format: "dd MM yyyy",
	startDate: "2015-02-14",
	autoclose: true,
	todayBtn: true
});

fillup();

function fillup(){
	// var elem = document.getElementById("fname");
	// elem.value = "My default value";

	// $('#fname').val("Staff");
	// $('#lname').val("One");
	// $('#staff_no').val("4007");
	// $('#email').val("staff@GPIT.com");
	// $('#password').val("assdww");
	// $('#mobile_number').val("4578145712");
	// $('#state').val("TN");
	// $('#city').val("CBE");
	// $('#address').val("XYZ street, Vellore");
	// $('#secondary_mark').val("999");
	// $('#higher_secondary_mark').val("1111");
	// $('#pincode').val("5784671");
	// $('#nationality').val("Indian");
	// $('#skills').val("java,pyhton,php,cassandra");
	
	
	// $('#department').val("IT");
	// $('#class_name').val("IT1A");
	
	
	
	
	$('#fname').val();
	$('#lname').val();
	$('#staff_no').val();
	$('#email').val();
	$('#password').val();
	$('#mobile_number').val();
	$('#state').val();
	$('#city').val();
	$('#address').val();
	$('#secondary_mark').val();
	$('#higher_secondary_mark').val();
	$('#pincode').val();
	$('#nationality').val();
	$('#skills').val();
	
	
	$('#department').val();
	$('#class_name').val();
	
}

function submitForm(){
	var fname = $('#fname').val();
	var lname = $('#lname').val();
	var gender = $('.gender .selectpicker').attr('title');
	var staff_no = $('#staff_no').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var mobile_number = $('#mobile_number').val();
	var dob = $('#dob input').val();
	var state = $('#state').val();
	var city = $('#city').val();
	var pincode = $('#pincode').val();
	var address = $('#address').val();
	var nationality = $('#nationality').val();

	var skills = $('#skills').val();
	var terms = true;
	

	var department = $('#department').val();
	var class_name = $('#class_name').val();


	var university_name = "GPIT";
	var college_id = "grad_C01X";


	var data = {college_id: college_id, university_name: university_name, class_names: class_name, departments: department, fname: fname, lname: lname, gender: gender, staff_no: staff_no, emailid: email, password: password, mobileno: mobile_number, datebirth: dob, state: state, city: city, pincode: pincode, address: address, nationality: nationality, skillset: skills, terms: terms};


	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/signupController.php",
		data: {signup_staff: data},
		success: function(data){
			console.log(data);
		}
	});

}






$.validator.setDefaults(
{
	submitHandler: function() {
		submitForm();
	},
	showErrors: function(map, list) 
	{
		this.currentElements.parents('label:first, div:first').find('.has-error').remove();
		this.currentElements.parents('.form-group:first').removeClass('has-error');
		
		$.each(list, function(index, error) 
		{
			var ee = $(error.element);
			var eep = ee.parents('label:first').length ? ee.parents('label:first') : ee.parents('div:first');
			
			ee.parents('.form-group:first').addClass('has-error');
			eep.find('.has-error').remove();
			eep.append('<p class="has-error help-block">' + error.message + '</p>');
		});
		//refreshScrollers();
	}
});




$("#validateForm").validate({
	rules: {
		firstname: "required",
		lastname: "required",
		roll_no: "required",
		mobile_number: "required",
		address: "required",
		password: {
			required: true,
			minlength: 5
		},
		confirm_password: {
			required: true,
			minlength: 5,
			equalTo: "#password"
		},
		email: {
			required: true,
			email: true
		},
		agree: "required"
	},
	messages: {
		firstname: "Please enter your firstname",
		lastname: "Please enter your lastname",
		roll_no: "Please enter your Roll Number",
		mobile_number: "Please enter your Mobile Number",
		address: "Please enter your Address",
		password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address",
		agree: "Please accept policy"
	}
});