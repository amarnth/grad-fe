var MY_CONSTANT = "assets/grad_assets";



multiSelectModule();

function multiSelectModule(){


	addActivity();
	actions();
	postActions();
	fileHandler();


	collapsableWidgets();


	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/multiselectController.php",
		data: {icon: true},
		success: function(data){
			$('#select-icon').empty();
			$('#select-icon').append(data);
		}
	});


	// templating
	function format(state) {
	    if (!state.id) return state.text; // optgroup
	    return "<i class='fa fa-fw "+ state.id.toLowerCase() + "'></i>  " + state.text + "</span>";
	}
	$("#select-icon").select2({
	    formatResult: format,
	    formatSelection: format,
	    escapeMarkup: function(m) { return m; }
	});

	$("#invite-mentors").select2({
	    placeholder: "Invite Staff",
	    allowClear: true
	});


	$("#invite-members").select2({
	    placeholder: "Invite Students",
	    allowClear: true
	});


	$("#add-activity-members").select2({
	    placeholder: "Activity To",
	    allowClear: true
	});


	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/multiselectController.php",
		data: {student: true},
		success: function(data){
			$('#invite-members').empty();
			$('#invite-members').append(data);
		}
	});



	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/multiselectController.php",
		data: {staff: true},
		success: function(data){
			$('#invite-mentors').empty();
			$('#invite-mentors').append(data);
		}
	});


	$(".activity-search input").keyup(function(){
		var searchText = $(".activity-search input").val();
		$(".activity-discussion").each(function(index){
			if($(this).attr('data-title')){
				if(!$(this).attr('data-title').match(new RegExp(searchText, "i"))){
					$(this).fadeOut("fast");
				}else{
					$(this).fadeIn("slow");
				}
			}
		});
	});
}


function addActivity(){

	// today button
	$('#activity-deadline').bdatepicker({
		format: "dd MM yyyy",
		startDate: "2015-06-29",
		autoclose: true,
		todayBtn: true
	});


		$('.project-members-list a').each(function(index){
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			var data = "<option value='"+id+"'>"+name+"</option>";
			$('#add-activity-members').append(data);
		});


	$(".add-activity-to-member").click(function(){

		var project_id = $(".over-view-of-project").attr("data-id");
		// var name = $(".s2id_add-activity-members a.select2-choice span").text();

		var selected_choices = new Array();
	
		$('#s2id_add-activity-members .selected-choices').each(function(index){
			selected_choices[index] = $(this).attr('data-id');
		});

		var assigned_to = selected_choices.toString();

		var activity_name = $(".activity-name").val();

		// var activity_desc = $(".activity-desc").text();

		var total_mark = $(".activity-mark").val();

		var deadline = $("#activity-deadline input").val();

			var myDate = new Date(deadline); // Your timezone!

		var deadline_time_stamp = myDate.getTime()/1000.0;

		var assigned_timestamp = epochDate();

		var assigned_on = humanTime(assigned_timestamp);

		var obtained_mark = 0;

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var user_id = session.user_id;

				var message_author = session.user_name;
		
				var author_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;


			var activity_id = author_id+"_"+assigned_timestamp;

			var assigned_by = {id: author_id, name: author_name, pic: author_pic};

				var data = {project_id: project_id, activity_id: activity_id, activity_name: activity_name, assigned_to: assigned_to, assigned_by: assigned_by, assigned_on: assigned_on, assigned_timestamp: assigned_timestamp, deadline: deadline, deadline_time_stamp: deadline_time_stamp, total_mark: total_mark, obtained_mark: obtained_mark};

				postNewActivity(data);

		});
		//

	})

}


function actions(){

	$(".invite-mentor").click(function(){
		var project_id = $(".over-view-of-project").attr("data-id");

			var selected_choices = new Array();
	
			$('#s2id_invite-mentors .selected-choices').each(function(index){
				selected_choices[index] = $(this).attr('data-id');
			});

		var assigned_to = selected_choices.toString();

		var data = {project_id: project_id, mentors: assigned_to};

		projectInvite(data);

	})


	$(".invite-member").click(function(){
		var project_id = $(".over-view-of-project").attr("data-id");

			var selected_choices = new Array();
	
			$('#s2id_invite-members .selected-choices').each(function(index){
				selected_choices[index] = $(this).attr('data-id');
			});

		var assigned_to = selected_choices.toString();

		var data = {project_id: project_id, assigned_to: assigned_to};

		projectInvite(data);
		
	})


	$(".mentor button.btn-success").click(function(){
		var project_id = $(".over-view-of-project").attr("data-id");
		// Session Values
		sessionHandler(function(output){
		  	var session = jQuery.parseJSON(output);
				var author_id = session.user_id;
			var data = {project_id: project_id, staff_id: author_id, accept: true};
				mentorAction(data);
		});
		//
	})


	$(".mentor button.btn-default").click(function(){
		var project_id = $(".over-view-of-project").attr("data-id");
		// Session Values
		sessionHandler(function(output){
		  	var session = jQuery.parseJSON(output);
			  	var author_id = session.user_id;
			var data = {project_id: project_id, staff_id: author_id, accept: false};
				mentorAction(data);
		});
		//
	})

}

	



function postActions(){

	$(".post-activity-discussion").click(function(){
		var activity_id = $(this).attr("data-id");
		postActivityDiscussion(activity_id);
	});


	$(".post-activity-link").click(function(){
		var activity_id = $(this).attr("data-id");
		postActivityLink(activity_id);
	});


	$(".post-activity-file-share").click(function(){
		var activity_id = $(this).attr("data-id");
		postActivityFileShare(activity_id);
	});
	

	$(".post-general-discussion").click(function(){

		var project_id = $(".over-view-of-project").attr("data-id");
		
		var user_comment = $(".project-discussion").val();
		
		var time_stamp = epochDate();

		var posted_on = humanTime(time_stamp);


			// Session Values
			sessionHandler(function(output){

			  	var session = jQuery.parseJSON(output);

				  	var author_name = session.user_name;

					var author_id = session.user_id;

					var author_pic = session.user_pic;

					var author_dept = session.user_dept;
					
					var college_name = session.college_name;

					var college_id = session.college_id;

				var discussion_id = author_id+"_"+time_stamp;

				var file_paths = "";

				var data = {project_id: project_id, discussion_id: discussion_id, user_name: author_name, user_id: author_id, user_comment: user_comment, user_pic: author_pic, time_stamp: time_stamp, posted_on: posted_on, file_paths: file_paths};

					postGeneralDiscussion(data);

			});
			//
	})
}




function projectInvite(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {invite: data},
		success: function(data){
			$("#view-project").empty();
			$("#view-project").append(data);

				multiSelectModule();

			// console.log(data);
		}
	})
}



function postNewActivity(data){
	console.log(data);
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {new_activity: data},
		success: function(data){
			$("#view-project").empty();
			$("#view-project").append(data);

				multiSelectModule();

			// console.log(data);
		}
	})
}

function postGeneralDiscussion(data){
	console.log(data);
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {post_discussion: data},
		success: function(data){
			$("#view-project").empty();
			$("#view-project").append(data);

				multiSelectModule();
				
			// console.log(data);
		}
	})
}


function postNewActivityDiscussion(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {post_activity_discussion: data},
		success: function(data){
			$("#view-project").empty();
			$("#view-project").append(data);

				multiSelectModule();

			// console.log(data);
		}
	})
}



function mentorAction(data){
	// console.log(data);
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {accept_mentorship: data},
		success: function(data){
			$("#view-project").empty();
			$("#view-project").append(data);

				multiSelectModule();

			
		}
	})
}





function postActivityDiscussion(activity_id){

	var project_id = $(".over-view-of-project").attr("data-id");

		var user_comment = $(".desc_"+activity_id).val();
		
		var time_stamp = epochDate();

		var posted_on = humanTime(time_stamp);

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var author_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;

			var discussion_id = author_id+"_"+time_stamp;

			var file_paths = "";

			var data = {project_id: project_id, activity_id: activity_id, discussion_id: discussion_id, user_name: author_name, user_id: author_id, user_comment: user_comment, user_pic: author_pic, time_stamp: time_stamp, posted_on: posted_on, file_paths: file_paths};

			console.log(data);
				
				postNewActivityDiscussion(data);

		});
		//

}


function postActivityLink(activity_id){

		var project_id = $(".over-view-of-project").attr("data-id");

		var user_comment = $(".link_"+activity_id).val();
		
		var time_stamp = epochDate();

		var posted_on = humanTime(time_stamp);

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var author_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;

			var discussion_id = author_id+"_"+time_stamp;

			var file_paths = "";
			
			var data = {project_id: project_id, activity_id: activity_id, discussion_id: discussion_id, user_name: author_name, user_id: author_id, user_comment: user_comment, user_pic: author_pic, time_stamp: time_stamp, posted_on: posted_on, file_paths: file_paths};

			console.log(data);
				// postDiscussion(data);

		});
		//

}

function postActivityFileShare(activity_id){

		var project_id = $(".over-view-of-project").attr("data-id");

		var user_comment = $(".link_"+activity_id).val();
		
		var time_stamp = epochDate();

		var posted_on = humanTime(time_stamp);

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var author_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;

			var discussion_id = author_id+"_"+time_stamp;

			
			var file_path = new Array();

				var media = true;

				var i=0;


				var path_location = "files/"+college_id+"/assignments/"+author_id+"/";

				filepathLocation(path_location, function(output){

					var file_count = output-1;
					
					console.log("Awesome Output "+output);
				
					$('.ajax-file-upload-statusbar').each(function(index){
						var status = $(this).attr('style');
						// alert(status);
						if(status.indexOf("block") > -1){
							var name =  $(this).find('.ajax-file-upload-filename').text();
					    	name = name.split("). ")

					    	var ransom_name = file_count-i;
					    	
					    	// file_path[i] = path_location+ransom_name+name[1];
					    	
					    	file_path[i] = path_location+name[1];

					    	console.log(file_path[i]);
					    	
					    	i++;
						}				    
					});
					



					file_path = file_path.toString();


			
			var data = {project_id: project_id, activity_id: activity_id, discussion_id: discussion_id, user_name: author_name, user_id: author_id, user_comment: user_comment, user_pic: author_pic, time_stamp: time_stamp, posted_on: posted_on, file_paths: file_path};
			

			console.log(data);
				postNewActivityDiscussion(data);
			})

		});
		//
		
}


function fileHandler(){

	var length = $(".activity-discussion").length;

	console.log(length);

	for (var j = 1; j <= length; j++) {

		var settings = {
		    url: MY_CONSTANT+"/upload.php",
		    dragDrop:true,
		    fileName: "myfile",
		    allowedTypes:"*",	
		    returnType:"json",
		    maxFileCount:1,
			onSuccess:function(files,data,xhr)
		    {
				alert(data);
		    },
		    showDelete:true,
		    deleteCallback: function(data,pd)
			{
			    for(var i=0;i<data.length;i++)
			    {
			        $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
			        function(resp, textStatus, jqXHR)
			        {
			            //Show Message  
			            $("#mulitplefileuploadStatus"+j).append("<div>File Deleted</div>");      
			        });
			     }      
			    pd.statusbar.hide(); //You choice to hide/not.
			}
		}

		var obj = $("#mulitplefileuploader"+j).uploadFile(settings);

	};

}




function collapsableWidgets(){
	(function($)
	{
		$('.widget[data-toggle="collapse-widget"] .widget-body')
			.on('show.bs.collapse', function(){
				$(this).parents('.widget:first').attr('data-collapse-closed', "false");
			})
			.on('shown.bs.collapse', function(){
				setTimeout(function(){ $(window).resize(); }, 500);
			})
			.on('hidden.bs.collapse', function(){
				$(this).parents('.widget:first').attr('data-collapse-closed', "true");
			});
		
		$('.widget[data-toggle="collapse-widget"]').each(function()
		{
			// append toggle button
			if (!$(this).find('.widget-head > .collapse-toggle').length)
				$('<span class="collapse-toggle"></span>').appendTo($(this).find('.widget-head'));
			
			// make the widget body collapsible
			$(this).find('.widget-body').not('.collapse').addClass('collapse');
			
			// verify if the widget should be opened
			if ($(this).attr('data-collapse-closed') !== "true")
				$(this).find('.widget-body').addClass('in');
			
			// bind the toggle button
			$(this).find('.collapse-toggle').on('click', function(){
				$(this).parents('.widget:first').find('.widget-body').collapse('toggle');
			});
		});
	})(jQuery);
}