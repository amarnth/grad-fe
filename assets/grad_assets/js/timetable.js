$('#external-events ul li').each(function() 
{

	// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
	// it doesn't need to have a start or end
	var eventObject = {
		title: $.trim($(this).text()) // use the element's text as the event title
	};
	
	// store the Event Object in the DOM element so we can get to it later
	$(this).data('eventObject', eventObject);
	
	// make the event draggable using jQuery UI
	$(this).draggable(
	{
		zIndex: 999,
		revert: true,      // will cause the event to go back to its
		revertDuration: 0,  //  original position after the drag,
		start: function() { if (typeof mainYScroller != 'undefined') mainYScroller.disable(); },
        stop: function() { if (typeof mainYScroller != 'undefined') mainYScroller.enable(); }
	});
	
});

$('.dropdown').dropdown();

	$("#add").click(function(){


		popup();
		var list = $('.hours');

		var length = list.length+1;

		//alert(length);

		$("thead tr").append("<th id='period_"+length+"' class='hours'> <p contenteditable='true'>09:00 - 09:50</p><a href='javascript:void(0);''  id='remove_"+length+"'' class='premove'><i class='remove icon'></i></a></th>");


		var week_day=["monday","tuesday","wednesday","thursday","friday","saturday"];



//alert(week_day[i]);


for(i=0;i<6;i++)
{
	var rows = week_day[i]+"_"+length;

	var row_id = '#day_'+i;

	$(row_id).append("<td class='cell' id='"+rows+"'></td>");


	popup();
	cell();

}

var remove = "#remove_"+length;
var row = "#period_"+length;
var col = ".period_"+length;


$('.premove').click(function(){
		var  id = $(this).attr('id');
	
		var split = id.split('_');

		var num = split[1];

		var period = "#period_"+num;
		
		$(period).remove();

		for(i=0;i<6;i++)
		{
			var cell_id = "#"+week_day[i]+"_"+num;
			$(cell_id).remove();
		}
		
});

});

	popup();

	$("#publish").click(function(){

		//alert("CLICK");

		var ttable =  getsubject();

		

		$(".item").hasClass("selected");
		$(".item").hasClass("selected");

		var degree = $(".degree .item.selected").attr("data-value");
		var year = $(".year .item.selected").attr("data-value");
		var department = $(".department .item.selected").attr("data-value");
		var section = $(".section .item.selected").attr("data-value");


		var staff_id ="CSE10167";

	  	var staff_name = "staff_name";

	  	var created_on ="Monday April 12 2015 5.30 PM";
      	
    	var staff_pic = "/img/pic1.jpeg";

       var college_name = "Amrita";

       var college_id = "Amrita101";

       var class_name  = "CSE-A";

        
		var d = new Date(); 
		var time_stamp =(d.getTime()-d.getMilliseconds())/1000;
			
		
			var table_id = degree+"_"+department+"_"+year+"_"+section;

			//alert(table_id);


 		var detail = {};
 
 		 //detail = {};
 		 detail['degree'] =  degree;
 		 detail['year'] = year;
 		 detail['department'] = department;
 		 detail['staff_id'] = "CSE10167";
 		 detail['staff_name'] = "staff_name";
 		 detail['created_on'] = "Monday April 12 2015 5.30 PM";
 		 detail['staff_pic'] = "/img/pic1.jpeg";
 		 detail['college_name'] = "Amrita";
 		 detail['college_id'] = "Amrita101";
 		 detail['class_name'] = "CSE-A";
 		 detail['time_stamp'] = 1429878613;
 		 detail['table_id'] = table_id;

 		 

			var time_period  = {};
			time_period['timings'] = ttable[0];
			time_period['schedule'] = ttable[1];
 		 	time_period['details']= detail;
 			

 		  // var finalResult = {};

 		  // finalResult["Timetable"] = time_period;

 		  //var data = JSON.stringify(finalResult);
 		  	
 		  setTimetable(time_period);
			
});



function setTimetable(data){
	
	$.ajax({
		type: "POST",
		url: "http://localhost/gradpower/controller/timetableController.php",
		data: data,
		
		success: function(data){
			//alert(data);
			$('table').append(data);
		}	
	});
}



function getsubject(){

	var header = Array();

$("table tr th").each(function(i, v){
	if( i==0){
		header[i] = "{{$ free}}";
	}
	if( i!=0){
		header[i] = $(this).text();
	}
	
})

	var list = $('.hours');

	var length = list.length+1;

	var week_day = ["monday","tuesday","wednesday","thursday","friday","saturday"];

	var day = Array();

// var sample_data = Array();
var schedule = {};



for(var dayIndex=0;dayIndex<6;dayIndex++){

	schedule[week_day[dayIndex]] = {};

	for(var subIndex=0;subIndex<length;subIndex++)
	{
		var row_id = "#"+week_day[dayIndex]+"_"+subIndex;
		if(subIndex!=0){

			var classes = $(row_id).text(); 

			var split = classes.split(':');

			var sub  = split[0];

			var staff = split[1];

			var classhours= { sub : sub , staff : staff };
			
			schedule[week_day[dayIndex]][subIndex] = classhours;

			
		}

	}
}

 var sample = [];
sample[0] = header;
sample[1] = schedule;

return sample;

}
popup();
function popup(){
	$('.cell').popup({
		popup : $('.flowing.popup'),
		inline   : true,
		hoverable: true,
		on :'click',
		position : 'bottom left',
		delay: {
			show: 50,
			hide: 800
		}  
	});  



}

popupReset();

popup();

function popupReset(){
	$('#subject').val("");
	$('#staff').val("");
}

function cell(){

	$('.cell').click(function(){

		var cell_id = $(this).attr("id");

		$('.assign').empty();

		$('.assign').append('<div class="ui blue button save" data-id="'+cell_id+'">Submit</div>');

		save();


	});

}

function nospaces(t){

	if(t.value.match(/\s/g)){

		alert('Sorry, you are not allowed to enter any spaces');

		t.value=t.value.replace(/\s/g,'');

	}


}

cell();


function save() {

	popupReset();

	$('.save').click(function(){

		var cell_id = $(this).attr("data-id");


		var  datalist = [];

		var subject =$('#subject').val();
		var staff = $('#staff').val();
		var res = subject + ':' + staff;
	

cell_id = "#"+cell_id;

$(cell_id).empty();


$(cell_id).append(res);

});

	cell();


}

