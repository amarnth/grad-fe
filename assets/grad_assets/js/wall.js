var MY_CONSTANT = "assets/grad_assets";

// console.log(root);

$('.post_wall_file').click(function(){

	var wall_message = $('#file_post_wall').val();
	
		var time_stamp = epochDate();	

		var path_to_files = new Array();

		var comments = "";

		var posted_on = humanTime(time_stamp);
		

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var user_id = session.user_id;

				var message_author = session.user_name;
		
			var wall_id = author_id+"_"+time_stamp;
			
			// var message_recepient = "std001,std002,"+author_id;

			var message_recepient = "std001,std002,stf001";

				var profile_picture = session.user_pic;

				var author_dept = session.user_dept;

				var college_id = session.college_id;
				
				var college_name = session.college_name;

				var file_paths = new Array();
				
				var i=0;

				var path_location = "files/"+college_id+"/wall/"+author_id+"/";


				filepathLocation(path_location, function(output){

					var file_count = output-1;
					
					console.log("Awesome Output "+output);
				
					$('.ajax-file-upload-statusbar').each(function(index){
						var status = $(this).attr('style');
						// alert(status);
						if(status.indexOf("block") > -1){
							var name =  $(this).find('.ajax-file-upload-filename').text();
					    	name = name.split("). ")

					    	var ransom_name = file_count-i;
					    	
					    	// file_paths[i] = path_location+ransom_name+name[1];
					    	
					    	file_paths[i] = path_location+name[1];

					    	console.log(file_paths[i]);
					    	
					    	i++;
						}				    
					});



					path_to_files = file_paths.toString();

					// alert(file_paths.toString());

					if(path_to_files!==""){

						var data = {comments: comments, user_id: user_id, wall_id: wall_id, wall_message: wall_message, path_to_files: path_to_files, posted_on: posted_on, time_stamp: time_stamp, profile_picture: profile_picture, author_id: author_id, message_author: message_author, message_recepient: message_recepient};

						console.log(data);

						emptyPreviewFile();

						wallPost(data);

					} else {
						alert('Please choose a file to share');
					}


				});
		});

		//
});


$(".fa-refresh.wall").click(function(){
	var get_wall = true;

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/wallController.php",
		data: {get_wall: get_wall},
		success: function(data){
			// alert(data);
			$('#wallPosts').empty();
			$('#wallPosts').prepend(data);
			wallcomments();
		}
	})
});

$('.post_wall').click(function(){


	var wall_message = $('#textareaWall').val();
	
	if(!wall_message){

		alert('Please Enter Post Value');

	} else {
		
		// alert(wall_message);
		
		var time_stamp = epochDate();

		var posted_on = humanTime(time_stamp);

		var comments = "";
		
		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var user_id = session.user_id;

				var message_author = session.user_name;
		
			var wall_id = author_id+"_"+time_stamp;
			
			// var message_recepient = "std001,std002,"+author_id;

			var message_recepient = "std001,std002,stf001";

				var profile_picture = session.user_pic;

				var author_dept = session.user_dept;

				var college_id = session.college_id;
				
				var college_name = session.college_name;

				var data = {comments: comments, user_id: user_id, wall_id: wall_id, wall_message: wall_message, posted_on: posted_on, time_stamp: time_stamp, profile_picture: profile_picture, author_id: author_id, message_author: message_author, message_recepient: message_recepient};

				wallPost(data);
		});
		//
		
	}
	
});

$('#newPostPreview').show();


fileHandler();

function fileHandler(){

	var settings = {
	    url: MY_CONSTANT+"/upload.php",
	    dragDrop:true,
	    fileName: "myfile",
	    allowedTypes:"*",	
	    returnType:"json",
		onSuccess:function(files,data,xhr)
	    {
			alert(data);
	    },
	    showDelete:true,
	    deleteCallback: function(data,pd)
		{
		    for(var i=0;i<data.length;i++)
		    {
		        $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
		        function(resp, textStatus, jqXHR)
		        {
		            //Show Message  
		            $("#mulitplefileuploadStatus").append("<div>File Deleted</div>");      
		        });
		     }      
		    pd.statusbar.hide(); //You choice to hide/not.
		}
	}

	var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

}


$('.submitNewPost .camera').click(function() {
	// document.getElementById('mulitplefileuploader').click();
	$('.ajax-file-upload').click();
});


/*

	var imageLoader = document.getElementById('newCommentFile');

    imageLoader.addEventListener('change', handleImage, false);

	function handleImage(e) {
	    var reader = new FileReader();
	    reader.onload = function (event) {
	        
	        $('.newCommentPreview img').attr('src',event.target.result);
	        $('.newCommentPreview').show();
	    }
	    reader.readAsDataURL(e.target.files[0]);

	}

	$('.newCommentPreview').click(function(){
		$('.newCommentFile').empty();
	});
*/



	// file preview and remove

	var selDiv = "";
	var storedFiles = [];
	
	$(document).ready(function() {
		$("#newPostFile").on("change", handleFileSelect);
		
		selDiv = $("#newPostPreview"); 

		$("body").on("click", ".selFile", removeFile);
	});
		


	function handleFileSelect(e) {
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {			

			if(!f.type.match("image.*")) {
				return;
			}
			storedFiles.push(f);
			
			var reader = new FileReader();
			

			reader.onload = function (e) {
				var html = "<div class='col-md-3'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='selFile' title='Click to remove'>" + f.name + "<br clear=\"left\"/></div>";
				selDiv.append(html);
				
			}
			reader.readAsDataURL(f); 
		});
		
	}
		
	function removeFile(e) {
		var file = $(this).data("file");
		for(var i=0;i<storedFiles.length;i++) {
			if(storedFiles[i].name === file) {
				storedFiles.splice(i,1);
				break;
			}
		}
		$(this).parent().remove();
		document.getElementById(newPostFile).innerHTML = 
                    document.getElementById(newPostFile).innerHTML;
	}

			function commentPost(data){
				//

				var wall_id = data.wall_id;

				$.ajax({
					type: "POST",
					url: MY_CONSTANT+"/controller/commentController.php",
					data: data,
					success: function(data){
						$('#wallPosts').empty();
						//var id = "#wallComments_"+wall_id;
						//$(id).append(data);
						$('#wallPosts').append(data);
						wallcomments();
					}
				})
			}


			function wallPost(data){

				//
				$.ajax({
					type: "POST",
					url: MY_CONSTANT+"/controller/wallController.php",
					data: data,
					success: function(data){
						$('#wallPosts').empty();
						$('#wallPosts').prepend(data);
						$('#textareaWall').val("");
						$('#newPostFile').val("");
						$('#newPostPreview').empty();
						wallcomments();
					}
				})
			}


			


		

viewMoreComments();

	wallcomments();

function viewMoreComments() {

	$('.viewMoreComments').click(function(){

		var wall_id = $(this).attr('id');

		$(".moreComments.wallComment_"+wall_id).addClass("active");

		$(this).hide();
			
	});
}

$('#appendedInputButtons').keyup(function(event){
	var searchText = $("#appendedInputButtons").val();
	$(".friends-list li").each(function(index){
		if($(this).attr("data-title")){
			if(!$(this).attr("data-title").match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
		}
	});
});



function wallcomments() {

	viewMoreComments();



	$('.newComment').keyup(function(event){
			if( event.which == 13 ) {

				// alert("commented");
				
				var comment = $(this).val();

				var wall_id = $(this).attr('id');

				var time_stamp = epochDate();

				var shared_on = humanTime(time_stamp);

				var stars = 0;

				var is_media_available = false;

				var file_path = "";


			// Session Values
			sessionHandler(function(output){

			  	var session = jQuery.parseJSON(output);

				  	var user_name = session.user_name;

					var author_id = session.user_id;

					var user_id = session.user_id;

						var comment_id = user_id+"_"+time_stamp;

					var message_author = session.user_name;
			
					var profile_pic = session.user_pic;

					var author_dept = session.user_dept;

					var data = {comment_id: comment_id, wall_id: wall_id, comment: comment, shared_on: shared_on, time_stamp: time_stamp, profile_pic: profile_pic, user_id: user_id, user_name: user_name, file_path: file_path, is_media_available: is_media_available, stars: stars};

					commentPost(data);

					//commentPost(comment);
					$(this).val("");
			});
			//

			}
	});
}



// $('[data-toggle="prettyPhoto"]').click(function(){
// 	var id = $(this).attr("data-id");

// 	var images = new Array();
				
// 	var i=0;

// 	$('.pretty-image'+id).each(function(index){

// 			images[i] = $(this).attr('href');

// 	    	console.log(images[i]);
	    	
// 	    	i++;
// 	});

// 	$.prettyPhoto.open(images);
// });

