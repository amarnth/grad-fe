var MY_CONSTANT = "assets/grad_assets";

$("#select-students").select2({
    placeholder: "Select Student",
    allowClear: true
});

$("#select-staffs").select2({
    placeholder: "Select Staff",
    allowClear: true
});



$.ajax({
	type: "POST",
	url: MY_CONSTANT+"/controller/multiselectController.php",
	data: {icon: true},
	success: function(data){
		$('#select-icon').empty();
		$('#select-icon').append(data);
	}
});


// templating
function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<i class='fa fa-fw "+ state.id.toLowerCase() + "'></i>  " + state.text + "</span>";
}
$("#select-icon").select2({
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function(m) { return m; }
});




$.ajax({
	type: "POST",
	url: MY_CONSTANT+"/controller/multiselectController.php",
	data: {student: true},
	success: function(data){
		// console.log(data);
		$('#select-students').empty();
		$('#select-students').append(data);
	}
});


$.ajax({
	type: "POST",
	url: MY_CONSTANT+"/controller/multiselectController.php",
	data: {staff: true},
	success: function(data){
		$('#select-staffs').empty();

		$('#select-staffs').append(data);
	}
});




$('.create_project').click(function(){
	
	var project_name = $(".project-name").val();
	var project_description = $(".project-desc").val();

	var icon = $('#s2id_select-icon span').text();

	var color = $('#colorpickerColor').val();

	var time_stamp = epochDate();

	var created_on = humanTime(time_stamp);

	var selected_students = new Array();
	var selected_staffs = new Array();
	
		$('#s2id_select-students .selected-choices').each(function(index){
			selected_students[index] = $(this).attr('data-id');
		});


		$('#s2id_select-staffs .selected-choices').each(function(index){
			selected_staffs[index] = $(this).attr('data-id');
		});

		selected_students = selected_students.toString();

		selected_staffs = selected_staffs.toString();


	console.log(selected_students);
	console.log(selected_staffs);



	// Session Values
	sessionHandler(function(output){

	  	var session = jQuery.parseJSON(output);

		  	var author_name = session.user_name;

			var author_id = session.user_id;

			var user_id = session.user_id;

			var message_author = session.user_name;
	
			var author_pic = session.user_pic;

			var author_dept = session.user_dept;
			
			var college_name = session.college_name;

			var college_id = session.college_id;

			var objectives = "";

			// group_members = selected_members+","+user_id;

		var project_id = user_id+"_"+time_stamp;	

		var data = {project_id: project_id, project_name: project_name, description: project_description, created_on: created_on, created_timestamp: time_stamp, created_by: author_id, creator_name: author_name, creator_pic: author_pic, members: selected_students, mentors: selected_staffs, icon: icon, colour: color, objectives: objectives};

				postProject(data);

	})
	//


		
	

});



function postProject(data){
	
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/projectController.php",
		data: {create_project: data},
		success: function(data){
			$("#get-projects").empty();
			$("#get-projects").append(data);
			console.log(data);
		}
	})

}


// $.ajax({
// 	type: "POST",
// 	url: MY_CONSTANT+"/controller/assignmentController.php",
// 	data: {upload_student_data: str},
// 	success: function(data){
// 		$("#upload-student-data").empty();
// 		$("#upload-student-data").append(data);
// 		uploadAnswer();
// 		fileHandler();
// 	}
// })

