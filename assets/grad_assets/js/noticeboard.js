var MY_CONSTANT = "assets/grad_assets";




$('.get-courses').hide();
$('.get-classes').hide();

$('.radio-custom').click(function(){
    var classes = $(this).hasClass('select-classes');
    selectNoticeboardTo(classes);
});

selectNoticeboardTo(false);

function selectNoticeboardTo(classes){

    var data = true;

    if(classes){
        
        $("#select-classes").select2({
            placeholder: "Select Classes",
            allowClear: true,
        });
        
        $('.get-courses').hide();

        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/multiselectController.php",
            data: {get_classes: data},
	    //data: {student: data},
            success: function(data){
                $('.select2-search-choice').remove();
                $('#select-classes').empty();
                $('#select-classes').append(data);
                $('.get-classes').show();
            }
        });
    } else {
        
        $('.select2-search-choice').remove();
        $('.get-classes').hide();
        $('.get-courses').show();
    }
}



$(".new-noticeboard").click(function(){

        var title = $('.new_noticeboard_title').val();

        
        var description = $(".new_noticeboard_description").val();
        

        var time_stamp = epochDate();

        var created_on = humanTime(time_stamp);



            var departments = new Array();

             $('.ms-selected').each(function(index){
                var str = $(this).attr('id');
                    var splitted = str.split("GRD");
                    // console.log(splitted);
                departments[index] = splitted[0];

            });

        var assign_to = {departments: departments.toString()};

            
            var classes = new Array();

            $('.selected-choices').each(function(index){
                classes[index] = $(this).attr('data-id');
            });

        if(classes!=""){
            var assign_to = {classes: classes.toString()};    
        }

        if(departments==""&&classes==""){
            alert("please select to option");
        }


        // Session Values
        sessionHandler(function(output){

            var session = jQuery.parseJSON(output);

                var author_name = session.user_name;

                var author_id = session.user_id;

                var user_id = session.user_id;

            var notice_id = author_id+"_"+time_stamp;
            
                var author_pic = session.user_pic;

                var author_dept = session.user_dept;

                var college_id = session.college_id;
                
                var college_name = session.college_name;


                var file_path = new Array();

                   var i = 0;

                   var path_location = "files/"+college_id+"/noticeboard/"+author_id+"/";

                    $('.ajax-file-upload-statusbar').each(function(index){
                        var status = $(this).attr('style');
                        // alert(status);
                        if(status.indexOf("block") > -1){
                            var name =  $(this).find('.ajax-file-upload-filename').text();
                            name = name.split("). ")
                            
                            file_path[i] = path_location+name[1];

                            console.log(file_path[i]);
                            
                            i++;
                        }                   
                    });

                    if(file_path == ""){
                        // alert("Empty");
                    }else{
                        // alert(file_path.toString())
                        file_path = file_path.toString();
                    }


            var data = {file_path: file_path, assign_to: assign_to, notice_id: notice_id, title: title, description: description, time_stamp: time_stamp, created_on: created_on, author_name: author_name, college_name: college_name, college_id: college_id, author_pic: author_pic, author_id: author_id};

            createNotice(data);

        });

});


function createNotice(data){
    $.ajax({
        type: "POST",
        url: MY_CONSTANT+"/controller/noticeboardController.php",
        data: {create_new_notice: data},
        success: function(data){
            $('.modal-header .close').trigger('click');
            emptyPreviewFile();
            $('.new_noticeboard_title').val("");
            $('.new_noticeboard_description').val("");

            $('#get-noticeboard').empty();
            $('#get-noticeboard').append(data);

            $('#blog-landing').pinterest_grid({
                no_columns: 4,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

            // location.reload();

            //alert("look in console");
            console.log(data);
        }
    });
}




fileHandler();

function fileHandler(){

    var settings = {
        url: MY_CONSTANT+"/upload.php",
        dragDrop:true,
        fileName: "myfile",
        multiple:false,
        allowedTypes:"jpg,png,gif,jpeg",    
        returnType:"json",
        maxFileCount:1,
        onSuccess:function(files,data,xhr)
        {
            alert(data);
            // $('.ajax-file-upload-filename').empty();
            // $('.ajax-file-upload-filename').append(data);
        },
        showDelete:true,
        deleteCallback: function(data,pd)
        {
            for(var i=0;i<data.length;i++)
            {
                $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
                function(resp, textStatus, jqXHR)
                {
                    //Show Message  
                    $("#mulitplefileuploadStatus").append("<div>File Deleted</div>");      
                });
            }      
            pd.statusbar.hide(); //You choice to hide/not.
        }
    }

    var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

}