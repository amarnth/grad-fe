var MY_CONSTANT = "assets/grad_assets";

// today button
$('#dob').bdatepicker({
	format: "dd MM yyyy",
	startDate: "2015-02-14",
	autoclose: true,
	todayBtn: true
});

fillup();

function fillup(){
	// var elem = document.getElementById("fname");
	// elem.value = "My default value";

//$('#fname').val("Amar");
//$('#lname').val("Ram");
//$('#roll_no').val("1");
//$('#email').val("a@gmail.com");
//$('#password').val("qwerty");
//$('#mobile_number').val("9898989898");
//$('#state').val("TN");
//$('#city').val("CBE");
//$('#address').val("XYZ street, Vellore");
//$('#secondary_mark').val("80");
//$('#higher_secondary_mark').val("80");
//$('#pincode').val("5784671");
//$('#nationality').val("Indian");
//$('#skills').val("java,pyhton,php,cassandra");

//$('#department').val("IT");
//$('#class_name').val("IT1A");
//$('#course').val("B.TECH");
//$('#current_semester').val("4");
	
	
	$('#fname').val();
	$('#lname').val();
	$('#roll_no').val();
	$('#email').val();
	$('#password').val();
	$('#mobile_number').val();
	$('#state').val();
	$('#city').val();
	$('#address').val();
	$('#secondary_mark').val();
	$('#higher_secondary_mark').val();
	$('#pincode').val();
	$('#nationality').val();
	$('#skills').val();
	
	$('#department').val();
	$('#class_name').val();
	$('#course').val();
	$('#current_semester').val();


	
	
}

function submitForm(){
	var fname = $('#fname').val();
	var lname = $('#lname').val();
	var gender = $('.gender .selectpicker').attr('title');
	var roll_no = $('#roll_no').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var mobile_number = $('#mobile_number').val();
	var dob = $('#dob input').val();
	var state = $('#state').val();
	var city = $('#city').val();
	var pincode = $('#pincode').val();
	var address = $('#address').val();
	var nationality = $('#nationality').val();
	var secondary_mark = $('#secondary_mark').val();
	var higher_secondary_mark = $('#higher_secondary_mark').val();
	var quota = $('.quota .selectpicker').attr('title');
	var skills = $('#skills').val();
	var terms = true;
	

	var department = $('#department').val();
	var class_name = $('#class_name').val();
	var course = $('#course').val();
	var current_semester = $('#current_semester').val();



	var university_name = "GPIT";
	var college_id = "grad_C01X";


	var data = {current_semester: current_semester, course: course, college_id: college_id, university_name: university_name, class_name: class_name, department: department, fname: fname, lname: lname, gender: gender, rollno: roll_no, emailid: email, password: password, mobileno: mobile_number, datebirth: dob, state: state, city: city, pincode: pincode, address: address, nationality: nationality, tenmark: secondary_mark, twelevemark: higher_secondary_mark, quota: quota, skillset: skills, terms: terms};


	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/signupController.php",
		data: {signup_student: data},
		success: function(data){
			console.log(data);
		}
	});

}






$.validator.setDefaults(
{
	submitHandler: function() {
		submitForm();
	},
	showErrors: function(map, list) 
	{
		this.currentElements.parents('label:first, div:first').find('.has-error').remove();
		this.currentElements.parents('.form-group:first').removeClass('has-error');
		
		$.each(list, function(index, error) 
		{
			var ee = $(error.element);
			var eep = ee.parents('label:first').length ? ee.parents('label:first') : ee.parents('div:first');
			
			ee.parents('.form-group:first').addClass('has-error');
			eep.find('.has-error').remove();
			eep.append('<p class="has-error help-block">' + error.message + '</p>');
		});
		//refreshScrollers();
	}
});




$("#validateForm").validate({
	rules: {
		firstname: "required",
		lastname: "required",
		roll_no: "required",
		mobile_number: "required",
		address: "required",
		secondary_mark: "required",
		higher_secondary_mark: "required",
		password: {
			required: true,
			minlength: 5
		},
		confirm_password: {
			required: true,
			minlength: 5,
			equalTo: "#password"
		},
		email: {
			required: true,
			email: true
		},
		agree: "required"
	},
	messages: {
		firstname: "Please enter your firstname",
		lastname: "Please enter your lastname",
		roll_no: "Please enter your Roll Number",
		mobile_number: "Please enter your Mobile Number",
		address: "Please enter your Address",
		secondary_mark: "Please enter your Secondary Mark",
		higher_secondary_mark: "Please enter your Higher Secondary Mark",
		password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address",
		agree: "Please accept our policy"
	}
});