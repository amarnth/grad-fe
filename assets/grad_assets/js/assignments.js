var MY_CONSTANT = "assets/grad_assets";

$('.get-department').hide();
$('.get-student').hide();

$('.radio-custom').click(function(){
	var student = $(this).hasClass('select-student');
	selectMembers(student);
});


selectMembers(true); // default call

function selectMembers(student){

	$("#assignment-select-students").select2({
	    placeholder: "Select students",
	    allowClear: true
	});

	$("#assignment-select-department").select2({
	    placeholder: "Select Department",
	    allowClear: true
	});

	if(student){
		$.ajax({
			type: "POST",
			url: MY_CONSTANT+"/controller/multiselectController.php",
			data: {student: true},
			success: function(data){
				
				$('#assignment-select-students').empty();

				$('#assignment-select-students').append(data);

				$('.get-department').hide();
				$('.get-student').show();
				$('.select2-search-choice').remove();
			}
		})
	} else {
		$.ajax({
			type: "POST",
			url: MY_CONSTANT+"/controller/multiselectController.php",
			data: {department: true},
			success: function(data){
				$('#assignment-select-department').empty();

				$('#assignment-select-department').append(data);

				$('.get-student').hide();
				$('.get-department').show();
				$('.select2-search-choice').remove();
			}
		})
	}	
}


// today button
$('#assignment-deadline').bdatepicker({
	format: "dd MM yyyy",
	startDate: new Date(),
	autoclose: true,
	todayBtn: true
});



$('#post_new_assignment').click(function(){


	var title = $('.assignment-title').val();

	var topic = $('.assignment-topic').val();
	var subject = $('.assignment-subject').val();
	var description = $('.assignment-desc').val();
	var deadline = $('#assignment-deadline input').val();


	var selected_choices = new Array();
	
		$('.selected-choices').each(function(index){
			selected_choices[index] = $(this).attr('data-id');
		});

	var student = $('.select-student').hasClass('checked');

	if(student){
		var assigned_to = {students: selected_choices.toString()};
	}else{
		var assigned_to = {department: selected_choices.toString()};
	}

	var time_stamp = epochDate();

	var created_on = humanTime(time_stamp);

	// Session Values
	sessionHandler(function(output){

	  	var session = jQuery.parseJSON(output);

		  	var author_name = session.user_name;

			var author_id = session.user_id;

			var user_id = session.user_id;

			var message_author = session.user_name;
	
			var author_pic = session.user_pic;

			var author_dept = session.user_dept;
			
			var college_name = session.college_name;

			var college_id = session.college_id;

			// group_members = selected_members+","+user_id;

			var assign_id = user_id+"_"+time_stamp;	

				var myDate = new Date(deadline); // Your timezone!

				var deadline_time_stamp = myDate.getTime()/1000.0;


			var file_path = new Array();

			var media = true;
		
				var i=0;


				var path_location = "files/"+college_id+"/assignments/"+author_id+"/";


				filepathLocation(path_location, function(output){

					var file_count = output-1;
					
					// console.log("Awesome Output "+output);
				
					$('.ajax-file-upload-statusbar').each(function(index){
						var status = $(this).attr('style');
						// alert(status);
						if(status.indexOf("block") > -1){
							var name =  $(this).find('.ajax-file-upload-filename').text();
					    	name = name.split("). ")

					    	var ransom_name = file_count-i;
					    	
					    	// file_path[i] = path_location+ransom_name+name[1];
					    	
					    	file_path[i] = path_location+name[1];

					    	// console.log(file_path[i]);
					    	
					    	i++;
						}				    
					});
					

					file_path = file_path.toString();

				// console.log("paths=>"+file_path);
				

				var data = {media: media, deadline_time_stamp: deadline_time_stamp, college_id: college_id, file_path: file_path, assign_id: assign_id, deadline: deadline, description: description, subject: subject, title: title, topic: topic, assigned_to: assigned_to, created_on: created_on, time_stamp: time_stamp, author_id: author_id, author_name: author_name, author_pic: author_pic, author_dept: author_dept};

				emptyPreviewFile();

				createAssignment(data);

			})

	});
	//
});


function createAssignment(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/assignmentController.php",
		data: {create_data: data},
		success: function(data){
			$('#assignmentTasks').empty();	
			$('#assignmentTasks').append(data); 
			saveMarks();
		}
	});
}



$(".assignment-search").keyup(function(){
	var searchText = $(".assignment-search").val();
	$(".assignment_title").each(function(index){
		if($(this).attr('data-s')){
			if(!$(this).attr('data-s').match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
		}
	});
});



