var MY_CONSTANT = "assets/grad_assets";




inviteMembers();

function inviteMembers(){
    $('.event-invite').click(function(){
        var str = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/eventController.php",
            data: {event_invite_members: str},
            success: function(data){
                $("#preview-data").empty();
                $("#preview-data").append(data);
                invitees();
            }
        })
    });    
}



function invitees(){
   
    $('.get-department').hide();
    $('.get-student').hide();

    $('.radio-custom').click(function(){
        var student = $(this).hasClass('select-student');
        selectMembers(student);
        if(student){
            $('.select-student .fa.fa-circle-o').addClass('checked');
            $('.select-department .fa.fa-circle-o').removeClass('checked');
        }else{
            $('.select-student .fa.fa-circle-o').removeClass('checked');
            $('.select-department .fa.fa-circle-o').addClass('checked');
        }
    });


    selectMembers(true); // default call
}

function selectMembers(student){

    $("#assignment-select-students").select2({
        placeholder: "Select students",
        allowClear: true
    });

    $("#assignment-select-department").select2({
        placeholder: "Select Department",
        allowClear: true
    });

    if(student){
        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/multiselectController.php",
            data: {individual: true},
            success: function(data){
                $('#assignment-select-students').empty();

                $('#assignment-select-students').append(data);

                $('.get-department').hide();
                $('.get-student').show();
                $('.select2-search-choice').remove();
            }
        })
    } else {
        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/multiselectController.php",
            data: {department: true},
            success: function(data){
                $('#assignment-select-department').empty();

                $('#assignment-select-department').append(data);

                $('.get-student').hide();
                $('.get-department').show();
                $('.select2-search-choice').remove();
            }
        })
    }


    $('.event_invite_members').click(function(){
               
        var event_id = $(this).attr('data-id');

        var selected_choices = new Array();
    
        $('.selected-choices').each(function(index){
            selected_choices[index] = $(this).attr('data-id');
        });

        var student = $('.select-student .fa.fa-circle-o').hasClass('checked');

        if(student){
            var invited = {students: selected_choices.toString()};
        }else{
            var invited = {department: selected_choices.toString()};
        }

        var data = {event_id: event_id, invited: invited};

        postEventInvites(data);


    });
}  


createNewEvent();

function createNewEvent(){


$('#event-start-date, #event-end-date').bdatepicker({
    format: "dd MM yyyy",
    startDate: new Date(),
    setDate: new Date(),
    autoclose: true,
    todayBtn: true
});



    $('.new-event').click(function(){

        var event_name = $('.event-name').val();
       
        var event_description =$('.event_description').val();

        var start_date =$('#event-start-date input').val();

        var end_date =$('#event-end-date input').val();

        var start_time =$('#event-start-time').val();

        var end_time =$('#event-end-time').val();

        var event_type =$('.event_type .selectpicker').attr('title');

        var event_inivite =$('.event_pre_invite .selectpicker').attr('title');

        //alert(start_date);

        //alert(end_date);

        var time_stamp = epochDate();

        var opened_to = "public";


        sessionHandler(function(output){

            var session = jQuery.parseJSON(output);

            var host_name = session.user_name;

            var host_id = session.user_id;

            var host_role = session.user_role;

                var event_id = host_id+"_"+time_stamp;

            var host_pic = session.user_pic;

            var host_dept = session.user_dept;

            var college_id = session.college_id;
            
            var college_name = session.college_name;

            var data = {host_dept: host_dept, college_name: college_name, college_id: college_id, host_role: host_role, opened_to: opened_to, event_description: event_description, host_name: host_name, host_pic: host_pic, time_stamp: time_stamp, event_id: event_id, host_id: host_id, event_name: event_name, start_date: start_date,start_time: start_time,end_date: end_date,end_time: end_time ,event_type: event_type,event_inivite: event_inivite};

            postEvent(data);
        });
    })
}



function postEvent(data){
	//
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/eventController.php",
		data: {create_event: data},
		success: function(data){
            $('#get_events').empty();
			$('#get_events').append(data);
            createNewEvent();
            inviteMembers();
		}
	})
}





function postEventInvites(data){
    $.ajax({
        type: "POST",
        url: MY_CONSTANT+"/controller/eventController.php",
        data: {set_event_invite: data},
        success: function(data){
            // alert(data);
            $('#get_events').empty();
            $('#get_events').append(data);
            reCall();
            createNewEvent();
            inviteMembers();
        }
    })
}



reCall();

function reCall(){

    $('.event_attending').click(function(){
        var id = $(this).attr('data-id');
        id = id.split("#");
        event_id = id[1];

        //alert(event_id);

        sessionHandler(function(output){
            var session = jQuery.parseJSON(output);
            var user_id = session.user_id;
            var data = {event_id: event_id, user_id: user_id};
            userAcceptEvent(data);
        });

    });



    $('.event_decline').click(function(){
        var id = $(this).attr('data-id');
        id = id.split("#");
        event_id = id[1];

        //alert(event_id);

        sessionHandler(function(output){
            var session = jQuery.parseJSON(output);
            var user_id = session.user_id;
            var data = {event_id: event_id, user_id: user_id};
            userDeclineEvent(data);
        });

    });


    $('.admin_decline').click(function(){
        var id = $(this).attr('data-id');
        id = id.split("#");
        event_id = id[1];
        
        var data = {event_id: event_id, status: -1};

        approveEvent(data);
    });



    $('.admin_approve').click(function(){
        var id = $(this).attr('data-id');
        //alert(id);
        id = id.split("#");
        var event_id = id[1];


        sessionHandler(function(output){

            var session = jQuery.parseJSON(output);

                var admin_name = session.user_name;

                var admin_id = session.user_id;

                var admin_pic = session.user_pic;

                var admin_dept = session.user_dept;

                var college_id = session.college_id;
                
                var college_name = session.college_name;

            // var data = {event_id: event_id, college_name: college_name, college_id: college_id, admin_id: admin_id, admin_name: admin_name, admin_pic: admin_pic, admin_dept: admin_dept};
            var data = {event_id: event_id, status: 1};

            approveEvent(data);
        });
    });



    function approveEvent(data){
        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/eventController.php",
            data: {approve: data},
            success:function(data){
                $('#get_events').empty();
                $('#get_events').append(data);
                createNewEvent();
                inviteMembers();
                reCall();
            }
        });
    }




    function userAcceptEvent(data){
        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/eventController.php",
            data: {user_accept_event: data},
            success:function(data){
                $('#get_events').empty();
                $('#get_events').append(data);
                createNewEvent();
                inviteMembers();
                reCall();
            }
        });
    }

    function userDeclineEvent(data){
        $.ajax({
            type: "POST",
            url: MY_CONSTANT+"/controller/eventController.php",
            data: {user_decline_event: data},
            success:function(data){
                $('#get_events').empty();
                $('#get_events').append(data);
                createNewEvent();
                inviteMembers();
                reCall();
            }
        });
    }
}