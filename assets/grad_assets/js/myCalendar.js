MY_CONSTANT = "assets/grad_assets";

// Session Values
sessionHandler(function(output){

	  	var session = jQuery.parseJSON(output);

		var author_id = session.user_id;

		console.log(author_id);

	$('#mycalendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		editable: true,
		draggable: false,
		events: "http://gradpowered.net/assets/grad_assets/calendarTrim.php",
		// events: "http://localhost/grad/assets/grad_assets/js/json/calendarEvents.json",
		// events: "http://gradpowered.net:9558/get/calendar/"+author_id,
		drop: function(date, allDay) 
		{
			// retrieve the dropped element's stored Event Object
			var originalEventObject = $(this).data('eventObject');
			
			// we need to copy it, so that multiple events don't have a reference to the same object
			var copiedEventObject = $.extend({}, originalEventObject);
			
			// assign it the date that was reported
			copiedEventObject.start = date;
			copiedEventObject.allDay = allDay;
			
			// render the event on the calendar
			// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
			$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
			
			// is the "remove after drop" checkbox checked?
			if ($('#drop-remove').is(':checked')) {
				// if so, remove the element from the "Draggable Events" list
				$(this).remove();
			}
			
		}
	});

});
//




function sessionHandler(handleData){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/sessionHandler.php",
		data: {get_session: true},
	    success: function(data) {
	    	handleData(data);
  	    }
	});
}




// reformat();

// function reformat(){
// 	var data = true;
// 	$.ajax({
// 		type: "POST",
// 		url: rootPath + "/assets/grad_assets/controller/calendarController.php",
// 		data: {get_calendar: data},
// 		success: function(data){
// 			// fn(data);
// 			console.log(data);
// 			$('#data').empty();
// 			$('#data').append(data);
// 		}
// 	});
// }

// var get = $('#mycalendar').fullCalendar('getCalendar');

// console.log(get);

