
function submitForm(){
	
	var fname = $('#fname').val();
	var lname = $('#lname').val();
	var gender = $('.gender .selectpicker').attr('title');
	var roll_no = $('#roll_no').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var mobile_number = $('#mobile_number').val();
	var dob = $('#dob input').val();
	var state = $('#state').val();
	var city = $('#city').val();
	var pincode = $('#pincode').val();
	var address = $('#address').val();
	var nationality = $('#nationality').val();
	var secondary_mark = $('#secondary_mark').val();
	var higher_secondary_mark = $('#higher_secondary_mark').val();
	var quota = $('.quota .selectpicker').attr('title');
	var skills = $('#skills').val();
	var terms = true;

	var data = {fname: fname, lname: lname, gender: gender, roll_no: roll_no, email: email, password: password, mobile_number: mobile_number, dob: dob, state: state, city: city, pincode: pincode, address: address, nationality: nationality, secondary_mark: secondary_mark, higher_secondary_mark: higher_secondary_mark, quota: quota, skills: skills, terms: terms};

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/signupController.php",
		data: data,
		success: function(data){
			console.log(data);
		}
	});

}


$.validator.setDefaults(
{
	submitHandler: function() {
		submitForm();
	},
	showErrors: function(map, list) 
	{
		this.currentElements.parents('label:first, div:first').find('.has-error').remove();
		this.currentElements.parents('.form-group:first').removeClass('has-error');
		
		$.each(list, function(index, error) 
		{
			var ee = $(error.element);
			var eep = ee.parents('label:first').length ? ee.parents('label:first') : ee.parents('div:first');
			
			ee.parents('.form-group:first').addClass('has-error');
			eep.find('.has-error').remove();
			eep.append('<p class="has-error help-block">' + error.message + '</p>');
		});
		//refreshScrollers();
	}
});




$("#validateForm").validate({
	rules: {
		fname: "required",
		lname: "required",
		roll_no: "required",
		mobile_number: "required",
		address: "required",
		secondary_mark: "required",
		higher_secondary_mark: "required",
		password: {
			required: true,
			minlength: 5
		},
		confirm_password: {
			required: true,
			minlength: 5,
			equalTo: "#password"
		},
		email: {
			required: true,
			email: true
		},
		agree: "required"
	},
	messages: {
		firstname: "Please enter your firstname",
		lastname: "Please enter your lastname",
		roll_no: "Please enter your Roll Number",
		mobile_number: "Please enter your Mobile Number",
		address: "Please enter your Address",
		secondary_mark: "Please enter your Secondary Mark",
		higher_secondary_mark: "Please enter your Higher Secondary Mark",
		password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address",
		agree: "Please accept our policy"
	}
});