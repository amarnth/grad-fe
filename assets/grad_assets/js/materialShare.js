$('div[for=individual]').hide();
$('div[for=department]').hide();
var MY_CONSTANT = "assets/grad_assets"
	// Multi select option
		var $selectator = $('#selectatorShareSelectedMembers');
		var $selectTo = $('#selectShareMembers');
		$selectTo.selectator({
			showAllOptionsOnFocus: true
		});
		$selectator.val('destroy selectator');
	//

	// Multi select option
		var $selectator = $('#selectatorShareSelectedDepartment');
		var $selectTo = $('#selectShareDepartment');
		$selectTo.selectator({
			showAllOptionsOnFocus: true
		});
		$selectator.val('destroy selectator');
	//

	$('.share_recipient').click(function(){
		
		var val = $(this).attr('data-bool');
		//alert(val);
		if(val == "true"){
			$('div[for=individual]').hide();
			$('div[for=department]').show();
			
			$.ajax({
				type: "POST",
				url: MY_CONSTANT+"/controller/assignmentController.php",
				data: {department: val},
				success: function(data){
					$('#selectShareDepartment').empty();
					$('.selectator_chosen_items').empty();
					$('#selectShareDepartment').append(data);
				}
			});

			
		} else{
			$('div[for=individual]').show();
			$('div[for=department]').hide();

			$.ajax({
				type: "POST",
				url: MY_CONSTANT+"/controller/assignmentController.php",
				data: {individual: val},
				success: function(data){
					$('#selectShareMembers').empty();
					$('.selectator_chosen_items').empty();
					$('#selectShareMembers').append(data);
				}
			});
		}
	});