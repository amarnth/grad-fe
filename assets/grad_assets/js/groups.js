var MY_CONSTANT = "assets/grad_assets";

$("#select2_members").select2({
    placeholder: "Select group members",
    allowClear: true
});

$(".create-new-group").click(function(){
	var val = true;
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/multiselectController.php",
		data: {student: val},
		success: function(data){
		        
		        // alert(data);
		        
			$('#select2_members').empty();

			$('#select2_members').append(data);
		}
	})
});

$.ajax({
	type: "POST",
	url: MY_CONSTANT+"/controller/multiselectController.php",
	data: {student: true},
	success: function(data){
		$('#select2_members').empty();
		$('#select2_members').append(data);
	}
})

// $('.group_recepient').click(function(){
		
// 	var val = $(this).attr('data-bool');
// 	//alert(val);
// 	if(val == "true"){
// 		$('div[for=individual]').hide();
// 		$('div[for=department]').show();
		
// 		$.ajax({
// 			type: "POST",
// 			url: MY_CONSTANT+"gradpower/controller/multiselectController.php",
// 			data: {department: val},
// 			success: function(data){
// 				$('#selectAssignmentDepartment').empty();
// 				$('.selectator_chosen_items').empty();
// 				$('#selectAssignmentDepartment').append(data);
// 			}
// 		});

		
// 	} else{
// 		$('div[for=individual]').show();
// 		$('div[for=department]').hide();

// 		$.ajax({
// 			type: "POST",
// 			url: MY_CONSTANT+"gradpower/controller/multiselectController.php",
// 			data: {individual: val},
// 			success: function(data){
// 				$('#selectAssignmentMembers').empty();
// 				$('.selectator_chosen_items').empty();
// 				$('#selectAssignmentMembers').append(data);
// 			}
// 		});
// 	}
// });



$('.create_a_group').click(function(){
	
	var group_name = $('.group-name').val();
	
	var group_description = $('.group-description').val();
	//alert(group_description);

		var selected_choices = new Array();
	
		$('.selected-choices').each(function(index){
			selected_choices[index] = $(this).attr('data-id');
		});

	var selected_members = selected_choices.toString();

	var group_type = $(".group-type-open").hasClass("checked");
		
	var only_admin_can_add = $(".switch-animate").hasClass("switch-on");

		var time_stamp = epochDate();

		var created_on = humanTime(time_stamp);

	// Session Values
	sessionHandler(function(output){

	  	var session = jQuery.parseJSON(output);

			  	var user_name = session.user_name;

				var author_id = session.user_id;

				var user_id = session.user_id;

				var message_author = session.user_name;
		
				var profile_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;

				group_members = selected_members+","+user_id;

			var group_id = user_id+"_"+time_stamp;	

			var admin_name = user_name;

			var admin_id = user_id;

			var admin_pic = profile_pic;

			var file_paths = new Array();
		
				var i=0;



				var path_location = "files/"+college_id+"/groups/"+author_id+"/";

				filepathLocation(path_location, function(output){

						var file_count = output-1;

						console.log("Awesome Output "+output);
					
						$('.ajax-file-upload-statusbar').each(function(index){
							var status = $(this).attr('style');
							// alert(status);
							if(status.indexOf("block") > -1){
								var name =  $(this).find('.ajax-file-upload-filename').text();
								name = name.split("). ")
								
						    	var ransom_name = file_count-i;
											// file_paths[i] = path_location+ransom_name+name[1];
											
											file_paths[i] = path_location+name[1];
						    	i++;
							}				    
						});
					

					group_image = file_paths.toString();

					console.log("paths=>"+group_image);
					// console.log("selected_members=>"+selected_members);
					// console.log("only_admin_can_add=>"+only_admin_can_add);
					// console.log("group_type=>"+group_type);


					var data = {description: group_description, group_image: group_image, user_id: user_id, group_id: group_id, college_name: college_name, college_id: college_id, admin_name: admin_name, admin_id: admin_id, admin_pic: admin_pic, group_name: group_name, group_members: group_members, group_type: group_type, only_admin_can_add: only_admin_can_add, time_stamp: time_stamp, created_on: created_on};

					console.log(data);

					groupPost(data);
				})

		});
		//
})


function groupPost(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/groupsController.php",
		data: data,
		success: function(data){
			$('#groupsInfo').empty();
			$('#groupsInfo').append(data);
			groupSearch();
			// selectGroup();
		}
	})
}


groupSearch();

function groupSearch(){
	$(".groups-search").keyup(function(){
		var searchText = $(this).val();
		$(".groups-list").each(function(index){
			if($(this).find("h4 .group_name")){
				if(!$(this).text().match(new RegExp(searchText, "i"))){
					$(this).fadeOut("fast");
				}else{
					$(this).fadeIn("slow");
				}
			}
		});
	});	
}


// selectGroup();

function selectGroup(){
	// $( ".group_name" ).click(function() {
	// 	var group_id = $(this).attr('data-id');
	// 	// var group_name = $(this).attr('data-search');

	// 	// //alert(group_id+", "+group_name);
	// 	// //$('#group_submit_'+group_id).submit();

	// 	$.ajax({
	// 		type: "POST",
	// 		url: MY_CONSTANT+"/sessionHandler.php",
	// 		data: {group_id: group_id},
	// 		success: function(data){
	// 			window.location= "http://localhost/grad/index.php?page=group_wall";
	// 		}
	// 	})


	// });
}



fileHandler();

function fileHandler(){

	var settings = {
	    url: MY_CONSTANT+"/upload.php",
	    dragDrop:true,
	    fileName: "myfile",
	    multiple:false,
	    allowedTypes:"jpg,png,gif,jpeg",	
	    returnType:"json",
	    maxFileCount:1,
		onSuccess:function(files,data,xhr)
	    {
			alert(data);
			// $('.ajax-file-upload-filename').empty();
			// $('.ajax-file-upload-filename').append(data);
	    },
	    showDelete:true,
	    deleteCallback: function(data,pd)
		{
		    for(var i=0;i<data.length;i++)
		    {
		        $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
		        function(resp, textStatus, jqXHR)
		        {
		            //Show Message  
		            $("#mulitplefileuploadStatus").append("<div>File Deleted</div>");      
		        });
		    }      
		    pd.statusbar.hide(); //You choice to hide/not.
		}
	}

	var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

}