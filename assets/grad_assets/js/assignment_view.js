var MY_CONSTANT = "assets/grad_assets";

$('.open-attachment').click(function(){
	var str = $(this).attr('data-hide');

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/assignmentController.php",
		data: {preview_modal_attachment: str},
		success: function(data){
			$("#preview-data").empty();
			$("#preview-data").append(data);
		}
	})

});


$('.open-modal-answer').click(function(){
	var str = $(this).attr('data-hide');

	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/assignmentController.php",
		data: {upload_student_data: str},
		success: function(data){
			$("#upload-student-data").empty();
			$("#upload-student-data").append(data);
			uploadAnswer();
			fileHandler();
		}
	})

});


function uploadAnswer(){
	$('.upload-assignment-answer').click(function(){

		var assignment_id = $(this).attr("data-id");

		// alert(assignment_id);

		// Session Values
		sessionHandler(function(output){

		  	var session = jQuery.parseJSON(output);

			  	var author_name = session.user_name;

				var author_id = session.user_id;

				var user_id = session.user_id;

				var message_author = session.user_name;
		
				var author_pic = session.user_pic;

				var author_dept = session.user_dept;
				
				var college_name = session.college_name;

				var college_id = session.college_id;

				var file_path = new Array();

				var media = true;

				var i=0;


				var path_location = "files/"+college_id+"/assignments/"+author_id+"/";

				filepathLocation(path_location, function(output){

					var file_count = output-1;
					
					console.log("Awesome Output "+output);
				
					$('.ajax-file-upload-statusbar').each(function(index){
						var status = $(this).attr('style');
						// alert(status);
						if(status.indexOf("block") > -1){
							var name =  $(this).find('.ajax-file-upload-filename').text();
					    	name = name.split("). ")

					    	var ransom_name = file_count-i;
					    	
					    	// file_path[i] = path_location+ransom_name+name[1];
					    	
					    	file_path[i] = path_location+name[1];

					    	console.log(file_path[i]);
					    	
					    	i++;
						}				    
					});
					



					file_path = file_path.toString();


				var deadline = $('.deadline'+assignment_id).text();

				var myDate = new Date(deadline); // Your timezone!

				var deadline_time_stamp = myDate.getTime()/1000.0;

				alert(deadline_time_stamp);


				console.log("paths=>"+file_path);

				var submitted_time_stamp = epochDate();

				var submitted_on = humanTime(submitted_time_stamp);

				var data = {assignment_id: assignment_id, submitted_file_path: file_path, media: media, student_id: author_id, student_name: author_name, student_pic: author_pic, submitted_on: submitted_on, time_stamp: submitted_time_stamp, deadline_time_stamp: deadline_time_stamp};

				console.log(data);

				$(".close.assignment-modal-close").click();

				postAnswer(data);


			})

		});
		//
	});
}


function postAnswer(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"/controller/assignmentController.php",
		data: {post_answer: data},
		success: function(data){
			console.log(data);
			$('#assignment_view').empty();	
			$('#assignment_view').append(data); 
		}
	});
}




saveMarks();

function saveMarks(){
	$('.save-student-mark').click(function(){
		
		var id = $(this).attr('id');
			id = id.split("#")
			id = id[1];
		
		var i = 0;
		var marks = new Array();
		
		getAllMarks(id, i, marks, "save");

	});

	$('.publish-student-mark').click(function(){

		var id = $(this).attr('id');
			id = id.split("#")
			id = id[1];
		
		var i = 0;
		var marks = new Array();
		
		getAllMarks(id, i, marks, "publish");

	});
}



function getAllMarks(id, i, marks, option){

	$('.'+id).each(function(index){
		var student_id = $(this).attr('id');
			student_id = student_id.split("#")
			student_id = student_id[1];

			var mark = "#mark"+student_id;
			var comment = "#comment"+student_id;

		mark = $(mark).text();
		comment = $(comment).text();

		if(mark == ""){
			marks[i] = {mark: -10167, comment: comment, student_id: student_id};	
		}else{
			marks[i] = {mark: parseInt(mark), comment: comment, student_id: student_id};
		}
		
		// alert(marks);
		i++;
	});

	var last_page = $('#data-table-id'+id+' .dataTables_paginate.paging_bootstrap li.next').hasClass('disabled');
	
	// alert(last_page);

	if(!last_page){
		$('#data-table-id'+id+' .dataTables_paginate.paging_bootstrap li.next a').trigger("click");
		getAllMarks(id, i, marks, option);

	}

	if(last_page){	
		// console.log(marks);
		updateMarks(id, marks, option);
	}
}



function updateMarks(id, marks, option){

	// Session Values
	sessionHandler(function(output){

	  	var session = jQuery.parseJSON(output);

		  	var author_name = session.user_name;

			var author_id = session.user_id;

			var user_id = session.user_id;

			var message_author = session.user_name;
	
			var author_pic = session.user_pic;

			var author_dept = session.user_dept;
			
			var college_name = session.college_name;

			var college_id = session.college_id;

				var save_time_stamp = epochDate();

		var deadline = $('.deadline'+id).text();

		var myDate = new Date(deadline); // Your timezone!

		var deadline_time_stamp = myDate.getTime()/1000.0;


			var data = {deadline_time_stamp: deadline_time_stamp, save_time_stamp: save_time_stamp, assignment_id: id, author_name: author_name, author_id: author_id, author_pic: author_pic, marks: marks};

			console.log(option);

			if(option=="save"){
				$.ajax({
					type: "POST",
					url: MY_CONSTANT+"/controller/assignmentController.php",
					data: {save: data},
					success: function(data){
						console.log(data);
						$('#assignment_view').empty();	
						$('#assignment_view').append(data); 
						saveMarks();
						// tableTools();
					}
				});
			}

			if(option=="publish"){
				console.log(data);
				$.ajax({
					type: "POST",
					url: MY_CONSTANT+"/controller/assignmentController.php",
					data: {publish: data},
					success: function(data){
						console.log(data);
						// $('#assignment_view').empty();	
						// $('#assignment_view').append(data); 
						// saveMarks();
						// tableTools();
					}
				});
			}


	});
	//
}



function submitAsStudent(data){
	$.ajax({
		type: "POST",
		url: MY_CONSTANT+"gradpower/controller/assignmentController.php",
		data: {student_submit: data},
		success: function(data){
			$('#randomAssignmentTask').empty();	//	Empty Random right side pane and change dynamically
			$('#randomAssignmentTask').append(data); // append to that empty right pane

			openAssignment();

		}
	});
}

function openAssignment(){

	$('.save_student_marks').click(function(){

		var marks = new Array();
		
		$(".editMark").each(function(index){

			var mark = $(this).val();

			var student_id = $(this).attr('data-studentId');

			//var assignment_id = $(this).attr('data-assignmentId');

			var comments = $('#comment_'+student_id).val();

			marks[index] = {student_id: student_id, mark: mark, comments: comments};

			//alert("mark===>>>"+ mark+", student_id===>>>"+ student_id+"COMMENT===>>>"+ comment+", assignment_id===>>>"+ assignment_id);

		});



		var assignment_id = $(this).attr('data-id');

		var deadline = $(this).attr('data-date');

			var myDate = new Date(deadline); // Your timezone!

			var deadline_time_stamp = myDate.getTime()/1000.0;
			
			var save_time_stamp = epochDate();

		var data = {assignment_id: assignment_id, deadline: deadline, deadline_time_stamp: deadline_time_stamp, save_time_stamp: save_time_stamp, marks: marks};

		$.ajax({
			type: "POST",
			url: MY_CONSTANT+"gradpower/controller/assignmentController.php",
			data: {save: data},
			success: function(data){
				//alert(data);
				//$('#overviewAssignment').append(data);

				$('#assignment_view').empty();
				$('#assignment_view').append(data);

				selectIndividual();
			}
		});

		//alert(data);

	});


	$('.publish_student_marks').click(function(){
		$(".editMark").each(function(index){
			var mark = $(this).val();
			if(mark==""){
				alert('Please Check there are marks undefined');
			}
		});
	});


	fileHandler();
}





fileHandler();

function fileHandler(){

	var settings = {
	    url: MY_CONSTANT+"/upload.php",
	    dragDrop:true,
	    fileName: "myfile",
	    multiple:false,
	    allowedTypes:"pdf",	
	    returnType:"json",
	    maxFileCount:1,
		onSuccess:function(files,data,xhr)
	    {
			alert(data);
	    },
	    showDelete:true,
	    deleteCallback: function(data,pd)
		{
		    for(var i=0;i<data.length;i++)
		    {
		        $.post(MY_CONSTANT+"/upload.php",{op:"delete",name:data[i]},
		        function(resp, textStatus, jqXHR)
		        {
		            //Show Message  
		            $("#mulitplefileuploadStatus").append("<div>File Deleted</div>");      
		        });
		    }      
		    pd.statusbar.hide(); //You choice to hide/not.
		}
	}

	var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

}





























function tableTools(){
	if ($('#DataTables_Table_0_wrapper').length)
        return;

	function fnInitCompleteCallback(that)
	{
		var p = that.parents('.dataTables_wrapper').first();
    	var l = p.find('.row').find('label');

    	l.each(function(index, el) {
    		var iw = $("<div>").addClass('col-md-8').appendTo($(el).parent());
    		$(el).parent().addClass('form-group margin-none').parent().addClass('form-horizontal');
            $(el).find('input, select').addClass('form-control').removeAttr('size').appendTo(iw);
            $(el).addClass('col-md-4 control-label');
    	});

    	var s = p.find('select');
    	s.addClass('.selectpicker').selectpicker();
	}

	/* DataTables */
	if ($('.dynamicTable').size() > 0)
	{
		$('.dynamicTable').each(function()
		{
			// DataTables with TableTools
			if ($(this).is('.tableTools'))
			{
				$(this).dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
					"oLanguage": {
						"sLengthMenu": "_MENU_ Show"
					},
					"oTableTools": {
						"sSwfPath": "http://localhost/grad/assets/plugins/tables_datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
						"aButtons": [ "copy", "csv", "print" ]
				    },
				    "aoColumnDefs": [
			          { 'bSortable': false, 'aTargets': [ 0 ] }
			       	],
			       	"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
				    "fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
			        }
				});
			}
			// colVis extras initialization
			else if ($(this).is('.colVis'))
			{
				$(this).dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row separator bottom'<'col-md-3'f><'col-md-3'l><'col-md-6'C>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
					"oLanguage": {
						"sLengthMenu": "_MENU_ per page"
					},
					"oColVis": {
						"buttonText": "Show / Hide Columns",
						"sAlign": "right"
					},
					"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
					"fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
			        }
				});
			}
			else if ($(this).is('.scrollVertical'))
			{
				$(this).dataTable({
					"bPaginate": false,
					"sScrollY": "200px",
					"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
			       	"sDom": "<'row separator bottom'<'col-md-12'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			       	"fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
			        }
				});
			}
			else if ($(this).is('.ajax'))
			{
				$(this).dataTable({
					"sPaginationType": "bootstrap",
					"bProcessing": true,
					"sAjaxSource": rootPath + 'admin/ajax/DataTables.json',
			       	"sDom": "<'row separator bottom'<'col-md-12'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			       	"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
			       	"fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
			        }
				});
			}
			else if ($(this).is('.fixedHeaderColReorder'))
			{
				$(this).dataTable({
					"sPaginationType": "bootstrap",
			       	"sDom": "R<'clear'><'row separator bottom'<'col-md-12'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			       	"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
			       	"fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
				    	var t = this;
				    	setTimeout(function(){
				    		new FixedHeader( t );
				    	}, 1000);
			        }
				});
			}
			// default initialization
			else
			{
				$(this).dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
					"sScrollX": "100%",
			       	"sScrollXInner": "100%",
			        "bScrollCollapse": true,
					"oLanguage": {
						"sLengthMenu": "_MENU_ per page"
					},
					"fnInitComplete": function () {
				    	fnInitCompleteCallback(this);
			        }
				});
			}
		});
	}
}