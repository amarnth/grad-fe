$(".contacts-search").keyup(function(){
	var searchText = $(".contacts-search").val();
	$(".contacts").each(function(index){
		if($(this).attr('data-s')){
			if(!$(this).attr('data-s').match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
		}
	});
});