


$(".students-card").fadeOut("fast");

$(".student-search").keyup(function(){
	var searchText = $(this).val();
	$(".students-card").each(function(index){
		if($(this).find("h4 .student-name")){

			if(!$(this).text().match(new RegExp(searchText, "i"))){
				$(this).fadeOut("fast");
			}else{
				$(this).fadeIn("slow");
			}
			
		}
	});
});	
