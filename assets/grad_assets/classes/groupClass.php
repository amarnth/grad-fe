<?php

class Groups extends Grad{

	function getInfo($id, $recall=false){

		// echo $id;

		//echo $id."<br>";

		// if(!$recall){
		//	$decipher = new Decipher();
			
		//	$id = $decipher->base64_decrypt($id);
		//}
		
		// echo $id;
		
		$decipher = new Decipher();
			
			$id = $decipher->base64_decrypt($id);
			

		//echo $id;
		

		//$json = file_get_contents('http://gradpowered.net/assets/grad_assets/js/json/group_info.json');
		
		$json = file_get_contents($this->server_path.'/get/group/info/'.$id);
		
		$data = json_decode($json);

			$group_data = $data->Group_Info;


			$group_type = $group_data->group_type;

			$group_pic = $group_data->group_pic;

			$created_on = $group_data->created_on;

			
			$group_type = $group_data->group_type;

			$group_desc = $group_data->group_description;

			
			// $time_stamp = $group_data->time_stamp;

				// $time = time(); // current Unix timestamp

			// $diff = Epoch::time_diff($time, $time_stamp);

				$pic = "assets/grad_assets/".$group_pic;

			$admin = $group_data->admin;

			$members = $group_data->members;

			$members_count = count($members);

			$admin_count = count($admin);

				
			//$requests = $v->Requests;
	
				// $request_count = count($counter);

			?>
				<div class="col-md-6 col-sm-12">
					<img src="<?php echo $pic; ?>" alt="photo" class="img-responsive" style="height: 250px;">
					<?php
						$admin_pic = $this->path."/grad_assets/".$admin->admin_pic;
					?>
				<img style="  position: absolute; bottom: 8px; height: 60px; width: 60px; right: 8px;" data-toggle="image-preview" data-title="<?php echo $value->name; ?>" data-image-preview="<?php echo $admin_pic; ?>" href="" src="<?php echo $admin_pic; ?>" alt="photo" class="img-responsive">
						
				</div>

                <div class="col-md-3 col-sm-12 ">
			<div class="media box-generic" style="min-height: 250px;">
						<!-- FRIENDS START -->
	                    <h4 class="border-bottom margin-none innerAll">
	                        Group Members <span class="badge badge-primary badge-stroke pull-right"><?php echo $members_count; ?></span>
	                    </h4>
	                    <div class="innerAll text-center">
	                        <?php
					foreach($members as $key => $value) {
						$admin_pic = $this->path."/grad_assets/".$value->pic;
						?>
							<img width="35" height="35" data-toggle="image-preview" data-title="<?php echo $value->name; ?>" data-image-preview="<?php echo $admin_pic; ?>" href="" src="<?php echo $admin_pic; ?>" alt="photo">
						<?php
					}
				?>
	                    </div>
	                    <!-- // END FRIENDS -->
                    </div>
                    
	        </div>

                <div class="col-md-3 col-sm-12">
	                <div class="media box-generic" style="min-height: 250px;">
	                	<h4>Group Description</h4>
						<p style="padding: 30px;"><?php echo $group_desc; ?></p>
					</div>
				</div>
				
				


            <!-- col -->
            <div class="col-lg-10 col-md-9 col-sm-12">
                <!-- col-separator -->
                <div class="col-separator col-unscrollable">
                    <!-- col-table -->
                    <div class="col-table">
                        <!-- box -->
                        <div class="row row-merge box margin-none">
                            
                            <!-- col -->
				            <div class="col-md-6 col-sm-12 reset-components">
				                <div class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none widget-activity-social">
				                    <!-- Tabs Heading -->
				                    <div class="widget-head">
				                        <ul>
				                            <li class="active">
				                                <a data-toggle="tab" href="#shareTab" class="glyphicons user"><i></i></a>
				                            </li>
				                            <!-- <li>
				                                <a data-toggle="tab" href="#webcamTab" class="glyphicons webcam"><i></i></a>
				                            </li> -->
				                            <li>
				                                <a data-toggle="tab" href="#attachmentTab" class="glyphicons link"><i></i></a>
				                            </li>
				                           <!--  <li>
				                                <a data-toggle="tab" href="#linkTab" class="glyphicons link"><i></i></a>
				                            </li> -->
				                        </ul>
				                    </div>
				                    <!-- // Tabs Heading END -->
				                    <div class="widget-body padding-none">
				                        <div class="tab-content">
				                            <div class="tab-pane active" id="shareTab">
				                                <div class="share">
				                                    <textarea id="textareaWall" class="form-control wall_message" rows="2" placeholder="Share what you've been up to..."></textarea>
				                                    <a class="btn btn-primary btn-sm group_post" data-id="<?php echo $id; ?>">Submit <i class="fa fa-arrow-circle-o-right"></i></a>
				                                </div>
				                            </div>
				                           
				                            <div class="tab-pane innerAll" id="attachmentTab">
				                                <div class="fileupload fileupload-new margin-none" data-provides="fileupload">

				                                    <div class="btn btn-default btn-file" id="mulitplefileuploader">Select</div>
				                                    <div id="newPostPreview"></div>
				                                    <div id="mulitplefileuploadStatus"></div>

				                                    <!-- <span class="btn btn-default btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
				                                    <input type="file" name="files" class="margin-none" /></span>
				                                    <span class="fileupload-preview"></span>
				                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a> -->
				                                </div>
				                                <div class="share">
				                                    <textarea id="file_post_wall" class="form-control" rows="2" placeholder="Share what you've been up to..."></textarea>
				                                    <a class="btn btn-primary btn-sm post_wall_file" data-id="<?php echo $id; ?>">Submit <i class="fa fa-arrow-circle-o-right"></i></a>
				                                </div>
				                            </div>
				                            
				                        </div>
				                    </div>
				                </div>
				            </div>
				            <!-- // END col -->
				            

		<?php
	}


	function filterGroups($data){

		?>

		<div class="input-group friends-search">
			<input class="form-control col-md-6 innerAll groups-search" id="appendedInputButtons" type="text" placeholder="Search">
			<div class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
			</div>
		</div>
			

		    
	<?php 

			foreach($data as $k => $v){

				$group_id = $v->group_id;
				
				$group_name = $v->group_name;
				
				$group_desc = $v->group_desc;

				$group_picture = $v->group_picture;
				
				$group_admin_id = $v->group_admin_id;
				
				$group_admin_name = $v->group_admin_name;

				$admin_pic = $v->admin_pic;

				$group_members = $v->group_members;
				
				$time_stamp = $v->time_stamp;

				$time = time(); // current Unix timestamp
	
				$diff = Epoch::time_diff($time, $time_stamp);

				$created = $v->created;

				//$members = explode(",", $group_members);

				$members_count = count($group_members);

				$group_picture = $this->path."/grad_assets/".$group_picture;

				$admin_pic = $this->path."/grad_assets/".$admin_pic;
				
				//				$admin_pic = $this->path."/grad_assets/".$group_admin_pic;
								
				

			?>
		 	<!-- col -->
		    <div class="col-lg-3 col-sm-4 col-md-4 groups-list" style="margin-top: 20px;">
		        <!-- col-separator.box -->
		        <div class="col-separator col-separator-last-sm">
		            <div class="relativeWrap">
		                <img style="height: 200px;" src="<?php echo $group_picture; ?>" alt="<?php echo $group_name; ?>" class="img-responsive padding-none border-none" />
		                <div class="fixed-bottom bg-inverse-faded">
		                    <div class="media margin-none innerAll">
		                        <a href="" class="pull-left">
		                            <img width="30" src="<?php echo $admin_pic; ?>" alt="<?php echo $group_admin_name; ?>" />
		                        </a>
		                        <div class="media-body text-white">
		                            by <strong class='group_admin_name'><?php echo $group_admin_name; ?></strong> 
		                            <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i><?php echo $diff; ?></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="innerAll inner-2x">
		            
		                <h4 class="group_name" data-id="<?php echo $group_id; ?>">
		                	<?php
		                		$encrypt = new Decipher();
                            			$encrypt_id = $encrypt->base64_encrypt($group_id);
		                	?>
		                    <a href="index.php?page=group_wall&gid=<?php echo $encrypt_id; ?>"><?php echo $group_name; ?></a>
		                </h4>
		                
		                <p class="margin-none">
		                	<?php 
			                	$string = strip_tags($group_desc);

								if (strlen($string) > 200) {

								    // truncate string
								    $stringCut = substr($string, 0, 200);

								    // make sure it ends in a word so assassinate doesn't become ass...
								    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="" class="text-primary">read more</a>'; 
								}
								echo $string;

		                		// echo $group_desc; 
		                	?>
		                </p>

		                
		            </div>
		            <div class="col-separator-h box"></div>
		        </div>
		        <!-- // END col-separator.box -->
		    </div>
		    <!-- // END col -->

			<?php
		}
	}




	function getGroups(){
		
		$user_id = $this->user_id;
		$json = file_get_contents($this->server_path.'/get/groups/info/'.$user_id);
		//$json = file_get_contents('http://gradpowered.net/assets/grad_assets/js/json/groupdata.json');

		$data = json_decode($json);
	
		$groups_data = $data->Group;

		// var_dump($data);

		$my_group = $groups_data->my_group;
		$friends = $groups_data->Friends;
		$suggested = $groups_data->Suggested;
		$department = $groups_data->Department;
		$pending = $groups_data->Pending;


		?>

			<div class="tab-content">
                <!-- Tab content -->
                <div class="tab-pane active" id="tab1-4">
                    <h4>
                        <!-- First tab -->
                    </h4>

					<?php self::filterGroups($my_group); ?>
                </div>
                <!-- // Tab content END -->
                <!-- Tab content -->
                <div class="tab-pane" id="tab2-4">
                    <h4>
                        <!-- Second tab -->
                    </h4>
                    <?php self::filterGroups($friends); ?>
                </div>
                <!-- // Tab content END -->
                <!-- Tab content -->
                <div class="tab-pane" id="tab3-4">
                    <h4>
                        <!-- Third tab -->
                    </h4>
                    <?php self::filterGroups($suggested); ?>
                </div>
                <!-- // Tab content END -->
                <!-- Tab content -->
                <div class="tab-pane" id="tab4-4">
                    <h4>
                        <!-- Fourth tab -->
                    </h4>
                    <?php self::filterGroups($pending); ?>
                </div>
                <!-- // Tab content END -->
            </div>

		<?php
	}




}

?>