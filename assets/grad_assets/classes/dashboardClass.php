<?php
/**
* 
*/
class Stats extends Grad
{
	
	function getOverview(){
	
		// $json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');
		
		
		$json = file_get_contents($this->server_path.'/get/dashboard/'.$this->user_id);

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$stats = $dashboard->stats;

			self::getAssignments($stats);

            echo '<div class="col-md-4">';
			 self::getProjects($stats);
            echo '</div>';
	}

    function getStudentOverview(){

        $json = file_get_contents($this->server_path.'/get/student/details/'.$this->user_id);

        $data = json_decode($json);

        $user = $data->student_overview;

        $projects = $user->projects;

        $assignments = $user->assignments;

        ?>
        <div class="col-md-4">
            <!-- START CV -->
            <div class="box-generic col-md-12">
                <div class="media innerAll">
                    <h4 style="margin: 10px 0px; text-align: center;">Assignments</h4>
                </div>

                <div class="bg-gray">
                    <ul class="list-unstyled">
                    	<li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $assignments->total_assignments; ?></span> <i class="fa fa-fw fa-dashboard"></i> Total Assignments </li>
                    
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $assignments->active_assignments; ?></span> <i class="fa fa-fw fa-dashboard"></i> Active Assignments </li>
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $assignments->submitted_assignments; ?></span> <i class="fa fa-fw icon-browser-check"></i> Submitted Assignments </li>
                        <!-- <li class="innerAll border-bottom"> <span class="badge badge-default pull-right">3</span> <i class="fa fa-fw fa-file-text-o"></i> Incomplete Assignments</li> -->
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $assignments->pending_assignments; ?></span> <i class="fa fa-fw fa-file-text-o"></i> Pending Assignments </li>
                        <!-- <li class="innerAll bg-white text-center"> <a href="" class="btn btn-info btn-sm">View Profile</a> </li> -->
                    </ul>
                </div>


               

                    <div class="col-md-12" style="padding: 15px;">
                        <div class="col-md-4 text-center">
                            <h5>
                                Min
                            </h5>
                            <div data-percent="<?php echo $assignments->min; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignments->min; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Average
                            </h5>
                            <?php 
                                $range = explode("-", $assignments->frequent_range);
                                $range = $range[0];

                            ?>
                            <div data-percent="<?php echo $range; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 11px;"><?php echo $assignments->frequent_range; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Max
                            </h5>
                            <div data-percent="<?php echo $assignments->max; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignments->max; ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

            </div>
            <!-- //END CV -->

            <div class="box-generic col-md-12 padding-none">
                <?php 
                    $notice = new Noticeboard();
                    $notice->getSlide($user);
                ?>
            </div>
        </div>

        <?php

        $student_obj = new Stats();
        $student_obj->studentCenterPane($user);
        $student_obj->studentRightPane($user);
    }

    function getAdminOverview(){
        $json = file_get_contents($this->path.'/grad_assets/js/json/dashboard_admin.json');

        $data = json_decode($json);

        $dashboard = $data->Dashboard;

        ?>
        <!-- col -->
        <div class="col-lg-4 col-md-6">
            <!-- col-separator.box -->
            <div class="col-separator col-separator-first box">
             
                <div class="innerAll">

                    <!-- BALLANCE START -->
                    <div class="box-generic bg-primary-light padding-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            Over All Population
                        </h5>
                        <div class="innerAll">
                            <div class="media">
                                <!-- <div class="pull-left">
                                    <div class="innerAll">
                                        <div data-percent="20" data-size="70" class="easy-pie primary" data-scale-color="false" data-track-color="#ffffff"><span class="value text-primary">20%</span></div>
                                    </div>
                                </div> -->
                                <div class="media-body">
                                    <div class="text-large text-regular text-center"><i class="fa fa-user"></i> <?php echo number_format($dashboard->population); ?></div>
                                    <!-- <p class="lead"><span class="text-primary">Current balance</span> this billing cycle</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // END BALLANCE -->


                    <!-- MALE START -->
                    <div class="box-generic padding-none margin-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            <i class="fa fa-bar-chart-o text-primary pull-right"></i> Male
                        </h5>
                        <div class="innerAll">
                            <i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i><i class="fa fa-male fa-fw text-male-light"></i> 
                            <div class="innerAll text-center">
                                <?php
                                    $overall_percentage_male = $dashboard->male/$dashboard->population;
                                    $overall_percentage_male = round($overall_percentage_male*100);

                                    $overall_percentage_female = $dashboard->female/$dashboard->population;
                                    $overall_percentage_female = round($overall_percentage_female*100);
                                ?>
                                <p class="lead"><span class="text-large text-regular"> <?php echo number_format($dashboard->male); ?> </span> (<?php echo $overall_percentage_male; ?>%)</p>
                                <!-- <p class="margin-none">of your male customers, living <span class="strong">nationwide</span>, aged <span class="strong">25-65</span>, who used to smoke,</p>
                                <p class="margin-none">suffer from <span class="strong text-male">lung cancer</span></p> -->
                            </div>
                        </div>
                    </div>
                    <!-- // END MALE -->


                </div>
                <div class="col-separator-h box"></div>
                <div class="innerAll">
                    <!-- FEMALE START -->
                    <div class="box-generic padding-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            <i class="fa fa-bar-chart-o text-primary pull-right"></i> Female
                        </h5>
                        <div class="innerAll">
                            <i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i><i class="fa fa-female fa-fw text-female-light"></i> 
                            <div class="innerAll text-center">
                                <p class="lead"><span class="text-large text-regular"> <?php echo number_format($dashboard->female); ?> </span> (<?php echo $overall_percentage_female; ?>%)</p>
                                <!-- <p class="margin-none">of your female customers, living <span class="strong">nationwide</span>, aged <span class="strong">25-35</span>, who used to smoke,</p>
                                <p class="margin-none">suffer from <span class="strong text-female">Hypertension</span></p> -->
                            </div>
                        </div>
                    </div>
                    <!-- // END FEMALE -->
                </div>


                <div class="col-separator-h box"></div>
            </div>
            <!-- // END col-separator.box -->
        </div>
        <!-- // END col -->

        <!-- col -->
        <div class="col-lg-4 col-md-6">
            <!-- col-separator.box -->
            <div class="col-separator box">
                <div class="innerAll">

                    <div class="box-generic bg-primary-light padding-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            Students
                        </h5>
                        <div class="innerAll">
                            <div class="media">
                                <div class="media-body">
                                    <div class="text-large text-regular text-center"><i class="fa fa-user"></i> <?php $student = $dashboard->students; echo number_format($student->no_of_students); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- GENDER START -->
                    <div class="box-generic padding-none margin-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            <i class="fa fa-group text-primary pull-right"></i> By Gender
                        </h5>
                        <div class="innerAll">
                            <div class="innerAll text-center">
                                <?php
                                    $student_percentage_male = $student->boys/$student->no_of_students;
                                    $student_percentage_male = round($student_percentage_male*100);

                                    $student_percentage_female = $student->girls/$student->no_of_students;
                                    $student_percentage_female = round($student_percentage_female*100);
                                ?>
                                <div data-percent="<?php echo $student_percentage_male; ?>" data-size="200" class="easy-pie inline-block easy-pie-gender" data-scale-color="false" data-track-color="#D67FB0" data-bar-color="#4193d0" data-line-width="8" data-animate="false">
                                    <div class="value text-center innerAll">
                                        <!-- <p class="lead">Your customers</p> -->
                                        <p class="sublead"><i class="fa fa-male"></i> Male: <strong class="male"><?php echo $student_percentage_male; ?>%</strong></p>
                                        <p class="sublead"><i class="fa fa-female"></i> Female: <strong class="female"><?php echo $student_percentage_female; ?>%</strong></p>
                                    </div>
                                </div>
                                <div class="separator"></div>

                            </div>
                        </div>
                    </div>
                    <!-- // END GENDER -->



                </div>
                <div class="col-separator-h box"></div>
                <div class="innerAll">


                    <!-- COMPARISON START -->
                    <div class="box-generic padding-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            <i class="fa fa-bar-chart-o text-primary pull-right"></i> Comparison
                        </h5>
                        <div class="innerAll">
                            <div class="innerAll text-center">
                                <p class="lead"><span class="text-large text-regular">1,210</span> of <span class="text-primary">active users</span></p>
                                <div class="progress progress-mini">
                                    <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                </div>
                                <!-- <p class="margin-none">living in <span class="strong">Bucharest</span>, aged <span class="strong">45+</span></p>
                                <p class="margin-none">suffer from a <span class="strong text-primary">deadly disease</span></p> -->
                            </div>
                        </div>
                    </div>
                    <!-- // END COMPARISON -->

                </div>
            </div>
            <!-- // END col-separator.box -->
        </div>
        <!-- // END col -->


        <!-- col -->
        <div class="col-lg-4 col-md-12">
            <!-- col-separator.box -->
            <div class="col-separator box">
                <div class="innerAll">


                    <div class="box-generic bg-primary-light padding-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            Staff
                        </h5>
                        <div class="innerAll">
                            <div class="media">
                                <div class="media-body">
                                    <div class="text-large text-regular text-center"><i class="fa fa-user"></i> <?php $staff = $dashboard->staff; echo number_format($staff->no_of_staffs); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- GENDER START -->
                    <div class="box-generic padding-none margin-none">
                        <h5 class="strong border-bottom innerAll margin-none">
                            <i class="fa fa-group text-primary pull-right"></i> By Gender
                        </h5>
                        <div class="innerAll">
                            <div class="innerAll text-center">
                                <?php 
                                    $staff_percentage_staff_male = $staff->male/$staff->no_of_staffs;
                                    $staff_percentage_staff_male = round($staff_percentage_staff_male*100); 

                                    $staff_percentage_staff_female = $staff->female/$staff->no_of_staffs;
                                    $staff_percentage_staff_female = round($staff_percentage_staff_female*100);
                                ?>
                                <div data-percent="<?php echo $staff_percentage_staff_male; ?>" data-size="200" class="easy-pie inline-block easy-pie-gender" data-scale-color="false" data-track-color="#D67FB0" data-bar-color="#4193d0" data-line-width="8" data-animate="false">
                                    <div class="value text-center innerAll">
                                        <!-- <p class="lead">Your customers</p> -->
                                        <p class="sublead"><i class="fa fa-male"></i> Male: <strong class="male"><?php echo $staff_percentage_staff_male; ?>%</strong></p>
                                        <p class="sublead"><i class="fa fa-female"></i> Female: <strong class="female"><?php echo $staff_percentage_staff_female; ?>%</strong></p>
                                    </div>
                                </div>
                                <div class="separator"></div>

                            </div>
                        </div>
                    </div>
                    <!-- // END GENDER -->

                </div>
            </div>
            <!-- // END col-separator.box -->
        </div>
        <!-- // END col -->


        <?php
    }



	function getClass() {

		$json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$class = $dashboard->class;

		?>

		<!-- Tabs -->
        <div class="relativeWrap" >
            <div class="widget widget-tabs widget-tabs-responsive">
                <!-- Tabs Heading -->
                <div class="widget-head">
                    <ul>

                    <?php
                    	$i = 0; 
                    	foreach ($class as $key => $value) {
                    		if($i==0){
						?>
							<li class="active">
	                            <a class="glyphicons coffe_cup" href="#<?php echo $value->id; ?>" data-toggle="tab"><i></i><?php echo $value->name; ?></a>
	                        </li>
						<?php
							} else {
						?>
							<li class="">
	                            <a class="" href="#<?php echo $value->id; ?>" data-toggle="tab"><i></i><?php echo $value->name; ?></a>
	                        </li>
						<?php
							}
							$i++;
						}
                    ?>
                        
                    </ul>
                </div>
                <!-- // Tabs Heading END -->



                <div class="widget-body">
                    <div class="tab-content">


                    	<?php
	                    	$i = 0; 
	                    	foreach ($class as $key => $value) {
	                    		if($i==0){
								?>
									<!-- Tab content -->
			                        <div class="tab-pane active" id="<?php echo $value->id; ?>">
								<?php
									} else {
								?>
									<!-- Tab content -->
			                        <div class="tab-pane" id="<?php echo $value->id; ?>">
								<?php
									}

									$stats = $value->stats;

									
								?>

									<div class="row">
					                    <?php
					                    	self::getAssignments($stats);
					                    	echo '<div class="col-md-4">';
                                                self::getProjects($stats);
                                            echo '</div>';
					                    ?>
					                </div>

		                        </div>
		                        <!-- // Tab content END -->
		                    <?php
			                    $i++;
							}
		                ?>
                      
                    </div>
                </div>
            </div>
        </div>
        <!-- // Tabs END -->

		<?php
	}




	function getStudent() {

		$json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$students = $dashboard->students;

			// var_dump($students);

			foreach ($students as $key => $value) {

				$pic = $this->path.'/grad_assets/'.$value->pic;

			if($value->pinned){
				echo '<div class="col-md-4">';
			}else{
				echo '<div class="col-md-4 students-card">';
			}
		?>
		 
            <!-- START CV -->
            <div class="box-generic col-md-12">
                <div class="media innerAll">
                    <a href="" class="pull-right">
                    	<?php
                    		if($value->pinned){
								echo '<i class="fa fa-fw icon-dropped-pin text-primary" style="font-size: 18px; margin: 8px -7px;"></i>';
							}else{
								echo '<i class="fa fa-fw icon-dropped-pin" style="color:#3a3a3a; font-size: 18px; margin: 8px -7px;"></i>';
							}
                    	?>
                    </a>
                    <a href="" class="pull-left">
                        <img src="<?php echo $pic; ?>" width="60" alt="">
                    </a>
                    <div class="media-body innerL half">
                        <h4 class="margin-none">
                            <a href="" class="text-primary student-name"><?php echo $value->name; ?></a>
                        </h4>
                        <p class="strong"><?php echo $value->year; ?> Year</p>
                        <div class="bg-gray">
                            <p class="margin-none innerAll"><!-- <a href="">4 members</a> --> <i class="fa fa-fw fa-code-fork fa-rotate-90"></i> <a href="index.php?page=projects"><?php echo $value->total_projects; ?> Projects</a></p>
                        </div>
                    </div>
                </div>


                <h4 class="innerAll margin-none border-bottom">
                    Skills
                </h4>
                <div class="innerAll">
                    <div class="innerAll">
                    <?php
                		foreach ($value->skills as $k => $v) {
                	?>
                        <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                            <div class="count"><?php echo $v; ?>%</div>
                            <div class="progress-bar progress-bar-primary" style="width: <?php echo $v; ?>%;"></div>
                            <div class="add"><?php echo $k; ?></div>
                        </div>
                    <?php
                     	}
                	?>
                    </div>
                </div>


                <div class="bg-gray">
                    <ul class="list-unstyled">
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->active_projects; ?></span> <i class="fa fa-fw fa-dashboard"></i> Active Projects </li>
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->completed_assignments; ?></span> <i class="fa fa-fw icon-browser-check"></i> Completed Assignments </li>
                        <!-- <li class="innerAll border-bottom"> <span class="badge badge-default pull-right">3</span> <i class="fa fa-fw fa-file-text-o"></i> Incomplete Assignments</li> -->
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->pending_assignments; ?></span> <i class="fa fa-fw fa-file-text-o"></i> Pending Assignments </li>
                        <!-- <li class="innerAll bg-white text-center"> <a href="" class="btn btn-info btn-sm">View Profile</a> </li> -->
                    </ul>
                </div>


                <div class="col-md-12 border-bottom" style="padding: 15px;">
                    <div class="col-md-6 text-center">
                        <h5>
                            Attendance
                        </h5>
                        <div data-percent="<?php echo $value->attendance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $value->attendance; ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 text-center">
                        <h5>
                            Performance
                        </h5>
                        <div data-percent="<?php echo $value->performance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $value->performance; ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-md-12" style="padding: 15px;">
                        <h4 style="margin-bottom: 15px;" class="text-center">Assignments</h4>

                        <?php $assignment = $value->assignment; ?>

                        <div class="col-md-4 text-center">
                            <h5>
                                Min
                            </h5>
                            <div data-percent="<?php echo $assignment->min; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignment->min; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Average
                            </h5>
                            <?php 
                            	$range = explode("-", $assignment->frequent_range);
                            	$range = $range[0];

                            ?>
                            <div data-percent="<?php echo $range; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 11px;"><?php echo $assignment->frequent_range; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Max
                            </h5>
                            <div data-percent="<?php echo $assignment->max; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignment->max; ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

            </div>
            <!-- //END CV -->
        </div>

		<?php
		}
	}


	function getProjects($data) {

		$projects = $data->projects;

		?>
        <div class="box-generic padding-none overflow-hidden">
            <h4 class="heading innerAll inner-2x text-center">
                Live Projects
            </h4>

            <?php
        	foreach ($projects->live_projects as $key => $value) {
        		# code...
        	?>
                <div class="innerAll border-bottom">
                    <div class="media innerAll half">
                       <a href="" class="pull-left innerAll bg-lightred"><i class="fa fa-fw icon-briefcase-1"></i></a>
                        <div class="media-body">
                            <h5 class="margin-none">
                                <?php echo $value->project_name; ?>
                            </h5>
                            <?php
                                $encrypt = new Decipher();
                                $encrypt_id = $encrypt->base64_encrypt($value->project_id);
                            ?>
                            <p class="text-small"><a href="index.php?page=project_view&pid=<?php echo $encrypt_id; ?>" class="text-underline">View Project Activity</a></p>
                            	<?php
                            		$d = $value->completed_activity/$value->total_activity;
                            		$d = round($d*100);
                            	?>
                            <p class="text-small margin-none innerB half"><?php echo $d; ?>% complete</p>
                            <div class="progress bg-gray progress-mini margin-none">
                                <div class="progress-bar progress-bar-success" style="width: <?php echo $d; ?>%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        	?>
        </div>
		<?php
	}



	function getAssignments($data) {

		$filter = 5;

		$assignments = $data->Assignments;

		$get = "top".$filter;

		$active_assignments = $assignments->$get;

		// var_dump($active_assignments);

		?>

		<div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <!-- OVERALL PERFORMANCE START -->
                    <div class="box-generic innerAll inner-2x text-center" style="height: 179px;">
                        <h4>
                            Total Assignments
                        </h4>
                        <p class="innerTB inner-2x text-xlarge text-condensed strong text-primary">+<?php echo $assignments->total; ?></p>
                        <!-- <div class="sparkline" sparkHeight="62"></div> -->
                    </div>
                    <!-- // END OVERALL PERFORMANCE -->
                </div>
                <div class="col-md-6">
                    <!-- REPORTS START -->
                    <div class="box-generic innerAll inner-2x text-center">
                        <h4>
                            Availability
                        </h4>
                        <div data-percent="65" data-size="114" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 32px;"><?php echo $data->Availability; ?></span>
                            </div>
                        </div>
                    </div>
               
                    <!-- // END REPORTS -->
                </div>
            </div>
           
            <div class="widget">
                <div class="widget-head">
                    <h4 class="heading">
                        Active Assignments
                        <span><?php echo $assignments->active; ?></span>
                    </h4>
                </div>
                <div class="widget-body innerAll inner-2x">
                    <table class="table table-striped margin-none">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="text-center">Deadline</th>
                                <th class="text-right" style="width: 100px;">Progress</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php
                        	$i = 1;
                        	foreach ($active_assignments as $key => $value) {
                        		# code...

                        		$d = $value->submitted/$value->assigned;

                        		$progress = round($d*100);

                        ?>

                            <tr>
                                <td><strong><?php echo $i++; ?>.</strong> <?php echo $value->name; ?></td>
                                <td class="text-center"><?php echo $value->deadline; ?></td>
                                <td class="text-right">
                                    <p class="text-small margin-none innerB half"><?php echo $progress; ?>% complete</p>
                                    <div class="progress bg-gray progress-mini margin-none">
                                        <div class="progress-bar progress-bar-success" style="width: <?php echo $progress; ?>px;"></div>
                                    </div>
                                    <!-- <div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#eb6a5a" sparkFillColor="" data-data="[226,168,116,296,365,403,281,104,419,385]"></div> -->
                                </td>
                            </tr>

                        <?php
                        	}
                    	?>

                        </tbody>
                    </table>
                </div>
                <!-- // END TRENDS -->
            </div>
        </div>


		<?php
	}

    function studentCenterPane($user){
        ?>
        <div class="col-md-4">

            <div class="box-generic col-md-12">

                 <div class="col-md-12 border-bottom" style="padding: 15px;">
                    <div class="col-md-6 text-center">
                        <h5>
                            Attendance
                        </h5>
                        <div data-percent="<?php echo $user->attendance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $user->attendance; ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 text-center">
                        <h5>
                            Performance
                        </h5>
                        <div data-percent="<?php echo $user->performance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $user->performance; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="box-generic col-md-12">
                <div class="media innerAll">
                    <h4 style="margin: 10px 0px; text-align: center;">Projects</h4>
                    <?php $projects = $user->projects; ?>
                    <div class="media-body innerL half">
                        <div class="bg-gray">
                            <p class="margin-none innerAll"><!-- <a href="">4 members</a> --> <i class="fa fa-fw fa-code-fork fa-rotate-90"></i> <a href="index.php?page=projects"><?php echo $projects->total_projects; ?> Projects</a></p>
                        </div>
                    </div>
                </div>

                <div class="bg-gray">
                    <ul class="list-unstyled">
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $projects->active_projects; ?></span> <i class="fa fa-fw fa-dashboard"></i> Active Projects </li>
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $projects->completed_projects; ?></span> <i class="fa fa-fw fa-dashboard"></i> Completed Projects </li>
                    </ul>
                </div>
            </div>

            <?php
                $get = new Stats();
                $get->getProjects($user);
            ?>

        </div>
        <?php
    }

    function studentRightPane($user){
        ?>
        <div class="col-md-4">
            <div class="box-generic col-md-12">
                <!-- DISCUSSIONS START -->
                <div class="col-md-12 padding-none">
                    <h4 style="margin: 10px 0px; text-align: center;">Group Activity</h4>
                    <!-- // Collapsible Widget END -->
                </div>

                <?php
                    $get = new Stats();
                    $get->groupActivity($user);
                ?>
            </div>

            <div class="box-generic col-md-12">
                <!-- DISCUSSIONS START -->
                <div class="col-md-12 padding-none">
                    <h4 style="margin: 10px 0px; text-align: center;">Events</h4>
                    <!-- // Collapsible Widget END -->
                </div>

                <?php
                    $get = new Stats();
                    $get->upcomingEvents($user);
                ?>
            </div>

        </div>
        <?php
    }


    function groupActivity($user){
    ?>
        <div id="group-activity" class="col-md-12 padding-none" style="overflow-y: scroll; max-height: 340px; margin-bottom: 10px;">
            <?php
                foreach ($user->group_activity as $k => $v) {

                    // $time_stamp = $v->time_stamp;

                    // $time = time(); // current Unix timestamp

                    // $diff = Epoch::time_diff($time, $time_stamp);

                    $pic = $this->path.'/grad_assets/'.$v->author_pic;

                    // var_dump($v);

            ?>

            <div class="media innerAll border-bottom margin-none" style="background: #fff;">
                <a href="" class="pull-left">
                    <img style="width: 35px; height: 35px;" src="<?php echo $pic; ?>" alt="people" class="img-responsive img-circle" />
                </a>
                <div class="media-body">
                    <small class="strong pull-right"><?php //echo $diff; ?></small>
                        <?php 
                            $encrypt = new Decipher();
                            $base64_encrypt = $encrypt->base64_encrypt($v->author_id); 

                            $encrypt_id = $encrypt->base64_encrypt($v->group_id);
                        ?>
                    <h5>
                        <a href="index.php?profile=<?php echo $base64_encrypt; ?>"> <?php echo $v->author_name; ?> </a>
                        posted in <a class="text-primary" href="index.php?page=group_wall&gid=<?php echo $encrypt_id; ?>"><?php echo $v->group_name; ?></a>
                    </h5>
                    <p class="innerB border-bottom"><?php // echo $v->role; ?></p>
                    <p><?php echo $v->message; ?>.</p><br/>
                    <?php if($v->media){
                        $file_paths = $this->path.'/grad_assets/'.$v->file_paths;
                        ?>
                            <img style="width: 250px; height: 250px;" src='<?php echo $file_paths; ?>'/>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <?php
                }
            ?>
            <!-- // END DISCUSSIONS -->
            <div class="load-more-activity get-group-activity btn-primary btn btn-block">
                View More
            </div>
        </div>
    <?php
    }


    function upcomingEvents($user){

        $event = $user->events;

        foreach ($event->upcoming as $key => $value) {
            # code...
    ?>
        <div class="col-md-6" data-gridalicious>
          <div class="col-separator col-separator-first box col-unscrollable">

              <!-- CALENDAR START -->
              <h4 class="bg-primary innerAll border-bottom margin-none center">
                  <?php echo $value->event_name; ?>
              </h4>
              <div class="innerAll bg-primary border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-2x"></i> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $value->start_date; ?></p>
                          <p class="strong margin-none"><?php echo "To: ".$value->end_date; ?></p>
                      </div>
                  </div>
              </div>
              <div class="innerAll bg-gray border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> 
                        <?php 
                            $pic = $this->path.'/grad_assets/'.$value->host_pic;
                            echo "<img style='width: 40px; height: 40px;' src='".$pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> 
                      </div>
                      <div class="media-body">
                          <p class="strong margin-none">
                            <?php 
                                $encrypt = new Decipher();
                                $encrypt_id = $encrypt->base64_encrypt($value->host_id);
                            ?>
                            <a href="index.php?profile=<?php echo $encrypt_id; ?>">
                                <?php 
                                    echo $value->host_name; 
                                ?>
                            </a>
                          </p>
                          <p class="strong margin-none" style="font-size: 12px;">
                            <?php 
                                $desc = strip_tags($value->event_description);

                                if (strlen($desc) > 50) {
                                    $stringCut = substr($desc, 0, 50);
                                    // make sure it ends in a word so assassinate doesn't become ass...
                                    $desc = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="index.php?page=events">Read More</a>';
                                }
                                echo $desc; 

                            ?>
                          </p>
                      </div>
                  </div>
              </div>
              <!-- // END CALENDAR -->

           </div>
        </div>
    <?php
        }
    }
}
?>