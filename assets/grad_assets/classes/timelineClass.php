<?php
/**
* 
*/


class Timeline extends Grad
{

	function get(){
		
		$json = file_get_contents($this->path.'/grad_assets/js/json/timeline.json');

		$raw_data = json_decode($json);

		$timeline = $raw_data->Timeline;

		$timeline_array = array();

        $i = 0;

        foreach ($timeline as $key => $value) {
            # code...
            $created_on = $value->created_on;

            if(array_key_exists($created_on, $timeline_array)){
                $arr = (array) $value;
                array_push($timeline_array[$created_on], $arr);
            }else{
                $arr = (array) $value;
                $timeline_array[$created_on];
                $timeline_array[$created_on][0] = $arr;
            }
        }


		foreach ($timeline_array as $key => $value) {
			# code...
			// var_export($value);
				 $count = count($value);
			for ($j=0; $j < $count; $j++) {
				if($value[$j]['comment_id'] !="" && $value[$j]['comment_id'] != NULL){
					self::getComment($value[$j]);
				}else{
					self::getTask($value[$j]);
				}
			}
		}

		// for ($i=0; $i < 10; $i++) {
		// 	# code...
		// 	self::getTask($i);
		// }
	}
	
	function getComment($data){
			$time = time(); // current Unix timestamp
			$diff = Epoch::time_diff($time, $data['time_stamp']);
		?>
		<li class="active">
			<!-- <div class="separator bottom">
                <span class="date box-generic"><?php echo $data['created_on']; ?></span> 
                <span class="type glyphicons comments">Comment <i></i><span class="time">11:00</span></span>
            </div> -->

            <span class="type glyphicons comments">Comment <i></i><span class="time"><?php echo $diff; ?></span></span>

            <div class="widget widget-heading-simple widget-body-white margin-none">
                <div class="widget-body">
                    <p class="glyphicons user margin-none">
                        <i></i> 
                        <?php
                            $base64_encrypt = Decipher::base64_encrypt($data['wall_id']);
                        ?>
                        <strong><a target="_blank" href="index.php?post=<?php echo $base64_encrypt; ?>"><?php echo $data['user_name'] ?></a> said</strong>
                        : <?php echo $data['comment']; ?>
                    </p>
                </div>
            </div>
        </li>
		<?php
	}

	function getTask($data){
			$time = time(); // current Unix timestamp
			$diff = Epoch::time_diff($time, $data['time_stamp']);
		?>
		<li class="active">
            <!-- <div class="separator bottom">
                <span class="date box-generic"><?php echo $data['created_on']; ?></span> 
                <span class="type glyphicons suitcase">Task <i></i><span class="time">08:00</span></span>
            </div> -->

            <span class="type glyphicons suitcase">Post <i></i><span class="time"><?php echo $diff; ?></span></span>

            <div class="widget widget-heading-simple widget-body-white margin-none">
                <div class="widget-body">
                    <div class="media">
                        <div class="media-object pull-left thumb">
                        <?php 
							$pic = $this->path.'/grad_assets/'.$data['profile_pic'];
                        ?>
                            <!-- <img src="../assets//images/avatar-51x51.jpg" alt="Image" /> -->
                            <img src="<?php echo $pic; ?>" alt="Image" style="width: 50px; height: 50px;" />
                        </div>
                        <div class="media-body">
                            <a class="author"><?php echo $data['message_author']; ?></a>
                            <br/>
                            <p><?php echo $data['wall_message']; ?></p>
                            <!-- <span class="muted">adrian@ennovation.ie</span>  -->
                        </div>
                    </div>
                    <div class="alert alert-gray">
                        <p class="glyphicons circle_info margin-none"><i></i> <?php echo $data['wall_info']; ?> 
                        	<?php
                                $base64_encrypt = Decipher::base64_encrypt($data['wall_id']);

                        		if($data['group_id']!=""&&$data['group_id']!=NULL){
                        			echo '<a target="_blank" href="index.php?post='.$base64_encrypt.'">'.$data['group_name'].'</a>';
                        		}
                        	?>
                        </p>
                    </div>
                    <!-- <a class="glyphicons single pencil"><i></i></a> -->
                </div>
            </div>
        </li>
		<?php
	}

	function getAlbum($data){

	}


}
?>
