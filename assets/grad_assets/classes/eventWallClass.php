<?php

class Eventwall{


	public static function getWall ($id){



		$json = file_get_contents(PATH.'gradpower/js/wall_data.json'); // this WILL do an http request for you
		$data = json_decode($json);

		$wall_data = $data->$id;

			foreach($wall_data as $k => $v){

				$comments = $v->comments;

			//var_dump($v);

			$wall_id = $v->wall_id;
			$message_author = $v->message_author;
			$profile_pic = $v->profile_pic;
			$wall_message = $v->wall_message;

			$is_media_available = $v->is_media_available;
			$path_to_files = $v->path_to_files;

			$time_stamp = $v->time_stamp;

			$time = time(); // current Unix timestamp
		
			$diff = Epoch::time_diff($time, $time_stamp);

			$posted_on = $v->posted_on;


		?>

				<div class="ui segment">
				  <a class="ui ribbon label"><?php echo $wall_id; ?></a>

					<div class="ui comments">
					  <div class="comment">
					    <a class="avatar">
					    <?php 
					      echo "<img src='".$profile_pic."' />";
					    ?>
					    </a>
					    <div class="content">
					      <a class="author"><?php echo $message_author; ?></a>
					      <div class="metadata">
					        <div class="date"><?php echo $diff; ?> <!-- 2 days ago --></div>
					        
					      </div>
					      <div class="text">
					        <div><?php echo $wall_message; ?></div>
					        <div class="wallpath_to_files">
						        <?php 
						        	if($is_media_available){
						        		Grad::getFileType($path_to_files);
						        	}
						        ?>
					        </div>
					        <div class="clearfix"></div>
					      </div>

					      <div class="actions">
					      	<a class="rating">
					          <i class="star icon"></i>
					          5 Faves
					        </a>
					        <a class="rely">Comment</a>
					        <a class="save">Share</a>
					        
					      </div>
					    </div>
					  </div>

					 
						  <?php 

						   echo "<div id='wallComments_".$wall_id."'>";

						  	if($comments!=""){
						  		Comment::getComment($comments);
						  	}
						  ?>
					  </div>


					<div class="submitNewComment">
						<div class="comment">
						    <a class="avatar">
						    <?php 
						    	$my_profile_pic = "img/sj.jpg";
						      echo "<img src='".$my_profile_pic."' />";
						    ?>
						    </a>
						   	<div>
						   		<input id="<?php echo $wall_id; ?>" class="newEventComment" type="text" name="newEventComment">
						   		<i class="camera icon"></i>
								<input class="newCommentFile" type="file" name="files">
						   	</div>
						</div>
						<div class="newCommentPreview">
							<img src="">
						</div>
					</div>








					 
					</div>



				</div>

		<?php 
		}

	}


	
	public static function viewWall ($data){

		$data = (object) $data;

		$author_id = $data->author_id;

		$profile_picture = $data->profile_picture;

		$message_author = $data->message_author;

		$date = $data->posted_on;

		$time_stamp = $data->time_stamp;

		$time = time(); // current Unix timestamp
		
		$diff = Epoch::time_diff($time, $time_stamp);

		$wall_message = $data->wall_message;

		$path_to_files = $data->path_to_files;

		$wall_id = $data->wall_id;

	?>

		<div class="ui segment">
		  <a class="ui ribbon label"><?php echo $wall_id; ?></a>

			<div class="ui comments">
			  <div class="comment">
			    <a class="avatar">
			    <?php 
			      echo "<img src='".$profile_picture."' />";
			    ?>
			    </a>
			    <div class="content">
			      <a class="author"><?php echo $message_author; ?></a>
			      <div class="metadata">
			        <div class="date"><?php echo $diff; ?><!-- 2 days ago --></div>
			        
			      </div>
			      <div class="text">
			        <div><?php echo nl2br($wall_message); ?></div>
			        <?php 
					if($path_to_files!=""){
						if (strpos($path_to_files,', ') !== false) {
				    		$image = explode(', ', $path_to_files);
				    		$imageCount = count($image);
				    		for($i=0;$i<$imageCount;$i++){
				    			echo "<img src='".$image[$i]."' style='width: 150px; height: 150px; float:left; margin: 15px;'>";
				    		}
						} else {
							echo "<img src='".$path_to_files."' style='width: 250px; height: 250px;'>";
						}
			        }
			        ?>
			      </div>
			      <div class="clearfix"></div>
			      <div class="actions">
			      	<a class="rating">
			          <i class="star icon"></i>
			          Faves
			        </a>
			        <a class="rely">Comment</a>
			        <a class="save">Share</a>
			        
			      </div>
			    </div>
			  </div>
				<?php 

				   echo "<div id='wallComments_".$wall_id."'>";

				  	if($comments!=""){
				  		Comment::getComment($comments);
				  	}
				?>
			</div>


				<div class="submitNewComment">
					<div class="comment">
					    <a class="avatar">
					    <?php 
					    	//$my_profile_pic = "img/sj.jpg";
					      echo "<img src='".$profile_picture."' />";
					    ?>
					    </a>
					   	<div>
					   		<input id="<?php echo $wall_id; ?>" class="newEventComment" type="text" name="newEventComment">
					   		<i class="camera icon"></i>
							<input class="newCommentFile" type="file" name="files">
					   	</div>
					</div>
					<div class="newCommentPreview">
						<img src="">
					</div>
				</div>



		</div>
		<?php 
	}
}

?>