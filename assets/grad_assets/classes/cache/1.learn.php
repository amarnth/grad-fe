<?php

/*
 * Welcome to Learn Lesson
 * This is very Simple PHP Code of Caching
 */

// Require Library
require_once("phpfastcache.php");

// simple Caching with:
$cache = phpFastCache();
// $cache = phpFastCache("redis");

// Try to get $products from Caching First
// product_page is "identity keyword";
$products = $cache->get("keyword");

if($products == null) {

    $products = file_get_contents("http://en.wikipedia.org/w/api.php?action=opensearch&search=Free");
    // Write products to Cache in 10 second with same keyword
    // 600 = 60 x 10 = 10 minutes
    $cache->set("keyword",$products , 10);
    
    echo " THIS TIME RUN WITHOUT CACHE <br> ";
    
  //  var_dump("No Caching: ".$products);

} else {
    echo " USE CACHE, NO QUERY CALL <br>  ";

//    var_dump($cache->get("keyword"));

	$object = $cache->getInfo("keyword");
	print_r($object);
}
 
// use your products here or return it;



