<?php
class Epoch{
	public static function time_diff($t1, $t2) {

	   $totalSeconds = $t1-$t2;

	   if($totalSeconds<60){
	   		//seconds
	   		return $totalSeconds." secs ago";
	   }
	   
	   if($totalSeconds>=60 && $totalSeconds <60*60 ){
	   		//minutes
	   		$minutes = $totalSeconds/60;
	   		$minutes = round($minutes);
	   		if(1>=$minutes){
	   			return $minutes." min ago";	
	   		}else{
	   			return $minutes." mins ago";
	   		}
	   		
	   }

	   if ($totalSeconds>=60*60 && $totalSeconds<60*60*12){
	   		//hours
	   		$hours = $totalSeconds/(60*60);
	   		$hours = round($hours);
	   		if(1>=$hours){
	   			return $hours." hours ago";
	   		}else{
	   			return $hours." hours ago";
	   		}
	   }

	   if ($totalSeconds>=60*60*12 && $totalSeconds<60*60*24){
	   		//today
	   		return "today";
	   }

	   if ($totalSeconds>=60*60*24){
	   		//days
	   		$days = $totalSeconds/(60*60*24);
	   		$days = round($days);
	   		if(1>=$days){
	   			return $days." day ago";
	   		}else{
	   			return false;
	   		}
	   		
	   }

	}
}

$epoch = new Epoch();
?>