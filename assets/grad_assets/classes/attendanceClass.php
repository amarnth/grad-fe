<?php
/**
* 
*/
class Attendance extends Grad
{
	
	function get()
	{
	?>
		
	<?php
		self::getStudentsList();
	}

	function getStudentsList(){
		$json = file_get_contents($this->path.'/grad_assets/js/json/list.json'); // this WILL do an http request for you
		$data = json_decode($json);
		$student_data = $data->Students;
		
	?>
	<!-- row-app -->
	<div class="row row-app bg-white">
	    <!-- col -->
	    <div class="col-separator col-unscrollable box col-separator-first">
			<div class="col-md-4 padding-none">
			
				<input style="margin: 10px 0px;" class="form-control innerAll students-table-search" id="appendedInputButtons" type="text" placeholder="Search">

				<div class="padding-none" style="overflow-y: scroll; max-height: 450px; margin: 10px 0px;">
			        <!-- Table -->
			        <table class="footable table table-striped table-primary">
			            <!-- Table heading -->
			            <thead>
			                <tr>
			                	<th data-hide="phone,tablet">Roll No.</th>
			                    <th data-class="expand">Name</th>
			                    <th data-hide="phone,tablet">Reg No.</th>
							</tr>
			            </thead>
			            <!-- // Table heading END -->
			            <!-- Table body -->
			            <tbody>

			            <?php 
			            foreach ($student_data as $key => $value) {
						 
						    foreach ($student_data->$key as $k => $v) {
						  
			            ?>
			                <!-- Table row -->
			                <tr class="gradeX student-row" data-id="<?php echo $v->roll_no; echo $v->name; echo $v->register_no; ?>">
			                    <td>
			                    	<div class="checkbox"> 
										<label class="checkbox-custom"> 
											<input type="checkbox" name="checkbox">
											<i class="fa fa-fw fa-square-o absentees" data-id="<?php echo $v->id; ?>"></i> <?php echo $v->roll_no; ?>
										</label> 
									</div>
			                    </td>
			                    <td><?php echo $v->name; ?></td>
			                    <td><?php echo $v->register_no; ?></td>
			                </tr>
			                <!-- // Table row END -->
						<?php
							}
						}
						?>

						 </tbody>
			            <!-- // Table body END -->
			        </table>
			        <!-- // Table END -->
			    </div>
		        <button class="btn btn-primary save">Save</button>
		    </div>
		</div>
	</div>
	<?php

	}
}
?>
