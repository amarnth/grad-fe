<?php
	/**
	* 
	*/
	class Todo extends Grad
	{
		function microtime_float()
        {
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }



		function get()
		{

            ?>
                

                 <div class="clearfix"></div>
                    <!-- START CALENDAR NOTES -->
                        <div id="myCarousel" class="box-generic padding-none overflow-hidden carousel slide" id="toDo">
                            <div class="">
                                <div class="btn-group pull-right innerAll">
                                    <a href="#myCarousel" data-slide="prev"><button class="btn btn-default"><i class="fa fa-caret-left"></i></button></a>
                                    <a href="#myCarousel" data-slide="next"><button class="btn btn-default"><i class="fa fa-caret-right"></i></button></a>
                                </div>

                                <div class="carousel-inner" >
                                   
                               
            <?php
			# code...
            $user_id = $this->user_id;

            // $json = file_get_contents("http://localhost/grad/assets/grad_assets/js/json/calendarEvents.json"); // this WILL do an http request for you

            $json = file_get_contents($this->server_path.'/get/calendar/'.$user_id);

            $calendar_data = json_decode($json);

            // $calendar_data = $data->Calendar;   
            
            // var_dump($calendar_data);

                    $todo_array = array();

                    $i = 0;

                    foreach ($calendar_data as $key => $value) {
                        # code...

                        if($value->todo && $value->todo_status == 1){

                            $end_date = $value->end;
                            
                            if(array_key_exists($end_date, $todo_array)){
                                $arr = (array) $value;
                                array_push($todo_array[$end_date], $arr);
                            }else{
                                $arr = (array) $value;
                                $todo_array[$end_date];
                                $todo_array[$end_date][0] = $arr;
                            }
                        }
                    }

                    foreach ($todo_array as $key => $value) {
                        # code...
                        
                        $end_day = date('l', strtotime($key));

                        $count = count($value);

                        if($i==0){
                                    echo '<div class="item active">';
                                } else{
                                    echo '<div class="item">';
                                }
                                ?>
                                <div class="innerAll inner-2x">
                                    <h4 class="text-uppercase text-primary">
                                        <?php
                                            $today = date("Y-m-d");
                                            if($today == $end_date){
                                                echo "Today";
                                            }else{
                                                echo $end_day;      
                                            }
                                        ?>
                                        <?php //echo $end_day; ?>
                                      </h4>
                                    <p class="text-medium margin-none text-condensed"><?php echo $key; ?></p>
                                </div>
                                    <div class="innerAll bg-gray">
                                        <div class="innerLR innerB">
                                            <div class="notebook">
                                                <ul>
                                                <?php

                        for ($j=0; $j < $count; $j++) { 
                            # code...
                            // var_dump($value[$i]['end_date']);

                            $end_date = $value[$j]['end_date'];
                            $todo_id = $value[$j]['todo_id'];
                            $title = $value[$j]['title'];

                            // echo "date=>".$value[$i]."<br>";
                        ?>
                            <li>
                                <i data-id="<?php echo $todo_id; ?>" data-dismiss="alert" class="todo_remove close fa fa-fw fa-times" style="font-size: 14px; float: left; margin: 8px 5px;"></i>
                                <?php echo '<span class="" data-toggle="tooltip" data-original-title="'.$end_date.', '.$end_time.'" data-placement="bottom">'.$title.'</span>'; ?>
                            </li>
                            <?php
                        }

                        $i++;

                        ?>  

                                 </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                        <?php
                     }
                    ?>
                                

                                </div>
                                <div class=" bg-gray innerAll">
                                    <a href="#create-new-todo" data-toggle="modal" class="btn btn-sm btn-primary"> <i class="fa fa-plus fa-fw"></i> Add note</a>
                                </div>

                            <!-- <a href="" class="btn btn-sm text-muted"><i class="fa fa-envelope fa-fw"></i></a> -->
                        </div>
                    </div>
                    <!-- //END CALENDAR NOTES -->
			<?php
            

		}
	}
?>