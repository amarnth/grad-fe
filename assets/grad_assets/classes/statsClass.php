<?php
/**
* 
*/
class Stats extends Grad
{
	
	function getOverview(){
		$json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$stats = $dashboard->stats;

			self::getAssignments($stats);
			self::getProjects($stats);
	}



	function getClass() {

		$json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$class = $dashboard->class;

		?>

		<!-- Tabs -->
        <div class="relativeWrap" >
            <div class="widget widget-tabs widget-tabs-responsive">
                <!-- Tabs Heading -->
                <div class="widget-head">
                    <ul>

                    <?php
                    	$i = 0; 
                    	foreach ($class as $key => $value) {
                    		if($i==0){
						?>
							<li class="active">
	                            <a class="glyphicons coffe_cup" href="#<?php echo $value->id; ?>" data-toggle="tab"><i></i><?php echo $value->name; ?></a>
	                        </li>
						<?php
							} else {
						?>
							<li class="">
	                            <a class="" href="#<?php echo $value->id; ?>" data-toggle="tab"><i></i><?php echo $value->name; ?></a>
	                        </li>
						<?php
							}
							$i++;
						}
                    ?>
                        
                    </ul>
                </div>
                <!-- // Tabs Heading END -->



                <div class="widget-body">
                    <div class="tab-content">


                    	<?php
	                    	$i = 0; 
	                    	foreach ($class as $key => $value) {
	                    		if($i==0){
								?>
									<!-- Tab content -->
			                        <div class="tab-pane active" id="<?php echo $value->id; ?>">
								<?php
									} else {
								?>
									<!-- Tab content -->
			                        <div class="tab-pane" id="<?php echo $value->id; ?>">
								<?php
									}

									$stats = $value->stats;

									
								?>

									<div class="row">
					                    <?php
					                    	self::getAssignments($stats);
					                    	self::getProjects($stats);
					                    ?>
					                </div>

		                        </div>
		                        <!-- // Tab content END -->
		                    <?php
			                    $i++;
							}
		                ?>
                      
                    </div>
                </div>
            </div>
        </div>
        <!-- // Tabs END -->

		<?php
	}




	function getStudent() {

		$json = file_get_contents($this->path.'/grad_assets/js/json/dashboard.json');

		$data = json_decode($json);

		$dashboard = $data->Dashboard;

			$students = $dashboard->students;

			// var_dump($students);

			foreach ($students as $key => $value) {

				$pic = $this->path.'/grad_assets/'.$value->pic;

			if($value->pinned){
				echo '<div class="col-md-4 padding-zero">';
			}else{
				echo '<div class="col-md-4 students-card">';
			}
		?>
		 
            <!-- START CV -->
            <div class="box-generic col-md-12 padding-zero">
                <div class="media innerAll">
                    <a href="" class="pull-right">
                    	<?php
                    		if($value->pinned){
								echo '<i class="fa fa-fw icon-dropped-pin text-primary" style="font-size: 18px; margin: 8px -7px;"></i>';
							}else{
								echo '<i class="fa fa-fw icon-dropped-pin" style="color:#3a3a3a; font-size: 18px; margin: 8px -7px;"></i>';
							}
                    	?>
                    </a>
                    <a href="" class="pull-left">
                        <img src="<?php echo $pic; ?>" width="60" alt="">
                    </a>
                    <div class="media-body innerL half">
                        <h4 class="margin-none">
                            <a href="" class="text-primary student-name"><?php echo $value->name; ?></a>
                        </h4>
                        <p class="strong"><?php echo $value->year; ?> Year</p>
                        <div class="bg-gray">
                            <p class="margin-none innerAll"><!-- <a href="">4 members</a> --> <i class="fa fa-fw fa-code-fork fa-rotate-90"></i> <a href=""><?php echo $value->total_projects; ?> Projects</a></p>
                        </div>
                    </div>
                </div>


                <h4 class="innerAll margin-none border-bottom">
                    Skills
                </h4>
                <div class="innerAll">
                    <div class="innerAll">
                    <?php
                		foreach ($value->skills as $k => $v) {
                	?>
                        <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                            <div class="count"><?php echo $v; ?>%</div>
                            <div class="progress-bar progress-bar-primary" style="width: <?php echo $v; ?>%;"></div>
                            <div class="add"><?php echo $k; ?></div>
                        </div>
                    <?php
                     	}
                	?>
                    </div>
                </div>


                <div class="bg-gray">
                    <ul class="list-unstyled">
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->active_projects; ?></span> <i class="fa fa-fw fa-dashboard"></i> Active Projects </li>
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->completed_assignments; ?></span> <i class="fa fa-fw icon-browser-check"></i> Completed Assignments </li>
                        <!-- <li class="innerAll border-bottom"> <span class="badge badge-default pull-right">3</span> <i class="fa fa-fw fa-file-text-o"></i> Incomplete Assignments</li> -->
                        <li class="innerAll border-bottom"> <span class="badge badge-default pull-right"><?php echo $value->pending_assignments; ?></span> <i class="fa fa-fw fa-file-text-o"></i> Pending Assignments </li>
                        <!-- <li class="innerAll bg-white text-center"> <a href="" class="btn btn-info btn-sm">View Profile</a> </li> -->
                    </ul>
                </div>


                <div class="col-md-12 border-bottom" style="padding: 15px;">
                    <div class="col-md-6 text-center">
                        <h5>
                            Attendance
                        </h5>
                        <div data-percent="<?php echo $value->attendance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $value->attendance; ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 text-center">
                        <h5>
                            Performance
                        </h5>
                        <div data-percent="<?php echo $value->performance; ?>" data-size="80" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 14px;"><?php echo $value->performance; ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-md-12" style="padding: 15px;">
                        <h4 style="margin-bottom: 15px;" class="text-center">Assignments</h4>

                        <?php $assignment = $value->assignment; ?>

                        <div class="col-md-4 text-center">
                            <h5>
                                Min
                            </h5>
                            <div data-percent="<?php echo $assignment->min; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignment->min; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Average
                            </h5>
                            <?php 
                            	$range = explode("-", $assignment->frequent_range);
                            	$range = $range[0];

                            ?>
                            <div data-percent="<?php echo $range; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 11px;"><?php echo $assignment->frequent_range; ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 text-center">
                            <h5>
                                Max
                            </h5>
                            <div data-percent="<?php echo $assignment->max; ?>" data-size="60" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="5">
                                <div class="value text-center">
                                    <span class="strong text-primary" style="font-size: 12px;"><?php echo $assignment->max; ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

            </div>
            <!-- //END CV -->
        </div>

		<?php
		}
	}


	function getProjects($data) {

		$projects = $data->projects;

		?>
		<div class="col-md-4">
            <div class="box-generic padding-none overflow-hidden">
                <h4 class="heading innerAll inner-2x text-center">
                    Projects
                </h4>

                <?php
            	foreach ($projects->top5 as $key => $value) {
            		# code...
            	?>
	                <div class="innerAll border-bottom">
	                    <div class="media innerAll half">
	                       <a href="" class="pull-left innerAll bg-lightred"><i class="fa fa-fw icon-briefcase-1"></i></a>
	                        <div class="media-body">
	                            <h5 class="margin-none">
	                                <?php echo $value->pro_name; ?>
	                            </h5>
	                            <p class="text-small"><a href="" class="text-underline">View Project Activity</a></p>
	                            	<?php
	                            		$d = $value->completed_activity/$value->total_activity;
	                            		$d = round($d*100);
	                            	?>
	                            <p class="text-small margin-none innerB half"><?php echo $d; ?>% complete</p>
	                            <div class="progress bg-gray progress-mini margin-none">
	                                <div class="progress-bar progress-bar-success" style="width: <?php echo $d; ?>%;"></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            <?php
	            }
            	?>
            </div>
        </div>
		<?php
	}



	function getAssignments($data) {

		$filter = 5;

		$assignments = $data->Assignments;

		$get = "top".$filter;

		$active_assignments = $assignments->$get;

		// var_dump($active_assignments);

		?>

		<div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <!-- OVERALL PERFORMANCE START -->
                    <div class="box-generic innerAll inner-2x text-center" style="height: 179px;">
                        <h4>
                            Total Assignments
                        </h4>
                        <p class="innerTB inner-2x text-xlarge text-condensed strong text-primary">+<?php echo $assignments->total; ?></p>
                        <!-- <div class="sparkline" sparkHeight="62"></div> -->
                    </div>
                    <!-- // END OVERALL PERFORMANCE -->
                </div>
                <div class="col-md-6">
                    <!-- REPORTS START -->
                    <div class="box-generic innerAll inner-2x text-center">
                        <h4>
                            Availability
                        </h4>
                        <div data-percent="65" data-size="114" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
                            <div class="value text-center">
                                <span class="strong text-primary" style="font-size: 32px;"><?php echo $data->Availability; ?></span>
                            </div>
                        </div>
                    </div>
               
                    <!-- // END REPORTS -->
                </div>
            </div>
           
            <div class="widget">
                <div class="widget-head">
                    <h4 class="heading">
                        Active Assignments
                        <span><?php echo $assignments->active; ?></span>
                    </h4>
                </div>
                <div class="widget-body innerAll inner-2x">
                    <table class="table table-striped margin-none">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="text-center">Deadline</th>
                                <th class="text-right" style="width: 100px;">Progress</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php
                        	$i = 1;
                        	foreach ($active_assignments as $key => $value) {
                        		# code...

                        		$d = $value->submitted/$value->assigned;

                        		$progress = round($d*100);

                        ?>

                            <tr>
                                <td><strong><?php echo $i++; ?>.</strong> <?php echo $value->name; ?></td>
                                <td class="text-center"><?php echo $value->deadline; ?></td>
                                <td class="text-right">
                                    <p class="text-small margin-none innerB half"><?php echo $progress; ?>% complete</p>
                                    <div class="progress bg-gray progress-mini margin-none">
                                        <div class="progress-bar progress-bar-success" style="width: <?php echo $progress; ?>px;"></div>
                                    </div>
                                    <!-- <div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#eb6a5a" sparkFillColor="" data-data="[226,168,116,296,365,403,281,104,419,385]"></div> -->
                                </td>
                            </tr>

                        <?php
                        	}
                    	?>

                        </tbody>
                    </table>
                </div>
                <!-- // END TRENDS -->
            </div>
        </div>


		<?php
	}
}
?>