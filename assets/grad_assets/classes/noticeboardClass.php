<?php
/**
* 
*/
class Noticeboard extends Grad
{
	
	function get() {

	?>
	<div class="clearfix"></div>
    	<!-- START NOTICEBOARD -->

    	<?php 
    		
    		$user_role = $this->user_role;

	    if($user_role == "admin" || $user_role == "staff"){
	    ?>
				<a style="margin-bottom: 15px;" href="#create-new-noticeboard" data-toggle="modal" class="btn btn-sm btn-primary"> <i class="fa fa-plus fa-fw"></i> Add note</a>
	        	<div class="clearfix"></div>

		<?php } 
		
			$json_file = file_get_contents($this->server_path.'/get/notices/'.$this->user_id);

			$data = json_decode($json_file);
		
		
			if($data->Noticeboard){
		?>
			<section id="blog-landing">
		<?php
		
		//$json_file = file_get_contents('assets/grad_assets/js/json/noticeboard.json');
		$json_file = file_get_contents($this->server_path.'/get/notices/'.$this->user_id);

		$data = json_decode($json_file);

		// var_dump($data);

			$noticeboard = $data->Noticeboard;
			$arr = (array) $noticeboard;

			$count = count($arr);

			$row = $count/3;

			$no_of_row = round($row);

			$i = 1;

		foreach ($noticeboard as $key => $value) {

			$title = $value->title;
			$message = $value->description;
			$to = $value->destination_to;
			$time_stamp = $value->time_stamp;

			$time = time(); // current Unix timestamp

			$diff = Epoch::time_diff($time, $time_stamp);

			$i++;

			$pic = "";

			if($value->media){
				if($value->file_paths){
					$pic = $this->path.'/grad_assets/'.$value->file_paths[0];
				}elseif($value->url){
					// echo '<iframe width="400" height="280" src="'.$value->url.'" frameborder="0" allowfullscreen></iframe>';
				}
			}

			?>

		  	<article class="white-panel"> 
		  		<h1><a href="#"><?php echo $title; ?></a> <?php echo $diff; ?> ( <?php echo $value->posted_on; ?></a> ) </h1>
		    		<?php
		    			$decipher = new Decipher();
			    			$base64_encrypt = $decipher->base64_encrypt($value->admin_id);
			    			
		    			$admin_pic = 'assets/grad_assets/'.$value->admin_pic;
		    		?>
		    	<p class="center">
		    		posted by <a href="index.php?profile=<?php echo $base64_encrypt; ?>"> <?php echo $value->admin_name; ?></a> 
		    		<img style="width: 34px; height: 34px; " src="<?php echo $admin_pic; ?>">
		    	</p>

		  		<?php if($pic != ""){ ?> 
		  			<img style="width: 100%; height: 250px;" src="<?php echo $pic;?>" alt="ALT">
		  		<?php }else{ ?>
		  			<!-- <img style="width: 50%; height: 50%;" src="assets/grad_assets/gow.jpg" alt="ALT"> -->
		  		<?php } ?>
		    	
		    	<p><?php echo $message;?>.</p>
		  	</article>
  <?php
			}
			?>
				</section>
			<?php
		}else {
		?>
			<h1 class="center text-primary">No Notices Available</h1>
		<?php
		}
		
	}


	function getSlide($user="") {

	?>
	<div class="clearfix"></div>
    	<!-- START CALENDAR NOTES -->
        <div id="myCarousel" class="box-generic padding-none overflow-hidden carousel slide" style="margin: 0; padding: 10px !important;">
            <div class="">
            	<h4 style="margin: 10px 0px; text-align: center;">Noticeboard</h4>
                <div class="btn-group pull-right innerAll">
                    <a href="#myCarousel" data-slide="prev"><button class="btn btn-default"><i class="fa fa-caret-left"></i></button></a>
                    <a href="#myCarousel" data-slide="next"><button class="btn btn-default"><i class="fa fa-caret-right"></i></button></a>
                </div>

                <div class="carousel-inner" >
		<?php

			$user_role = $this->user_role;

			if("" != $user){
				$noticeboard = $user->Noticeboard;
			}else{
				$json_file = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/noticeboard.json');

				$data = json_decode($json_file);

				$noticeboard = $data->Noticeboard;
			}
			

				$i = 0;

				foreach ($noticeboard as $key => $value) {

					$title = $value->title;
					$message = $value->message;
					$to = $value->to;
					$time_stamp = $value->time_stamp;

					$time = time(); // current Unix timestamp

					$diff = Epoch::time_diff($time, $time_stamp);

					$i++;

					$pic = "";

					$url = "";

					if($value->media){
						if($value->file_paths){
							$pic = $this->path.'/grad_assets/'.$value->file_paths[0];
						}elseif($value->url){
							$url = $value->url;
						}
					}

					if($i==1){

			?>

	           		<div class="item active">
	                   <h1><a href="#"><?php echo $title; ?></a> <?php // echo $diff; ?> </h1>
	                   <p>( <?php echo $value->posted_on; ?></a> ) </p>
				    		<?php
				    			$decipher = new Decipher();
				    			$base64_encrypt = $decipher->base64_encrypt($value->admin_id);
				    			$admin_pic = 'assets/grad_assets/'.$value->admin_pic;
				    		?>

				  		<?php if($pic != ""){ ?> 
				  			<img style="width: 50%;" src="<?php echo $pic;?>" alt="ALT">
				  		<?php }elseif($url != ""){ ?>
				  				<!-- <iframe style="margin: 0px 6%;" width="300" height="250" src="<?php echo $url; ?>" frameborder="0" allowfullscreen></iframe> -->
				  		<?php } ?>
				    	
				    	<p><?php echo $message;?>.</p>

				    	<p class="text-right">
				    		posted by <a href="index.php?profile=<?php echo $base64_encrypt; ?>"> <?php echo $value->admin_name; ?></a> 
				    		<img style="width: 34px; height: 34px; " src="<?php echo $admin_pic; ?>">
				    	</p>
	                </div>
	                
			<?php
				} else {
					?>

					<div class="item">

	                    <h1><a href="#"><?php echo $title; ?></a> <?php // echo $diff; ?> </h1>
	                    <p>( <?php echo $value->posted_on; ?></a> ) </p>
				    		<?php
				    			$decipher = new Decipher();
				    			$base64_encrypt = $decipher->base64_encrypt($value->admin_id);
				    			$admin_pic = 'assets/grad_assets/'.$value->admin_pic;
				    		?>
				    	
				  		<?php if($pic != ""){ ?> 
				  			<img style="width: 50%;" src="<?php echo $pic;?>" alt="ALT">
				  		<?php }elseif($url != ""){ ?>
				  				<!-- <iframe style="margin: 0px 6%;" width="300" height="250" src="<?php echo $url; ?>" frameborder="0" allowfullscreen></iframe> -->
				  		<?php } ?>
				    	
				    	<p><?php echo $message;?>.</p>

				    	<p class="text-right">
				    		posted by <a href="index.php?profile=<?php echo $base64_encrypt; ?>"> <?php echo $value->admin_name; ?></a> 
				    		<img style="width: 34px; height: 34px; " src="<?php echo $admin_pic; ?>">
				    	</p>
	                </div>

					<?php
				}

			}

			?>
				</div>
				<?php if($user_role == "admin" || $user_role == "staff"){?>
                <div class=" bg-gray innerAll">
                    <a href="#create-new-noticeboard" data-toggle="modal" class="btn btn-sm btn-default"> <i class="fa fa-plus fa-fw"></i> Add note</a>
                </div>
                <?php } ?>
          	</div>
        </div>

	<?php

	}
	


	
}
?>