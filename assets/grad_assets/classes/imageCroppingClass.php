<?php 
/**
* 
*/
class Crop
{
	
	function get($image, $option)
	{
		// The file
		$filename = $image;
		
		if($option == "thumb"){
			$percent = 0.24;
			$ffname = "aa_thumb";
		}

		if($option == "small"){
			$percent = 0.1;
			$ffname = "aa_small";
		}

		if($option == "tiny"){
			$percent = 0.04;
			$ffname = "aa_tiny";
		}

		// Content type
		header('Content-Type: image/jpg');

		// Get new dimensions
		list($width, $height) = getimagesize($filename);
		$new_width = $width * $percent;
		$new_height = $height * $percent;

		// Resample
		$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromjpeg($filename);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		// Output
		imagejpeg($image_p, null, 100);
		// imagejpeg( $image_p, "upload/".$ffname.".jpg" );
	}
}
?>