<?php
/**
* 
*/
class Sharing extends Grad
{
	
	function __construct()
	{
		# code...
	}

	function getStaffShare(){
		?>

		<div class="col-md-6" >
		    <div style="background: #f7f8fa;padding: 50px;">
		        <input type="file" multiple="multiple" name="files[]" id="input2">
		    </div>
	    </div>
	    	
		<div class="col-md-6" id="shareDescription">
			<div class="ui form">
			    <div class="grouped inline fields">

			      <div class="field">
			        <div class="ui radio checkbox share_recipient" data-bool="true">
			          <input name="share_recipient" type="radio" >
			          <label>Whole department</label>
			        </div>
			      </div>

			      <div class="field">
			        <div class="ui radio checkbox share_recipient" data-bool="false">
			          <input name="share_recipient" type="radio" >
			          <label>Individual</label>
			        </div>
			      </div>

			    </div>
			</div>


			<div for="individual">
				<label for="selectShareMembers">Select Members:</label>
					<select id="selectShareMembers" name="selectShareMembers" multiple>
						<?php
							//Util::getListStudent();
						?>
					</select>
				<input value="activate selectator" data-id="selectator_" id="selectatorShareSelectedMembers" style="display: none;">
			</div>

			<div for="department">
				<label for="selectShareDepartment">Select Department:</label>
					<select id="selectShareDepartment" name="selectShareDepartment" multiple>
						<?php
							//Util::getListDepartment();
						?>
					</select>
				<input value="activate selectator" data-id="selectator_" id="selectatorShareSelectedDepartment" style="display: none;">
			</div>


	    	<div class="ui form">
			  <div class="field">
			    <!--<label>User Text</label>-->
			    <textarea class="shareMessage" placeholder="Enter Your Message..."></textarea>
			  </div>
			</div>
			
			<input type="submit" class="ui button primary pull-right" value="Share" id="share">
	    </div>

		<?php
	}
}
?>