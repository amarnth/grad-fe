<?php

require_once("cache/phpfastcache.php");


include "wallClass.php";
include "epochClass.php";
include "commentClass.php";

include "groupClass.php";
include "groupWallClass.php";

include "eventClass.php";
include "eventWallClass.php";
include "assignmentsClass.php";

include "utilities.php";

include "todoClass.php";

include "notificationClass.php";

include "userClass.php";

//include "redisClass.php";

include "materialShareClass.php";

include "timetableClass.php";

include "noticeboardClass.php";

include "gradCacheClass.php";

include "fileRegulatedClass.php";

include "imageCroppingClass.php";

include "accountClass.php";

include "calendarClass.php";

include "timelineClass.php";

include "loginClass.php";

include "postClass.php";

include "contactsClass.php";

include "viewProfileClass.php";

include "dashboardClass.php";

include "projectsClass.php";

include "projectViewClass.php";

include "doubtsClass.php";

include "doubtsAnswersClass.php";

include "messagesClass.php";

include "decipherClass.php";

include "attendanceClass.php";


class Grad{

	public $user_role;
	public $user_id;
	public $user_pic;
	public $user_name;
	public $college_id;
	public $college_name;
	public $path;
	public $is_redis_up;

	// public $server_path = "http://gradpowered.net:9558";
	public $server_path = "http://localhost:9558";

	function __construct()
	{	
		session_start();
		
		
		$this->is_redis_up = false;
		

		$this->path = "assets";

		$this->user_role = $_SESSION['user_role'];
		$this->user_id = $_SESSION['user_id'];
		$this->user_pic = $_SESSION['user_pic'];
		$this->user_name = $_SESSION['user_name'];
		$this->college_id = $_SESSION['college_id'];
		$this->college_name = $_SESSION['college_name'];


		// $this->user_role = "staff";
		// $this->user_id = "stf001";
		// $this->user_pic = ASSETS_PATH.'/grad_assets/img/profile/ceresei.jpeg';
		// $this->user_name = "Cersei";
		// $this->college_id = "rsm001";
		// $this->college_name = "RSM";
	}


	public function run() { 
		print "Works\n"; 

		$json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/virginCall.json');
		
		var_dump($json);
		
	}
	
	public function test($v) {
		$json = file_get_contents(ASSETS_PATH.'/grad_assets/js/sample_data.json'); // this WILL do an http request for you
		$data = json_decode($json);
		return self::getData($v, $data);
	}

	public function getData($v, $data) {
		return $val = $v;
	}



	public function sendPostData($url, $post){
		  $ch = curl_init($url);
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		  $result = curl_exec($ch);
		  curl_close($ch);
		  return $result;
	  
	}


	public function setWall ($data){

		$jsonDataEncoded = json_encode(array("Wall"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			// exit;
			//$cache = new Cache();
			//$cache->setWallCache($data);

            $url_send = $this->server_path."/update/wall/student";
			
			self::sendPostData($url_send, $jsonDataEncoded);

		$wall = new Wall();
		$wall->getWall();

	}

	


	public function setEventWall ($data){

		$jsonDataEncoded = json_encode(array("EventWall"=>$data));
       
  			var_dump($jsonDataEncoded);
  			exit;

            $url_send = $this->server_path."/update/wall/student";
			
			//self::sendPostData($url_send, $jsonDataEncoded);

		$wall = new Wall();
		$wall->getWall("EVENT0001", "event");

	}



	public function setComment ($data){

		$jsonDataEncoded = json_encode(array("Comment"=>$data));
       
  			//var_dump($jsonDataEncoded);
  			//exit;

			//$cache = new Cache();
			//$cache->setCommentCache($data);

            $url_send = $this->server_path."/update/comment/wall";
			
			self::sendPostData($url_send, $jsonDataEncoded);

			


		$wall = new Wall();
		$wall->getWall();

		//$wall = new Comment();
		//$wall->viewComment($data);

	}

	public function setGroupWall ($data){

		$jsonDataEncoded = json_encode(array("Group-Wall"=>$data));
       
  			//var_dump($jsonDataEncoded);
  			// var_dump($data['group_id']);
			// exit;

  			$group_id = $data['group_id'];


            		$url_send = $this->server_path."/update/wall/group";
			
			self::sendPostData($url_send, $jsonDataEncoded);

		$wall = new Groupwall();

			$base64_encrypt = new Decipher();
			$gid = $base64_encrypt->base64_encrypt($group_id);

		$wall->getWall($gid);
		

	}

	public function setGroupComment ($data){

		$jsonDataEncoded = json_encode(array("Comment"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			//exit;


			$group_id = $data['group_id'];

            $url_send = $this->server_path."/update/comment/group_Wall";
			
			self::sendPostData($url_send, $jsonDataEncoded);
		
		$wall = new Groupwall();
		
		$base64_encrypt = new Decipher();
			$gid = $base64_encrypt->base64_encrypt($group_id);

		$wall->getWall($gid);
	}


	public function setGroup ($data){

		$jsonDataEncoded = json_encode(array("Group"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			// exit;
		

            $url_send = $this->server_path."/create/group/student";
			self::sendPostData($url_send, $jsonDataEncoded);

		$group = new Groups();
		$group->getGroups();

	}
	
	public function inviteGroupMembers($data){

		$jsonDataEncoded = json_encode(array("Group_Members"=>$data));
       
  			var_dump($jsonDataEncoded);
  			//exit;

			$group_id = $data['group_id'];

            //$url_send = $this->server_path."/update/comment/group_Wall";
			
			//self::sendPostData($url_send, $jsonDataEncoded);
		
		//$wall = new Groupwall();
		//$wall->getWall($group_id);
	}
	


	public function setNotification ($data){

		$jsonDataEncoded = json_encode(array("Notification"=>$data));
       
		// var_dump($jsonDataEncoded);
		// exit;


		$url_send = $this->server_path."/update/notification/status";
		self::sendPostData($url_send, $jsonDataEncoded);


		//$cache = new Cache();
		//$cache->setNotificationStatus($data);

	}



	


	public function setCalendar ($data){

		if($data['remove']){

			$jsonDataEncoded = json_encode(array("To-do"=>$data));
			
			// var_dump($jsonDataEncoded);
			// exit;

			$url_send = $this->server_path."/remove/todo";

		} else {

			$jsonDataEncoded = json_encode(array("Calendar"=>$data));
			// var_dump($jsonDataEncoded);
			// exit;

		    $url_send = $this->server_path."/update/calender/student";
		}


			self::sendPostData($url_send, $jsonDataEncoded);

			// echo $reponse;
       	
       		

		$todo = new Todo();
		$todo->get();

	}



	public function setAssignmentMarks ($data){
		var_dump($data);
	}

	public function setAssignmentStatus ($data){
		var_dump("Remove ====>>>>> ".$data);
	}


	public function setAssignmentAnswer ($data){
		$jsonDataEncoded = json_encode(array("Submit"=>$data));
		//var_dump($jsonDataEncoded);

		// $url_send = $this->server_path."/make/submission/student";
		$url_send = $this->server_path."/update/student";
		self::sendPostData($url_send, $jsonDataEncoded);

		$assignment = new Assignments();
			$base64_encrypt = new Decipher();
			$aid = $base64_encrypt->base64_encrypt($data['assignment_id']);

		$assignment->getAsStudent($aid);
	}

	public function setNewAssignment ($data){

		$jsonDataEncoded = json_encode(array("Assignments"=>$data));
       
		// var_dump($jsonDataEncoded); 

		//echo($this->user_id); exit;
	
  		$url_send = $this->server_path."/create/new/assignment";
		self::sendPostData($url_send, $jsonDataEncoded);

		$assignment = new Assignments();
		$assignment->get();
	}


	public function setAsStudent ($data){

		$jsonDataEncoded = json_encode(array("Submit"=>$data));
       
		//var_dump($jsonDataEncoded); exit;
		
	
        	//$url_send = $this->server_path."/make/submission/student";
		//self::sendPostData($url_send, $jsonDataEncoded);

		//$assignment = new Assignments();
		//$assignment->getAsStudent($data['assignment_id']);
	}



	
	public function saveAssignmentMarks ($data){

		$jsonDataEncoded = json_encode(array("Save"=>$data));

		// var_dump($jsonDataEncoded);

       $url_send = $this->server_path."/save/assignment";
       self::sendPostData($url_send, $jsonDataEncoded);

		$assignment = new Assignments();
			$base64_encrypt = new Decipher();
			// echo $data['assignment_id'];
			$aid = $base64_encrypt->base64_encrypt($data['assignment_id']);

		$assignment->getAsStaff($aid);

	}


	public function publishAssignmentMarks ($data){

		$jsonDataEncoded = json_encode(array("Publish"=>$data));

		// var_dump($jsonDataEncoded);

		$url_send = $this->server_path."/publish/assignment";
		self::sendPostData($url_send, $jsonDataEncoded);

		$assignment = new Assignments();
			$base64_encrypt = new Decipher();
			// echo $data['assignment_id'];
			$aid = $base64_encrypt->base64_encrypt($data['assignment_id']);

		$assignment->getAsStaff($aid);
	}


	public function setEvent ($data){

		$jsonDataEncoded = json_encode(array("Event"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			//exit;
		

            $url_send = $this->server_path."/create/event/student";
			self::sendPostData($url_send, $jsonDataEncoded);

		$event = new Events();
		$event->getEvents();

	}


	public function approveEvent ($data){

		$jsonDataEncoded = json_encode(array("Event"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			// exit;
	

		$url_send = $this->server_path."/update/admin/approval";
		self::sendPostData($url_send, $jsonDataEncoded);

		$event = new Events();
		$event->getEvents();

	}


	public function inviteEvent ($data){

		$jsonDataEncoded = json_encode(array("Event"=>$data));
       	
  			// var_dump($jsonDataEncoded);
  			// exit;
	

		$url_send = $this->server_path."/add/members/event";
		self::sendPostData($url_send, $jsonDataEncoded);

		$event = new Events();
		$event->getEvents();

	}


	public function userAcceptEvent ($data){

		$jsonDataEncoded = json_encode(array("Event"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			// exit;
	

		$url_send = $this->server_path."/update/event/attending";
		self::sendPostData($url_send, $jsonDataEncoded);

		$event = new Events();
		$event->getEvents();

	}


	public function userDeclineEvent ($data){

		$jsonDataEncoded = json_encode(array("Event"=>$data));
       
  			// var_dump($jsonDataEncoded);
  			// exit;
	

		$url_send = $this->server_path."/add/members/event";
		self::sendPostData($url_send, $jsonDataEncoded);

		$event = new Events();
		$event->getEvents();

	}

		

	


	public function setTimetable ($data){

			$jsonDataEncoded = json_encode(array("Timetable"=>$data));
       
  			
  			// var_dump($jsonDataEncoded);
  			// exit;
			//$jsonDataEncoded = '{ "Timetable": { "Amar":"RAM" } }';

            $url_send = $this->server_path."/update/timetable/";
			self::sendPostData($url_send, $jsonDataEncoded);
	}


	public function setLogin ($data){


		$jsonDataEncoded = json_encode(array("Login"=>$data));

		// // var_dump($jsonDataEncoded);

		// $url_send = $this->server_path."/valid/login/credentials";
		
		// // $obj = new Grad();

		// // $response = $obj->sendPostData($url_send, $jsonDataEncoded);
		
		// $response = self::sendPostData($url_send, $jsonDataEncoded);

		// $data = json_decode($response);

		// // var_dump($response);

		// if($data->Status){
		// 	echo "Invalid Credentials";
		// }elseif($data->Credentials){
		// 	$user = $data->Credentials;

		// 		$_SESSION['user_role'] = $user->user_role;	

		// 		$_SESSION['user_id'] = $user->user_id;

		// 		$_SESSION['user_name'] = $user->user_name;

		// 		$_SESSION['user_pic'] = $user->user_pic;

		// 		$_SESSION['user_dept'] = $user->user_department;

		// 		$_SESSION['college_name'] = $user->college_name;

		// 		$_SESSION['college_id'] = $user->college_id;
		// }


		
			if("admin" == $data['email']){
				$_SESSION['user_role'] = "admin";	

				$_SESSION['user_id'] = "admin001";

				$_SESSION['user_name'] = "Ned Stark";

				$_SESSION['user_pic'] = "img/profile/ned.jpg";

				$_SESSION['user_dept'] = "IT";

				$_SESSION['college_name'] = "RSM001";

				$_SESSION['college_id'] = "colg001";

			}elseif("a"==$data['email']){
				
				$_SESSION['user_role'] = "staff";	

				$_SESSION['user_id'] = "stf001";

				$_SESSION['user_name'] = "Khaleesi";

				$_SESSION['user_pic'] = "img/profile/kh.jpg";

				$_SESSION['user_dept'] = "IT";

				$_SESSION['college_name'] = "RSM001";

				$_SESSION['college_id'] = "colg001";

			}elseif("s"==$data['email']){
				$json = file_get_contents('http://grad-grad.44fs.preview.openshiftapps.com/assets/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
				$user_data = json_decode($json);

				if($user_data->UserInfo){
					
					$user = $user_data->UserInfo;

					$_SESSION['user_name'] = $user->name;

					$_SESSION['user_pic'] = $user->prof_pic;

					$_SESSION['user_dept'] = $user->dept;

					$_SESSION['user_role'] = $user->role;

					$_SESSION['user_id']	= $user->id;

					$_SESSION['college_name'] = $user->college_name;

					$_SESSION['college_id'] = $user->college_id;

					//session_write_close();
			
				}else{
					echo "Invalid Credentials";
				}
			}

	}
	
	
	function createStudentAccount($data){
		$jsonDataEncoded = json_encode(array("student_registration"=>$data));

		// var_dump($jsonDataEncoded);

		$url_send = $this->server_path."/update/student/1";
		
		self::sendPostData($url_send, $jsonDataEncoded);

		// $noticeboard = new Noticeboard();
		// $noticeboard->get();
	}
	

	function createStaffAccount($data){
		$jsonDataEncoded = json_encode(array("staff_registration"=>$data));

		// var_dump($jsonDataEncoded);

		$url_send = $this->server_path."/register/staff";
		
		self::sendPostData($url_send, $jsonDataEncoded);

		// $noticeboard = new Noticeboard();
		// $noticeboard->get();
	}
	
	

	function setNoticeboard($data){
		$jsonDataEncoded = json_encode(array("Noticeboard"=>$data));

		// var_dump($jsonDataEncoded);
		
		$url_send = $this->server_path."/update/noticeboard";
		
		self::sendPostData($url_send, $jsonDataEncoded);

		$noticeboard = new Noticeboard();
		$noticeboard->get();
		
		
	}


	// projects 

	function setNewProject($data){
		$jsonDataEncoded = json_encode(array("Project"=>$data));

		//var_dump($jsonDataEncoded);
		
		if("student"==$this->user_role){
			$url_send = $this->server_path."/create/project/student";	
		}else{
			$url_send = $this->server_path."/create/project/staff";
		}
		
		 // $url_send = $this->server_path."/create/project";
		
		self::sendPostData($url_send, $jsonDataEncoded);
		
		$projects = new Project();
		$projects->getProjects();
	}
	

	function projectInvite($data){
		$jsonDataEncoded = json_encode(array("Project"=>$data));

		// var_dump($jsonDataEncoded);

		// var_dump($data['project_id']);

		$url_send = $this->server_path."/invite/mentors";
		self::sendPostData($url_send, $jsonDataEncoded);
		$obj = new ViewProject();
		$obj->view($data['project_id'], $recall=true);
	}	


	function setNewActivity($data){
		$jsonDataEncoded = json_encode(array("Activity"=>$data));

		// var_dump($jsonDataEncoded);

		// var_dump($data['project_id']);

		$url_send = $this->server_path."/add/project/activity";
		self::sendPostData($url_send, $jsonDataEncoded);
		$obj = new ViewProject();
		$obj->view($data['project_id'], $recall=true);
	}
	
	
	function acceptProjectMentorship($data){
		$jsonDataEncoded = json_encode(array("Project"=>$data));

		// var_dump($jsonDataEncoded);

		// var_dump($data['project_id']);

		$url_send = $this->server_path."/accept/mentorship";
		self::sendPostData($url_send, $jsonDataEncoded);

		$obj = new ViewProject();
		$obj->view($data['project_id'], $recall=true);
	}	
	


	function setActivityDiscussion($data){
		$jsonDataEncoded = json_encode(array("Project-Discussion"=>$data));

		// var_dump($jsonDataEncoded);

		// var_dump($data['project_id']);

		$url_send = $this->server_path."/add/project/activity/discussion";
		self::sendPostData($url_send, $jsonDataEncoded);
		$obj = new ViewProject();
		$obj->view($data['project_id'], $recall=true);
	}


	function setGeneralDiscussion($data){
		$jsonDataEncoded = json_encode(array("Project-Discussion"=>$data));

		// var_dump($jsonDataEncoded);

		// var_dump($data['project_id']);

		$url_send = $this->server_path."/add/project/discussion";
		self::sendPostData($url_send, $jsonDataEncoded);
		$obj = new ViewProject();
		$obj->view($data['project_id'], $recall=true);
	}
	


}

$grad = new Grad();

?>