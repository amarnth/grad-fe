<?php
/**
* 
*/
class ViewProject extends Grad
{
	
	function view($id, $recall=false)
	{

		// echo $id;

		if(!$recall){
			$decipher = new Decipher();
			
			$id = $decipher->base64_decrypt($id);
		}

		// $json = file_get_contents($this->path.'/grad_assets/js/json/project_view.json');
		$json = file_get_contents('http://gradpowered.net:9558/get/project/details/'.$id);

		$data = json_decode($json);

		$project = $data->ProjectDetails;

		$activity = $project->Activity;
		// var_dump($project);
		?>

		<div style="display: none;" data-id="<?php echo $id; ?>" class="over-view-of-project"></div>

		<div class="col-md-7 col-lg-6">
	        <div class="col-separator col-separator-first box col-unscrollable">
	            <div class="col-table">
	                <div class="col-table-row">
	                    <div class="col-app col-unscrollable">
	                        <div class="col-app">


	                        <div class="widget innerAll bg-white border-none">
				                <div class="widget widget-body-gray margin-none">
				                    <div class="widget-body text-center">

				                        <i class="<?php echo $project->icon; ?> fa-5x"></i>

				                        <h4>
				                            <?php echo $project->project_name; ?>
				                        </h4>
				                        <p class="margin-none">
				                            <?php echo $project->project_description; ?>
				                            <br>
				                            Created by <a href=""><?php echo $project->creator_name; ?></a>
				                           <!--  <small class="text-center"><i class="fa fa-clock-o"></i>
		                                    	<?php
		                                    		echo $created->on;
		                                    	?>
		                                    </small> -->
				                        </p>
				                    </div>
				                </div>
				            </div>

				            <div class="col-separator-h"></div>

				            <div class="input-group activity-search">
								<input class="form-control col-md-12 innerAll" id="appendedInputButtons" type="text" placeholder="Search">
								<div class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="col-separator-h"></div>
							

	                            <!-- DISCUSSIONS START -->
	                           <div class="row">
	                            <?php

	                            	$i = 1;

	                            	foreach ($activity as $key => $value) {
	                            			
	                            		$activity_id = $value->activity_id;

	                            		$activity_to = $value->to;

	                            		// var_dump($value);
	                            ?>
	                          		<div class="widget activity-discussion col-md-12 padding-none" data-toggle="collapse-widget" data-collapse-closed="true" data-title="<?php echo $value->activity_name.' @'.$activity_to->name; ?>">	
	                                    <div class="widget-head border-bottom" style="background: #FFF;">
	                                        <h4 class="heading">
	                                            <?php echo $value->activity_name; ?> <span class="pull-right"><span class="col-md-1"> - </span> <?php echo $activity_to->name; ?></span>
	                                        </h4>
	                                    </div>
	                                    <div class="widget-body col-md-12 padding-none">

	                                        <!-- col -->
	                                        <div class="col-md-12 col-sm-12 reset-components">
	                                            <div class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none widget-activity-social">
	                                                <!-- Tabs Heading -->
	                                                <div class="widget-head">
	                                                    <ul>
	                                                        <li class="active">
	                                                            <a data-toggle="tab" href="#shareTab<?php echo $activity_id; ?>" class="glyphicons user"><i></i></a>
	                                                        </li>
	                                                        <li>
	                                                            <a data-toggle="tab" href="#linkTab<?php echo $activity_id; ?>" class="glyphicons link"><i></i></a>
	                                                        </li>
	                                                        <li>
	                                                            <a data-toggle="tab" href="#fileShare<?php echo $activity_id; ?>" class="glyphicons file"><i></i></a>
	                                                        </li>
	                                                    </ul>
	                                                </div>
	                                                <!-- // Tabs Heading END -->
	                                                <div class="widget-body padding-none">
	                                                    <div class="tab-content">
	                                                        <div class="tab-pane active" id="shareTab<?php echo $activity_id; ?>">
	                                                            <div class="share">
	                                                                <textarea class="form-control desc_<?php echo $activity_id; ?>" rows="2" placeholder="Share what you've been up to..."></textarea>
	                                                                <a data-id="<?php echo $activity_id; ?>" class="btn btn-primary btn-sm post-activity-discussion" >Submit <i class="fa fa-arrow-circle-o-right"></i></a>
	                                                            </div>
	                                                        </div>

	                                                        <div class="tab-pane" id="linkTab<?php echo $activity_id; ?>">
	                                                            <div class="share">
	                                                                <textarea class="form-control link_<?php echo $activity_id; ?>" rows="2" placeholder="Share a link ..."></textarea>
	                                                                <a data-id="<?php echo $activity_id; ?>" class="btn btn-default post-activity-link" >Share <i class="text-primary fa fa-arrow-circle-o-right"></i></a>
	                                                            </div>
	                                                        </div>


	                                                        <div class="tab-pane innerAll text-center" id="fileShare<?php echo $activity_id; ?>">
	                                                            <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                                                                <div class="btn btn-default btn-file" id="mulitplefileuploader<?php echo $i; ?>">Select</div>
	                                                                <div id="newPostPreview<?php echo $i; ?>"></div>
	                                                                <div id="mulitplefileuploadStatus<?php echo $i; ?>"></div>
	                                                            </div>
	                                                            <?php $i++; ?>
	                                                           
	                                                            <div class="share">
	                                                                <textarea id="file_post_wall" class="form-control share_<?php echo $activity_id; ?>" rows="2" placeholder="Share what you've been up to..."></textarea>
	                                                                <a data-id="<?php echo $activity_id; ?>" class="btn btn-primary btn-sm post-activity-file-share" >Submit <i class="fa fa-arrow-circle-o-right"></i></a>
	                                                            </div>
	                                                        </div>
	                                                        
	                                                        
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!-- // END col -->

	                                        <div class="col-md-12 padding-none">
	                                        <?php
	                                        	foreach ($value->Discussion as $k => $v) {

	            									// $time_stamp = $v->time_stamp;

													// $time = time(); // current Unix timestamp

													// $diff = Epoch::time_diff($time, $time_stamp);

	                                        		$pic = $this->path.'/grad_assets/'.$v->pic;

	                                        		// var_dump($v);

	                                        ?>

	                                      	<div class="media innerAll border-bottom margin-none">
	                                            <a href="" class="pull-left">
	                                                <img style="width: 35px; height: 35px;" src="<?php echo $pic; ?>" alt="people" class="img-responsive img-circle" />
	                                            </a>
	                                            <div class="media-body">
	                                                <small class="strong pull-right"><?php //echo $diff; ?></small>
	                                                <h5>
	                                                    <?php echo $v->name; ?>
	                                                </h5>
	                                                <p class="innerB border-bottom"><?php // echo $v->role; ?></p>
	                                                <p><?php echo $v->comment; ?>.</p><br/>
	                                                <?php if($v->media){
	                                                	$file_paths = $this->path.'/grad_assets/'.$v->file_paths;
	                                                	?>
	                                                		<img style="width: 250px; height: 250px;" src='<?php echo $file_paths; ?>'/>
	                                                	<?php
	                                                }
	                                                ?>
	                                            </div>
	                                        </div>


	                                        <?php
	                                        	}
	                                        ?>
	                                        </div>

	                                    </div>
	                                </div>
	                                <!-- // Collapsible Widget END -->
	                        <?php

	                        	}
	                        ?>



	                                <div class="col-separator-h"></div>
	                            </div>
	                            <!-- // END DISCUSSIONS -->
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<?php

		$viewproject = new ViewProject();

		$viewproject->getMembersAndMentors($project);

		$viewproject->getGeneralDiscussion($project);
		
	}

	function getMembersAndMentors($data){
			$members = $data->members;
			$mentors = $data->mentors;

			// $members_count = (array) $members;
			
		?>
		<div class="col-lg-3 col-md-5">
	        <div class="col-separator box">
	            <!-- FRIENDS START -->
	            
	            <h4 class="heading">
	                        Members <span class="badge badge-primary badge-stroke pull-right" style="position: absolute; right: 30px; top: 8px;"><?php echo count($members); ?></span>
	                    </h4>
	                    
	                    
	            <!-- Collapsible Widget -->
	            <!-- <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
	                <div class="widget-head border-bottom" style="background: #FFF;">
	                    <h4 class="heading">
	                        Members <span class="badge badge-primary badge-stroke pull-right" style="position: absolute; right: 30px; top: 8px;"><?php echo count($members); ?></span>
	                    </h4>
	                </div>
	                <div class="widget-body">

	                    <div class="form-group get-staffs">
	                        <label class="control-label">Select Student</label>
	                        <select multiple="multiple" style="width: 100%;" id="invite-members">
	                        </select>
	                    </div>

	                    <div class="text-center innerAll">
	                        <a class="btn btn-primary invite-member" data-dismiss="modal" aria-hidden="true">Invite</a>
	                    </div>

	                </div>
	            </div> -->
	            <!-- // Collapsible Widget END -->

	            

	           <!--  <h4 class="border-bottom margin-none innerAll">
	                Members <a href="" class="btn btn-primary btn-xs pull-right " style="margin-left: 15px;"><i class="fa fa-plus-circle"></i></a> <span class="badge badge-primary badge-stroke pull-right">5</span> 
	            </h4> -->

	            <div class="innerAll text-center project-members-list">
	                <?php
	                	foreach ($members as $key => $value) {
	                		$pic = $this->path.'/grad_assets/'.$value->pic;
	                	?>
	                	<a href="#" class="border-none" data-id="<?php echo $value->id; ?>" data-name="<?php echo $value->name; ?>">
		                    <img style="width: 35px; height: 35px;" src="<?php echo $pic; ?>" alt="photo" width="35">
		                </a>
	                	<?php
	                	}
	                ?>
	            </div>
	            <!-- // END FRIENDS -->
	            <div class="col-separator-h"></div>
	                <!-- Collapsible Widget -->
	                <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
	                    <div class="widget-head bg-inverse margin-none innerAll">
	                        <h4 class="heading bg-inverse">
	                            Mentors
	                        </h4>
	                    </div>
	                    <div class="widget-body">

	                        <div class="form-group get-staffs">
	                            <label class="control-label">Select Staff</label>
	                            <select multiple="multiple" style="width: 100%;" id="invite-mentors">
	                            </select>
	                        </div>

	                        <div class="text-center innerAll">
	                            <a class="btn btn-primary invite-mentor" data-dismiss="modal" aria-hidden="true">Invite</a>
	                        </div>

	                    </div>
	                </div>
	                <!-- // Collapsible Widget END -->
	           <!--  <h4 class="bg-inverse margin-none innerAll">
	                Mentors <a href="#invite-mentor" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus-circle"></i></a>
	            </h4> -->
	            <div id="events-speakers" class="owl-carousel owl-theme">

	            	<?php
	            		if($mentors !== ""){
	            			foreach ($mentors as $key => $value) {
		            			$pic = $this->path.'/grad_assets/'.$value->pic;
		            			?>
	            				<div class="item">
				                    <div class="widget widget-pinterest">
				                        <div class="widget-body padding-none">
				                            <a href="" class="thumb col-md-12">
				                                <img src="<?php echo $pic;?>" alt="photo" class="img-responsive" />
				                            </a>
				                            <div class="description col-md-12">
				                                <h5 class="text-uppercase">
				                                    <?php echo $value->name;?>
				                                </h5>
				                                <p class="strong"><?php echo $value->dept;?>.</p>
				                            </div>
				                        </div>

				                    </div>
				                </div>
		            			<?php
		            		}
	            		}	

	            		if($data->invited_mentors){
	            			foreach ($data->invited_mentors as $key => $value) {
	            				# code...
	            				if($value==$this->user_id){
	            				?>
		            			<div class="text-center col-md-12" style="padding: 10px;">
			                                <div class="btn-group mentor">
			                                    <button class="btn btn-success"><i class="fa fa-fw fa-check"></i> Accept</button>
			                                    <button class="btn btn-default"> Deny</button>
			                                </div>
		                            	</div>
		            			<?php
		            			}
		            		}
	            		}
	            	?>
	                
	            </div>



	            <!-- Collapsible Widget -->
	            <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
	                <div class="widget-head bg-inverse margin-none innerAll">
	                    <h4 class="heading bg-inverse">
	                        Edit
	                    </h4>
	                </div>
	                <div class="widget-body">

	                    <div class="form-group get-student">
	                        <label class="control-label">Select Icon</label>
	                        <select style="width: 100%;" id="select-icon">
	                            
	                        </select>
	                    </div>


	                    <div class="form-group get-student">
	                        <label class="control-label">Select Color</label>
	                        <input type="text" id="colorpickerColor" class="form-control" value="#5a6a87" />
	                        <div class="separator bottom"></div>
	                        <div id="colorpicker"></div>
	                    </div>

	                    <div class="text-center innerAll">
	                        <a class="btn btn-primary create_project" >Update</a>
	                    </div>

	                </div>
	            </div>
	            <!-- // Collapsible Widget END -->

	        </div>
	    </div>
		<?php
	}


	function getGeneralDiscussion($data){

		$activity = $data->Activity;

		$discussion = $data->ProjectDiscussion;

		?>
		<div class="col-lg-3 col-md-12">
	        <div class="col-separator box">
	            <!-- SCHEDULE START -->
		
			
			
  		    <?php
  		    // echo $data->creator_id."<br>";
  		    // echo $this->user_id;
  		    
	            	if($this->user_id == $data->creator_id || "staff" == $this->user_role){
	            ?>
	            
	            <!-- Collapsible Widget -->
	            <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
	                <div class="widget-head border-bottom" style="background: #FFF;">
	                    <h4 class="heading ">
	                        Activity 
	                    </h4>
	                </div>
	                <div class="widget-body">

	                    <div class="form-group">
	                        <label class="control-label">To Student</label>
	                        <select multiple="multiple" style="width: 100%;" id="add-activity-members">
	                        </select>
	                    </div>


	                    <!-- <div class="form-group">
	                        <input type="text" class="form-control activity-name" placeholder="Activity Name">
	                    </div> -->

	                    <div class="form-group">
	                        <textarea class="form-control activity-name" placeholder="Activity Name"></textarea>
	                    </div>

	                    <div class="form-group">
	                        <input type="text" class="form-control activity-mark" placeholder="Mark">
	                    </div>

	                    <div class="form-group">
	                        <label class="control-label">Deadline</label>
	                        <div class="input-group date" id="activity-deadline">
	                            <input class="form-control" type="text">
	                            <span class="input-group-addon"><i class="fa fa-th"></i></span>
	                        </div>
	                    </div>

	                    <div class="text-center innerAll">
	                        <span class="btn btn-primary add-activity-to-member">Add</span>
	                    </div>
	                </div>
	            </div>
	            <!-- // Collapsible Widget END -->
		    
		    <?php } ?>
			
	           <!--  
	            <h4 class="innerAll margin-none border-bottom">
	                Activity <a href="" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-circle"></i></a>
	            </h4> -->
	            <ul class="list-unstyled timeline-appointments border-bottom bg-gray" style="max-height: 300px; overflow: auto;">

	            	<?php
	            		foreach ($activity as $key => $value) {
	            		?>
            			<li>
		                    <div class="time"><?php echo $value->created_on; ?></div> <!-- can have deadline or created on -->
		                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
		                    <div class="appointments">
		                        <ul class="list-unstyled">
		                            <li>
		                                <div class="apt">
		                                    <a href="" class="text-regular strong"><?php echo $value->assigned_to_name; ?></a>
		                                    <br/>
		                                    <?php echo $value->activity_name; ?>
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                </li>
	            		<?php
	            		}
	            	?>
	            </ul>
	            <!-- // END SCHEDULE -->
	            <div class="col-separator-h"></div>



	            <div class="col-md-12 padding-none">
		            <!-- DISCUSSIONS START -->

		            <div class="col-md-12 padding-none">
		                <!-- <a href="" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-circle"></i></a> -->

		                <!-- Collapsible Widget -->
		                <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
		                    <div class="widget-head border-bottom" style="background: #FFF;">
		                        <h4 class="heading">
		                            Discussions
		                        </h4>
		                    </div>
		                    <div class="widget-body">

		                        <div class="form-group">
		                           <textarea class="form-control project-discussion" placeholder=""></textarea>
		                        </div>

		                        <div class="text-center innerAll">
		                            <span class="btn btn-primary post-general-discussion">Post</span>
		                        </div>

		                    </div>
		                </div>
		                <!-- // Collapsible Widget END -->
		            </div>

		           	<div class="col-md-12 padding-none" style="overflow-y: scroll; max-height: 340px;">
			            <?php
		                	foreach ($discussion as $k => $v) {

								// $time_stamp = $v->time_stamp;

								// $time = time(); // current Unix timestamp

								// $diff = Epoch::time_diff($time, $time_stamp);

		                		$pic = $this->path.'/grad_assets/'.$v->pic;

		                		// var_dump($v);

		                ?>

		              	<div class="media innerAll border-bottom margin-none" style="background: #fff;">
		                    <a href="" class="pull-left">
		                        <img style="width: 35px; height: 35px;" src="<?php echo $pic; ?>" alt="people" class="img-responsive img-circle" />
		                    </a>
		                    <div class="media-body">
		                        <small class="strong pull-right"><?php //echo $diff; ?></small>
		                        <h5>
		                            <?php echo $v->name; ?>
		                        </h5>
		                        <p class="innerB border-bottom"><?php // echo $v->role; ?></p>
		                        <p><?php echo $v->comment; ?>.</p><br/>
		                        <?php if($v->media){
		                        	$file_paths = $this->path.'/grad_assets/'.$v->file_paths;
		                        	?>
		                        		<img style="width: 250px; height: 250px;" src='<?php echo $file_paths; ?>'/>
		                        	<?php
		                        }
		                        ?>
		                    </div>
		                </div>


		                <?php
		                	}
		                ?>
			            <!-- // END DISCUSSIONS -->
			        </div>

		        </div>
	           
	        </div>
	    </div>
		<?php
	}
}
?>