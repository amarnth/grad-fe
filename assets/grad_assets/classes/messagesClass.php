<?php
/**
* 
*/
class Messages extends Grad
{
	
	function get()
	{

		$json = file_get_contents($this->path.'/grad_assets/js/json/messages.json'); // this WILL do an http request for you

		$data = json_decode($json);

		$chats = $data->Chats;

		$i = 0;

		foreach ($chats as $key => $value) {
			# code...

			foreach ($value->conversation as $k => $v) {
				# code...
				if($v->status==0){
					$i++;
				}
			}

		}
		?>
        <!-- col -->
        <div class="col-md-3">
            <!-- col-separator -->
            <div class="col-separator col-separator-first box">
                <div class="innerAll border-bottom">
                    <form autocomplete="off" class="form-inline margin-none">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control find-person" placeholder="Find someone .." />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-xs pull-right"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="bg-primary innerAll half text-center"><?php echo $i; ?> new messages</div>
                <div class="innerAll">
                    <ul class="list-unstyled messages-list">

                    <?php 
                        $i = 1;
                        foreach ($chats as $key => $value) { 
                            $pic = $this->path."/grad_assets/".$value->pic;

                            $count_unread_messages = 0;

                            foreach ($value->conversation as $k => $v) {
                                if($v->status == 0){
                                    $count_unread_messages++;
                                }
                            }
                    ?>

                    <li class="bg-gray border-bottom <?php if($i==1){ echo "active"; } $i++;?>" data-id="<?php echo $value->id; ?>" data-title="<?php echo $value->name; ?>" style="cursor: pointer;">
                        <div class="media innerAll half">
                            <div class="media-object pull-left hidden-phone">
                                <img src="<?php echo $pic; ?>" alt="Image" style="width: 35px; height: 35px;" />
                            </div>
                            <div class="media-body">
                                <span class="strong"><?php echo $value->name; ?></span>

                                <?php foreach ($value->conversation as $k => $v) {  

                                    if($v->from){
                                            if (strlen($v->from) > 30)
                                                $v->from = substr($v->from, 0, 26) . '...';
                                        echo '<small>'.$v->from.'</small>';    
                                    }else{
                                        if (strlen($v->to) > 30)
                                                $v->to = substr($v->to, 0, 26) . '...';
                                        echo '<small>'.$v->to.'</small>';
                                    }

                                   
                                echo '<br/>';
                                echo '<small class="text-italic text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> '.$v->posted_on.'</small>';
                                    if($count_unread_messages!=0){
                                        echo '<span class="badge badge-primary badge-stroke pull-right">'.$count_unread_messages.'</span>';
                                    }

                                 break; 

                                } ?>
                            </div>
                        </div>
                    </li>

                    <?php } ?>
                    </ul>
                </div>
            </div>
            <!-- // END col-separator -->
        </div>
        <!-- // END col -->
		<?php
		# code...

        self::open($chats);

	}

	function open($data)
	{
		?>
		<!-- col -->
        <div class="col-md-9" id="open-message">
            <!-- col-separator -->
            <div class="col-separator box">
                <div class="innerAll">

                <?php
                    foreach ($data as $key => $value) {
                        # code...

                        $pic = $this->path."/grad_assets/".$value->pic;

                        $user_pic = $this->path."/grad_assets/".$this->user_pic;

                        $i=1;

                        foreach ($value->conversation as $k => $v) {
                            # code...

                ?>

                    <div class="media">

                        <?php
                             if($v->from){
                                ?>
                                <a href="" class="pull-left">
                                    <img src="<?php echo $pic;?>" width="100" alt="Image" /> 
                                </a>

                                <div class="media-body innerTB half">
                                    <div class="btn-group btn-group-xs pull-right">

                                        <?php if($i==1){ ?>
                                        <a href="#reply" data-toggle="collapse" class="btn btn-info"><i class="fa fa-reply"></i> reply</a>
                                        <?php } ?>
                                        <!-- <button class="btn btn-default"><i class="fa fa-reply text-primary"></i></button> -->
                                        <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                                    </div>
                                    <a href="" class="strong"><?php echo $value->name; ?></a>

                                    <p>
                                        <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> <?php echo $v->posted_on; ?></span>
                                    </p>
                                    <p class="margin-none"><?php echo $v->from; ?>.</p>

                                    <div class="clearfix"></div>

                                    <?php if($i==1){ ?>
                                    <div class="collapse" id="reply">
                                        <div class="innerAll bg-gray inner-2x border-bottom">
                                            <textarea class="notebook border-none form-control padding-none" rows="3" placeholder="Write your content here..."></textarea>
                                            <div class="text-right innerT">
                                                <button class="btn btn-success" data-id="">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } $i++; ?>

                                </div>

                                <?php
                            }else{
                                ?>
                                <a href="" class="pull-right">
                                    <img src="<?php echo $user_pic;?>" width="100" alt="Image" />
                                </a>

                                <div class="media-body innerTB half">
                                    <div class="btn-group btn-group-xs pull-right">
                                        
                                        <!-- <a href="#reply<?php echo $v->chat_id; ?>" data-toggle="collapse" class="btn btn-info"><i class="fa fa-reply"></i> reply</a> -->
                                        <!-- <button class="btn btn-default"><i class="fa fa-reply text-primary"></i></button> -->
                                        <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                                    </div>
                                    <a href="" class="strong"><?php echo $this->user_name; ?></a>

                                    <p>
                                        <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> <?php echo $v->posted_on; ?></span>
                                    </p>
                                    <p class="margin-none"><?php echo $v->to; ?>.</p>

                                    <!-- <div class="clearfix"></div>
                                    <div class="collapse" id="reply<?php echo $v->chat_id; ?>">
                                        <div class="innerAll bg-gray inner-2x border-bottom">
                                            <textarea class="notebook border-none form-control padding-none" rows="3" placeholder="Write your content here..."></textarea>
                                            <div class="text-right innerT">
                                                <button class="btn btn-success" data-id="<?php echo $v->chat_id; ?>">Submit</button>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>
                            <?php
                            }
                        ?>

                    </div>
                    <div class="innerAll border-bottom"></div>
                <?php   
                        }
                        break;
                    }
                ?>
                    <!-- <div class="innerAll border-bottom"></div>
                    <div class="media">
                        <a href="" class="pull-right">
                            <img src="assets/images/people/250/3.jpg" width="100" alt="Image" />
                        </a>
                        <div class="media-body innerTB half">
                            <div class="btn-group btn-group-xs">
                                <button class="btn btn-default"><i class="fa fa-reply text-primary"></i></button>
                                <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                            </div>
                            <div class="pull-right text-right">
                                <a href="" class="strong">joanne</a>

                                <p>
                                    <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> July 8, 2013 12:38 pm</span>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, nesciunt, quia, beatae sint tempora provident autem ex vero sapiente libero rem reprehenderit illum hic recusandae deleniti assumenda ad quas doloremque.</p>
                        </div>
                    </div>
                    <div class="innerAll border-bottom"></div>
                    <div class="media">
                        <a href="" class="pull-left">
                            <img src="assets/images/people/250/2.jpg" width="100" alt="Image" />
                        </a>
                        <div class="media-body innerTB half">
                            <div class="btn-group btn-group-xs pull-right">
                                <button class="btn btn-default"><i class="fa fa-reply text-primary"></i></button>
                                <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                            </div>
                            <a href="" class="strong">adrian</a>

                            <p>
                                <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> July 8, 2013 12:38 pm</span>
                            </p>
                            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, nesciunt, quia, beatae sint tempora provident autem ex vero sapiente libero rem reprehenderit illum hic recusandae deleniti assumenda ad quas doloremque.</p>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- // END col-separator -->
        </div>
        <!-- // END col -->
		<?php
	}




    function openMessage($id)
    {

        // echo $id;

        ?>

            <!-- col-separator -->
            <div class="col-separator box">
                <div class="innerAll">

                <?php

                $json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/messages.json'); // this WILL do an http request for you

                $data = json_decode($json);

                $chats = $data->Chats;

                // var_dump($data);

                foreach ($chats as $key => $value) {
                    # code...

                    $pic = $this->path."/grad_assets/".$value->pic;

                    $user_pic = $this->path."/grad_assets/".$this->user_pic;

                    if($value->id == $id){

                    $i=1;

                    foreach ($value->conversation as $k => $v) {
                            # code...

                ?>

                    <div class="media">

                        <?php
                             if($v->from){
                                ?>
                                <a href="" class="pull-left">
                                    <img src="<?php echo $pic;?>" width="100" alt="Image" /> 
                                </a>

                                <div class="media-body innerTB half">
                                    <div class="btn-group btn-group-xs pull-right">

                                        <?php if($i==1){ ?>
                                        <a href="#reply" data-toggle="collapse" class="btn btn-info"><i class="fa fa-reply"></i> reply</a>
                                        <?php } ?>
                                        <!-- <button class="btn btn-default"><i class="fa fa-reply text-primary"></i></button> -->
                                        <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                                    </div>
                                    <a href="" class="strong"><?php echo $value->name; ?></a>

                                    <p>
                                        <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> <?php echo $v->posted_on; ?></span>
                                    </p>
                                    <p class="margin-none"><?php echo $v->from; ?>.</p>

                                    <div class="clearfix"></div>

                                    <?php if($i==1){ ?>
                                    <div class="collapse" id="reply">
                                        <div class="innerAll bg-gray inner-2x border-bottom">
                                            <textarea class="notebook border-none form-control padding-none" rows="3" placeholder="Write your content here..."></textarea>
                                            <div class="text-right innerT">
                                                <button class="btn btn-success" data-id="">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } $i++; ?>

                                </div>

                                <?php
                            }else{
                                ?>
                                <a href="" class="pull-right">
                                    <img src="<?php echo $user_pic;?>" width="100" alt="Image" />
                                </a>

                                <div class="media-body innerTB half">
                                    <div class="btn-group btn-group-xs pull-right">
                                        <button class="btn btn-default btn-stroke"><i class="fa fa-times text-danger"></i></button>
                                    </div>
                                    <a href="" class="strong"><?php echo $this->user_name; ?></a>

                                    <p>
                                        <span class="text-faded"><i class="fa fa-fw fa-calendar"></i> <?php echo $v->posted_on; ?></span>
                                    </p>
                                    <p class="margin-none"><?php echo $v->to; ?>.</p>

                                </div>
                            <?php
                            }
                        ?>

                    </div>
                    <div class="innerAll border-bottom"></div>
                <?php   
                        }
                    }
                       
                    }
                ?>
                    
                </div>
            </div>
            <!-- // END col-separator -->
        <?php
    }




}
?>
