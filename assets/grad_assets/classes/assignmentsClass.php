<?php
/**
* 
*/
class Assignments extends Grad
{

	function get() {
		
		$role = $this->user_role;
		// $role = "student";
		$user_id = $this->user_id;
		$user_pic = $this->user_pic;
		
		// if($role=="staff"){
		// 	$json = file_get_contents($this->server_path.'/get/assignment/staff/'.$user_id); 
		// } else{
		// 	$json = file_get_contents($this->server_path.'/get/assignment/student/'.$user_id);
		// }

		$json = file_get_contents($this->server_path.'/get/assignments/'.$user_id);

		$json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/assignment.json'); // this WILL do an http request for you

		$raw_data = json_decode($json);

		$data = $raw_data->Assignments;

		// var_dump($data);

		if (!$raw_data->Assignments) {
			?>	
			<div class="col-lg-12 assignment_title" data-s="<?php echo $v->title." @".$v->author_name; ?>">
	            <div class="box-generic bg-primary-light text-center margin-none">
	                <div class="innerTB">
	                    <div class="innerAll"></div>
	                    <h3 class="strong">No Assignments Available!!</h3>
	                </div>
	            </div>
	        </div>

			<?php
		}else{
			foreach ($data as $k => $v) {

				$created_time_stamp = $v->created_time_stamp;


				$time = time(); // current Unix timestamp

				$diff = Epoch::time_diff($time, $created_time_stamp);

			?>

			<div class="col-lg-3 assignment_title" data-s="<?php echo $v->title." @".$v->author_name; ?>">
	            <div class="box-generic bg-primary-light text-center margin-none">
	                <div class="innerTB">
	                    <div class="innerAll"></div>
	                    <h5 class="strong">
	                    <?php
				        	$decipher = new Decipher();
			            	$base64_encrypt = $decipher->base64_encrypt($v->assignment_id);
			            ?>
			            <a href="index.php?page=assignment_view&aid=<?php echo $base64_encrypt;?>"> <?php echo $v->title; ?></a>
	                    </h5>
	                    <p><?php echo $v->topic; ?>.</p>
	                    <?php 
	                    	$string = strip_tags($v->description);
	                    	if (strlen($string) > 50) {
	                    		$stringCut = substr($string, 0, 50);
	                    		$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
	                    	}
	                    ?>
	                    <p><?php echo $string; ?>.</p>
	                    <div class="innerAll">
	                        <div class="progress bg-white progress-mini">
                        		<?php
                            		$d = $v->submitted_student/$v->total_student;
                            		$d = round($d*100);
                            	?>
	                            <div class="progress-bar progress-bar-primary" style="width: <?php echo $d; ?>%;"></div>
	                        </div>
	                    </div>
	                    <a href="index.php?page=assignment_view&aid=<?php echo $base64_encrypt;?>"><button class="btn btn-sm btn-primary">Completed <?php echo $v->submitted_student;?>/<?php echo $v->total_student;?> <i class="fa fa-fw fa-arrow-right"></i> </button></a>
	                </div>
	            </div>
	        </div>

			<?php 

			}
		}
	}


	function getAsStudent($marks, $data){  // for student

		$decipher = new Decipher();
		$base64_decrypt = $decipher->base64_decrypt($id);

		echo $id;
		echo $base64_decrypt;

		$json = file_get_contents($this->server_path.'/get/assignment/'.$base64_decrypt);

		$json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/assignment_view.json'); // this WILL do an http request for you

		$raw_data = json_decode($json);

		$data = $raw_data->Assignments;

		// echo $
		if(1 == $data->assignment_status){
		?>

			<div class="widget col-md-5">
				<div class="widget-body innerAll inner-2x">
	                <div class="innerAll text-center">
	                    <div class="innerAll">

	                        <h1>
	                            <?php echo $data->title; ?>
	                        </h1>

	                        <p><?php echo $data->description; ?>.</p>
	                        <p><?php echo $data->topic; ?>.</p>
	                        <p><?php echo $data->subject; ?>.</p>
	                        <button class="btn btn-inverse btn-lg deadline<?php echo $data->assignment_id; ?>"><?php echo $data->deadline; ?></button><br><br>
	                        <!-- <button class="btn btn-secondary answer-student" data-id="std001">Answer</button> -->
	                        <?php
	                        	if(!($data->submission_status == "completed")){
	                        		?>
										<a class="btn btn-primary open-modal-answer" href="#open-modal-answer" data-toggle="modal" data-hide='<?php echo $data->assignment_id; ?>'>Answer</a>
			                		<?php
	                        	}
	                        ?>

	                    </div>
	                </div>
				</div>
			</div>


			<div class="col-md-7">
				<div class="widget-body innerAll inner-2x">
					<?php $count = count($data->file_path); 
						if($count>1){
							for ($i=0; $i < $count; $i++) { 
	                    		// var_export($data->file_path[$i]);
	                    		
	                    		$path = $this->path."/grad_assets/".$data->file_path[$i];
	                    		# code...
	                    		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
	                    	}	
						}else{
							$path = $this->path."/grad_assets/".$data->file_path;
                		?>
                			<h1 style="width: 85%;">Assignment Attachment </h1> 
                			<a style="position: absolute; top: 22px; right: 25px;" href="<?php echo $path; ?>" download>download</a>
	                        
                		<?php
                			
                    		# code...
                    		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
						}
                    	

                    ?>
					<!-- <iframe frameborder='0' src='http://localhost/grad/assets/grad_assets/files/<?php echo $data->file_path;?>' width='100%' height='400'></iframe> -->
				</div>
			</div>
		
		<?php
		} else {
		?>


		<div class="widget-body innerAll inner-2x bg-secondary">
			
            <div class="innerAll text-center">
                <div class="innerAll">
                    <h1>
                        <?php echo $data->title; ?>
                    </h1>

                    <p><?php echo $data->description; ?>.</p>
                    <p><?php echo $data->topic; ?>.</p>
                    <p><?php echo $data->subject; ?>.</p>
                    <button class="btn btn-inverse btn-lg deadline<?php echo $data->assignment_id; ?>"><?php echo $data->deadline; ?></button>

                </div>
            </div>
            
		</div>


			<div class="widget col-md-7">
				<div class="widget-body innerAll inner-2x">
					<!-- Table -->
					<table class="dynamicTable tableTools table table-striped checkboxs" style="min-width: 100%;">
					  <!-- Table heading -->
					  <thead>
					    <tr>
					      <th class="text-center">
					        <div class="checkbox checkbox-single margin-none">
				                  <label class="checkbox-custom">
				                      <i class="fa fa-fw fa-square-o"></i>
				                      <input type="checkbox">
				                  </label>
				            </div>
					      </th>
					      <th>Name</th>
					      <th>Marks</th>
					      <th>Comments</th>
					      <th class="col-sm-2">Attachment</th>
					      <th>Submitted on</th>
					    </tr>
					  </thead>
					  <!-- // Table heading END -->
					  
					  <!-- Table body -->
					  <tbody>
				<?php				    
				foreach ($data->Marks as $key => $value) {
					
					$std_pic = $this->path."/grad_assets/".$value->std_pic;

					// $assignment_id = $value->assignment_id;
					$assignment_id = $data->assignment_id;

					$std_name = $value->std_name;
					$std_id = $value->std_id;
					$status = $value->status;
					$comment = $value->comments;
					$mark = $value->mark;
					$media = $value->media;
					$attachment = $value->attachment;
					$submitted_on = $value->submitted_on;
					$time_stamp = $value->time_stamp;


				?>
			    <!-- Table row -->
			    <tr class="gradeC">
			      <td class="text-center">
			        <div class="checkbox checkbox-single margin-none">
		                  <label class="checkbox-custom">
		                      <i class="fa fa-fw fa-square-o"></i>
		                      <input type="checkbox" checked="checked">
		                  </label>
		            </div>
			      </td>
	                <td>
	                    <!-- <img src="<?php echo $std_pic; ?>" width="20" class="img-circle" alt="people" /> -->
	                    <span><?php  echo $std_name; ?></span>
	                </td>
			      <td><?php if(-10167 != $mark){ echo $mark; } ?></td>
			      <td><?php 
			      	$count = count($comment);
			      	if(1<$count){
			      		echo $comment[0]; 
			      	}else{
			      		echo $comment; 
			      	}
			      	?></td>
			      <td class="center demo pdf">
			      	<?php
			      		$count = count($attachment);

			      		if(1<$count){
			      			$pattern = $std_id."!".$std_name."@".$attachment[0]."#".$mark."$".$comment[0]."&;".$assignment_id."*/student";	
			      		}else{
			      			$pattern = $std_id."!".$std_name."@".$attachment."#".$mark."$".$comment[0]."&;".$assignment_id."*/student";
			      		}
			      		
			      		if(-10167 != $mark){
			      	?>
					  <a class="open-attachment" href="#open-attachment" data-toggle="modal" data-hide='<?php echo $pattern; ?>'><i class="fa fa-file"></i></a>
					  <?php } ?>
			      </td>
			      <td class="center"><?php  echo $submitted_on; ?></td>
			    </tr>
			    <!-- // Table row END -->
				<?php 
				}
				?>

					  </tbody>
					  <!-- // Table body END -->
					  
					</table>
					<!-- // Table END -->
				</div>
			</div>

			<div class="col-md-5">
			
				<div class="widget-body innerAll inner-2x">
					
	                <div class="innerAll text-center">
	                    <div class="innerAll">
	                        <!-- <h1>
	                            <?php echo $data->title; ?>
	                        </h1>

	                        <p><?php echo $data->description; ?>.</p>
	                        <p><?php echo $data->topic; ?>.</p>
	                        <p><?php echo $data->subject; ?>.</p>
	                        <button class="btn btn-primary btn-lg deadline<?php echo $data->assignment_id; ?>"><?php echo $data->deadline; ?></button> -->
	                        <?php $count = count($data->file_path); 
	                        	if(1 < $count){
	                        		for ($i=0; $i < $count; $i++) { 
		                        		// var_export($data->file_path[$i]);
		                        		$path = $this->path."/grad_assets/".$data->file_path[$i];
		                        		# code...
		                        		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
		                        	}
	                        	}else{
										$path = $this->path."/grad_assets/".$data->file_path;
                        		?>
                        			<h2 style="width: 85%; font-weight: 100;">Assignment Attachment </h2> 
                        			<a style="position: absolute; top: 22px; right: 25px;" href="<?php echo $path; ?>" download>download</a>
			                        
                        		<?php
	                        		# code...
	                        		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
	                        	}
		                        ?>

	                    </div>
	                </div>
	                
				</div>
				
			</div>

		<?php
		}
	}

	function getAsStaff($id){ 	// for staff
		// var_dump($data);  

		$decipher = new Decipher();
		$base64_decrypt = $decipher->base64_decrypt($id);

		// echo $id;
		// echo $base64_decrypt;

		$json = file_get_contents($this->server_path.'/get/assignment/'.$base64_decrypt);

		$json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/assignment_view.json'); // this WILL do an http request for you

		$raw_data = json_decode($json);

		$data = $raw_data->Assignments;

		?>

		<div class="widget-body innerAll inner-2x bg-secondary">
			
            <div class="innerAll text-center">
                <div class="innerAll">
                    <h1>
                        <?php echo $data->title; ?>
                    </h1>

                    <p><?php echo $data->description; ?>.</p>
                    <p><?php echo $data->topic; ?>.</p>
                    <p><?php echo $data->subject; ?>.</p>
                    <button class="btn btn-inverse btn-lg deadline<?php echo $data->assignment_id; ?>"><?php echo $data->deadline; ?></button>

                </div>
            </div>
            
		</div>
		

		<div class="widget col-md-7">
			<div class="widget-body innerAll inner-2x" id="data-table-id<?php echo $data->assignment_id; ?>">
				<!-- Table -->
				<table class="dynamicTable tableTools table table-striped checkboxs" style="min-width: 100%;">
				  <!-- Table heading -->
				  <thead>
				    <tr>
				     <!--  <th class="text-center">
				        <div class="checkbox checkbox-single margin-none">
			                  <label class="checkbox-custom">
			                      <i class="fa fa-fw fa-square-o"></i>
			                      <input type="checkbox">
			                  </label>
			            </div>
				      </th> -->
				      <th>Name</th>
				      <th>Marks</th>
				      <th>Comments</th>
				      <th class="col-sm-2">Attachment</th>
				      <th>Submitted on</th>
				    </tr>
				  </thead>
				  <!-- // Table heading END -->
				  
				  <!-- Table body -->
				  <tbody>
		<?php				    
		foreach ($data->Marks as $key => $value) {
			
			$std_pic = $this->path."/grad_assets/".$value->std_pic;

			// $assignment_id = $value->assignment_id;
			$assignment_id = $data->assignment_id;
			$std_name = $value->std_name;
			$std_id = $value->std_id;
			$status = $value->status;
			$comment = $value->comments;
			$mark = $value->mark;
			$media = $value->media;
			$attachment = $value->attachment;
			$submitted_on = $value->submitted_on;
			$time_stamp = $value->submitted_time_stamp;

			$get = new Epoch();

			$time = time(); // current Unix timestamp

			$res = $get->time_diff($time, $time_stamp);

			if(!$res){
				$time_stamp = $submitted_on;
			}

			// $status = $value->status;


		?>
		    <!-- Table row -->
		    <tr class="gradeC student-assignment <?php echo $assignment_id; ?>" id="student#<?php echo $std_id; ?>">
		     	
                <td>
                    <!-- <img src="<?php // echo $std_pic; ?>" width="20" class="img-circle" alt="people" /> -->
                    <span><?php  echo $std_name; ?></span>
                </td>
                <?php
		      		if($status=="pending"){?>
		      			 <td >Pending</td>
		      			 <td></td>
		      			 <td></td>
		      			 <td></td>
		      	<?php } else { 
		      		if(-10167 != $mark){
		      	?>
		      <td contenteditable class="mark" id="mark<?php echo $std_id; ?>"><?php if(-10167 != $mark){ echo $mark; } ?></td>
		      <td contenteditable class="comment" id="comment<?php echo $std_id; ?>" style="width: 120px; word-break: break-word;">
		      	<?php 
		      		$count = count($comment);
			      	if(1<$count){
			      		echo $comment[0]; 
			      	}else{
			      		echo $comment; 
			      	}
		      	?></td>
		      <?php } else { ?>
		      
		      <td contenteditable class="mark" id="mark<?php echo $std_id; ?>"><?php if(-10167 != $mark){ echo $mark; } ?></td>
		      <td contenteditable class="comment" id="comment<?php echo $std_id; ?>" style="width: 120px; word-break: break-word;">
		      	<?php
		      		$count = count($comment);
			      	if(1<$count){
			      		echo $comment[0]; 
			      	}else{
			      		echo $comment; 
			      	}
		      	?></td>
		      
		      <?php } ?>
		      <td class="center demo pdf">
		      	<?php
		      		$count = count($attachment);

			      		if(1<$count){
			      			$pattern = $std_id."!".$std_name."@".$attachment[0]."#".$mark."$".$comment[0]."&;".$assignment_id."*/student";	
			      		}else{
			      			$pattern = $std_id."!".$std_name."@".$attachment."#".$mark."$".$comment[0]."&;".$assignment_id."*/student";
			      		}

		      		// if(-10167 != $mark){
		      	?>
		      	
				  <a class="open-attachment" href="#open-attachment" data-toggle="modal" data-hide='<?php echo $pattern; ?>'><i class="fa fa-file"></i></a>
				  
				  	<?php // } ?>
				 
		      </td>
		      <td class="center"><?php  echo $time_stamp; ?></td>
		      <?php } ?>
		    </tr>
		    <!-- // Table row END -->
		<?php 
			// self::previewAttachment($value);
		}
		?>

				  </tbody>
				  <!-- // Table body END -->
				  
				</table>
				<!-- // Table END -->
				<?php
					if(0 == $data->assignment_status){
				?>
					<button class="btn btn-primary save-student-mark" id="save#<?php echo $data->assignment_id;?>">Save</button>
					<button class="btn btn-primary publish-student-mark" id="publish#<?php echo $data->assignment_id;?>">Publish</button>
				<?php
					}
				?>
			</div>
		</div>


		
		<div class="col-md-5">
			
			<div class="widget-body innerAll inner-2x">
				
                <div class="innerAll text-center">
                    <div class="innerAll">
                        

                        <!-- <p><?php echo $data->description; ?>.</p>
                        <p><?php echo $data->topic; ?>.</p>
                        <p><?php echo $data->subject; ?>.</p>
                        <button class="btn btn-primary btn-lg deadline<?php echo $data->assignment_id; ?>"><?php echo $data->deadline; ?></button> -->
                        <?php $count = count($data->file_path); 
                        	if(1<$count){
                        		for ($i=0; $i < $count; $i++) { 
	                        		// var_export($data->file_path[$i]);
	                        		$path = $this->path."/grad_assets/".$data->file_path[$i];
	                        		# code...
	                        		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
	                        	}
                        	}else{
                        		$path = $this->path."/grad_assets/".$data->file_path;
                        		?>
                        			<h2 style="width: 85%; font-weight: 100;">Assignment Attachment </h2> 
                        			<a style="position: absolute; top: 22px; right: 25px;" href="<?php echo $path; ?>" download>download</a>
                        		<?php
	                        		# code...
	                        		echo "<iframe frameborder='0' src='".$path."' width='100%' height='400'></iframe>";
                        	}
	                        ?>
                    </div>
                </div>
                
			</div>
			
		</div>


		
		<?php
	}


	function previewAttachment($data){

		$str = explode("!", $data);
			$id = $str[0];
		$str = explode("@", $str[1]);
			$name = $str[0];
		$str = explode("#", $str[1]);
			$attachment = $str[0];
		$str = explode("$", $str[1]);
			$mark = $str[0];
		$str = explode("&;", $str[1]);
			$comment = $str[0];
		$str = explode("*/", $str[1]);
			$assignment_id = $str[0];
			$role = $str[1];

		?>

          <!-- Modal heading -->
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">
                  <?php echo $name; ?>
              </h3>
              
          </div>
          <!-- // Modal heading END -->
          <!-- Modal body -->
          <div class="modal-body">
              <div class="innerAll" id="preview-data">
					<div class="">
						<a href="<?php echo $this->path; ?>/grad_assets/<?php echo $attachment;?>" download style="float: right; margin-bottom: 10px;">download</a>
              			<iframe frameborder='0' src='<?php echo $this->path; ?>/grad_assets/<?php echo $attachment;?>' width='100%' height='400'></iframe>
              			<div class="form-group">
	 				  		<!-- <label>Marks</label> -->
	 				  		<?php
	 				  			if($role == "staff")
	 				  			{ ?>
				        			<!-- <input type="text" class="form-control student-mark_<?php echo $id;?>" placeholder="0" value='<?php echo $mark;?>'> -->
				        	<?php 	 				  				
	 				  			} elseif($role == "student") {
	 				  		?>
	 				  				<!-- <p><?php echo $mark;?></p> -->
	 				  		<?php 	 				  				
	 				  			}
	 				  		?>
				    	</div>
				      	<div class="form-group">
	 				  		<!-- <label>Comments</label> -->
	 				  		<?php
	 				  			if($role == "staff")
	 				  			{ ?>
				        			<!-- <input type="text" class="form-control student-comment_<?php echo $id;?>" placeholder="Nice Work" value='<?php echo $comment;?>'> -->
				        	<?php 	 				  				
	 				  			} elseif($role == "student") {
	 				  		?>
	 				  				<!-- <p><?php echo $comment;?></p> -->
	 				  		<?php 	 				  				
	 				  			}
	 				  		?>
				      	</div>
				      	<?php
 				  			if($role == "staff")
 				  			{ 
 				  		?>
				        	<!-- <button class="btn btn-primary save-student-mark" data-sid="<?php echo $id;?>" data-aid="<?php echo $assignment_id;?>">Save</button> -->
			        	<?php 	 				  				
 				  			}
 				  		?>
				      	
              		</div>
              </div>
          </div>


		<?php
	}



	function uploadStudentData($data){

		?>

          <!-- Modal heading -->
          <div class="modal-header">
              <button type="button" class="close assignment-modal-close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">
                  <?php // echo $data; ?>
                  Upload Your Answer
              </h3>
          </div>
          <!-- // Modal heading END -->
          <!-- Modal body -->
          <div class="modal-body">
              <div class="innerAll" id="">
					<div class="">

						<div class="alert alert-warning" style="padding: 6px; text-align: center;">
							<button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>!</strong> Please make sure, You cannot modify once you have uploaded the answer.
                        </div>


                        <span style="font-size: 12px; color: rgb(147, 50, 50);"><b>Note: </b>Only pdf files are allowed.</span>
						<div class="btn btn-default btn-file" id="mulitplefileuploader">Select</div>
	                    <div id="newPostPreview"></div>
	                    <div id="mulitplefileuploadStatus"></div>

	                    <button class="btn btn-primary upload-assignment-answer" data-id="<?php echo $data;?>">Upload</button>
              		</div>
              </div>
          </div>


		<?php
	}


	function getInfo($data){
	}

	function setStatus($id){
	}


}




?>