<?php
/**
* 
*/
class Timetable extends Grad
{
	
	function __construct()
	{
		# code...
	}

	function generate()
	{
			$days = 5;
			$periods = 5;
			$split = 0.45;
			$start = 9.30;
			$end = 5.00;

			$arr = array();
			
			$i = 0;

			// echo $start + $split;

			while($i<=$periods){
				$i++;
				if($i==0){
					$arr[$i] = $start+$split;
				}elseif($i==$periods){
					$arr[$i] = $arr[$i-1];
				}else{
					$arr[$i] = $arr[$i-1]+$split;
				}
			}

			// var_dump($arr);

	}

	function set()
	{
		echo "string";
		?>


	<!-- Widget -->
	<div class="widget">
		
		<!-- Widget heading -->
		<div class="widget-head">
			<h4 class="heading">Time Table</h4>
		</div>
		<!-- // Widget heading END -->
		
		<div class="widget-body innerAll inner-2x">
		
			<!-- Table -->
			<table class="table table-bordered table-primary">
				
				<!-- Table heading -->
				<thead>
					<tr>

					<?php
						$json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/getTimetable.json');
						$data = json_decode($json);
						
						$data = $data->Timetable;
						// var_dump($data);

						echo '<th class="center">day/time</th>';

						foreach ($data->timings as $key => $value) {
							# code...
							echo '<th class="center">'.$value.'</th>';
						}

					?>
					</tr>
				</thead>
				<!-- // Table heading END -->
				
				<!-- Table body -->
				<tbody>
					
					<!-- Table row -->
					
						<?php
							foreach ($data->schedule as $key => $value) {
								echo "<tr>";
								echo '<td class="center">'.$key.'</td>';
								foreach ($value as $key => $value) {
									echo '<td class="center">'.$value.'</td>';
								}
								echo "</tr>";
							}
						?>
					
					<!-- // Table row END -->
					
				</tbody>
				<!-- // Table body END -->
				
			</table>
			<!-- // Table END -->
			
		</div>
	</div>
	<!-- // Widget END -->


	<!-- col -->
   
    <!-- // END col -->
<script type="text/javascript">
	
</script>

	<?php
	}


	function get(){
		?>
			<div class="ui page grid">
				  <div class="fourteen wide column">         
				    <div class="ui inverted segment">

				      <?php
				      $json = file_get_contents('http://localhost/gradpower/js/json/getTimetable.json');

				      $data = json_decode($json);

				      $timetable = $data->Timetable;

				      $timings = $timetable->timings;

				      foreach ($timings as  $value) {
				//echo $value;
				# code...
				      }

				      $schedule = $timetable->schedule;
				      $tuesday = $schedule->Tuesday;
				      $monday = $schedule->Monday;
				      $wednesday = $schedule->Wednesday;
				      $thursday = $schedule->Thursday;
				      $friday = $schedule->Friday;
				      $saturday = $schedule->Saturday;

				      echo "<a class='ui ribbon red label'>Monday</a>";
				      foreach ($monday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>
				        <div class="ui very relaxed horizontal list">
				          <div class="item">

				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>

				        <?php
				      }

				      echo '<div class="ui divider"></div>';
				      echo "<a class='ui ribbon red label'>Tuesday</a>";

				      foreach ($tuesday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>

				        <div class="ui very relaxed horizontal list">
				          <div class="item"> 
				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>
				        <?php
				      }

				      echo '<div class="ui divider"></div>';
				     echo "<a class='ui ribbon red label'>Wednesday</a>";
				      foreach ($wednesday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>

				        <div class="ui very relaxed horizontal list">
				          <div class="item"> 
				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>
				        <?php
				      }

				      echo '<div class="ui divider"></div>';
				     echo"<a class='ui ribbon teal label'>". date("l") ."</a>";
				      foreach ($thursday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>

				        <div class="ui very relaxed horizontal list">
				          <div class="item" > 
				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>
				        <?php
				      }
				      echo '<div class="ui divider"></div>';
				      echo'<a class="ui ribbon  red label">Friday</a>';
				      foreach ($friday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>

				        <div class="ui very relaxed horizontal list">
				          <div class="item"> 
				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>
				        <?php
				      }
				      echo '<div class="ui divider"></div>';
				      echo'<a class="ui ribbon red label">Saturday</a>';


				      foreach ($saturday as $key => $value) {


				        list($sub, $staff) = split('_', $value);

				        ?>

				        <div class="ui very relaxed horizontal list">
				          <div class="item"> 
				            <div class="content <?php echo strtolower($sub);?>" >
				              <?php echo $sub;?> 
				            </div>
				            <span><?php echo $key;?> </span>
				          </div>
				        </div>
				        <?php
				      }


				      ?>

				    </div>
				  </div>
				</div>
	<?php

	}
}
?>