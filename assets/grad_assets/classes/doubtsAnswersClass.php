<?php
/**
* 
*/
class Answers extends Grad
{
	
	function get($id)
	{

		$json = file_get_contents($this->path.'/grad_assets/js/json/answers.json'); // this WILL do an http request for you
	
			$data = json_decode($json);

			$support = $data->Support;

			$posted_by = $support->posted_by;

			$pic = $this->path.'/grad_assets/'.$posted_by->pic;
		?>
		<div class="innerAll border-bottom media">
            <a class="pull-left" href="#">
                <img style="width: 54px; height: 54px;" src="<?php echo $pic; ?>" alt="<?php echo $posted_by->name; ?>">
            </a>
            <div class="media-body">
                <h3 class="margin-bottom-none padding-none">
                    <?php echo $support->title; ?>

                </h3>
                <span class="text-muted-dark">posted by <a href=""><?php echo $posted_by->name; ?></a> <i class="icon-time-clock fa fa-fw"></i> <?php echo $support->posted_on; ?></span>
            </div>
        </div>
        <div class="innerAll inner-2x">
            <p><?php echo $support->description; ?></p>
            <p class="text-faded">
                Thanks, 
                <br/>
                <em> <?php echo $posted_by->name; ?></em>
            </p>
        </div>
        <div style="display: none;" data-id="<?php echo $support->id; ?>" class="support-answers"></div>
        <div class="border-top border-bottom text-center innerAll bg-gray">
            <a href="#write" data-toggle="collapse" class="btn btn-primary strong btn-sm">Write an Answer <i class="icon-compose fa fa-fw"></i></a>
        </div>
        <div class="collapse" id="write">
            <div class="innerAll bg-gray inner-2x border-bottom">
                <textarea class="notebook border-none form-control padding-none" rows="3" placeholder="Write your content here..."></textarea>
                <div class="text-right innerT">
                    <button class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>

        <div class="innerAll border-bottom tickets">
            <ul class="media-list">

            <?php
            	foreach ($support->answers as $key => $value) {

            			$posted_by = $value->posted_by;

            		$pic = $this->path.'/grad_assets/'.$posted_by->pic;
            ?>
                <li class="media">
                    <a class="pull-left" href="#">
                        <img style="width: 54px; height: 54px;" class="media-object" src="<?php echo $pic; ?>" alt="..." >
                    </a>
                    <div class="media-body">
                        <div class="btn-group pull-right btn-group-xs">

                            	<a href="#reply<?php echo $value->id; ?>" data-toggle="collapse" class="btn btn-info"><i class="fa fa-reply"></i> reply</a>
                            	<?php
                            		if($value->is_approved){
                            	?>
                            		<span class="btn btn-default "><i class="fa fa-flag text-primary"></i></span>
                            	<?php
                            		}else{
                            	?>
                            		<span class="btn btn-default "><i class="fa fa-flag"></i></span>
                            	<?php
                            		}
                            	?>
                        </div>
                        <small>posted by <a href="" class="text-primary strong"> <?php echo $posted_by->name; ?> </a> <i class="icon-time-clock fw"></i> <?php echo $value->posted_on; ?></small>
                        <p><?php echo $value->comment; ?>.</p>
                        <div class="clearfix"></div>

                    	<div class="collapse" id="reply<?php echo $value->id; ?>">
				            <div class="innerAll bg-gray inner-2x border-bottom">
				                <textarea class="notebook border-none form-control padding-none" rows="3" placeholder="Write your content here..."></textarea>
				                <div class="text-right innerT">
				                    <button class="btn btn-success" data-id="<?php echo $value->id; ?>">Submit</button>
				                </div>
				            </div>
				        </div>

				        <?php 
				        	if($value->reply!=""){
				        		foreach ($value->reply as $k => $v) {

				        			$posted_by = $v->posted_by;

				        			$pic = $this->path.'/grad_assets/'.$posted_by->pic;
				        ?>

                        <div class="media">
                            <a class="pull-left" href="#">
                                <!-- <span class="empty-photo"><i class="icon-user-2 fa-2x"></i></span> -->
                                <img style="width: 54px; height: 54px;" class="media-object" src="<?php echo $pic; ?>" alt="" >
                            </a>
                            <div class="media-body">
                                <a href="" class="btn btn-default btn-xs pull-right"><i class="fa fa-flag"></i></a>
                                <small>posted by <a href=""><?php echo $posted_by->name; ?></a> <i class="icon-time-clock fw"></i> <?php echo $v->posted_on; ?></small>
                                <p><?php echo $v->comment; ?>.</p>
                                <div class="clearfix"></div>
                                <!-- <a class="btn btn-info btn-xs"><i class="fa fa-reply"></i></a> -->
                            </div>
                        </div>

                        <?php
                        		}
                        	}
                        ?>

                    </div>
                </li>
            <?php
            	}
            ?>

            </ul>
        </div>
		<?php
	}
}
?>