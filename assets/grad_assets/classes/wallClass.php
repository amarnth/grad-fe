<?php
/**
* 
*/
class Wall extends Grad
{

	function getWall($id="") {

		$role = $this->user_role;

		$user_id = $this->user_id;

		if($id!=""){
			$user_id = $id;
		}

		$user_pic = $this->user_pic;


		
	if($this->is_redis_up){
		try {
	
			$redis = new Redis();
			
		 	$redis->connect('127.0.0.1', 6379);
	
		  	$ping = $redis->ping();
	
		    $wall_data = $redis->lrange($user_id."_wall", 0 ,-1);
		    // echo "Connection to server sucessfully";
	
		    if(!$redis->EXISTS($user_id."_wall")){
		    	// echo ASSETS_PATH."/grad_assets/js/json/virginCall.json";
	
				$json = file_get_contents(ASSETS_PATH.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
	
				// $json = file_get_contents('http://192.168.1.102:9558/get/wall/'.$user_id); // this WILL do an http request for you
	
				$data = json_decode($json);
	
				$wall_data = $data->Wall;   // here append the user_id or user_name from session;
	
				foreach ($wall_data as $key => $value) {
					$comments = $value->comments;
	
						$redis_id = $user_id."_".$value->wall_id;
							
	
	
					foreach ($comments as $k => $v) {
	
							$comment_id = $user_id."_c_".$v->comment_id."_w_".$value->wall_id;
	
						$redis->set($comment_id, json_encode($v));
	
						$redis->lpush($redis_id."_comments", $comment_id);
					}
	
					$redis->set($redis_id, json_encode($value));
	
						$redis->set($redis_id."_wallrecepient", "std001,std002,stf001");
	
					$redis->lpush($user_id."_wall", $redis_id);
				}
	
				
			}else{
				$wall_data = $redis->lrange($user_id."_wall", 0 ,-1);
			}
	
			$is_redis = true;
	
		} catch (Exception $e) {
			// $json = file_get_contents(ASSETS_PATH.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
	
			$json = file_get_contents($this->server_path.'/get/wall/'.$user_id); // this WILL do an http request for you
	
			$data = json_decode($json);
	
			$wall_data = $data->Wall;
			// var_dump($wall_data);
	
			$is_redis = false;
	
		}
	}else{
	
			//$json = file_get_contents($this->path.'/grad_assets/js/json/virginCall.json'); 
	
			$json = file_get_contents($this->server_path.'/get/wall/'.$user_id);
	
			$data = json_decode($json);
	
			$wall_data = $data->Wall;
			// var_dump($wall_data);
	
			$is_redis = false;
	
	}
	
	

		foreach($wall_data as $k => $v){

			// var_dump($v);
			if($is_redis){
				$comments = $redis->lrange($v."_comments", 0 ,-1);	
				$v = json_decode($redis->get($v));			
			}else{
				$comments = $v->comments;	
			}

			
		$wall_id = $v->wall_id;
		$message_author = $v->message_author;
		$profile_picture = $v->profile_picture;
		$wall_message = $v->wall_message;
		$wall_info = $v->wall_info;


		$is_media_available = $v->is_media_available;
		$path_to_files = $v->path_to_files;

		$time_stamp = $v->time_stamp;

		$time = time(); // current Unix timestamp
			


		$diff = Epoch::time_diff($time, $time_stamp);

		// $date = new DateTime();
		// echo $date->format('Y-m-d H:i:s');

		$pic = $this->path.'/grad_assets/'.$profile_picture;

	?>
		 <li class="active">
		    <!-- <i class="list-icon fa fa-share"></i> -->
		    <i class="list-icon fa fa-user"></i>
		    <!-- <img src="<?php echo $pic; ?>" /> -->

		    <div class="block">
		        <div class="caret"></div>
		        <div class="box-generic">

		            <div class="timeline-top-info bg-gray border-bottom innerAll">
		            	<div class="pull-right label label-default"> <em><?php echo $diff; ?></em></div>
                        <h5 class="strong muted text-uppercase">
                            <i class="fa fa-user "></i> <?php echo $message_author; ?>
                        </h5>
                        <span>on <a href="#">Wall</a></span>
		            </div>

		            <div class="innerAll">
		            	<!-- <img src="<?php echo $pic; ?>"> -->
                    	<p class="margin-none"><?php echo $wall_message; ?></p>
                    	<?php
                    		$count = count($path_to_files);
                    		if($count>=0){
                    			for ($i=0; $i < $count; $i++) { 
	                    			# code...
	                    			// echo $path_to_files[$i];
	                    			$path = $this->path."/grad_assets/".$path_to_files[$i];


	                    			// check if string ends with image extension
									if (preg_match('/(\.jpeg|\.jpg|\.png|\.bmp)$/', $path_to_files[$i])) {
										if($i<6){
											echo "<img src='".$path."' style='height: 300px;'>";
										}
										if($i>6){
											echo "<div style='position: relative; top: -298px; right: 94px; float: right; background: rgba(0, 0, 0, 0.85); height: 296px; width: 29.5%; color: #fff;'>
													<i class='fa fa-plus fa-fw' style='  font-size: 41px; font-weight: normal; margin: 121px 110px;'></i>
												</div>";
											break;
										}
									// check if there is youtube.com in string
									} elseif (strpos($path_to_files[$i], "youtube.com") !== false) {
									    return "youtube";
									// check if there is vimeo.com in string
									} elseif (strpos($path_to_files[$i], "vimeo.com") !== false) {
									    return "vimeo";
									} else {
									    ?>
		                    				<a href="<?php echo $path;?>" target="_blank">attachment <?php echo $i;?></a><br>
		                    			<?php
									}

	                    			
	                    		}	
                    		}
                    	?>
					</div>

					

					<div class="border-bottom innerAll bg-gray">
                        <a href="" class="text-primary"><i class="fa fa-comment"></i> Comment</a>
                        &nbsp;

                        <a href="" class="text-primary"><i class="fa fa-share"></i> Share Post</a>
                    </div>
				<div class="innerAll">
					<ul class="list-group social-comments margin-none">
			            <?php 
			            $count = 0;
			            if($comments!=""){
			            			$comment = new Comment();
						  	$comment->getComment($comments, $count);
						}

						?>
  
                        <li class="list-group-item innerAll" style="margin: 0px; padding: 10px;">
                            <input id="<?php echo $wall_id; ?>" type="text" name="message" class="form-control newComment input-sm" placeholder="Comment here ...">
                        </li>

			        </ul>
		        </div>
		        
		        </div>
		    </div>
		</li>
	<?php 
		}
	}


	
	function viewWall ($data){


		$data = (object) $data;

		$author_id = $data->author_id;

		$profile_picture = $data->profile_picture;

		$message_author = $data->message_author;

		$date = $data->posted_on;

		$time_stamp = $data->time_stamp;

		$time = time(); // current Unix timestamp
		
		$diff = Epoch::time_diff($time, $time_stamp);

		$wall_message = $data->wall_message;

		$path_to_files = $data->path_to_files;

		$wall_id = $data->wall_id;

	?>

		<div class="ui segment">
		  <a class="ui ribbon label"><?php echo $wall_id; ?></a>

			<div class="ui comments">
			  <div class="comment">
			    <a class="avatar">
			    <?php 
			      echo "<img src='".$profile_picture."' />";
			    ?>
			    </a>
			    <div class="content">
			      <a class="author"><?php echo $message_author; ?></a>
			      <div class="metadata">
			        <div class="date"><?php echo $diff; ?><!-- 2 days ago --></div>
			        
			      </div>
			      <div class="text">
			        <div><?php echo nl2br($wall_message); ?></div>
			        <?php 
					if($path_to_files!=""){
						if (strpos($path_to_files,', ') !== false) {
				    		$image = explode(', ', $path_to_files);
				    		$imageCount = count($image);
				    		for($i=0;$i<$imageCount;$i++){
				    			echo "<img src='".$image[$i]."' style='width: 150px; height: 150px; float:left; margin: 15px;'>";
				    		}
						} else {
							echo "<img src='".$path_to_files."' style='width: 250px; height: 250px;'>";
						}
			        }
			        ?>
			      </div>
			      <div class="clearfix"></div>
			      <div class="actions">
			      	<a class="rating">
			          <i class="star icon"></i>
			          Faves
			        </a>
			        <a class="rely">Comment</a>
			        <a class="save">Share</a>
			        
			      </div>
			    </div>
			  </div>
				<?php 

				   echo "<div id='wallComments_".$wall_id."'>";

				  	if($comments!=""){
				  	         $comments = new Comment();
				  	         
				  		$comments->getComment($comments);
				  	}
				?>
			</div>


				<div class="submitNewComment">
					<div class="comment">
					    <a class="avatar">
					    <?php 
					    	echo "<img style='width: 36px; height: 34px; margin-top: -5px;' src='".$user_pic."' />";
					    ?>
					    </a>
					   	<div>
					   		<input id="<?php echo $wall_id; ?>" class="newComment" type="text" name="newComment">
					   		<i class="camera icon"></i>
							<input class="newCommentFile" type="file" name="files">
					   	</div>
					</div>
					<div class="newCommentPreview">
						<img src="">
					</div>
				</div>



		</div>
		<?php 
	}

	function getSingleWall($id){

		echo $id;

		if($this->is_redis_up){
			$redis = new Redis();

			try {
				
				$redis->connect('127.0.0.1', 6379);
				
				$wall_data = $redis->lrange($this->user_id."_wall", 0 ,-1);
	
				$is_redis = true;
	
			} catch (Exception $e) {
				$json = file_get_contents(ASSETS_PATH.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
	
				// $json = file_get_contents('http://192.168.1.102:9558/get/wall/'.$user_id); // this WILL do an http request for you
	
				$data = json_decode($json);
	
				$wall_data = $data->Wall;
				// var_dump($wall_data);
	
				$is_redis = false;
			}
		}else{
			$json = file_get_contents(ASSETS_PATH.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
	
				// $json = file_get_contents('http://192.168.1.102:9558/get/wall/'.$user_id); // this WILL do an http request for you
	
				$data = json_decode($json);
	
				$wall_data = $data->Wall;
				// var_dump($wall_data);
	
				$is_redis = false;
		}

		
    	

		foreach($wall_data as $k => $v){

			if($is_redis){
				$comments = $redis->lrange($v."_comments", 0 ,-1);	
				$v = json_decode($redis->get($v));			
			}else{
				$comments = $v->comments;	
			}

			if($v->wall_id==$id){
			
				$wall_id = $v->wall_id;
				$message_author = $v->message_author;
				$profile_picture = $v->profile_picture;
				$wall_message = $v->wall_message;
				$wall_info = $v->wall_info;


				$is_media_available = $v->is_media_available;
				$path_to_files = $v->path_to_files;

				$time_stamp = $v->time_stamp;

				$time = time(); // current Unix timestamp
					


				$diff = Epoch::time_diff($time, $time_stamp);

				// $date = new DateTime();
				// echo $date->format('Y-m-d H:i:s');

				$pic = $this->path.'/grad_assets/'.$profile_picture;
				?>
				<li class="active">
				    <!-- <i class="list-icon fa fa-share"></i> -->
				    <!-- <i class="list-icon fa fa-user"></i> -->
				    <!-- <img src="<?php echo $pic; ?>" /> -->

				    <div class="block">
				        <div class="caret"></div>
				        <div class="box-generic">

				            <div class="timeline-top-info bg-gray border-bottom innerAll">
				            	<div class="pull-right label label-default"> <em><?php echo $diff; ?></em></div>
		                        <h5 class="strong muted text-uppercase">
		                            <i class="fa fa-user "></i> <?php echo $message_author; ?>
		                        </h5>
		                        <span>on <a href="#">Wall</a></span>
				            </div>

				            <div class="innerAll">
				            	<!-- <img src="<?php echo $pic; ?>"> -->
		                    	<p class="margin-none"><?php echo $wall_message; ?></p>
		                    	<?php
		                    		$count = count($path_to_files);
		                    		if($count>=0){
		                    			for ($i=0; $i < $count; $i++) { 
			                    			# code...
			                    			// echo $path_to_files[$i];
			                    			$path = $this->path."/grad_assets/".$path_to_files[$i];


			                    			// check if string ends with image extension
											if (preg_match('/(\.jpeg|\.jpg|\.png|\.bmp)$/', $path_to_files[$i])) {
											    echo "<img src='".$path."' style='width: 25%; height: 250px;'>";
											// check if there is youtube.com in string
											} elseif (strpos($path_to_files[$i], "youtube.com") !== false) {
											    return "youtube";
											// check if there is vimeo.com in string
											} elseif (strpos($path_to_files[$i], "vimeo.com") !== false) {
											    return "vimeo";
											} else {
											    ?>
				                    				<a href="<?php echo $path;?>" target="_blank">attachment <?php echo $i;?></a><br>
				                    			<?php
											}

			                    			
			                    		}	
		                    		}
		                    	?>
							</div>

							

							<div class="border-bottom innerAll bg-gray">
		                        <a href="" class="text-primary"><i class="fa fa-comment"></i> Comment</a>
		                        &nbsp;

		                        <a href="" class="text-primary"><i class="fa fa-share"></i> Share Post</a>
		                    </div>
						<div class="innerAll">
							<ul class="list-group social-comments margin-none">
					            <?php 
					            $count = 0;
					            if($comments!=""){
					            	$comments = new Comment();
								  	$comments->getComment($comments, $count);
								}

								?>
		  
		                        <li class="list-group-item innerAll" style="margin: 0px; padding: 10px;">
		                            <input id="<?php echo $wall_id; ?>" type="text" name="message" class="form-control newComment input-sm" placeholder="Comment here ...">
		                        </li>

					        </ul>
				        </div>
				        
				        </div>
				    </div>
				</li>
			<?php
			}
		}
	}


}

?>