<?php
/**
* 
*/
class SocialAccount extends Grad
{

    function getHeader()
    {
        $pic = $this->path.'/grad_assets/'.$this->user_pic;

        ?>
          
            <span class="media margin-none">
                <span class="pull-left">
                    <!-- <img src="<?php echo $pic; ?>" alt="user" class="img-square" style="width: 30px; height: 30px;"> --> 
                    <span style="width: 30px; height: 30px; background: url('<?php echo $pic; ?>'); display: block; background-size: 38px 55px; background-position: -3px -1px; "></span>
                </span>
                <span class="media-body"> <?php echo $this->user_name; ?> <span class="caret"></span>  </span>
            </span>

        <?php

    }
    
    
    function get()
    {
        $role = $this->user_role;

        $user_id = $this->user_id;

        $user_pic = $this->user_pic;


        $json = file_get_contents($this->path.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you

        // $json = file_get_contents('http://192.168.1.109:9558/get/wall/'.$user_id); // this WILL do an http request for you

        $data = json_decode($json);

        $user_data = $data->UserInfo;


        $gender = $user_data->gender;
        $age = $user_data->age;
        $dob = $user_data->dob;
        $about = $user_data->about;
        $fname = $user_data->fname;
        $lname = $user_data->lname;
        $phone = $user_data->phone;
        $fb = $user_data->fb;
        $twitter = $user_data->twitter;
        // $email = $user_data->email;
        $skype = $user_data->skype;
        $google = $user_data->google;
        $website = $user_data->website;


        ?>

        <form class="form-horizontal" id="validateForm">
            <div class="tab-content">
                <!-- Tab content -->
                <div class="tab-pane active" id="account-details">
                    <!-- Row -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-6">
                            <!-- Group -->
                            <div class="form-group margin-none innerB">
                                <label class="col-md-3 control-label">First name</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input name="fname" type="text" value="<?php echo $fname; ?>" class="form-control" />
                                        <span class="input-group-addon" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="First name is mandatory"><i class="fa fa-question-circle"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- // Group END -->
                            <!-- Group -->
                            <div class="form-group margin-none innerB">
                                <label class="col-md-3 control-label">Last name</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input name="lname" type="text" value="<?php echo $lname; ?>" class="form-control" />
                                        <span class="input-group-addon" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Last name is mandatory"><i class="fa fa-question-circle"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- // Group END -->
                            <!-- Group -->
                            <div class="form-group margin-none innerB">
                                <label class="col-md-3 control-label">Date of birth</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input name="dob" type="text" id="datepicker1" class="form-control" value="<?php echo $dob; ?>" />
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- // Group END -->
                        </div>
                        <!-- // Column END -->
                        <!-- Column -->
                        <div class="col-md-6">
                            <!-- Group -->
                            <div class="form-group margin-none innerB">
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-9">
                                    <select class="form-control selectpicker">
                                        <?php 
                                            if($gender=="female"){
                                                ?> 
                                                    <option selected>Female</option><option>Male</option>
                                                    
                                                <?php        
                                            }else{
                                                ?>
                                                    <option selected>Male</option><option>Female</option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- // Group END -->
                            <!-- Group -->
                            <div class="form-group margin-none innerB">
                                <label class="col-md-3 control-label">Age</label>
                                <div class="col-md-9">
                                    <input name="age" type="text" value="<?php echo $age; ?>" class="form-control" />
                                </div>
                            </div>
                            <!-- // Group END -->
                        </div>
                        <!-- // Column END -->
                    </div>
                    <!-- // Row END -->
                    <div class="separator line bottom"></div>
                    <textarea name="about" id="mustHaveId" class=" form-control" rows="5"><?php echo $about; ?>.</textarea>
                    <!-- Form actions -->
                    <div class="separator top">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check-square-o"></i> Save changes</button>
                        <button type="button" class="btn btn-default"><i class="fa fa-fw fa-times"></i> Cancel</button>
                    </div>
                    <!-- // Form actions END -->
                </div>
                <!-- // Tab content END -->




                <!-- Tab content -->
                <div class="tab-pane" id="account-settings">
                    <!-- Row -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-3">
                            <strong>Change password</strong>

                            <p class="muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <!-- // Column END -->
                        <!-- Column -->
                        <div class="col-md-9">
                            <label for="inputUsername">Username</label>
                            <div class="input-group">
                                <input type="text" id="inputUsername" class="form-control" value="john.doe2012" disabled="disabled" />
                                <span class="input-group-addon" data-toggle="tooltip" data-placement="top" data-container="body" data-original-title="Username can't be changed"><i class="fa fa-question-sign"></i></span>
                            </div>
                            <div class="separator bottom"></div>
                            <label for="inputPasswordOld">Old password</label>
                            <div class="input-group">
                                <input name="old_pasword" type="password" id="inputPasswordOld" class="form-control" value="" placeholder="Leave empty for no change" />
                                <span class="input-group-addon" data-toggle="tooltip" data-placement="top" data-container="body" data-original-title="Leave empty if you don't wish to change the password"><i class="fa fa-question-sign"></i></span>
                            </div>
                            <div class="separator bottom"></div>
                            <label for="inputPasswordNew">New password</label>
                                <input name="new_pasword" type="password" id="inputPasswordNew" class="form-control" value="" placeholder="Leave empty for no change" />
                            <div class="separator bottom"></div>
                            <label for="inputPasswordNew2">Repeat new password</label>
                                <input name="repeat_new_pasword" type="password" id="inputPasswordNew2" class="form-control" value="" placeholder="Leave empty for no change" />
                            <div class="separator bottom"></div>
                        </div>
                        <!-- // Column END -->
                    </div>
                    <!-- // Row END -->
                    <div class="separator line bottom"></div>
                    <!-- Row -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-3">
                            <strong>Contact details</strong>

                            <p class="muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <!-- // Column END -->
                        <!-- Column -->
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="inputPhone">Phone</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input name="phone" value="<?php echo $phone; ?>" type="text" id="inputPhone" class="form-control" placeholder="01234567897" />
                                    </div>
                                    <div class="separator bottom"></div>
                                    <label for="inputEmail">E-mail</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input name="email" value="<?php echo $email; ?>" type="email" id="inputEmail" class="form-control" placeholder="contact@mosaicpro.biz" />
                                    </div>
                                    <div class="separator bottom"></div>
                                    <label for="inputWebsite">Website</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                        <input name="website" value="<?php echo $website; ?>" type="text" id="inputWebsite" class="form-control" placeholder="http://www.mosaicpro.biz" />
                                    </div>
                                    <div class="separator bottom"></div>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputFacebook">Facebook</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                        <input name="fb" value="<?php echo $fb; ?>" type="text" id="inputFacebook" class="form-control" placeholder="mosaicpro" />
                                    </div>
                                    <div class="separator bottom"></div>
                                    <label for="inputTwitter">Twitter</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                        <input name="twitter" value="<?php echo $twitter; ?>" type="text" id="inputTwitter" class="form-control" placeholder="mosaicpro" />
                                    </div>
                                    <div class="separator bottom"></div>
                                    <label for="inputSkype">Skype ID</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-skype"></i></span>
                                        <input name="skype" value="<?php echo $skype; ?>" type="text" id="inputSkype" class="form-control" placeholder="mySkypeID" />
                                    </div>
                                    <div class="separator bottom"></div>
                                    <label for="inputgplus">Google</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                        <input name="google" value="<?php echo $google; ?>" type="text" id="inputgplus" class="form-control" placeholder="google ID" />
                                    </div>
                                    <div class="separator bottom"></div>
                                </div>
                            </div>
                        </div>
                        <!-- // Column END -->
                    </div>
                    <!-- // Row END -->
                    <!-- Form actions -->
                    <div class="form-actions" style="margin: 0;">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check-square-o"></i> Save changes</button>
                    </div>
                    <!-- // Form actions END -->
                </div>
                <!-- // Tab content END -->
            </div>
        </form>
        <?php
    }
}
?>