<?php
/**
* 
*/
class Profile extends Grad
{
	
	    function viewProfile($id)
	    {
	        ?>
	        <h1 class="center" style=" margin: 150px; 0px; background: #EBEFF2; ">Oops! You are on wrong path</h1>
	        <?php
	    }
		
	function viewProfiles($id)
	{
	
		echo $id;

		$json = file_get_contents($this->path.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you

		$data = json_decode($json);

		$user_data = $data->UserInfo;

		// var_dump($user_data);

		$pic = $this->path.'/grad_assets/'.$user_data->prof_pic;

        echo $base64_decrypt = Decipher::base64_decrypt($id);

		?>

		<div class="row row-app margin-none">
            <!-- col -->
            <div class="col-sm-12 col-md-3 col-lg-3">
                <!-- col-separator -->
                <div class="col-separator col-unscrollable box reset-components">
                    <!-- col-table -->
					<div class="col-table">
		                <!-- Profile Photo -->
		                <div class="border-bottom">
		                    <a href="">
		                        <img src="<?php echo $pic; ?>" class="img-responsive img-clean"/>
		                    </a>
		                </div>
		                <div class="innerAll inner-2x text-center">
		                    <p class="lead strong margin-none"><?php echo $user_data->name; ?></p>
		                    <p class="lead">Year: <?php echo $user_data->year; ?></p>
		                    <div class="btn-group">
		                        <a href="" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Add Friend</a>
		                        <a href="" class="btn btn-primary btn-stroke"><i class="fa fa-envelope"></i></a>
		                    </div>
		                </div>
		                <div class="col-separator-h"></div>
		                <!-- col-table-row -->
		                <div class="col-table-row">
		                    <!-- col-app -->
		                    <div class="col-app col-unscrollable">
		                        <!-- col-app -->
		                        <div class="col-app">
		                            <div class="innerAll border-bottom bg-gray">
		                                <ul class="list-unstyled">
		                                	<p></p>
		                                    <li><p><i class="fa fa-graduation-cap"></i> Studied at <?php echo $user_data->school_name; ?></p></li>
		                                    <li><p><i class="fa fa-map-marker"></i> Lives in <?php echo $user_data->current_location; ?></p></li>
		                                    <!-- <li>User Experience</li> -->
		                                    <p></p>
		                                </ul>
		                            </div>

		                            <div class="innerAll">
		                                <h4 class="text-primary">
		                                    <i class=""></i> <a href='?contacts=<?php echo $user_data->id; ?>'>Friends</a>
		                                    <a> . </a>
		                                    <?php
		                                    	$array =  (array) $user_data->Friends;
		                                    	echo count($array);
		                                    ?>
		                                </h4>

		                                <ul class="list-unstyled">
		                                <?php
		                                	$i = 0;
		                                	foreach ($user_data->Friends as $k => $v) {
		                                		if($i<9){
													$prof_pic = $this->path.'/grad_assets/'.$v->prof_pic;
													// echo "<li style='float:left;'><a href='?profile=".$v->name_id."'><img style='width: 60px; height: 60px;' src='".$prof_pic."'><p style='margin: 0px; color:#fff; position: relative; top: -22px; left: 5px;'>".$v->name."</p></a></li>";
													?>
														<li data-toggle="tooltip" data-title="<?php echo $v->name; ?>" data-placement="bottom" style="float:left; margin: 1px;"><a href="?profile=<?php echo $v->name_id; ?>" style="display: block; position: relative;  background: url('<?php echo $prof_pic;?>') no-repeat; background-size: 70px 100px; width: 70px; height: 70px;">  </a></li>
														<!-- <span style="position: absolute; bottom: 3px; left: 3px; color: #fff;"><?php echo $v->name; ?></span> -->
													<?php
		                                		}

												$i++;
		                                	}
		                                ?>

		                                
		                                </ul>
		                            </div>
		                            <div class="clearfix"></div>
				                            <!-- <div class="innerAll">
				                                <h4 class="text-primary">
				                                    <i class=""></i> Education
				                                </h4>
				                                <div class="bg-gray innerAll half box-generic">
				                                    <p class="margin-none">Adobe Photoshop Cerficate</p>
				                                    <span class="label label-default ">on 16 Dec 2013</span>

				                                </div>
				                                <div class="bg-gray innerAll half box-generic">
				                                    <p class="margin-none">Bachelor Degree in Web Graphics</p>
				                                    <span class="label label-default ">on 16 Dec 2013</span>

				                                </div>
				                                <a href="#" class="btn btn-primary btn-xs">View all</a>

				                            </div> -->
		                        </div>
		                        <!-- // END col-app -->
		                    </div>
		                    <!-- // END col-app -->
		                </div>
		                <!-- // END col-table-row -->
		            </div>
		         	<!-- // END col-table -->
                </div>
                <!-- // END col-separator -->
            </div>
            <!-- // END col -->

            <!-- col -->
            <div class="col-lg-9 col-md-9 col-sm-12">
                <!-- col-separator -->
                <div class="col-separator col-unscrollable col-separator-last">
                    <!-- col-table -->
                    <div class="col-table">
                        <div class="innerAll border-bottom">
                            <h4 class="pull-left margin-none">
                                Short Bio
                            </h4>
                            <div class="dropdown pull-right dropdown-icons dropdown-icons-xs">
                                <a data-toggle="dropdown" href="#" class="btn btn-default btn-stroke btn-circle dropdown-toggle"><i class="fa fa-download"></i></a>
                                <ul class="dropdown-menu">
                                    <li data-toggle="tooltip" data-title="Resume" data-placement="left" data-container="body">
                                        <a href="#"><i class="fa fa-file"></i></a>
                                    </li>
                                    <li data-toggle="tooltip" data-title="vCard" data-placement="left" data-container="body">
                                        <a href="#"><i class="fa fa-link"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="innerAll inner-2x">
                            <p><?php echo $user_data->about; ?></p>
                            <!-- <a href="#" class="btn btn-info btn-sm"><i class="fa fa-download fa-fw"></i> Resume</a> -->
                        </div>
                        <div class="col-separator-h"></div>
                        <!-- col-table-row -->
                        <div class="col-table-row">
                            <!-- box -->
                            <div class="col-app box col-unscrollable overflow-hidden">
                                <!-- col-table -->
                                <div class="col-table">

                                    <div class="innerLR heading-buttons border-bottom">
                                        <h4 class="innerTB margin-none pull-left">
                                            Projects
                                        </h4>
                                        <!-- <button class="btn btn-default btn-xs pull-right"><i class="fa fa-cog "></i></button> -->
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="innerAll">
                                        <ul class="list-unstyled resume-documents">
                                            <li>
                                                <a href=""><i class="fa fa-file-o"></i> mosaicpro <span>17/12/2013</span></a>
                                            </li>
                                            <li>
                                                <a href=""><i class="fa fa-file-o"></i> Company LTD <span>17/12/2013</span></a>
                                            </li>
                                            <li>
                                                <a href=""><i class="fa fa-file-o"></i> Adrian <span>17/12/2013</span></a>
                                            </li>
                                            <li>
                                                <a href=""><i class="fa fa-file-o"></i> Bogdan <span>17/12/2013</span></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                       <!--  <div class="innerLR innerB">
                                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil fa-fw"></i> Write New </a>
                                        </div> -->
                                    </div>
                                    
                                    <div class="col-separator-h"></div>
                                    <div class="innerLR heading-buttons border-bottom">
                                        <!-- <h4 class="innerTB margin-none pull-left">
                                            Work History
                                        </h4> -->
                                        <div class="clearfix"></div>
                                    </div>


                                    <!-- col-table-row -->
                                    <div class="col-table-row">
                                        <!-- col-app -->
                                        <div class="col-app col-unscrollable">
                                            <!-- col-app -->
                                            <div class="col-app">
                                                <ul class="timeline-activity list-unstyled">
                                                   <?php
                                                   		Wall::getWall($id);
                                                   ?>
                                                </ul>
                                            </div>
                                            <!-- // END col-app -->
                                        </div>
                                        <!-- // END col-app -->
                                    </div>
                                    <!-- // END col-table-row -->


                                </div>
                                <!-- // END col-table -->
                            </div>
                            <!-- // END col-app.box -->
                        </div>
                        <!-- // END col-table-row -->
                    </div>
                    <!-- // END col-table -->
                </div>
                <!-- // END col-separator -->
            </div>
            <!-- // END col -->
        </div>
        <!-- // END row -->


		<?php
	}
}
?>