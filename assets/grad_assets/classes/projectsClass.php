<?php
/**
* 
*/
class Project extends Grad
{
	
	function getProjects()
	{
			$json = file_get_contents($this->server_path.'/get/projects/'.$this->user_id);

			// $json = file_get_contents($this->path.'/grad_assets/js/json/projects.json');

			// var_dump($json);
	
			$data = json_decode($json);

			$project = $data->Projects;

			if($project->count!=0){
		?>
		 	<div class="col-md-9">
		 <?php }else{
		 	?>
		 		<div class="col-md-12">
		 	<?php	
		 	} ?>
		 
			<div class="col-separator">


				<!-- START OVERVIEW -->
	            <div class="widget">
	                <div class="text-center innerAll inner-2x">
	                    <h2 class="innerT">
	                        <?php echo $project->count; ?> Projects
	                    </h2>
	                    <!-- <p class="lead"><span class="text-danger">import</span> <span class="text-success">knowledge</span></p> -->
	                    <p class="lead">
	                        <a href="#create-new-project" data-toggle="modal" class="btn btn-primary btn-sm btn-info"> <i class="fa fa-plus fa-fw"></i> Add Project</a>
	                        <!-- <a href="" class="btn btn-primary">Membership plans</a> -->
	                    </p>
	                </div>
	            </div>
	            <!-- // END OVERVIEW -->

	            <div class="col-separator-h"></div>

	            <?php 
	            if($project->count!=0){
	            ?>
	            <div class="innerAll">
	                <!-- Start Featured -->
	                <div class="innerLR">
	                	<div class="row ">
		                <?php
		                	$i = 0;

		                	$colors = array("bg-purple", "bg-lightred", "bg-mustard", "bg-primary", "bg-info", "bg-inverse");

		                	foreach ($project->details as $key => $value) {
		                		
		                		if($i==6){
	                				$i = 0;
	                			}
	                			
		                		?>

	                			<div class="col-sm-3">
		                            <!-- WIDGET START -->
		                            <div class="widget">
		                                <a href="#" class="display-block <?php echo $value->colour; // $colors[$i]; ?> innerAll text-center text-white" style="background: <?php echo $value->colour; ?>" >
		                                	<i class="<?php echo $value->icon; ?> fa-5x"></i>
		                                	<?php 
		                                		$pic = $this->path.'/grad_assets/'.$value->creator_pic;
		                                	?>
		                                	<!-- <img src="<?php echo $pic; ?>" style='width: 80px; height: 80px;'> -->
		                                	<p><?php echo $value->creator_name; ?></p>
		                                </a>
		                                <div class="text-center innerAll">
			                                <?php
			                                	$decipher = new Decipher();
		                                		$base64_encrypt = $decipher->base64_encrypt($value->project_id);
		                                	?>
		                                    <a href="index.php?page=project_view&pid=<?php echo $base64_encrypt;?>" class="strong "><?php echo $value->project_name; ?></a> 
		                                    <div class="clearfix"></div>
		                                    <small class="text-center"><i class="fa fa-clock-o"></i>
		                                    	<?php
		                                    		echo $value->created_on;
		                                    	?>
		                                    </small>
		                                </div>
		                            </div>
		                            <!-- // END WIDGET -->
		                        </div>

	                			<?php
	                			$i++;

		                	}
		                ?>
		            	</div>

	                    


	                </div>
	                <!-- End Featured -->
	            </div>

	            <?php } ?>
	           
	            <div class="col-separator-h"></div>
	        </div>
	    </div>

		<?php

		if($project->count!=0){
			$obj = new Project();
			$obj->getPopularProject($project);
		}
	}


	function getPopularProject($data){
		?>
			<div class="col-md-3">
	            <div class="col-separator col-separator-last">
	                <!-- START OFFER -->
	               <div class="row row-merge border-bottom">
	                    <div class="bg-gray">
	                        <!-- Start Most Viewed -->
	                        <h5 class="innerT innerLR text-center margin-none ">
	                            Most Viewed
	                        </h5>
	                        <?php $most_viewed = $data->most_viewed; 
	                        	foreach ($most_viewed as $k => $v) {
	                        		# code...
	                        ?>
	                        <div class="innerAll">
	                            <!-- START TODO -->
	                            <div class="widget innerAll margin-none">
	                                <a href="" class="display-block bg-inverse innerAll text-center">
	                                	<i class="text-white <?php echo $v->icon; ?> fa-5x"></i>
	                                	<p><?php echo $v->creator_name; ?></p>
	                                </a>
	                                <div class="text-center innerT">
	                                    <?php
	                                    	$decipher = new Decipher();
	                                		$base64_encrypt = $decipher->base64_encrypt($v->project_id);
	                                	?>
	                                    <a href="index.php?page=project_view&pid=<?php echo $base64_encrypt;?>" class="strong text-inverse "><?php echo $v->project_name; ?></a> 
	                                    <div class="clearfix"></div>
	                                    <small class="text-center"><i class="fa fa-clock-o"></i> <?php echo $v->created_on; ?></small>
	                                </div>
	                            </div>
	                            <!-- // END TODO -->
	                        </div>
	                        <!-- End Most Viewed -->
	                       	<?php break; } ?>
	                    </div>
	                    <div class="bg-gray">
	                        <!-- Start Popular this Month -->
	                        <h5 class="innerT innerLR text-center margin-none ">
	                            Popular this Month
	                        </h5>
	                        <?php $popular = $data->popular; 
	                        	foreach ($popular as $k => $v) {
	                        		# code...
	                        ?>
	                        <div class="innerAll">
	                           <!-- START TODO -->
	                            <div class="widget innerAll margin-none">
	                                <a href="" class="display-block bg-inverse innerAll text-center">
	                                	<i class="text-white <?php echo $v->icon; ?> fa-5x"></i>
	                                	<p><?php echo $v->creator_name; ?></p>
	                                </a>
	                                <div class="text-center innerT">
	                                	<?php
	                                		$decipher = new Decipher();
	                                		$base64_encrypt = $decipher->base64_encrypt($v->project_id);
	                                	?>
	                                    <a href="index.php?page=project_view&pid=<?php echo $base64_encrypt;?>" class="strong text-inverse "><?php echo $v->project_name; ?></a> 
	                                    <div class="clearfix"></div>
	                                    <small class="text-center"><i class="fa fa-clock-o"></i> <?php echo $v->created_on; ?></small>
	                                </div>
	                            </div>
	                            <!-- // END TODO -->
	                        </div>
	                        <!-- End Popular this Month-->
	                       	<?php break; } ?>
	                    </div>
	                </div>
	                <!-- // END OFFER -->
	                <div class="col-separator-h"></div>
	            </div>
	        </div>
		<?php
	}
}
?>