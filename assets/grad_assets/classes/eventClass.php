<?php

class Events extends Grad{

  function getInfo(){
    echo "EVENT Content";
  }


  function getEvents(){

      $user_id = $this->user_id;

      $user_role = $this->user_role;

      if($user_role=="admin"){

        // $json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/eventwall_data.json');
        $json = file_get_contents($this->server_path.'/get/admin/events/'.$user_id); // this WILL do an http request for you
        // var_dump($json);

      }else{

        // $json = file_get_contents('http://localhost/grad/assets/grad_assets/js/json/eventwall_data.json'); // this WILL do an http request for you
        $json = file_get_contents($this->server_path.'/get/event/info/'.$user_id); // this WILL do an http request for you
       
        // $data = json_decode($json);
      }
      

      $data = json_decode($json);
      
      

      $events_data = $data->Events;

      // var_dump($events_data->Hosted);

      
      $attending = $events_data->Attending;
      $hosted = $events_data->Hosted;


      $upcoming = $events_data->Upcoming;
      // $completed = $events_data->Completed;
?>
  <div class="tab-content">
  
<?php
      if($user_role=="admin"){

        $approval = $events_data->Approval;  

        $approved = $events_data->Approved;

        ?>

        <!-- Tab content -->
        <div class="tab-pane" id="tab-approved">
            <h4>
                <!-- Five tab -->
            </h4>
            <?php self::getApproved($approved); ?>
        </div>
        <div class="tab-pane" id="tab-approval">
            <h4>
                <!-- Six tab -->
            </h4>
            <?php self::getApproval($approval); ?>
        </div>
        <!-- // Tab content END -->

      <?php
      }

    if($user_role!="admin"){

      $pending = $events_data->Pending;

      $invites = $events_data->Invites;

    ?>

    <!-- Tab content -->
         <div class="tab-pane active" id="tab-invites">
            <h4>
              <!-- Five tab -->
            </h4>
            <?php self::getInvites($invites); ?>
        </div>
       
        <div class="tab-pane" id="tab-pending">
            <h4>
               <!--  Six tab -->
            </h4>
            <?php self::getPending($pending); ?>
        </div>
        <!-- // Tab content END -->
    <?php
      }

    ?>


    
        <!-- Tab content -->
        <div class="tab-pane" id="tab-event-all">
            <h4>
                <!-- First tab -->
            </h4>

          <?php self::getInvites($invites); ?>
          <?php self::getAttending($attending); ?>

        </div>
        <!-- // Tab content END -->
        <!-- Tab content -->
        <div class="tab-pane" id="tab-attending">
            <h4>
                <!-- Second tab -->
            </h4>
            <?php self::getAttending($attending); ?>
        </div>
        <!-- // Tab content END -->
        <!-- Tab content -->
        <div class="tab-pane" id="tab-hosted">
            <h4>
                <!-- Three tab -->
            </h4>
            <?php self::getHosted($hosted); ?>
        </div>
        <!-- // Tab content END -->
         <!-- Tab content -->
        <div class="tab-pane" id="tab-upcoming">
            <h4>
                <!-- Four tab -->
            </h4>
            <?php self::getUpcoming($upcoming); ?>
        </div>
        <!-- // Tab content END -->
    </div>

          

<?php

  }


    function getApproval($data){

      echo "<h1 class='center' style='color: rgb(16, 121, 113);'>Events waiting for your Approval</h1>";

       foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
          $host_id = $v->host_id;
          $host_name = $v->host_name;
          $host_pic = $v->host_pic;
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;

          $status = $v->approved;

          $host_pic = $this->path."/grad_assets/".$host_pic;

    ?>
    <div class="col-md-5 col-lg-3 ">
      <div class="col-separator col-separator-first box col-unscrollable">
          <!-- CALENDAR START -->
          <h4 class="bg-primary innerAll border-bottom margin-none center">
              <?php echo $event_name; ?>
          </h4>
          <div class="innerAll bg-primary border-bottom">
              <div class="media innerAll margin-none">
                  <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                  <div class="media-body">
                      <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                      <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                  </div>
              </div>
          </div>
          <div class="innerAll bg-gray border-bottom">
              <div class="media innerAll margin-none">
                  <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                  <div class="media-body">
                      <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                      <p class="strong margin-none"><?php echo $event_description; ?></p>
                  </div>
              </div>
          </div>
          <div class="innerAll text-center border-bottom">
              <div class="innerAll">
                  <div class="btn-group">
                      <?php echo '<a class="btn btn-success admin_approve" data-id="approve#'.$event_id.'"><i class="fa fa-fw fa-check"></i> Approve</a>
                      <a class="btn btn-default admin_decline" data-id="dismiss#'.$event_id.'">Dismiss</a>'; ?>
                  </div>
              </div>
          </div>
          <!-- // END CALENDAR -->
          <div class="col-separator-h"></div>
      </div>
    </div>
    <?php

      }
    }



    function getPending($data){

      echo "<h1 class='center' style='color: rgb(16, 121, 113);'>Events pending status</h1>";

       foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
          $host_id = $v->host_id;
          $host_name = $v->host_name;
          $host_pic = $v->host_pic;
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;

          $host_pic = $this->path."/grad_assets/".$host_pic;

    ?>

        <div class="col-md-5 col-lg-3 ">
          <div class="col-separator col-separator-first box col-unscrollable">
              <!-- CALENDAR START -->
              <h4 class="bg-primary innerAll border-bottom margin-none center">
                  <?php echo $event_name; ?>
              </h4>
              <div class="innerAll bg-primary border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                          <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                      </div>
                  </div>
              </div>
              <div class="innerAll bg-gray border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                          <p class="strong margin-none"><?php echo $event_description; ?></p>
                      </div>
                  </div>
              </div>
              <!-- // END CALENDAR -->
           </div>
        </div>

      <?php
      }
    }


    function getInvites($data){

      foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
          $host_id = $v->host_id;
          $host_name = $v->host_name;
          $host_pic = $v->host_pic;
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          $attending = $v->attending;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;

          $host_pic = $this->path."/grad_assets/".$host_pic;

          ?>

           <div class="col-md-5 col-lg-3 ">
              <div class="col-separator col-separator-first box col-unscrollable">
                  <!-- CALENDAR START -->
                  <h4 class="bg-primary innerAll border-bottom margin-none center">
                      <?php echo $event_name; ?>
                  </h4>
                  <div class="innerAll bg-primary border-bottom">
                      <div class="media innerAll margin-none">
                          <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                          <div class="media-body">
                              <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                              <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                          </div>
                      </div>
                  </div>
                  <div class="innerAll bg-gray border-bottom">
                      <div class="media innerAll margin-none">
                          <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                          <div class="media-body">
                              <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                              <p class="strong margin-none"><?php echo $event_description; ?></p>
                          </div>
                      </div>
                  </div>
                  <div class="innerAll text-center border-bottom">
                      <div class="innerAll">
                          <div class="btn-group">
                              <?php 
                                //echo count($attending);
                              echo '<a class="btn btn-success event_attending" data-id="attend#'.$event_id.'"><i class="fa fa-fw fa-check"></i> Attend</a>
                              <a class="btn btn-default event_decline" data-id="decline#'.$event_id.'">I might go</a>'; ?>
                          </div>
                      </div>
                  </div>
                  <!-- // END CALENDAR -->
                  <div class="col-separator-h"></div>
                  <!-- FRIENDS START -->
                   <!-- <h4 class="border-bottom margin-none innerAll">
                      Friends
                  </h4>
                  <div class="border-bottom">
                      <div class="innerAll"> <span class="badge badge-primary pull-right">215</span> Attending </div>
                  </div>
                  <div class="border-bottom">
                      <div class="innerAll"> <span class="badge badge-primary badge-stroke pull-right">585</span> Might Go </div>
                  </div>
                  <div class="innerAll text-center">
                      <a href="#" class="border-none">
                          <img src="../assets/images/people/80/1.jpg" alt="photo" width="35">
                      </a>
                      <a href="#" class="border-none">
                          <img src="../assets/images/people/80/2.jpg" alt="photo" width="35">
                      </a>
                      <a href="#" class="border-none">
                          <img src="../assets/images/people/80/4.jpg" alt="photo" width="35">
                      </a>
                      <a href="#" class="border-none">
                          <img src="../assets/images/people/80/14.jpg" alt="photo" width="35">
                      </a>
                  </div> -->
                  <!-- // END FRIENDS -->
              </div>
          </div>
     
          <?php

            }

      }


      function getHosted($data){

        foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
         
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          //$status = $v->status;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;

          $status = $v->status;
          
          $host_id = $this->user_id;
          $host_name = $this->user_name;

          $host_pic = $this->path."/grad_assets/".$this->user_pic;

    ?>

        <div class="col-md-5 col-lg-3 " data-gridalicious>
          <div class="col-separator col-separator-first box col-unscrollable">
              <!-- CALENDAR START -->
              <h4 class="bg-primary innerAll border-bottom margin-none center">
                  <?php echo $event_name; ?>
              </h4>
              <div class="innerAll bg-primary border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                          <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                      </div>
                  </div>
              </div>
              <div class="innerAll bg-gray border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                          <p class="strong margin-none"><?php echo $event_description; ?></p>
                      </div>
                  </div>
              </div>
              <?php if($this->user_role!="student" || $status=="approved"){?>
                <div class="innerAll bg-gray border-bottom">
                    <a href="#event-invite" data-toggle="modal" class="btn btn-default event-invite pull-right" data-id="<?php echo $event_id; ?>"> <i class="fa fa-plus fa-fw"></i> Invite </a>
                </div>
              <?php } else{
                ?>
                <div class="innerAll bg-gray border-bottom">
                    <a disabled="true" href="#event-invite" data-toggle="modal" class="btn btn-default event-invite pull-right" data-id="<?php echo $event_id; ?>"> <i class="fa fa-plus fa-fw"></i> Invite </a>
                </div>
                <?php
                } ?>
              <!-- // END CALENDAR -->
           </div>
        </div>

        <?php

        }

      }




      function getAttending($data){

        foreach($data as $k => $v){

            $event_id = $v->event_id;

            $event_name = $v->event_name;  
            $host_id = $v->host_id;
            $host_name = $v->host_name;
            $host_pic = $v->host_pic;
            $approved = $v->approved;
            $event_description = $v->event_description;
            $start_date = $v->start_date;
            $start_time = $v->start_time;
            $end_date = $v->end_date;

            $end_time = $v->end_time;

            $time_stamp = $v->time_stamp;

        $host_pic = $this->path."/grad_assets/".$host_pic;

     ?>

      <div class="col-md-5 col-lg-3 ">
        <div class="col-separator col-separator-first box col-unscrollable">
              <!-- CALENDAR START -->
              <h4 class="bg-primary innerAll border-bottom margin-none center">
                  <?php echo $event_name; ?>
              </h4>
              <div class="innerAll bg-primary border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                          <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                      </div>
                  </div>
              </div>
              <div class="innerAll bg-gray border-bottom">
                  <div class="media innerAll margin-none">
                      <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                      <div class="media-body">
                          <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                          <p class="strong margin-none"><?php echo $event_description; ?></p>
                      </div>
                  </div>
              </div>
              <!-- // END CALENDAR -->
           </div>
        </div>

        <?php

        }
      }





  function getUpcoming($data){

    foreach($data as $k => $v){

        $event_id = $v->event_id;

        $event_name = $v->event_name;  
        $host_id = $v->host_id;
        $host_name = $v->host_name;
        $host_pic = $v->host_pic;
        $approved = $v->approved;
        $event_description = $v->event_description;
        $start_date = $v->start_date;
        $start_time = $v->start_time;
        $end_date = $v->end_date;

        $end_time = $v->end_time;

        $time_stamp = $v->time_stamp;

        $host_pic = $this->path."/grad_assets/".$host_pic;

        ?>

         <div class="col-md-5 col-lg-3 ">
            <div class="col-separator col-separator-first box col-unscrollable">
                <!-- CALENDAR START -->
                <h4 class="bg-primary innerAll border-bottom margin-none center">
                    <?php echo $event_name; ?>
                </h4>
                <div class="innerAll bg-primary border-bottom">
                    <div class="media innerAll margin-none">
                        <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                        <div class="media-body">
                            <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                            <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                        </div>
                    </div>
                </div>
                <div class="innerAll bg-gray border-bottom">
                    <div class="media innerAll margin-none">
                        <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                        <div class="media-body">
                            <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                            <p class="strong margin-none"><?php echo $event_description; ?></p>
                        </div>
                    </div>
                </div>
                <!-- <div class="innerAll text-center border-bottom">
                    <div class="innerAll">
                        <div class="btn-group">
                            <?php echo '<a class="btn btn-success event_attending" data-id="attend#'.$event_id.'"><i class="fa fa-fw fa-check"></i> Attend</a>
                            <a class="btn btn-default event_decline" data-id="decline#'.$event_id.'">I might go</a>'; ?>
                        </div>
                    </div>
                </div> -->
                <!-- // END CALENDAR -->
                <div class="col-separator-h"></div>
                <!-- FRIENDS START -->
               <!-- <h4 class="border-bottom margin-none innerAll">
                    Friends
                </h4>
                <div class="border-bottom">
                    <div class="innerAll"> <span class="badge badge-primary pull-right">215</span> Attending </div>
                </div>
                <div class="border-bottom">
                    <div class="innerAll"> <span class="badge badge-primary badge-stroke pull-right">585</span> Might Go </div>
                </div>
                <div class="innerAll text-center">
                    <a href="#" class="border-none">
                        <img src="../assets/images/people/80/1.jpg" alt="photo" width="35">
                    </a>
                    <a href="#" class="border-none">
                        <img src="../assets/images/people/80/2.jpg" alt="photo" width="35">
                    </a>
                    <a href="#" class="border-none">
                        <img src="../assets/images/people/80/4.jpg" alt="photo" width="35">
                    </a>
                    <a href="#" class="border-none">
                        <img src="../assets/images/people/80/14.jpg" alt="photo" width="35">
                    </a>
                </div> -->
                <!-- // END FRIENDS -->
            </div>
        </div>

      <?php
      }

    }


    function getCompleted($data){

      foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
          $host_id = $v->host_id;
          $host_name = $v->host_name;
          $host_pic = $v->host_pic;
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;
      ?>

      <?php


      }

    }

    function getApproved($data){

      foreach($data as $k => $v){

          $event_id = $v->event_id;

          $event_name = $v->event_name;  
          $host_id = $v->host_id;
          $host_name = $v->host_name;
          $host_pic = $v->host_pic;
          $approved = $v->approved;
          $event_description = $v->event_description;
          $start_date = $v->start_date;
          $start_time = $v->start_time;
          $end_date = $v->end_date;

          $end_time = $v->end_time;

          $time_stamp = $v->time_stamp;
       $host_pic = $this->path."/grad_assets/".$host_pic;

   ?>

    <div class="col-md-5 col-lg-3 ">
      <div class="col-separator col-separator-first box col-unscrollable">
            <!-- CALENDAR START -->
            <h4 class="bg-primary innerAll border-bottom margin-none center">
                <?php echo $event_name; ?>
            </h4>
            <div class="innerAll bg-primary border-bottom">
                <div class="media innerAll margin-none">
                    <div class="pull-left"> <i class="fa fa-fw fa-calendar fa-3x"></i> </div>
                    <div class="media-body">
                        <p class="strong lead margin-none"><?php echo $start_date; ?></p>
                        <p class="strong margin-none"><?php echo "To: ".$end_date; ?></p>
                    </div>
                </div>
            </div>
            <div class="innerAll bg-gray border-bottom">
                <div class="media innerAll margin-none">
                    <div class="pull-left"> <?php echo "<img width='50' src='".$host_pic."' >"; ?> <!-- <i class="fa fa-fw fa-map-marker fa-3x"></i> --> </div>
                    <div class="media-body">
                        <p class="strong lead margin-none"><?php echo $host_name; ?></p>
                        <p class="strong margin-none"><?php echo $event_description; ?></p>
                    </div>
                </div>
            </div>
            <!-- // END CALENDAR -->
         </div>
      </div>
      <?php


      }

    }



    function createInvitees($data){

      ?>

              <!-- Modal heading -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">
                      Invite
                  </h3>
              </div>
              <!-- // Modal heading END -->
              <!-- Modal body -->
              <div class="modal-body">
                  <div class="innerAll">

                    <div class="form-group">
                        <label class="">Invite To: </label>
                        <div class="radio ">
                              <label class="radio-custom select-student col-md-3" style="padding: 0px;">
                                  <input type="radio" name="radio" checked="checked"> 
                                  <i class="fa fa-circle-o checked"></i> Individual
                              </label> 
                              <label class="radio-custom select-department col-md-3" style="padding: 0px;"> 
                                  <input type="radio" name="radio"> 
                                  <i class="fa fa-circle-o"></i> Department
                              </label> 
                          </div> 
                      </div>

                         
                            
                            <div class="form-group">
                              
                    </div>

                    <div class="form-group get-student">
                        <label class="control-label">Select Students</label>
                        <select multiple="multiple" style="width: 100%;" id="assignment-select-students">
                           <optgroup label="Alaskan/Hawaiian Time Zone">
                               <option value="AK">Alaska</option>
                               <option value="HI">Hawaii</option>
                           </optgroup>
                           <optgroup label="Pacific Time Zone">
                               <option value="CA"><span hidden style="display: none !important;">CA</span> California</option>
                               <option id="NV" value="NV">Nevada</option>
                               <option id="OR" value="OR">Oregon</option>
                               <option value="WA">Washington</option>
                           </optgroup>
                           <optgroup label="Mountain Time Zone">
                               <option value="AZ">Arizona</option>
                               <option value="CO">Colorado</option>
                               <option value="ID">Idaho</option>
                               <option value="MT">Montana</option><option value="NE">Nebraska</option>
                               <option value="NM">New Mexico</option>
                               <option value="ND">North Dakota</option>
                               <option value="UT">Utah</option>
                               <option value="WY">Wyoming</option>
                           </optgroup>
                        </select>
                    </div>

                    <div class="form-group get-department">
                        <label class="control-label">Select Department</label>
                        <select multiple="multiple" style="width: 100%;" id="assignment-select-department">
                         </select>
                    </div>

                    <a href="#" class="btn btn-primary event_invite_members" data-dismiss="modal" data-id="<?php echo $data; ?>" aria-hidden="true">Invite</a>
              </div>
      <?php
    }

}

?>