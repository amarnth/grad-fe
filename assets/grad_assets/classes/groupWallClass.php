<?php

class Groupwall extends Grad{


	function getWall ($id, $recall=false){

		// echo $id."<br>";

		// if(!$recall){
		// 	$decipher = new Decipher();
			
		// 	$id = $decipher->base64_decrypt($id);
		// }

		// echo $id;

		$decipher = new Decipher();
			
		$id = $decipher->base64_decrypt($id);
		
		
		
		$user_id = $this->user_id;

		$user_picture = $this->user_pic;

		// var_dump('http://localhost:8383/get/group/wall/'.$id);

		$json = file_get_contents($this->server_path.'/get/group/wall/'.$id); // this WILL do an http request for you
		
		// $json = file_get_contents($this->path.'/grad_assets/js/json/virginCall.json'); // this WILL do an http request for you
		
		$data = json_decode($json);

		//echo $id;

		$wall_data = $data->$id;

			foreach($wall_data as $k => $v){
				$comments = $v->comments;

			//var_dump($v);

			$wall_id = $v->wall_id;
			$message_author = $v->message_author;
			$profile_picture = $v->profile_picture;
			$wall_message = $v->wall_message;

			$is_media_available = $v->is_media_available;
			$path_to_files = $v->path_to_files;

			$time_stamp = $v->time_stamp;

			$time = time(); // current Unix timestamp
			
			$obj = new Epoch();
				
			$diff = $obj->time_diff($time, $time_stamp);
			
		
			//$diff = Epoch::time_diff($time, $time_stamp);

			$posted_on = $v->posted_on;


		?>

		<li class="active">
		    <!-- <i class="list-icon fa fa-share"></i> -->
		    <i class="list-icon fa fa-user"></i>
		    <!-- <img src="<?php echo $pic; ?>" /> -->

		    <div class="block">
		        <div class="caret"></div>
		        <div class="box-generic">


		            <div class="timeline-top-info bg-gray border-bottom innerAll">
		            	<div class="pull-right label label-default"> <em><?php echo $diff; ?></em></div>
                        <h5 class="strong muted text-uppercase">
                            <i class="fa fa-user "></i> <?php echo $message_author; ?>
                        </h5>
                        <span>on <a href="#">Wall</a></span>
		            </div>


		            <div class="innerAll">
		            	<!-- <img src="<?php echo $pic; ?>"> -->
                    	<p class="margin-none"><?php echo $wall_message; ?></p>
                    	
			<?php
			
                    		$count = count($path_to_files);
                    		if($count>=0){
                    			for ($i=0; $i < $count; $i++) { 
	                    			# code...
	                    			// echo $path_to_files[$i];
	                    			$path = $this->path."/grad_assets/".$path_to_files[$i];


	                    			// check if string ends with image extension
									if (preg_match('/(\.jpeg|\.jpg|\.png|\.bmp)$/', $path_to_files[$i])) {
										if($i<6){
											echo "<img src='".$path."' style='height: 300px;'>";
										}
										if($i>6){
											echo "<div style='position: relative; top: -298px; right: 94px; float: right; background: rgba(0, 0, 0, 0.85); height: 296px; width: 29.5%; color: #fff;'>
													<i class='fa fa-plus fa-fw' style='  font-size: 41px; font-weight: normal; margin: 121px 110px;'></i>
												</div>";
											break;
										}
									// check if there is youtube.com in string
									} elseif (strpos($path_to_files[$i], "youtube.com") !== false) {
									    return "youtube";
									// check if there is vimeo.com in string
									} elseif (strpos($path_to_files[$i], "vimeo.com") !== false) {
									    return "vimeo";
									} else {
									    ?>
		                    				<a href="<?php echo $path;?>" target="_blank">attachment <?php echo $i;?></a><br>
		                    			<?php
									}

	                    			
	                    		}	
                    		}
                    	?>
                    	
                    	
					</div>


					<div class="border-bottom innerAll bg-gray">
                        <a href="" class="text-primary"><i class="fa fa-comment"></i> Comment</a>
                        &nbsp;

                        <a href="" class="text-primary"><i class="fa fa-share"></i> Share Post</a>
                    </div>
				<div class="innerAll">
					<ul class="list-group social-comments margin-none">
			            <?php 
			            $count = 0;
			            if($comments!=""){
			            	$comment = new Comment();
						  		$comment->getGroupComment($comments, $count);
						}

						?>
  
                        <li class="list-group-item innerAll" style="margin: 0px; padding: 10px;">
                            <input id="<?php echo $wall_id; ?>" type="text" name="message" class="form-control newComment input-sm" placeholder="Comment here ...">
                        </li>

			        </ul>
		        </div>
		        
		        </div>
		    </div>
		</li>

		<?php 
		}
	
	}


	
	public static function viewWall ($data){

		$data = (object) $data;

		$author_id = $data->author_id;

		$profile_picture = $data->profile_picture;

		$message_author = $data->message_author;

		$date = $data->posted_on;

		$time_stamp = $data->time_stamp;

		$time = time(); // current Unix timestamp
		
		$diff = Epoch::time_diff($time, $time_stamp);

		$wall_message = $data->wall_message;

		$path_to_files = $data->path_to_files;

		$wall_id = $data->wall_id;

	?>

		<div class="ui segment">
		  <a class="ui ribbon label"><?php echo $wall_id; ?></a>

			<div class="ui comments">
			  <div class="comment">
			    <a class="avatar">
			    <?php 
			      echo "<img src='".$profile_picture."' />";
			    ?>
			    </a>
			    <div class="content">
			      <a class="author"><?php echo $message_author; ?></a>
			      <div class="metadata">
			        <div class="date"><?php echo $diff; ?><!-- 2 days ago --></div>
			        
			      </div>
			      <div class="text">
			        <div><?php echo nl2br($wall_message); ?></div>
			        <?php 
					if($path_to_files!=""){
						if (strpos($path_to_files,', ') !== false) {
				    		$image = explode(', ', $path_to_files);
				    		$imageCount = count($image);
				    		for($i=0;$i<$imageCount;$i++){
				    			echo "<img src='".$image[$i]."' style='width: 150px; height: 150px; float:left; margin: 15px;'>";
				    		}
						} else {
							echo "<img src='".$path_to_files."' style='width: 250px; height: 250px;'>";
						}
			        }
			        ?>
			      </div>
			      <div class="clearfix"></div>
			      <div class="actions">
			      	<a class="rating">
			          <i class="star icon"></i>
			          Faves
			        </a>
			        <a class="rely">Comment</a>
			        <a class="save">Share</a>
			        
			      </div>
			    </div>
			  </div>
				<?php 

				   echo "<div id='wallComments_".$wall_id."'>";

				  	if($comments!=""){
				  		$comment = new Comment();
						  		$comment->getComment($comments);

				  		// Comment::getComment($comments);
				  	}
				?>
			</div>


				<div class="submitNewComment">
					<div class="comment">
					    <a class="avatar">
					    <?php 
					    	//$my_profile_pic = "img/sj.jpg";
					      echo "<img src='".$profile_picture."' />";
					    ?>
					    </a>
					   	<div>
					   		<input id="<?php echo $wall_id; ?>" class="newGroupComment" type="text" name="newGroupComment">
					   		<i class="camera icon"></i>
							<input class="newCommentFile" type="file" name="files">
					   	</div>
					</div>
					<div class="newCommentPreview">
						<img src="">
					</div>
				</div>



		</div>
		<?php 
	}
}

?>