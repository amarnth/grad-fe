<?php
	/**
	* 
	*/
	class Cache extends Grad
	{	
		
		function setWallCache($data)
		{

			$new_wall_data = json_encode($data);

				$user_id = $this->user_id;

				$user_pic = $this->user_pic;

				$user_name = $this->user_name;

				$wall_id = $data['wall_id'];

			$message_recepient = explode(',',$data['message_recepient']);

			$redis = new Redis();
			$redis->connect('127.0.0.1', 6379);

			// $redis_id = $user_id."_".$wall_id;
			// $redis->set($redis_id, $new_wall_data);
	  		// $redis->lpush("wall".$user_id, $redis_id);

			foreach ($message_recepient as $key => $value) { 		// $value contains message_recepient id's
				if($redis->EXISTS($value."_wall")){ 	// if message_recepient exists in cache append wall to each and every message_recepient

					$redis_id = $value."_".$wall_id;

					$redis->set($redis_id, $new_wall_data);

			   		$redis->lpush($value."_wall", $redis_id);

			   		$redis->set($redis_id."_wallrecepient", "std001,std002,stf001");

// $data = array('recepient' => $value, 'user_name' => $user_name, 'user_id' => $user_id, 'user_pic' => $user_pic, 'status' => "wall");

			   			self::setNotificationCache($data, $value, $redis, "wall");

				}
			}
		}


		function setNotificationCache($data, $value, $redis, $status){

				$notification_id = $value."_".$data['time_stamp'];

			$notification  = $notification_id."##".$data['user_id']."##".$this->user_pic."##".$this->user_name;

			if($status == "wall"){	
				$notification = $notification."##has posted on a wall";
			}

			if($status == "comment"){	
				$notification = $notification."##has commented on a wall";
			}

			if($this->user_id!=$value){
				$redis->lpush($value."_notification_unread", $notification);	
			}

			// Notification::get();
			// Notification::rapidCall($redis, $value);
		}


		function setNotificationStatus($data){
			$redis = new Redis();
			$redis->connect('127.0.0.1', 6379);

			$user_id = $this->user_id;

			$unread = $redis->lrange($user_id."_notification_unread", 0, -1);

			foreach ($unread as $key => $value) {
				$redis->lpush($user_id."_notification_read", $value);
			}

			$redis->del($user_id."_notification_unread");

		}



		function setCommentCache($data)
		{	

			$new_comment_data = json_encode($data);

			$redis = new Redis();

   			$redis->connect('127.0.0.1', 6379);


   				$user_id = $this->user_id;

				$user_pic = $this->user_pic;

				$user_name = $this->user_name;


			$arList = $redis->lrange($this->user_id."_wall", 0 ,-1);

				$user_id = $this->user_id;

				$wall_id = $data['wall_id'];

				$comment_id = $data['comment_id'];

			foreach ($arList as $key => $value) {
				if(!strpos($value, $wall_id)==false){
					$message_recepient = $redis->get($value."_wallrecepient");
				}
			}

			$message_recepient = explode(',',$message_recepient);

			foreach ($message_recepient as $key => $value) { 		// $value contains message_recepient id's
				if($redis->EXISTS($value."_wall")){ 	// if message_recepient exists in cache append wall to each and every message_recepient

					// comments_stf001_stf001_1430823718

					$new_id = $value."_c_".$comment_id."_w_".$wall_id;

					$redis->set($new_id, $new_comment_data);
			   		$redis->lpush($value."_".$wall_id."_comments", $new_id);


// $data = array('recepient' => $value, 'user_name' => $user_name, 'user_id' => $user_id, 'user_pic' => $user_pic, 'status' => "comment");	   		

				self::setNotificationCache($data, $value, $redis, "comment");

				}
			}

		}

		function deleteCache(){
			$redis = new Redis();

   			$redis->connect('127.0.0.1', 6379);

			// $arList = $redis->lrange("wall".$this->user_id, 0 ,-1);

			$array = $redis->keys($this->user_id.'*');

			foreach ($array as $key => $value) {
				$redis->del($value);
			}
		}

		function deleteAllCache(){

			$redis = new Redis();

	    	try {
	    		$redis->connect('127.0.0.1', 6379);

				$array = $redis->keys('*');

				foreach ($array as $key => $value) {
					$redis->del($value);
				}

	    	} catch (Exception $e) {
	    		
	    	}
			
		}

	}
?>