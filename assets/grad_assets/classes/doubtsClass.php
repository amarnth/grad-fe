<?php
/**
* 
*/
class Doubts extends Grad
{
	
	function get()
	{
		?>
		<!-- col -->
	    <div class="col-md-3">
	        <!-- col-separator -->
	        <div class="col-separator col-separator-first">

	            <!-- Collapsible Widget -->
	            <!-- <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
	                <div class="widget-head bg-inverse margin-none innerAll">
	                    <h4 class="heading bg-inverse">
	                        Categories
	                    </h4>
	                </div>
	                <div class="widget-body">

	                    <div class="form-group">
	                        <input type="text" class="form-control new-category-name" placeholder="Category Name">
	                    </div>

	                    <div class="text-center innerAll">
	                        <a href="" class="btn btn-primary invite-mentor">Add</a>
	                    </div>

	                </div>
	            </div> -->
	            <!-- // Collapsible Widget END -->
	           
	            <!-- <ul class="list-group list-group-1 margin-none borders-none category-list">
	                <li class="list-group-item animated fadeInUp">
	                    <a href="#"><span class="badge pull-right badge-default hidden-md" data-id="cat001" data-name="Data Science">15</span> Data Science</a>
	                </li>
	                <li class="list-group-item animated fadeInUp">
	                    <a href="#"><span class="badge pull-right badge-default hidden-md" data-id="cat001" data-name="Machine Learning">10</span> Machine Learning</a>
	                </li>
	                <li class="list-group-item animated fadeInUp">
	                    <a href="#"><span class="badge pull-right badge-default hidden-md" data-id="cat001" data-name="Artificial Intelligence">10</span> Artificial Intelligence</a>
	                </li>
	                <li class="list-group-item border-bottom-none animated fadeInUp">
	                    <a href="#"><span class="badge pull-right badge-default hidden-md" data-id="cat001" data-name="Probability and Statistics">10</span> Probability and Statistics</a>
	                </li>
	            </ul> -->

	            <div class="col-separator-h"></div>
	            <h5 class="innerAll border-bottom margin-bottom-none">
	                Filter by
	            </h5>
	            <form role="form">
	                <div class="innerAll inner-2x">
	                   <!--  <div class="form-group">
	                        <div class="input-group">
	                            <span class="input-group-addon"><i class="fa fa-question-circle text-faded"></i></span>
	                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search for a topic">
	                        </div>
	                    </div> -->
	                    <div class="form-group margin-none">
	                        <div class="checkbox">
	                            <label class="checkbox-custom">
	                                <i class="fa fa-fw fa-square-o filter-un"></i>

	                                <input type="checkbox" id="inlineCheckbox1" class="checkbox" value="option1">
	                                Unanswered 

	                            </label>
	                        </div>
	                        <div class="checkbox margin-none">
	                            <label class="checkbox-custom">
	                                <i class="fa fa-fw fa-square-o filter-ans"></i>

	                                <input type="checkbox" id="inlineCheckbox2" value="option2" class="checkbox">
	                                Answered

	                            </label>
	                        </div>
	                    </div>
	                </div>
	                <!-- <div class="border-top border-bottom text-center innerAll bg-gray">
	                    <a href="#write" data-toggle="collapse" class="btn btn-primary strong btn-sm">Write an Question <i class="icon-compose fa fa-fw"></i></a>
	                </div> -->
	                <!-- <div class = "text-center border-top innerTB">
	                    <a href = "index.php?page=support_questions" class="btn btn-primary"><i class = "fa fa-fw fa-search"></i> Search</a>
	                </div> -->
	            </form>
	            <div class="col-separator-h"></div>

	            <div class="widget">
	                <div class="text-center innerAll inner-2x">
	                    <h2 class="innerT">
	                        Question <i class="icon-compose fa fa-fw"></i>
	                    </h2>

	                    <div class="form-group">
	                        <input type="text" class="form-control new-category-name" placeholder="have any doubt?">
	                    </div>

	                    <!-- <div class = "form-group">
	                        <label class = "control-label">Category</label>
	                        <select multiple = "multiple" style = "width: 100%;" id = "select-category">
	                        </select>
	                    </div> -->

	                    <div class="text-center innerAll">
	                        <a href="" class="btn btn-primary invite-mentor">Ask</a>
	                    </div>

	                </div>
	            </div>


	        </div>
	        <!-- // END col-separator -->
	    </div>
	    <!-- // END col -->
		<?php

		self::getQuestions();
	}

	function getQuestions(){

		$json = file_get_contents($this->path.'/grad_assets/js/json/doubts.json'); // this WILL do an http request for you
		// $json = file_get_contents('http://192.168.1.102:9558/get/wall/'.$user_id); // this WILL do an http request for you
		$data = json_decode($json);

		$doubts = $data->Doubts;

		$questions = $doubts->questions;

		?>

	    <!-- col -->
		<div class="col-md-9">
		    <!-- col-separator -->
		    <div class="col-separator">
		        <div class="col-table">
		            <div class="row innerAll border-bottom bg-gray">
		                <div class="col-sm-6">
		                    <h3 class="margin-none">
		                        Questions and Answers
		                    </h3>
		                </div>
		                <div class="col-sm-6">
		                    <div class="input-group">
		                        <input type="text" class="form-control search-question" placeholder="Type in a question...">
		                        <span class="input-group-btn">
		                            <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
		                        </span>
		                    </div>
		                </div>
		            </div>
		            <div class="col-table-row">
		                <div class="col-app col-unscrollable">
		                    <div class="col-app">

		                    <?php
		                    	foreach ($questions as $key => $value) {

		                    		$posted_by = $value->posted_by;

		                    		$pic = $this->path.'/grad_assets/'.$posted_by->pic;
		                    ?>
		                        <div class="innerAll border-bottom tickets doubts-list" data-title="<?php echo $value->name; ?>" data-option="<?php echo $value->answered?>">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <ul class="media-list margin-none">
		                                        <li class="media">
		                                            <a class="pull-left" href="index.php?page=support_answers">
		                                                <img style="width: 54px; height: 54px;" class="media-object" src="<?php echo $pic; ?>" alt="..." >
		                                            </a>
		                                            <div class="media-body innerT half">
		                                                <a href="index.php?page=support_answers" class="media-heading"><?php echo $value->name; ?>?</a> 
		                                                	
		                                                	<?php
		                                                		if($value->answered){
		                                                			?>
		                                                			<label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
		                                                			<?php
		                                                		}else{
		                                                			?>
		                                                			<label class="label label-default"><a href="" class="user-ticket-action"> Unread </a></label>
		                                                			<?php
		                                                		}
		                                                	?>

		                                                <div class="clearfix"></div>
		                                                <small>posted by <a href=""><?php echo $posted_by->name; ?></a> <i class="fa fa-fw icon-time-clock"></i> <?php echo $value->posted_on; ?></small>
		                                            </div>
		                                        </li>
		                                    </ul>
		                                </div>
		                                <div class="col-sm-2">
		                                    <div class="innerT pull-right">
		                                        <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> <?php echo $value->answers_count; ?></a>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>

		                    <?php
		                    	}
		                    ?>

		                        <div class="innerAll border-bottom tickets">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <ul class="media-list margin-none">
		                                        <li class="media">
		                                            <a class="pull-left" href="index.php?page=support_answers">
		                                                <img class="media-object" src="assets/images/people/50/2.jpg" alt="..." >
		                                            </a>
		                                            <div class="media-body innerT half">
		                                                <a href="index.php?page=support_answers" class="media-heading">What makes Coral such an outstanding Template?</a> 
		                                                <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
		                                                <div class="clearfix"></div>
		                                                <small>posted by <a href="">Adrian Demian</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
		                                            </div>
		                                        </li>
		                                    </ul>
		                                </div>
		                                <div class="col-sm-2">
		                                    <div class="innerT pull-right">
		                                        <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        


		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		    <!-- // END col-separator -->
		</div>
		<!-- // END col --> 

		<?php
	}
}
?>