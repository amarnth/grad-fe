<?php
/**
* 
*/
class Assignments extends Grad
{
	function get()
	{
		//echo $this->user_id;
		$json = file_get_contents(PATH.'gradpower/js/json/assignment_data.json'); // this WILL do an http request for you
		$raw_data = json_decode($json);

		$data = $raw_data->Assignments;

		?>

		<img class="ui tiny image pull-left" src="img/3.jpg">

			<div class="ui icon button hover_icon_info pull-right" data-position="bottom right" data-content="Create new Assignment">
			   	<i class="add circle link icon createNewAssignment" style="margin: 20px 10px 0px 0px; font-size: 1.4em;"></i>
			</div>

		<h1 class="center">All Assignments</h1>

		<div class="clearfix"></div>
			<div class="ui input" style="float: right; margin-right: 15px;">
			  <input placeholder="Search..." type="text" class="search_all_assignments">
			</div>
		<div class="clearfix"></div>


		<?php

		foreach ($data as $k => $v) {
			# code...
			$assign_id = $v->id;
			$created_on = $v->created_on;
			$title = $v->title;

			$deadline = $v->deadline;
			$assigned_to = $v->assigned_to;
			$topic = $v->topic;
			$subject = $v->subject;
			$description = $v->description;
			$media = $v->media;
			$file_path = $v->file_path;
			$time_stamp = $v->time_stamp;

			$student_marks = $v->Marks;

		?>
	
			<div class="task all_assignments" data-assignmentId="<?php echo $assign_id; ?>"><i class="file text icon"></i> <?php echo $title; ?></div>
		<?php 

		}
	}


	function getInviduals($id){

  		$json = file_get_contents('http://localhost/gradpower/js/json/assignment_data.json'); // this WILL do an http request for you
			$raw_data = json_decode($json);

			$data = $raw_data->Assignments;

			foreach ($data as $k => $v) {
				# code...
				//var_dump($v->Marks);

				$id = $v->id;
				$created_on = $v->created_on;
				$title = $v->title;

				$deadline = $v->deadline;
				$assigned_to = $v->assigned_to;
				$topic = $v->topic;
				$subject = $v->subject;
				$description = $v->description;
				$media = $v->media;
				$file_path = $v->file_path;
				$time_stamp = $v->time_stamp;

				$student_marks = $v->Marks;

	  
	  		echo "<div class='unique_assignment' id='unique_$id'>";	
	  		echo $id; 
	?>	
		
			<div class="ui icon button pull-right hover_icon_info" data-position="bottom right" data-content="Delete this Assignment">
				<?php echo "<i data-id='remove_".$id."' class='trash icon negative cursor'></i>"; ?>
			</div>

			<div class="ui pointing secondary menu">
		        <a class="active item" data-tab="assignment">Assignment</a>
		        <a class="item" data-tab="status">Status</a>
		    </div>



	        <div class="ui bottom attached tab active" data-tab="assignment">
		      	<div class="six wide column">
		            <div class="ui form">
				
						<div class="field">
							<label>Assignment Name</label>

							<div class="field">
								<input style="border: none;" type="text" id="name_<?php echo $id; ?>" name="edit_assignment_name" placeholder="UnNamed" value="<?php echo $title; ?>">
							</div>

						</div>

						<div class="field">
							<label>Assignment Description</label>

							<div class="field">
								<input style="border: none;" type="text" id="desc_<?php echo $id; ?>" name="edit_assignment_desc" placeholder="UnNamed" value="<?php echo $description; ?>">
							</div>

						</div>
				
					<?php if($assigned_to==""){ ?>

						<div class="field">
							<label>Assign To</label>
							
							<div class="ui form">
							    <div class="grouped inline fields">

							      <div class="field">
							        <div class="ui radio checkbox assignment_recipient" data-bool="true">
							          <input name="assignment_recipient" type="radio" >
							          <label>Whole department</label>
							        </div>
							      </div>

							      <div class="field">
							        <div class="ui radio checkbox assignment_recipient" data-bool="false">
							          <input name="assignment_recipient" type="radio" >
							          <label>Individual</label>
							        </div>
							      </div>

							    </div>
							</div>


							<div class="field">
								
							</div>

						</div>


						

						<div for="individual">
							<label for="selectAssignmentMembers">Select Members:</label>
								<select class="selectAssignmentMembers" name="selectAssignmentMembers" multiple>
									<?php
										Util::getListStudent();
									?>
								</select>
							<input value="activate selectator" id="selectator_<?php echo $id; ?>" class="selectatorAssignmentSelectedMembers" style="display: none;">
						</div>

						<div for="department">
							<label for="selectAssignmentDepartment">Select Department:</label>
								<select class="selectAssignmentDepartment" name="selectAssignmentDepartment" multiple>
									<?php
										Util::getListDepartment();
									?>
								</select>
							<input value="activate selectator" id="selectator_<?php echo $id; ?>" class="selectatorAssignmentSelectedDepartment" style="display: none;">
						</div>

						<?php  
							echo "<div class='ui button post positive submit_new_assignment' data-id='post-$id'>Post</div>";
							}
						?>

					</div>
		      	</div>
		    </div>



		    <div class="ui bottom attached tab " data-tab="status">
		      	<div class="six wide column">
		          <?php self::getAllStudents($student_marks); ?>
		       	</div>
		    </div>
		
		<?php

			echo "</div>";

		 }
	}


	function getInfo($data){

		 ?>

		 	

		<?php
	}


	function getAllStudents($data){

		?>
			<table class="studentAssignments">
			<thead>
				<tr>
					<th><i class="student icon"></i> Student Name</th>
					<th>Marks</th>
					<th>Attachment</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				
		<?php

		foreach ($data as $k => $v) {
			
			$std_name = $v->std_name;
			$std_id = $v->std_id;
			$assignment_id = $v->assignment_id;
			$status = $v->status;
			$mark = $v->mark;
			$media = $v->media;
			$submitted_on = $v->submitted_on;
			$time_stamp = $v->time_stamp;

			
				echo "<tr><td>".$std_name."</td>";

				if($mark!="" || $status == "completed"){
					echo "<td><input type='text' class='editMark' maxlength='3' data-studentId='".$std_id."' data-assignmentId='".$assignment_id."' value='".$mark."'></td>";
				} else{
					echo "<td></td>";
				}

				echo "<td></td>";

				if($status=="completed"){
					echo "<td class='status positive'><i class='checkmark icon'></i></td>";
				}

				if($status=="pending"){
					echo "<td class='status neutral'><i class='minus icon'></i></td>";
				}

				echo "</tr>";
			
		?>	

			
			
		<?php

		}

		echo "</tbody>";
		echo "</table>";
	}

	function setStatus($id){
		
	}


	function getMarks($id){
		
			//echo "<div class='assignmentMarks' id='marks_".$assignmentId."_".$id."'>".$id."</div>";
		?>
			
		<?php

	}

}



?>