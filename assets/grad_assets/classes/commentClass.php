<?php
class Comment extends Grad{
	public static function viewComment($data){
		
			$wall_id = $data['wall_id'];
			$comment = $data['comment'];

			$time_stamp = $data['time_stamp'];


			$time = time(); // current Unix timestamp
		
			$diff = Epoch::time_diff($time, $time_stamp);

			$profile_pic = $data['profile_pic'];

			$user_id = $data['user_id'];

			$name = $data['fname']." ".$data['lname'];

			$is_media_available = false;

			$stars = 0;

			$file_path = "";

			
				echo "<div class='comment wallComment_".$wall_id."'>";

			?>

			 
			    <a class="avatar">
			    	<?php echo "<img src='$profile_pic'>"; ?>
			    </a>
			    <div class="content">
			      <a class="author"><?php echo $name; ?></a>
			      <div class="metadata">
			        <span class="date"><?php echo $diff; ?></span>
			      </div>
			      <div class="text">
			        <?php 
			        	
			        	echo "<div class='commentText'>".$comment."</div>";
			        	echo "<div class='commentAttachment'>";

				        	if($is_media_available){
				        		$file_path = array($file_path);
				        		Util::getFileType($file_path);
				        		//echo "<img class='commentImage' src='$file_path'>";
				        	}

			        	echo "</div>";

			        ?>

			      </div>
			      <div class="actions">
			        <i class="star icon"></i><?php echo $stars; ?> Stars
			      </div>
			    </div>
			  </div>

	<?php

	}

	function getComment($data, $count){
		
		$i=0;

		foreach($data as $key => $value){

			if($is_redis){
				$redis = new Redis();
	    		$redis->connect('127.0.0.1', 6379);
	    		$value = json_decode($redis->get($value));
			}

			// var_dump($value);
			
			$user_name = $value->user_name;
			$profile_pic = $value->profile_pic;
			$comment = $value->comment;
			$user_id = $value->user_id;

			$time_stamp = $value->time_stamp;

			$is_media_available = $value->is_media_available;
			$file_path = $value->file_path;

			$stars = $value->stars;

			$current_time = time();
			
			$obj = new Epoch();
				
			$time_stamp = $obj->time_diff($current_time, $time_stamp);

			$wall_id = $value->wall_id;

			$i++;

			if($i % 2 == 0){
				// echo "<div class='media innerAll margin-none comment wallComment_".$wall_id."'>";
			}else{
				// echo "<div class='media innerAll margin-none bg-gray border-top border-bottom wallComment_".$wall_id."'>";
			}
			
			$pic = $this->path.'/grad_assets/'.$profile_pic;

			?>
				<li class="list-group-item" style="margin: 0px; padding: 10px;">
				    <?php 
				    	echo "<img src='".$pic."' alt='photo' class='pull-left' style='width: 32px; height: 32px;'>"; 
				    ?>

				    <!-- <img src="../assets/images/avatar-36x36.jpg" alt="Avatar" class="pull-left"> -->
				    <div class="user-info">
				        <div class="row">
				            <div class="col-md-3"> <a href=""><?php echo $user_name; ?></a> <abbr><?php echo $time_stamp; ?></abbr>
							</div>
				            <div class="col-md-12"> <span> <?php echo $comment; ?> </span> </div>
				        </div>
				    </div>
				</li>
        	<?php
		}

	}



	function getGroupComment($data, $count){
		
		$i=0;

		foreach($data as $key => $value){

			// $value = json_decode($redis->get($v));

			// var_dump($value);
			
			$user_name = $value->user_name;
			$profile_pic = $value->profile_pic;
			$comment = $value->comment;
			$user_id = $value->user_id;

			$time_stamp = $value->time_stamp;

			$is_media_available = $value->is_media_available;
			$file_path = $value->file_path;

			$stars = $value->stars;

			$current_time = time();
			
				$obj = new Epoch();
				
			$time_stamp = $obj->time_diff($current_time, $time_stamp);

			$wall_id = $value->wall_id;

			$i++;

			if($i % 2 == 0){
				// echo "<div class='media innerAll margin-none comment wallComment_".$wall_id."'>";
			}else{
				// echo "<div class='media innerAll margin-none bg-gray border-top border-bottom wallComment_".$wall_id."'>";
			}
			
			$pic = $this->path.'/grad_assets/'.$profile_pic;

			?>
				<li class="list-group-item" style="margin: 0px; padding: 10px;">
				    <?php 
				    	echo "<img src='".$pic."' alt='photo' class='pull-left' width='35'>"; 
				    ?>

				    <!-- <img src="../assets/images/avatar-36x36.jpg" alt="Avatar" class="pull-left"> -->
				    <div class="user-info">
				        <div class="row">
				            <div class="col-md-3"> <a href=""><?php echo $user_name; ?></a> <abbr><?php echo $time_stamp; ?></abbr>
							</div>
				            <div class="col-md-12"> <span> <?php echo $comment; ?> </span> </div>
				        </div>
				    </div>
				</li>
        	<?php
		}

	}
}

$comment = new Comment();
?>