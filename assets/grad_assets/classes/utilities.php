<?php

Class Util{

	public $secondary_path;
	public $get_from_server;
	public $is_server_up;
	public $cid;

	function __construct()
	{	
		// $this->secondary_path = "http://gradpowered.net/";
		// $this->get_from_server = "http://gradpowered.net:9558";

		$this->secondary_path = "http://localhost";
		$this->get_from_server = "http://localhost:9558";
		
		$this->is_server_up = true;
		$this->cid = "grad_C01X";
	}

	function getUserMenu(){
	
		$is_redis_up = false;
		
		if($is_redis_up){
			try {

				$redis = new Redis();
			    
				$redis->connect('127.0.0.1', 6379);
		        $ping = $redis->ping();
		        
	        	if(!$redis->EXISTS("std008_47851244_userinfo")){
			    	$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/virginCall.json'); 
					$data = json_decode($json);
					
					$user_data = $data->UserInfo;
	
					$redis->set($user_data->id."_userinfo", json_encode($user_data));
	
			    }else{
			    	$json = $redis->get("std008_47851244_userinfo");
			    	$user_data = json_decode($json);
			    	// var_dump($user_data);
			    }
	
			} catch (Exception $e) {
				$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/virginCall.json'); 
	    		$data = json_decode($json);
				$user_data = $data->UserInfo;
			}
		}else{
			
			if($this->is_server_up){
				$json = file_get_contents($this->get_from_server.'/get/info/user/'.$_SESSION['user_id']);
				
				// echo $this->get_from_server.'/get/info/user/'.$_SESSION['user_id'];
				
			}else{
				$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/virginCall.json');
			}

	    		$data = json_decode($json);
				$user_data = $data->UserInfo;


		}
		
		if(isset($_SESSION)){
		
		$pic = $this->secondary_path.'assets/grad_assets/'.$_SESSION['user_pic'];

		?>
			<?php if($_SESSION['user_role']=="staff"){ ?>
			<!-- <div style="background: url('<?php echo $pic; ?>') no-repeat; width: 100%; height: 400px; background-position: -54px 0px;"> </div> -->
			<?php } if($_SESSION['user_role']=="admin") { ?>
			<!-- <div style="background: url('<?php echo $pic; ?>') no-repeat; width: 100%; height: 400px; background-position: -275px 0px;"> </div> -->
			<?php } if($_SESSION['user_role']=="student"){ ?>
			<!-- <div style="background: url('<?php echo $pic; ?>') no-repeat; width: 100%; height: 400px; background-position: -224px 0px;"> </div> -->
		<?php } ?>
			<!-- <img src="<?php echo $pic; ?>" style="height: 50%;"> -->
			<!-- <p><h3 style="color: #fff; text-align: center; text-transform: capitalize;"><?php echo $_SESSION['user_name']; ?></h3></p> -->
		<?php } ?>
			<?php
				//var_dump($_SESSION);
				
			?>
			<style>
				.border-top{
					border-top: 1px solid #000;
				}
			</style>
		<?php $pic = $this->secondary_path.'assets/grad_assets/'.$user_data->prof_pic; ?>
		<?php // $pic = $this->secondary_path.'assets/grad_assets/img/profile/'.$user_data->gender.'.jpg'; ?>
		<?php //$pic = $this->secondary_path.'assets/grad_assets/img/profile/male.jpg'; ?>

			<!-- <div style="background: url('<?php echo $pic; ?>') no-repeat; width: 100%; height: 400px; background-position: -54px 0px;"> </div> -->
			<div class="" style="padding: 5px;"><img style="width: 100%;" src="<?php echo $pic; ?>"></div>
			<p><h3 style="color: #000; text-align: center; text-transform: capitalize;"><?php echo $user_data->name; ?></h3></p>

			<!-- <p style="text-align: center; text-transform: capitalize;"><?php echo $user_data->gender; ?></p> -->
			<!-- <p style="text-align: center;">Year: <?php echo $user_data->year; ?></p>

			<div class="innerAll">
		                <ul class="list-unstyled">
		                    <li style="width: 92%;"><i class="fa fa-graduation-cap"></i> Studied at <?php echo $user_data->school_name; ?></li>
		                    <li style="width: 92%;"><i class="fa fa-map-marker"></i> Lives in <?php echo $user_data->current_location; ?></li>
		                </ul>
		         </div> -->
            	
            	<div class="innerAll">	
           	    <h5>Attendance</h5>
	            <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
	                <div class="count">80%</div>
	                <div class="progress-bar progress-bar-primary" style="width: 80%;"></div>
	            </div>
	            <br>
	            <h5>Performance</h5>
	            <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
	                <div class="count">60%</div>
	                <div class="progress-bar progress-bar-primary" style="width: 60%;"></div>
	            </div>
	          <br>
	
		        <?php
	        	$json = file_get_contents($this->get_from_server.'/get/groups/info/'.$_SESSION['user_id']);

				$data = json_decode($json);
			
				$groups_data = $data->Group;
				$i=0;
				$groups = (array) $groups_data->my_group;
				$count = count($groups);

					if(0==$count){
					?>
						<h5 class="border-top"><a href="index.php?page=groups">Groups</a></h5>
					<?php
					}else{
						?>
						<!-- <h5 class="border-top">Groups</h5> -->
						<ul class="menu list-unstyled" id="navigation_current_page">
							<li class="hasSubmenu">
								<a href="#menu-2e4a4532a537256acbf331654a27ff5a" data-toggle="collapse" class="collapsed">
									<i class="fa fa-fw icon-group"></i>
									<span>Groups</span>
								</a>
								<ul class="collapse" id="menu-2e4a4532a537256acbf331654a27ff5a" style="height: auto;">
									
						<?php
					}
					
					foreach($groups_data->my_group as $k => $v){
						if($i<2){
	                		$encrypt = new Decipher();
					$encrypt_id = $encrypt->base64_encrypt($v->group_id);
						?>
						<li><a style="text-indent: 10px; color:#000;" href="index.php?page=group_wall&gid=<?php echo $encrypt_id; ?>"><?php echo $v->group_name; ?></a></li>
						<?php
							$i++;
						}
					}
					
				if(0!=$count && 3<=$i){
				
			        ?>		

			        	<li><a href="index.php?page=groups">... more</a></li>
			        <?php } ?>
			        
						</ul>
					</li>
			        

			        <?php
			        	$json = file_get_contents($this->get_from_server.'/get/projects/'.$_SESSION['user_id']);

						$data = json_decode($json);
					
						$project = $data->Projects;
						
						$i=0;

						$count = $project->count;

						if(0==$count){
						?>
							<h5 class="border-top"><a href="index.php?page=projects">Projects</a></h5>
						<?php
						}else{
							?>
							<!-- <h5 class="border-top">Groups</h5> -->

								<li class="hasSubmenu">
									<a href="#menu-a661f05e95f401ef72528b0fe60d43af" data-toggle="collapse" class="collapsed">
										<i class="fa fa-fw icon-folder"></i>
										<span>Projects</span>
									</a>
									<ul class="collapse" id="menu-a661f05e95f401ef72528b0fe60d43af" style="height: auto;">
									
							<?php
						}
						foreach($project->details as $k => $v){
							if($i<2){
                        		$encrypt = new Decipher();
                        		$encrypt_id = $encrypt->base64_encrypt($v->project_id);
							?>
								<li><a style="text-indent: 10px; color: #000;" href="index.php?page=project_view&pid=<?php echo $encrypt_id;?>"><?php echo $v->project_name; ?></a></li>
							<?php
								$i++;
							}
						}
					
					if(0!=$count && 3<=$i){
			        ?>		
			        	<li><a href="index.php?page=projects">... more</a></li>
			        <?php } ?>
			        		</ul>
					</li>
					
			        
			        </ul>
			        

		        </div>

            <div class="clearfix"></div>
            
            
		<?php
	}

	function getMembers($data){

		echo "<ul class='membersList'>";

		foreach ($data as $key => $value) {
			$name = $value->name; 
			$pic = $value->pic;

		?>
						
			<li class="image">
				<?php echo "<img style='width: 50px; height: 50px; border: 2px solid #000' src='".PATH."gradpower/".$pic."'>"; ?>
				<h3><?php echo $name;?></h3>
			</li>

		<?php

		}

		echo "</ul>";
	}


	function getFileType ($data){
		$count = count($data);
		

		for($i=0;$i<$count;$i++){
			//echo "<br>name: ".$data[$i];

			$file_type = substr($data[$i], -3, 3);
			
			if($file_type=='pdf'){
				echo "PDFFFFFFF";
			}

			if($file_type=='jpg' || $file_type=='png' || $file_type=='peg' || $file_type=='gif'){
				echo "<img src='$data[$i]'>";
			}

			if($file_type=='doc'){
				echo "document";
			}

			if($file_type=='ppt'){
				echo "PowerPoint";
			}

		}
	}

	


	function getOnline (){

		// $json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json'); 
		// $data = json_decode($json);
		//var_dump($json);
		
		if($this->is_redis_up){

			$redis = new Redis();

		    	try {
		    		$redis->connect('127.0.0.1', 6379);
					$json = $redis->get("std008_47851244_userinfo");
					$user_data = json_decode($json);
		    	} catch (Exception $e) {
		    		$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/virginCall.json');
					$data = json_decode($json);
					$user_data = $data->UserInfo;
		    	}
		}else{

			if($this->is_server_up){
				$json = file_get_contents($this->get_from_server.'/get/student/friends/'.$_SESSION['user_id']);
				$data = json_decode($json);

			}else{
				$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/virginCall.json');
				$data = json_decode($json);
				$user_data = $data->UserInfo;

			}
					
		}

		$i=0;

		foreach ($data->Friends as $key => $value) {
			
			$time_stamp = $value->recent_activity;

			$time = time(); // current Unix timestamp
			
			$diff = Epoch::time_diff($time, $time_stamp);

			$pic = "assets/grad_assets/".$value->prof_pic;

			// echo $pic;

			if($i<8){

				if($value->gender == "male"){
					?>
					<li data-title="<?php echo $value->name; ?>">
			<!-- <i class="fa fa-circle text-success fa-fw" style=" position: relative; right: 10px; float: right; top: 19px;"></i> -->
						<a href="index.php?profile=<?php echo $value->name_id; ?>" style="padding-left: 10px;" data-toggle="image-preview" data-title="<?php echo $value->name; ?>" data-content="<small>
						 <?php echo $diff; ?>
						</small>
						" data-image-preview="<?php echo $pic; ?>" href="" class="glyphicons user"><img style="width: 32px; height: 32px; margin-right: 8px;" src="<?php echo $pic; ?>"><?php echo $value->name; ?>
						</a>
					</li>
					<?php
				} else {
					?>
					<li data-title="<?php echo $value->name; ?>">
			<i class="fa fa-circle text-success fa-fw" style=" position: relative; right: 10px; float: right; top: 19px;"></i>
						<a href="index.php?profile=<?php echo $value->name_id; ?>" style="padding-left: 10px;" data-toggle="image-preview" data-title="<?php echo $value->name; ?>" data-content="<small>
						 <?php echo $diff; ?>
						</small>
						" data-image-preview="<?php echo $pic; ?>" href="" class="glyphicons girl"><img style="width: 32px; height: 32px; margin-right: 8px;" src="<?php echo $pic; ?>"><?php echo $value->name; ?>
						</a>
					</li>
					<?php
				}
			}

			if($i==8){
				?>
					<li>
						<a class="btn-primary btn btn-small view-all" href="index.php?page=contacts" style="">View All</a>
					</li>
				<?php
			}

			$i++;
		}
	}



	function getStudentList (){

		if($this->is_server_up){

			if("student" == $_SESSION['user_role']){

				$json = file_get_contents($this->get_from_server.'/get/student/friends/'.$_SESSION['user_id']);

				$data = json_decode($json);
				//var_dump($json);

				$student_data = $data->Friends;
			    foreach ($student_data as $k => $v) {
			    	echo "<option value='".$v->id."'>".$v->name."</option>";
			    }

			}elseif("staff" == $_SESSION['user_role']){

				$json = file_get_contents($this->get_from_server.'/get/students/staff/'.$_SESSION['user_id']);

				$data = json_decode($json);
				//var_dump($json);

				$student_data = $data->Students;

				foreach ($student_data as $key => $value) {
					 echo "<optgroup label='".$key."'>";
				    foreach ($student_data->$key as $k => $v) {
				    	echo "<option value='".$v->id."'>".$v->name."</option>";
				    }
				    echo "</optgroup>";
				}

			}
			
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json');
		}
		
		
	}
	
	

	function getStaffList (){

		if($this->is_server_up){
			$json = file_get_contents($this->get_from_server.'/get/staff/list/'.$this->cid);
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json');
		}

		$data = json_decode($json);
		//var_dump($json);

		$staff_data = $data->Staffs;

		//foreach ($staff_data as $key => $value) {
			 //echo "<optgroup label='".$key."'>";
		//foreach ($staff_data->$key as $k => $v) {
		    foreach ($staff_data as $k => $v) {
		    	echo "<option value='".$v->staff_id."'>".$v->staff_name."</option>";
		    }
		    //echo "</optgroup>";
		//}
		
	}



	function getClasses(){
		
		// echo $this->college_id;

		if($this->is_server_up){
			$json = file_get_contents($this->get_from_server.'/get/students/class/'.$this->cid);
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json');
		}

		
		$data = json_decode($json);

		//$dept_data = $data->Courses;
		
		//if($data->Students){
			foreach ($data->Students as $key => $value) {
				echo "<option value='".$key."' >".$key."</option>";
			}
		
		//}else{
		
			//foreach ($dept_data as $key => $value) {
				//echo "<option value='".$value->course_id."' >".$value->course_name."</option>";
			//}	
		//}
	}



	function getCoursesGroup(){


		if($this->is_server_up){
			$json = file_get_contents($this->get_from_server.'/get/courses/'.$this->cid);
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/notice.json');
		}
		
		$data = json_decode($json);

		$dept_data = $data->Courses;
		
		foreach ($dept_data as $key => $value) {

			echo "<optgroup label='".$key."'>";

		  foreach ($dept_data->$key as $k => $v) {
		   	echo "<option value='".$v->department_id."GRD'>".$v->department_name."</option>";
		  }

	   		echo "</optgroup>";
		}
	}

	function getDepartmentList ($id=""){
		
		// $json = file_get_contents($this->path.'/grad_assets/js/json/department.json'); 

		if($this->is_server_up){
			$json = file_get_contents($this->get_from_server.'/get/departments/'.$this->cid);
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json');
		}
		
		$data = json_decode($json);
		// var_dump($dept_data->Department);

		$dept_data = $data->Departments;
		
		if($id==""){
			foreach ($dept_data as $key => $value) {
				echo "<option value='".$value->department_id."' >".$value->department_name."</option>";
				// <li class="ms-elem-selectable ms-selected" id="elem_1-selectable" style="display: none;"><span>elem 1</span></li>
			}	
		}else{
			foreach ($dept_data as $key => $value) {
				if($value->course_id == $id){
					echo "<option value='".$value->department_id."' >".$value->department_name."</option>";	
				}
			}
		}
		
		
	}


	function selectFriends ($data){
		//var_dump($data);
		foreach ($data as $key => $value) {
			
			$id = $value->staffId;
			$fname = $value->fname;
			$lname = $value->lname;
			
			$name = $fname." ".$lname;

			$image = $value->image;
			$gender = $value->gender;
			$dept = $value->dept;

			$id = $value->staffId;

			echo "<option value='".$id."' class='".$id."' data-id='".$id."' data-subtitle='".$gender."' data-left='<img src=".$image.">' data-right='".$dept."'>".$fname."</option>";
		}
	}


	function getIcons(){

		$get = new Util();
		$icons = $get->icons_list();

		foreach ($icons as $key => $value) {
			echo "<option value='".$value."'>".$value."</option>";
		}
	}


	function icons_list(){
		$icons = array('icon-angle-1', 'icon-angle-2', 'icon-angle-3', 'icon-angle-4', 'icon-arrow-bottom-left', 'icon-arrow-bottom-right', 'icon-arrow-down', 'icon-arrow-left', 'icon-arrow-right', 'icon-arrow-top-left', 'icon-arrow-top-right', 'icon-arrow-up', 'icon-chevron-down-thick', 'icon-chevron-down', 'icon-chevron-left-thick', 'icon-chevron-left', 'icon-chevron-right-thick', 'icon-chevron-right', 'icon-chevron-up-thick', 'icon-chevron-up', 'icon-circle-chevron-down', 'icon-circle-chevron-left', 'icon-circle-chevron-right', 'icon-circle-chevron-up', 'icon-collapse', 'icon-diagonal-1', 'icon-diagonal-2', 'icon-expand', 'icon-horizontal-orbit', 'icon-horizontal-switch', 'icon-loop', 'icon-refresh-1', 'icon-refresh-2', 'icon-refresh-3', 'icon-refresh-4', 'icon-refresh-5', 'icon-refresh-6', 'icon-refresh-7', 'icon-refresh-8', 'icon-refresh-9', 'icon-refresh-heart-fill', 'icon-refresh-heart', 'icon-refresh-location-fill', 'icon-refresh-location', 'icon-refresh-music', 'icon-refresh-star-fill', 'icon-refresh-star', 'icon-refresh-wifi', 'icon-reply-all-fill', 'icon-reply-all', 'icon-reply-fill', 'icon-reply', 'icon-right-left', 'icon-turn-back-down', 'icon-turn-back-left', 'icon-turn-back-right', 'icon-turn-back-up', 'icon-up-down', 'icon-vertical-orbit', 'icon-vertical-switch', 'icon-ball-cap', 'icon-belt', 'icon-boot', 'icon-bow-tie', 'icon-boxers', 'icon-bra', 'icon-button-up-shirt', 'icon-button', 'icon-cardigan', 'icon-clothes-pin', 'icon-cloths-hanger', 'icon-dress-1', 'icon-dress-2', 'icon-glasses-1', 'icon-glasses-2', 'icon-glasses-3', 'icon-hand-bag', 'icon-hat-1', 'icon-hat-2', 'icon-heels', 'icon-hoodie', 'icon-jacket', 'icon-long-sleeved-shirt', 'icon-mittens', 'icon-panties', 'icon-pants', 'icon-pocket-watch', 'icon-purse', 'icon-sandals', 'icon-shoe', 'icon-shorts', 'icon-shutter-shades', 'icon-skirt', 'icon-socks-1', 'icon-socks-2', 'icon-sports-coat', 'icon-sun-glasses-1', 'icon-sun-glasses-2', 'icon-swim-trunks', 'icon-tanktop-1', 'icon-tanktop-2', 'icon-thong', 'icon-tie', 'icon-top-hat', 'icon-tshirt-1', 'icon-tshirt-2', 'icon-tshirt-fill', 'icon-vneck-tshirt', 'icon-winter-hat', 'icon-womens-tshirt', 'icon-wrist-watch', 'icon-zip-hoodie', 'icon-angel', 'icon-angry-sick', 'icon-angry', 'icon-bitter', 'icon-concerned', 'icon-cool', 'icon-crushing', 'icon-cyclopse', 'icon-dead', 'icon-depressed', 'icon-devil', 'icon-disappointed-1', 'icon-disappointed-2', 'icon-excited-smirk', 'icon-excited', 'icon-expletive', 'icon-feeling-loved', 'icon-geeky', 'icon-greedy', 'icon-grumpy', 'icon-happy-content', 'icon-happy-wink', 'icon-happy', 'icon-hard-laugh', 'icon-in-love', 'icon-joyful', 'icon-laughing', 'icon-looking-shocked', 'icon-looking-smirk', 'icon-looking-talking', 'icon-loving', 'icon-no-snitching', 'icon-not-excited', 'icon-question', 'icon-quiet-laugh', 'icon-quiet', 'icon-sad', 'icon-sick', 'icon-silly', 'icon-sneaky-wink', 'icon-sneaky', 'icon-speechless', 'icon-surprised', 'icon-thumbs-down-fill', 'icon-thumbs-down', 'icon-thumbs-up-fill', 'icon-thumbs-up', 'icon-tired', 'icon-whistle-blower', 'icon-yawning', 'icon-yelling-disbelief', 'icon-yelling', 'icon-apple', 'icon-bacon', 'icon-beer', 'icon-bread', 'icon-cake', 'icon-candy-sucker', 'icon-candy', 'icon-cheese', 'icon-chef-hat', 'icon-cherries', 'icon-chicken-leg', 'icon-chop-sticks', 'icon-coffee-mug-fill', 'icon-coffee-mug-heart', 'icon-coffee-mug', 'icon-cooked-eggs', 'icon-cookie', 'icon-cooking-pan', 'icon-covered-platter', 'icon-cracked-egg', 'icon-cup-of-tea', 'icon-cupcake', 'icon-drink', 'icon-fork-knife', 'icon-fountain-drink', 'icon-grapes', 'icon-grill', 'icon-hamburger', 'icon-hard-liquor', 'icon-hotdog', 'icon-icecream-cone', 'icon-kabobs', 'icon-latte', 'icon-lollypop', 'icon-martini', 'icon-orange', 'icon-oven', 'icon-pie', 'icon-pizza', 'icon-soda-can', 'icon-soup', 'icon-steak', 'icon-strawberry', 'icon-sushi', 'icon-tomato', 'icon-turkey-dinner', 'icon-watermelon', 'icon-wheat', 'icon-wine-bottle', 'icon-wine-glass-1', 'icon-wine-glass-2', 'icon-add-symbol', 'icon-address-book', 'icon-alert', 'icon-app-icon', 'icon-battery-full', 'icon-battery-low', 'icon-bell-fill', 'icon-bell', 'icon-bin', 'icon-bluetooth-thick', 'icon-bluetooth', 'icon-bookmark', 'icon-briefcase-1', 'icon-briefcase-2', 'icon-browser-check', 'icon-browser-heart', 'icon-browser-location', 'icon-browser-star', 'icon-browser', 'icon-building', 'icon-bulleted-list', 'icon-calendar-1', 'icon-calendar-2', 'icon-cancel', 'icon-categories', 'icon-checkmark-thick', 'icon-checkmark', 'icon-cigarette', 'icon-circle-delete', 'icon-cloud-download', 'icon-cloud-upload', 'icon-collage', 'icon-comment-1', 'icon-comment-2', 'icon-comment-add', 'icon-comment-fill-1', 'icon-comment-fill-2', 'icon-comment-heart-2', 'icon-comment-heart-fill', 'icon-comment-heart', 'icon-comment-star-fill', 'icon-comment-star', 'icon-comment-typing', 'icon-compose', 'icon-computer-mouse', 'icon-connection', 'icon-contrast', 'icon-copyright', 'icon-crop-tool', 'icon-cross-fill', 'icon-cross', 'icon-crossing', 'icon-crown', 'icon-delete-bin', 'icon-delete-symbol', 'icon-dial-pad', 'icon-document-add', 'icon-document-bar', 'icon-document-blank-fill', 'icon-document-blank', 'icon-document-check', 'icon-document-delete', 'icon-document-heart', 'icon-document-line', 'icon-document-open', 'icon-document-sub', 'icon-documents-bar', 'icon-documents-check', 'icon-documents-fill', 'icon-documents-heart', 'icon-documents-line', 'icon-documents', 'icon-download-1', 'icon-download-2', 'icon-download-all', 'icon-easel', 'icon-envelope-1', 'icon-envelope-2', 'icon-envelope-3', 'icon-envelope-fill-1', 'icon-factory', 'icon-file-box', 'icon-filing-cabinet', 'icon-flag-1', 'icon-flag-2', 'icon-floppy-disk', 'icon-folder-add', 'icon-folder-check', 'icon-folder-delete', 'icon-folder-fill', 'icon-folder-open', 'icon-folder-sub', 'icon-folder', 'icon-ghost', 'icon-graduation', 'icon-group', 'icon-hammer', 'icon-high-definition', 'icon-hour-glass', 'icon-identification', 'icon-inbox-1', 'icon-inbox-2', 'icon-inbox-3', 'icon-inbox-fill-1', 'icon-inbox-fill-2', 'icon-information', 'icon-link', 'icon-location-pin', 'icon-lock-fill', 'icon-lock', 'icon-manager', 'icon-minus-symbol', 'icon-mirror', 'icon-new-window', 'icon-newspaper', 'icon-note-pad', 'icon-outbox-fill', 'icon-outbox', 'icon-paint-brushes', 'icon-paint-palette', 'icon-paper-document-image', 'icon-paper-document', 'icon-paper-documents-image', 'icon-paper-documents', 'icon-paper-stack', 'icon-paperclip', 'icon-path-tool', 'icon-phone-fill', 'icon-phone', 'icon-plane-fill', 'icon-plane', 'icon-power-on', 'icon-presentation', 'icon-printer', 'icon-projector-screen-bar', 'icon-projector-screen-line', 'icon-projector-screen', 'icon-quotes', 'icon-rook', 'icon-ruler', 'icon-save', 'icon-scissors', 'icon-scope', 'icon-search-add', 'icon-search-sub', 'icon-search', 'icon-server', 'icon-settings-wheel-fill', 'icon-settings-wheel', 'icon-share', 'icon-shield', 'icon-signal', 'icon-sliders', 'icon-star-of-david', 'icon-task-check', 'icon-think-bubbles', 'icon-tic-tac-toe', 'icon-time-clock', 'icon-trash-can', 'icon-unlock-fill', 'icon-unlock', 'icon-upload', 'icon-usb', 'icon-user-1', 'icon-user-2', 'icon-venn-diagram', 'icon-view-more-fill', 'icon-view-more', 'icon-wifi-1', 'icon-wifi-2', 'icon-wrench-fill', 'icon-wrench', 'icon-alarm-clock', 'icon-baby-monitor', 'icon-bar-stool', 'icon-bed', 'icon-book-shelf', 'icon-broom', 'icon-cabinet', 'icon-chair-1', 'icon-chair-2', 'icon-crib', 'icon-dish-washer', 'icon-door-key', 'icon-door', 'icon-fence', 'icon-garbage-can', 'icon-garden-rake', 'icon-garden-spade', 'icon-gate', 'icon-globe', 'icon-home-1', 'icon-home-2', 'icon-home-fill-1', 'icon-hung-picture', 'icon-lamp-1', 'icon-lamp-2', 'icon-lamp-3', 'icon-light-bulb', 'icon-light-fixture', 'icon-light-switch', 'icon-microwave', 'icon-pain-can', 'icon-paint-brush', 'icon-paint-roller', 'icon-plug-fill', 'icon-plug', 'icon-plunger', 'icon-recycle', 'icon-refrigerator-1', 'icon-refrigerator-2', 'icon-shovel', 'icon-soap-bottle', 'icon-sofa-chair', 'icon-stove-top', 'icon-stove-vent', 'icon-swing', 'icon-table-1', 'icon-table-2', 'icon-thermostat', 'icon-toilet', 'icon-washer-dryer', 'icon-window-1', 'icon-window-2', 'icon-window-3', 'icon-alien', 'icon-ambulance', 'icon-atom-1', 'icon-atom-2', 'icon-bandage', 'icon-big-bang', 'icon-biological', 'icon-blood-sample', 'icon-cell', 'icon-clipboard', 'icon-crutches', 'icon-dna', 'icon-eye-dropper', 'icon-female', 'icon-first-aid', 'icon-galaxy', 'icon-handicap', 'icon-heart-beat', 'icon-heart-fill', 'icon-heart-monitor', 'icon-heart', 'icon-hospital-bed', 'icon-hospital', 'icon-lab-beaker', 'icon-magnet', 'icon-male', 'icon-medical-alert-fill', 'icon-medical-alert', 'icon-medical-symbol-fill', 'icon-medical-symbol', 'icon-medicine', 'icon-microscope', 'icon-molecule-1', 'icon-molecule-2', 'icon-no-smoking', 'icon-nuclear', 'icon-perscription', 'icon-pill', 'icon-planet', 'icon-reflex-hammer', 'icon-rocket', 'icon-satellite-1', 'icon-satellite-2', 'icon-satellite-3', 'icon-shooting-star-fill', 'icon-shooting-star', 'icon-skull-bones', 'icon-space-helmet', 'icon-space-shuttle', 'icon-star-fill', 'icon-star', 'icon-stethoscope', 'icon-syringe', 'icon-ufo', 'icon-albums', 'icon-bass-cleff', 'icon-beat-machine', 'icon-clapper-board', 'icon-compact-disc', 'icon-desktop-heart', 'icon-desktop-location', 'icon-desktop-play', 'icon-desktop-star', 'icon-desktop', 'icon-download-photo', 'icon-drum', 'icon-eye-cry', 'icon-fast-forward-fill', 'icon-fast-forward', 'icon-film-roll', 'icon-film-strip', 'icon-flip-camera-fill', 'icon-flip-camera', 'icon-game-controller', 'icon-guitar', 'icon-headphones', 'icon-headset', 'icon-industry', 'icon-laptop', 'icon-media', 'icon-microphone', 'icon-midi-fill', 'icon-midi', 'icon-mixer', 'icon-movie-camera-fill', 'icon-movie-camera', 'icon-music-cloud', 'icon-music-heart', 'icon-music-list', 'icon-music-note-1', 'icon-music-note-2', 'icon-music-note-fill-1', 'icon-music-note-fill-2', 'icon-music-player', 'icon-music-star', 'icon-pause-fill', 'icon-pause', 'icon-photo-camera-fill', 'icon-photo-camera', 'icon-piano-keys', 'icon-picture', 'icon-play-fill', 'icon-play', 'icon-radio', 'icon-record-player', 'icon-rewind-fill', 'icon-rewind', 'icon-skip-back-fill', 'icon-skip-back', 'icon-skip-forward-fill', 'icon-skip-forward', 'icon-small-camara', 'icon-smart-phone', 'icon-speaker', 'icon-stop-fill', 'icon-stop', 'icon-tablet', 'icon-tape', 'icon-ticket', 'icon-treble-cleff', 'icon-tv-heart', 'icon-tv-location', 'icon-tv-play', 'icon-tv-star', 'icon-tv', 'icon-upload-photo', 'icon-vinyl-record', 'icon-visual-eye-fill', 'icon-visual-eye', 'icon-volume-1', 'icon-volume-2', 'icon-volume-3', 'icon-volume-4', 'icon-volume-levels', 'icon-volume-off', 'icon-acorn', 'icon-anchor', 'icon-axe', 'icon-back-pack', 'icon-beach', 'icon-binoculars', 'icon-boat', 'icon-butterfly', 'icon-camp-fire', 'icon-cantine', 'icon-clover', 'icon-compass-1', 'icon-compass-2', 'icon-dead-tree', 'icon-earth', 'icon-ever-greens', 'icon-fire-fill', 'icon-fire', 'icon-fish-hook', 'icon-flashlight', 'icon-flower-1', 'icon-flower-2', 'icon-flower-3', 'icon-flower-4', 'icon-flower-5', 'icon-flower-pot', 'icon-kayak', 'icon-lady-bug', 'icon-lantern', 'icon-leaf-1', 'icon-leaf-2', 'icon-leaf-3', 'icon-leaf-4', 'icon-life-raft', 'icon-love-recycle', 'icon-mountains', 'icon-mushroom', 'icon-north', 'icon-picnic-table', 'icon-pine-cone', 'icon-pocket-knife', 'icon-pot-plant-fill', 'icon-pot-plant', 'icon-sail-boat', 'icon-ship-wheel', 'icon-swimming', 'icon-telescope', 'icon-tent', 'icon-trailer', 'icon-tree-1', 'icon-tree-2', 'icon-tree-3', 'icon-water', 'icon-worldwide', 'icon-add-to-cart', 'icon-bank', 'icon-barcode', 'icon-calculator', 'icon-cash-bag', 'icon-cash-bands', 'icon-cash-dispenser', 'icon-cash-money', 'icon-cash-register', 'icon-cash-stack', 'icon-cash', 'icon-check', 'icon-checkout', 'icon-cogs', 'icon-coins', 'icon-credit-card-back', 'icon-credit-card-front', 'icon-diamond', 'icon-dollar', 'icon-euro', 'icon-gift', 'icon-graph-down-1', 'icon-graph-down-2', 'icon-graph-up-1', 'icon-graph-up-2', 'icon-money', 'icon-open-sign', 'icon-percent', 'icon-pie-graph', 'icon-pound', 'icon-reciept-1', 'icon-reciept-2', 'icon-reconciled', 'icon-safe', 'icon-scale-1', 'icon-scale-2', 'icon-shippment', 'icon-shopping-bag-add', 'icon-shopping-bag-empty', 'icon-shopping-bag-fill', 'icon-shopping-bag-open', 'icon-shopping-bag-sub', 'icon-shopping-bag', 'icon-shopping-basket', 'icon-shopping-cart', 'icon-stocks-down', 'icon-stocks-up', 'icon-store-front', 'icon-tag-1', 'icon-tag-2', 'icon-tag-3', 'icon-tag-fill-1', 'icon-wallet', 'icon-yen', 'icon-arrow-target', 'icon-badminton-racket', 'icon-badminton-shuttlecock', 'icon-ball-and-bat', 'icon-barbell', 'icon-baseball-bat', 'icon-baseball-diamond', 'icon-baseball-glove', 'icon-baseball-helmet', 'icon-basebeall', 'icon-basketball-court', 'icon-basketball-hoop', 'icon-basketball', 'icon-bicycle', 'icon-billiards-table', 'icon-bowling-ball', 'icon-bowling-pin', 'icon-boxing-glove', 'icon-boxing-ring', 'icon-first-place', 'icon-football-1', 'icon-football-2', 'icon-football-field', 'icon-football-helmet', 'icon-football-play', 'icon-goal-post', 'icon-golf-bag', 'icon-golf-ball-on-tee', 'icon-golf-ball', 'icon-golf-hole', 'icon-golf-putter', 'icon-hockey-puck-stick', 'icon-hockey-rink', 'icon-home-plate', 'icon-jersey', 'icon-lacrosse', 'icon-ping-pong', 'icon-pool-ball-fill', 'icon-pool-ball', 'icon-pool-rack', 'icon-race-flag-fill', 'icon-race-flag', 'icon-rugby-ball', 'icon-skateboard', 'icon-soccer-field', 'icon-soccerball-fiil', 'icon-soccerball', 'icon-stop-watch', 'icon-tennis-ball', 'icon-tennis-court', 'icon-tennis-racket', 'icon-trophy-fill', 'icon-trophy', 'icon-volleyball', 'icon-whistle', 'icon-axles', 'icon-bolt', 'icon-brakes', 'icon-bus', 'icon-car-battery', 'icon-car-jack', 'icon-car-key', 'icon-car-temp', 'icon-car', 'icon-do-not-enter', 'icon-dropped-pin', 'icon-engine-block', 'icon-engine-cylinder', 'icon-fan', 'icon-filter', 'icon-fork-lift', 'icon-four-wheeler', 'icon-gas-pump', 'icon-gear-shift', 'icon-hazard', 'icon-headlight', 'icon-left-right', 'icon-location-1', 'icon-location-2', 'icon-location-3', 'icon-location-fill-1', 'icon-location-fill-2', 'icon-location-fill-3', 'icon-map-location-1', 'icon-map-location-2', 'icon-map-location-fill-1', 'icon-map-location-fill-2', 'icon-motor-bike', 'icon-motor-oil', 'icon-no-parking', 'icon-oil-drip-1', 'icon-oil-drip-2', 'icon-parking', 'icon-race-helmet', 'icon-road-sign', 'icon-road', 'icon-speedometer', 'icon-steering-wheel', 'icon-stop-sign', 'icon-tank-empty', 'icon-tank-full', 'icon-tire-tread', 'icon-tire-wrench', 'icon-tire', 'icon-traction-control', 'icon-traffic-cone', 'icon-traffic-light', 'icon-truck', 'icon-turn-left', 'icon-turn-right', 'icon-windshield-wiper', 'icon-celsius', 'icon-clear-night', 'icon-clearn-night-moon', 'icon-cloud-stars', 'icon-cloudy-fill', 'icon-cloudy-night', 'icon-cloudy', 'icon-dusk', 'icon-eclipse', 'icon-fahrenheit', 'icon-haze-night', 'icon-haze', 'icon-heavy-fog', 'icon-heavy-rain-fill', 'icon-heavy-rain', 'icon-heavy-snow', 'icon-hurricane-fill', 'icon-hurricane', 'icon-ice', 'icon-light-fog', 'icon-light-rain', 'icon-light-sleet', 'icon-light-snow', 'icon-light-storm', 'icon-lightning-1', 'icon-lightning-2', 'icon-lightning-fill-1', 'icon-lightning-fill-2', 'icon-moon-fill', 'icon-moon', 'icon-multiple-clouds', 'icon-night-day', 'icon-partly-cloudy', 'icon-rain-cloud-1', 'icon-rain-cloud-2', 'icon-rain', 'icon-rainbow', 'icon-sleet', 'icon-snow-shovel', 'icon-snow', 'icon-storm', 'icon-sun-down', 'icon-sun-up', 'icon-sun-valley', 'icon-sunny-fill', 'icon-sunny', 'icon-thermometer-1', 'icon-thermometer-2', 'icon-tornado', 'icon-umbrella-fill', 'icon-umbrella', 'icon-weather-alert', 'icon-weather-balloon', 'icon-weather-radar', 'icon-wind-sock', 'icon-wind-speed-censor', 'icon-wind-turbine', 'icon-windy');
		return $icons;
	}



	function getSubjectsList(){

		if($this->is_server_up){
			$json = file_get_contents($this->get_from_server.'/get/subjects/'.$this->cid);
		}else{
			$json = file_get_contents($this->secondary_path.'assets/grad_assets/js/json/list.json');	
		}
		
		$data = json_decode($json);

		// var_dump($data->Subjects);

		foreach ($data->Subjects as $key => $value) {
			echo "<option value='".$value->subject_id."' >".$value->subject_name."</option>";
		}	
	}






	// test functions


}