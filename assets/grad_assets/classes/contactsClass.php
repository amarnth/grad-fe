<?php
/**
* 
*/
class Contacts extends Grad
{
	
	function get()
	{
		# code...
		if($this->is_redis_up){
		
			$redis = new Redis();

		    	try {
		    		$redis->connect('127.0.0.1', 6379);
					$json = $redis->get("std008_47851244_userinfo");
					$user_data = json_decode($json);
		    	} catch (Exception $e) {
		    		$json = file_get_contents('http://localhost/grad_coral/assets/grad_assets/js/json/virginCall.json');
					$data = json_decode($json);
					$user_data = $data->UserInfo;
		    	}
		
		}else{
		
			if("student" == $_SESSION['user_role']){

				$json = file_get_contents($this->server_path.'/get/student/friends/'.$_SESSION['user_id']);

				$data = json_decode($json);
				//var_dump($json);

				$contacts = $data->Friends;
			   
			}elseif("staff" == $_SESSION['user_role']){

				$json = file_get_contents($this->server_path.'/get/students/staff/'.$_SESSION['user_id']);

				$data = json_decode($json);
				//var_dump($json);

				$contacts = $data->Students;

			}
		
					// $json = file_get_contents($this->path.'/grad_assets/js/json/virginCall.json');
					// $data = json_decode($json);
					// $user_data = $data->UserInfo;
					
		
		}

    	
	    
		// $contacts = $user_data->Friends;

		foreach ($contacts as $key => $value) {
			
	        $pic = 'assets/grad_assets/'.$value->prof_pic;
		?>
		<div class="col-md-6 bg-white border-bottom contacts" data-s="<?php echo $value->name.' '.$value->dept;?>">
            <div class="row">
                <div class="col-sm-9">
                    <div class="media">
                        <a class="pull-left margin-none" href="#" style="background: url('<?php echo $pic; ?>') no-repeat; background-size: 100px 143px; width: 100px; height: 100px;">
                            <!-- <img class="img-clean" src="<?php echo $pic;?>" alt="..." style="width: 100px; height: 100px;"> -->
                        </a>
                        <div class="media-body innerAll inner-2x padding-right-none padding-bottom-none">
                            <h4 class="media-heading">
                            	<?php
                            		$base64_encrypt = Decipher::base64_encrypt($value->id);
                            	?>
                                <a href="?profile=<?php echo $base64_encrypt; ?>"><?php echo $value->name; ?></a>
                            </h4>
                            <p><?php echo $value->dept; ?></p>
                            <p style="text-transform: capitalize;">
                                <span class="text-success strong"><i class="fa fa-check"></i> Friend</span>
                                &nbsp; <i class="fa fa-fw fa-map-marker text-muted"></i> Living in <?php echo $value->city; ?>, <?php echo $value->state; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="innerAll text-right">
                        <div class="btn-group-vertical btn-group-sm">
                            <a href="" class="btn btn-info"><i class="fa fa-thumbs-up"></i> Like</a>
                            <a href="" class="btn btn-default"><i class="fa fa-envelope-o"></i> Chat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php

		}
	}
}
?>