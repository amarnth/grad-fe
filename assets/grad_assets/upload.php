<?php
include 'classes/myClass.php';

$create_file_path = new Fileupload();

if(!isset($_SESSION)){
	// header("Location: http://localhost/gradpower/assignments.php");
}

if(strpos($_SERVER['HTTP_REFERER'], 'page=assignments') !== false){
	$output_dir = $create_file_path->endorsement("assignments");
}

if(strpos($_SERVER['HTTP_REFERER'], 'page=groups') !== false){
	$output_dir = $create_file_path->endorsement("groups");
}


if(strpos($_SERVER['HTTP_REFERER'], 'page=noticeboard') !== false){
	$output_dir = $create_file_path->endorsement("noticeboard");
}


if(strpos($_SERVER['HTTP_REFERER'], 'page=group_wall') !== false){
	$output_dir = $create_file_path->endorsement("groups_wall");
}


if(strpos($_SERVER['HTTP_REFERER'], 'page=project_view') !== false){
	$output_dir = $create_file_path->endorsement("projects");
}


$rest = substr($_SERVER['HTTP_REFERER'], -1);

if($rest=="?"){
	$output_dir = $create_file_path->endorsement("wall");
}

if(strpos($_SERVER['HTTP_REFERER'], 'page=index') !== false){
	$output_dir = $create_file_path->endorsement("wall");
}

// var_dump($_FILES["myfile"]);

// var_dump($_POST);

	// Desired folder structure
	// $output_dir = 'files/rsm101/assignments/stf001/';

	// To create the nested structure, the $recursive parameter 
	// to mkdir() must be specified.

	// if (!mkdir($output_dir, 0777, true)) {
	//     die('Failed to create folders...');
	// }



if(isset($_FILES["myfile"]))
{
	$ret = array();

	//$path = array();

	$error =$_FILES["myfile"]["error"];
	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData() 
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$fileName = $_FILES["myfile"]["name"];
 	 	 	 	
 	 			$type = $_FILES["myfile"]["type"];

				if (strpos($type,'image') !== false) {
				    
				}
 	 	
 		move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
    	$ret[]= $output_dir.$fileName;

    	//$path[] = $output_dir.$fileName;
	}
	else  //Multiple files, file[]
	{
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
	  	$fileName = $_FILES["myfile"]["name"][$i];

	  	$ex = file_exists($output_dir.$fileName);

		move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
	  	$ret[]= $output_dir.$fileName.$ex;
	  	//$path[] = $output_dir.$fileName;
	  }
	
	}
    echo json_encode($ret);
    //echo json_encode($path);
}


if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
{
	$fileName =$_POST['name'];
	//$filePath = $output_dir. $fileName;
	$filePath = $fileName;
	if (file_exists($filePath)) 
	{
        unlink($filePath);
    }
	echo "Deleted File ".$fileName."<br>";
}

 ?>