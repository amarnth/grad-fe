(function($)
{

	// World map markers
	window.initWorldMapMarkers = function()
	{
		$('#world-map-markers').vectorMap({
			map: 'world_mill_en',
			scaleColors: ['#C8EEFF', '#0071A4'],
			normalizeFunction: 'polynomial',
			hoverOpacity: 0.7,
			hoverColor: false,
			// focusOn:{
			//    	x: 0.5,
			//   	y: 0.5,
			//   	scale: 1.1
			// },
			markerStyle: {
				initial: {
					fill: primaryColor,
					stroke: '#383f47'
				}
			},
			regionStyle: {
				initial: { fill: infoColor }
			},
			backgroundColor: 'transparent',
			zoomOnScroll:false,
			markers: [
                {"latLng": [13.0827, 80.2707], "name": "Chennai"},
                {"latLng": [10.9925, 76.9614], "name": "Coimbatore"},
                {"latLng": [12.9667, 77.5667], "name": "Bangalore"},
                {"latLng": [9.9700, 76.2800], "name": "Cochin"},
                {"latLng": [28.6139, 77.2090], "name": "Delhi"},
            ]
		});
	}
	
	$(window).on('load', function(){

		if ($('#maps_vector_tabs').length)
			return;

        if (!$('#world-map-markers').length)
            return;

		if ($('#world-map-markers.fs').length)
			$('#world-map-markers.fs').height($(window).height() - $('#footer').height() - $('.navbar.main').height());

		setTimeout(function(){
			initWorldMapMarkers();
		}, 100);
	});
	
})(jQuery, window);