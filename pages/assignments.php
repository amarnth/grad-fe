<div class="row row-app">
	<div class="widget show-assignments">
		<div class="widget-body innerAll inner-2x">

			<div class="input-group friends-search row pull-right col-md-4">
				<input class="form-control col-md-6 innerAll assignment-search" type="text" placeholder="Search">
				<div class="input-group-btn">
					<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
				</div>
			</div>
			
			<br>
			<br>

			<div class="panel-group accordion accordion-2" id="accordion">
				<?php if($_SESSION['user_role']=="staff") { ?>
			    <div class="panel panel-default">
			    	<div class="panel-heading">
		                <a class="accordion-toggle glyphicons circle_plus font" data-toggle="collapse" data-parent="#accordion" href="#create-a-new-assignment"><i></i>Create a New Assignment</a>
			        </div>

			        <div id="create-a-new-assignment" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="widget create-new" id="">
								<div class="widget-body innerAll inner-2x">
									<form class="margin-none innerLR inner-2x">

						 				  <div class="form-group">
						 				  		<label>Title</label>
									          <input type="text" class="form-control assignment-title" placeholder="Assignment Title">
									      </div>
									      <div class="form-group">
									      	  <label>Topic</label>
									          <input type="text" class="form-control assignment-topic" placeholder="Topic">
									      </div>
									      <div class="form-group">
									      	  <label>Subject</label>
									          <input type="text" class="form-control assignment-subject" placeholder="Subject">
									      </div>
									      <div class="form-group">
									      	  <label>Description</label>
									          <input type="text" class="form-control assignment-desc" placeholder="Description">
									      </div>

									     <div class="form-group">

									            <label>Deadline</label>
									            <div class="input-group date" id="assignment-deadline">
									                <input class="form-control" type="text" />
									                <span class="input-group-addon"><i class="fa fa-th"></i></span>
									            </div>
									        
									      </div>

									       <div class="form-group">
				                              <label>Assignment File <span style='font-size: 12px; color: rgb(147, 50, 50);'><b>Note: </b>Only pdf files are allowed.</span></label>
				                              <div class="btn btn-default btn-file" id="mulitplefileuploader">Select</div>
				                              <div id="newPostPreview"></div>
				                              <div id="mulitplefileuploadStatus"></div>
				                          	</div>

									    <div class="form-group">
								            <label class="">Assigned To: </label>
								            <div class="radio ">
							                    <label class="radio-custom select-student col-md-1">
							                        <input type="radio" name="radio" checked="checked"> 
							                        <i class="fa fa-circle-o checked"></i> Individual
							                    </label> 
							                    <label class="radio-custom select-department col-md-2"> 
							                        <input type="radio" name="radio"> 
							                        <i class="fa fa-circle-o"></i> Department
							                    </label> 
							                </div> 
									    </div>

									       
						                
						                <div class="form-group">
							                
										</div>

										<div class="form-group get-student">
						                      <label class="control-label">Select Students</label>
						                      <select multiple="multiple" style="width: 100%;" id="assignment-select-students">
						                         <optgroup label="Alaskan/Hawaiian Time Zone">
						                             <option value="AK">Alaska</option>
						                             <option value="HI">Hawaii</option>
						                         </optgroup>
						                         <optgroup label="Pacific Time Zone">
						                             <option value="CA"><span hidden style="display: none !important;">CA</span> California</option>
						                             <option id="NV" value="NV">Nevada</option>
						                             <option id="OR" value="OR">Oregon</option>
						                             <option value="WA">Washington</option>
						                         </optgroup>
						                         <optgroup label="Mountain Time Zone">
						                             <option value="AZ">Arizona</option>
						                             <option value="CO">Colorado</option>
						                             <option value="ID">Idaho</option>
						                             <option value="MT">Montana</option><option value="NE">Nebraska</option>
						                             <option value="NM">New Mexico</option>
						                             <option value="ND">North Dakota</option>
						                             <option value="UT">Utah</option>
						                             <option value="WY">Wyoming</option>
						                         </optgroup>
						                      </select>
						                  </div>

						                  <div class="form-group get-department">
						                      <label class="control-label">Select Department</label>
						                      <select multiple="multiple" style="width: 100%;" id="assignment-select-department">
											</select>
						                  </div>


									      <div class="text-center innerAll">
									          <a href="#" id="post_new_assignment" class="btn btn-primary create_a_group" data-dismiss="modal" aria-hidden="true">Create</a>
									      </div>

									</form>

								</div>
							</div>
						</div>
					</div>
			    </div>

			    <?php } ?>
			</div>



			<div class="innerAll bg-white">
    			<div class="row">

					<div id="assignmentTasks">
					    <?php
					    	$assignment = new Assignments();
					    	$assignment->get();
					    ?>
				    </div>

				</div>
			</div>



		</div>
	</div> 


  	

	
</div>






<style>



.ajax-file-upload-error{
  color: red;
}

.ajax-file-upload-statusbar {
margin-top: 10px;
width: 420px;
margin-right: 10px;
margin: 5px;
padding: 5px 5px 5px 5px
}
.ajax-file-upload-filename {
display: inline-block;
height: auto;
margin: 0 5px 5px 10px;
color: #807579
}

.ajax-file-upload-progress {
    display: none !important;
}
.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}
.ajax-file-upload-green {
    display: none !important;;
}

.ajax-file-upload-bar {
background-color: #0ba1b5;
width: 0;
height: 20px;
border-radius: 3px;
color:#FFFFFF;
}
.ajax-file-upload-percent {
position: absolute;
display: inline-block;
top: 3px;
left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
  text-align: center;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 100% !important;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px;
}


 textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}

</style>