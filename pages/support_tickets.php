<!-- row -->
<div class="row row-app">

	<!-- col -->
	<div class="col-md-12">

		<!-- col-separator -->
		<div class="col-separator box col-separator-first col-unscrollable">

			<!-- col-table -->
			<div class="col-table">

				<div class="innerAll">
					<a href="" class="btn btn-xs pull-right btn-primary"><i class="fa fa-plus fa-fw"></i> New Ticket</a>
					<h4 class="margin-none">Tickets</h4>
					<div class="clearfix"></div>
				</div>
				<div class="col-separator-h"></div>

				<!-- col-table-row -->
				<div class="col-table-row">

					<!-- col-app.col-unscrollable -->
					<div class="col-app col-unscrollable">

						<!-- col-app -->
						<div class="col-app">

							<!-- row -->
							<div class="row row-app">

								<!-- col -->
								<div class="col-md-3">

									<!-- col-separator -->
									<div class="col-separator">

										<!-- sidebar content -->
										<ul class="list-group list-group-1 margin-none borders-none">
											<li class="list-group-item active animated fadeInUp"><a href="#"><span class="badge pull-right badge-primary hidden-md">30</span><i class="fa fa-exclamation-circle"></i> Unread Tickets</a></li>
											<li class="list-group-item animated fadeInUp"><a href="#"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-ticket"></i> New Tickets</a></li>
											<li class="list-group-item animated fadeInUp"><a href="#"><i class="fa fa-spinner"></i> Pending Tickets</a></li>
											<li class="list-group-item border-bottom-none animated fadeInUp"><a href="#"><i class="fa fa-pencil"></i> Drafts</a></li>
											<li class="list-group-item border-bottom-none animated fadeInUp"><a href="#"><i class="icon-folder-fill"></i> Archive</a></li>
										</ul>

										<div class="col-separator-h"></div>
										<h5 class="innerAll border-bottom margin-bottom-none">Filter by</h5>
									
										<form role="form">
											<div class="innerAll inner-2x">			
												<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user text-faded"></i></span>
														<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search by Name">
													</div>
												</div>
												<div class="form-group">
											  		<div class="input-group">
														<span class="input-group-addon"><i class="icon-tag-fill-1 text-faded"></i></span>
											        	<input type="text" class="form-control" id="exampleInputEmail1" placeholder="#ticket-id">
													</div>
												</div>
												<div class="form-group"><div class="input-group date" id="datepicker2">
    <input class="form-control" type="text" value="14 February 2013" />
    <span class="input-group-addon"><i class="fa fa-th"></i></span>
</div>
</div>
												<div class="form-group margin-none">
											  		<label class="checkbox-inline checkbox-custom">
											  			<i class="fa fa-fw fa-square-o"></i>
														<input type="checkbox" id="inlineCheckbox1" value="option1"> New
													</label>
													<label class="checkbox-inline checkbox-custom">
														<i class="fa fa-fw fa-square-o"></i>
														<input type="checkbox" id="inlineCheckbox2" value="option2" checked="checked"> Answered
													</label>
												</div>
											</div>
										  	<div class="text-center border-top innerTB">
										  		<a href="" class="btn btn-primary" ><i class="fa fa-spinner"></i> Update</a>
											</div>
										</form>
										<div class="col-separator-h"></div>
										<!-- // END sidebar-content -->

									</div>
									<!-- // END col-separator -->

								</div>
								<!-- // END col -->

								<!-- col -->
								<div class="col-md-9">

									<!-- col-separator -->
									<div class="col-separator col-separator-last">

										<!-- col-table -->
										<div class="col-table">

											<h4 class="innerAll bg-gray border-bottom margin-bottom-none">Unread Tickets <div class="pull-right display-block"><a href="" class="btn btn-default btn-xs ">Select All</a> <a href="" class="btn btn-default btn-xs ">Mark as Read</a></div></h4>

											<!-- col-table-row -->
											<div class="col-table-row">

												<!-- col-app.col-unscrollable -->
												<div class="col-app col-unscrollable">

													<!-- col-app -->
													<div class="col-app">
														
														<!-- tickets -->
																																										<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/1.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Perpetua Inger</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/2.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Zoticus Axel</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox" checked="checked">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/3.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Yun Ragna</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    	<div class="media">
																		            <a class="pull-left" href="#">
																			             <span class="empty-photo agent">
																			      			<i class="icon-life-raft"></i>
																			      		</span>
																		            </a>
																		            <div class="media-body">
																		              <a href="" class="media-heading "> Bogdan</a> <small class="pull-right">Today <i class="icon-time-clock "></i></small>
																		              <div class="clearfix"></div>
																		              You need to open navbar.main.less and change the line 130 and change it to:
																		               <div class="innerT half">
																		               		<pre class="prettyprint">&:hover {  background:#efefef;}</pre>
																		           		</div>
																		           		
																		            </div>
																		        </div>
																			    																		    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      		<span class="empty-photo"><i class="fa fa-user"></i></span>
																		  																				    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Victor Tacitus</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/5.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Arden Catharine</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/6.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Mihovil Govinda</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/7.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Mariya Hadya</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/8.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Tahir Benedikt</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/9.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Olayinka Kristin</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/10.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Danko Nikodim</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/11.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Zoja Aileas</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/12.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Alphonsus Braidy</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/13.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Helene Liana</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/14.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Sebastian Niklas</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/15.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Elvire Maya</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  																				  		<label class="label label-success"><i class=" icon-checkmark-thick fw"></i></label> 																		    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    <a href="">adipisicing elit</a>. Fugiat molestiae qui fuga ..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<div class="innerAll border-bottom tickets">
															<div class="row">
																
																<div class="col-sm-10">
																	<ul class="media-list">
																		<li class="media">
																		  	<div class="pull-left">
																				<div class="center">
																					<div class="checkbox">
																					    <label class="checkbox-custom">
																					    	<i class="fa fa-fw fa-square-o"></i>
																					      	<input type="checkbox">
																					    </label>
																					 </div>
																				 </div>
																			</div>
																		    <a class="pull-left" href="#">
																		      																			      	<img class="media-object" src="../assets/images/people/50/16.jpg" alt="..." >
																		      																			    </a>
																		    <div class="media-body">
																		    	<a href="" class="media-heading">Kerman Otakar</a><label class="label label-default">#2129</label>
																		  		<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a> </label>
																		  		 <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pending</a> </label> 
																		  																				    	<div class="clearfix"></div>
																		    	<strong>[RE: Coral Question]</strong> Lorem ipsum dolor sit amet, consectetur    repellendus amet distinctio inventore possimus cum dicta enim!..
																		    	<div class="clearfix"></div>
																		    	<small><i class="icon-time-clock fw"></i> on: 12:51 AM</small>  <small><i class=" icon-life-raft innerL"></i> Agent: <a href="">Bogdan</a></small>
																																						    </div>
																		</li>
																	</ul>
																</div>
																<div class="col-sm-2">
																	<div class="innerT pull-right">
																		<a href="" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Mark Solved</a>
																	</div>
																</div>
															</div>
														</div>
																												<!-- // END tickets -->

													</div>
													<!-- // END col-app -->

												</div>
												<!-- // END col-app.col-unscrollable -->

											</div>
											<!-- // END col-table-row -->

										</div>
										<!-- // END col-table -->

									</div>
									<!-- // END col-separator -->

								</div>
								<!-- // END col -->

							</div>
							<!-- // END row -->

						</div>
						<!-- // END col-app -->

					</div>
					<!-- // END col-app.col-unscrollable -->

				</div>
				<!-- // END col-table-row -->

			</div>
			<!-- // END col-table -->

		</div>
		<!-- // END col-separator -->

	</div>
	<!-- // END col -->

</div>
<!-- // END row -->




