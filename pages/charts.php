<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Charts
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Simple chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Simple Chart -->
                                        <div id="chart_simple" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-white">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Lines chart with fill & without points
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Chart with lines and fill with no points -->
                                        <div id="chart_lines_fill_nopoints" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Ordered bars chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Ordered bars Chart -->
                                        <div id="chart_ordered_bars" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-white">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Donut chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Donut Chart -->
                                        <div id="chart_donut" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Stacked bars chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Stacked bars Chart -->
                                        <div id="chart_stacked_bars" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-white">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Pie chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Pie Chart -->
                                        <div id="chart_pie" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Horizontal bars chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Horizontal Bars Chart -->
                                        <div id="chart_horizontal_bars" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                                <!-- Widget -->
                                <div class="widget widget-heading-simple widget-body-white">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Auto updating chart
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body">
                                        <!-- Live Chart -->
                                        <div id="chart_live" class="flotchart-holder"></div>
                                    </div>
                                </div>
                                <!-- // Widget END -->
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->