<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <div class="col-table">
                <div class="innerTB">
                
                
                   <?php
                        if("student" != $_SESSION['user_role']){
                    ?>
                    <h3 class="margin-none pull-left">
                        <!-- Dashboard Analytics &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> -->
                        Dashboard Overview &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> 
                    </h3>
                    
                    <div class="btn-group pull-right">
                        <a href="index.php?page=dashboard" class="btn btn-primary"><i class="fa fa-fw fa-bar-chart-o"></i> Overview</a>
                        <a href="index.php?page=stats_class" class="btn btn-default"><i class="fa fa-fw fa-dashboard"></i> Class</a>
                        <a href="index.php?page=stats_student" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Students</a>
                    </div>
                    <div class="clearfix"></div>
                    <?php }elseif ("student" == $_SESSION['user_role']) {
                        ?>
                        <h3 class="margin-none">
                            Dashboard Overview &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> 
                        </h3>
                        <?php
                    } ?>
                </div>
                <div class="col-separator-h"></div>
                <div class="row">
                    <?php
                        $stats = new Stats();

                        if('staff' == $_SESSION['user_role']){
                            $stats = $stats->getOverview();
                        }

                        if("admin" == $_SESSION['user_role']){
                            $stats = $stats->getAdminOverview();
                        }

                        if("student" == $_SESSION['user_role']){
                            $stats = $stats->getStudentOverview();   
                        }
                    ?>



                    <div class="col-md-4">
                    
                        <!-- // END STATS -->
                        <!-- DEMOGRAPHICS START -->
                        <!-- <div class="box-generic padding-none">
                            <h4 class="strong border-bottom innerAll margin-none">
                                <i class="fa fa-fw icon-worldwide"></i> Demographic
                            </h4>
                            <div class="bg-gray border-bottom text-center">
                                <div id="world-map-markers" style="height: 200px"></div>
                            </div>
                            <ul class="list-unstyled">
                                <li class="innerAll border-bottom">
                                    <div class="pull-right strong">1249</div>
                                    <i class="fa fa-fw fa-circle text-info"></i> Coimbatore
                                </li>
                                <li class="innerAll border-bottom">
                                    <div class="pull-right strong">892</div>
                                    <i class="fa fa-fw fa-circle text-success"></i> Chennai
                                </li>
                                <li class="innerAll border-bottom">
                                    <div class="pull-right strong">530</div>
                                    <i class="fa fa-fw fa-circle text-primary"></i> Cochin
                                </li>
                                <li class="innerAll">
                                    <div class="pull-right strong">380</div>
                                    <i class="fa fa-fw fa-circle"></i> Bangalore
                                </li>
                            </ul>
                        </div> -->
                        <!-- // END DEMOGRAPHICS -->
                       
                    </div>

                </div>
                <div class="col-separator-h"></div>
            </div>
        </div>
    </div>
</div>