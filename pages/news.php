<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-separator-first box bg-none">
            <div class="col-table">
                <!-- <div class="bg-white">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="innerAll margin-none">
                            News
                        </h4>
                    </div>
                    <div class="col-md-8">
                        <div class="tabsbar tabsbar-2 bg-gray margin-none">
                            <ul class="row row-merge">
                                <li class="col-md-6 active">
                                    <a href="#tab1-4" data-toggle="tab"><i class="fa fa-fw fa-file-text-o"></i> Latest news</a>
                                </li>
                                <li class="col-md-6">
                                    <a href="#tab2-4" data-toggle="tab"><i class="fa fa-fw fa-file-text-o"></i> <span>Announcements</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            --> 
            <div class="innerTB">
                <h3 class="pull-left margin-none">
                    News &nbsp;<i class="fa fa-fw text-primary icon-refresh-star-fill"></i> <i class="fa fa-fw text-muted icon-refresh-star-fill"></i>
                </h3>
                <div class="btn-group btn-group-sm pull-right">
                    <a href="" class="btn btn-primary"><i class="fa fa-th"></i></a>
                    <a href="" class="btn btn-default"><i class="fa fa-list"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-separator-h"></div>
            <div class="col-table-row">
                <div class="col-app col-unscrollable bg-none">
                    <div class="col-app">
                        <div id="news-featured-2" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326648-signed-contract-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/6.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Getting the job done
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 613</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 78</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326650-pointing-at-document-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/7.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            UX The Right Way
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 660</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 55</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374559-laboratory-tubes-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/8.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Dexter's Real Laboratory
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 630</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 104</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374531-at-celebration-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/9.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Like there's no tomorrow
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 264</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 91</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326648-signed-contract-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/6.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Getting the job done
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 513</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 72</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326650-pointing-at-document-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/7.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            UX The Right Way
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 515</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 68</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374559-laboratory-tubes-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/8.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Dexter's Real Laboratory
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 255</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 78</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374531-at-celebration-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/9.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Like there's no tomorrow
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 277</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 94</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326648-signed-contract-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/6.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Getting the job done
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 671</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 70</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6326650-pointing-at-document-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/7.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            UX The Right Way
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 676</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 72</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374559-laboratory-tubes-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/8.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Dexter's Real Laboratory
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 512</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 96</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="box-generic padding-none margin-none overflow-hidden">
                                    <div class="relativeWrap overflow-hidden" data-height="175px">
                                        <img src="../assets/images/photodune-6374531-at-celebration-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                                        <div class="fixed-bottom bg-inverse-faded">
                                            <div class="media margin-none innerAll">
                                                <a href="" class="pull-left">
                                                    <img src="../assets/images/people/35/9.jpg" alt="" />
                                                </a>
                                                <div class="media-body text-white">
                                                    by <strong>Ewan Shelah</strong>

                                                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4>
                                            Like there's no tomorrow
                                        </h4>
                                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
                                    </div>
                                    <div class="row row-merge">
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 462</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 56</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-separator-h box"></div>
            <div id="news-featured-1" class="owl-carousel owl-theme">
                <div class="item">
                    <div class="row row-merge bg-gray">
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, voluptatum, distinctio eius aliquam sed ad consequatur debitis commodi at architecto est itaque laudantium sint tempora inventore alias qui libero accusantium?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row row-merge bg-gray">
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, voluptatum, distinctio eius aliquam sed ad consequatur debitis commodi at architecto est itaque laudantium sint tempora inventore alias qui libero accusantium?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row row-merge bg-gray">
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, voluptatum, distinctio eius aliquam sed ad consequatur debitis commodi at architecto est itaque laudantium sint tempora inventore alias qui libero accusantium?</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="innerAll inner-2x">
                                <h4>
                                    <a href="" class="text-primary">Best of Web Design in 2013</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem. Commodi, numquam repellendus neque adipisci cumque dicta rem. Tempore, tempora beatae unde ducimus. Blanditiis?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-separator-h"></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="widget widget-tabs widget-tabs-double-2 widget-tabs-vertical widget-tabs-responsive row row-merge box-generic overflow-hidden padding-none">
                        <div class="col-md-8">
                            <div id="news-featured-3" class="owl-carousel owl-theme">
                                <div class="item">
                                    <a href="">
                                        <img src="../assets/images/photodune-3598444-caveman-xs.jpg" alt="" class="img-responsive img-clean">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <img src="../assets/images/photodune-6374559-laboratory-tubes-xs.jpg" alt="" class="img-responsive img-clean">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <img src="../assets/images/photodune-6374531-at-celebration-xs.jpg" alt="" class="img-responsive img-clean">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="widget-head col-md-4 border-bottom bg-gray">
                            <ul>
                                <li class="active">
                                    <a class="glyphicons list" href="#tabAll" data-toggle="tab"><i></i><span>All topics</span></a>
                                </li>
                                <li>
                                    <a class="glyphicons pencil" href="#tabAccount" data-toggle="tab"><i></i><span>Design</span></a>
                                </li>
                                <li>
                                    <a class="glyphicons user" href="#tabUsability" data-toggle="tab"><i></i><span>Usability</span></a>
                                </li>
                                <li>
                                    <a class="glyphicons snowflake" href="#tabContent" data-toggle="tab"><i></i><span>Content</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-generic bg-white innerAll inner-2x">
                        <div class="media margin-none">
                            <a href="" class="pull-left">
                                <img src="../assets/images/photodune-5868894-scary-and-evil-clown-smiling-in-dark-spooky-style-xs.jpg" alt="" width="100">
                            </a>
                            <div class="media-body innerTB">
                                <h4>
                                    <a href="">Best of Web Design</a>
                                </h4>
                                <p class="text-muted">30/12/2013</p>
                            </div>
                        </div>
                        <div class="separator bottom"></div>
                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita exercitationem.</p>
                    </div>
                    <div class="box-generic padding-none overflow-hidden bg-gray">
                        <div class="innerAll inner-2x border-bottom">
                            <h4>
                                <a href="">Best of Web Design in 2013</a>
                            </h4>
                            <p class="text-muted">30/12/2013</p>
                            <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, est, iste similique eius perspiciatis expedita.</p>
                        </div>
                        <div class="row row-merge bg-white">
                            <div class="col-md-6">
                                <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> 391</a>
                            </div>
                            <div class="col-md-6">
                                <a href="" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-star-fill"></i> 78</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // END col-separator.box -->
</div>
<!-- // END col -->
</div>
<!-- // END row-app -->