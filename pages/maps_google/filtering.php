<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- heading -->
                <div class="innerTB">
                    <h3 class="margin-none pull-left">
                        Google Maps &nbsp;<i class="fa fa-fw fa-map-marker text-muted"></i>
                    </h3>
                    <div class="btn-group pull-right"> <a href="index.php?page=maps_vector" class="btn btn-default">Vector maps</a> <a href="index.php?page=maps_google" class="btn btn-primary">Google Maps</a> </div>
                    <div class="clearfix"></div>
                </div>
                <!-- // END heading -->
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app.col-unscrollable -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- Tabs -->
                            <div class="widget widget-tabs widget-tabs-vertical row row-merge" id="maps_google_tabs">
                                <!-- Tabs Heading -->
                                <div class="widget-head bg-gray col-md-3">
                                    <ul>
                                        <li><a href="?page=maps_google">Load markers from JSON</a></li>
                                        <li><a href="?page=maps_google&amp;section=clustering">Clustering</a></li>
                                        <li><a href="?page=maps_google&amp;section=extend-pagination">Extend with pagination</a></li>
                                        <li class="active"><a href="?page=maps_google&amp;section=filtering">Filtering</a></li>
                                        <li><a href="?page=maps_google&amp;section=geocoding">Click and drag with geo search</a></li>
                                        <li><a href="?page=maps_google&amp;section=streetview">Streetview with microformats</a></li>
                                        <li>
                                            <a href="?page=maps_google&amp;section=fullscreen"><span class="pull-right label label-primary">new</span> Full screen google map</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- // Tabs Heading END -->
                                <div class="widget-body col-md-9">
                                    <!-- Tab content -->
                                    <div class="tab-content innerAll inner-2x">
                                        <!-- Filtering -->
                                        <h4 class="separator bottom">
                                            Filtering
                                        </h4>
                                        <div class="thumbnail">
                                            <div class="map_canvas" id="google-map-filters" style="height: 400px"></div>
                                        </div>
                                        <div id="radios" class="well"></div>
                                        <!--<div id="tags-control" class="well">
                                        <label style="color:#555;font-size:12px; font-weight:bold;" for="tags">Filter by tag</label>
                                        <select id="tags" style="outline:none;"></select>
                                    </div>
                                    -->

                                    <!-- // Filtering END -->
                                </div>
                                <!-- // Tab content END -->
                            </div>
                        </div>
                        <!-- // Tabs END -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-app.col-unscrollable -->
            </div>
            <!-- // END col-table-row -->
        </div>
        <!-- // END col-table -->
    </div>
    <!-- // END col-separator -->
</div>
<!-- // END col -->
</div>
<!-- // END row -->