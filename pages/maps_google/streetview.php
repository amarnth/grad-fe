<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- heading -->
                <div class="innerTB">
                    <h3 class="margin-none pull-left">
                        Google Maps &nbsp;<i class="fa fa-fw fa-map-marker text-muted"></i>
                    </h3>
                    <div class="btn-group pull-right"> <a href="index.php?page=maps_vector" class="btn btn-default">Vector maps</a> <a href="index.php?page=maps_google" class="btn btn-primary">Google Maps</a> </div>
                    <div class="clearfix"></div>
                </div>
                <!-- // END heading -->
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app.col-unscrollable -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- Tabs -->
                            <div class="widget widget-tabs widget-tabs-vertical row row-merge" id="maps_google_tabs">
                                <!-- Tabs Heading -->
                                <div class="widget-head bg-gray col-md-3">
                                    <ul>
                                        <li><a href="?page=maps_google">Load markers from JSON</a></li>
                                        <li><a href="?page=maps_google&amp;section=clustering">Clustering</a></li>
                                        <li><a href="?page=maps_google&amp;section=extend-pagination">Extend with pagination</a></li>
                                        <li><a href="?page=maps_google&amp;section=filtering">Filtering</a></li>
                                        <li><a href="?page=maps_google&amp;section=geocoding">Click and drag with geo search</a></li>
                                        <li class="active"><a href="?page=maps_google&amp;section=streetview">Streetview with microformats</a></li>
                                        <li>
                                            <a href="?page=maps_google&amp;section=fullscreen"><span class="pull-right label label-primary">new</span> Full screen google map</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- // Tabs Heading END -->
                                <div class="widget-body col-md-9">
                                    <!-- Tab content -->
                                    <div class="tab-content innerAll inner-2x">
                                        <!-- Streetview -->
                                        <h4 class="separator bottom">
                                            Streetview with microformats <span>Click on marker or "Spinal Tap" title to open Street View</span>
                                        </h4>
                                        <div class="thumbnail">
                                            <div class="map_canvas" id="google-map-streetview" style="height: 400px"></div>
                                        </div>
                                        <div class="separator"></div>
                                        <div class="vevent well box-generic bg-gray margin-none">
                                            <a href="#spinaltap">
                                                <h3 class="url summary">
                                                    Spinal Tap
                                                </h3>
                                            </a>
                                            <p class="box-generic">After their highly-publicized search for a new
drummer, Spinal Tap kicks off their latest comeback tour with a San
Francisco show.</p>
                                            <p>
                                                <strong>When:</strong>

                                                <span class="dtstart"> Oct 15, 7:00PM <span class="value-title" title="2015-10-15T19:00-08:00"></span></span>
                                                - 
                                                <span class="dtend"> 9:00PM<span class="value-title" title="2015-10-15T21:00-08:00"></span></span>
                                                <br/>
                                                <strong>Where:</strong>

                                                <span class="location vcard">
                                                    <span class="fn org">Warfield Theatre</span>, 
                                                    <span class="adr"> <span class="street-address">982 Market St</span>, <span class="locality">San Francisco</span>, <span class="region">CA</span>
</span>
                                                    <span class="geo">
                                                        <span class="latitude"><span class="value-title" title="37.774929"></span></span>
                                                        <span class="longitude"><span class="value-title" title="-122.419416"></span></span>
                                                    </span>
                                                </span>
                                            </p>
                                        </div>
                                        <!-- // Streetview END -->
                                    </div>
                                    <!-- // Tab content END -->
                                </div>
                            </div>
                            <!-- // Tabs END -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->