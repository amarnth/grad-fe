<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <!-- col-separator.box -->
    <div class="col-separator col-unscrollable box">
        <!-- col-table -->
        <div class="col-table">
            <h4 class="innerAll margin-none border-bottom text-center bg-primary">
                <i class="fa fa-pencil"></i> Create a Student Account
            </h4>
            <!-- col-table-row -->
            <div class="col-table-row">
                <!-- col-app -->
                <div class="col-app col-unscrollable">
                    <!-- col-app -->
                    <div class="col-app innerAll">
                        <div class="login">
                            <div class="placeholder text-center">
				<img src="assets/grad_assets/img/signup_logo.png" alt="" style="border: none;">
                            </div>
                            <div class="panel panel-default col-sm-8 col-md-offset-2">
                                <div class="panel-body">
                                    <form role="form" id="validateForm">

                                        <div>
                                            <a style="float: right;" href="index.php?page=signup_staff" class="btn btn-primary">Create Staff Account</a>

                                            <a style="text-align: left; margin-bottom: 15px;" href="index.php?page=login" class="btn btn-primary">back to Login</a><br>
                                            
                                        </div>
                                        

                                        <div class="col-md-12">
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="exampleInputEmail1">First Name</label>
                                                    <input type="text" class="form-control" name="firstname" id="fname" placeholder="First Name">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="exampleInputEmail1">Last Name</label>
                                                    <input type="text" class="form-control" name="lastname" id="lname" placeholder="Last Name">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-6 form-group">
                                                <label for="gender">Gender</label>
                                                <select class="selectpicker gender" data-style="btn-default">
                                                  <option>Female</option>
                                                  <option>Male</option>
                                                </select>
                                            </div>
                                        </div>
                                                <style type="text/css">
                                                    .btn-group.bootstrap-select{
                                                        width: 100% !important;
                                                    }
                                                </style>

                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                <div class="col-md-12">
                                                    <label for="rollno">Roll No</label>
                                                    <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="CSE001245">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-10">
                                                <div class="col-md-6 form-group">
                                                    <label for="email">Email address</label>
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control" name="password" id="password" placeholder="*******">
                                                </div>
                                            </div>
                                        </div>
                                        

                                        <div class="col-md-12">
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="number">Mobile Number</label>
                                                    <input type="number" class="form-control" name="mobile_number" id="mobile_number" placeholder="9999999999">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="dob">Date of Birth</label>
                                                    <div class="input-group date" id="dob">
                                                        <input class="form-control" type="text" value="14 Jan 1990" />
                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                    </div>
                                                    <!-- <input type="date" class="form-control" id="" placeholder="mm/dd/yyyy"> -->
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="state">State</label>
                                                 <!--    <select style="width: 100%;" id="select2_1">
                                                       <optgroup label="Alaskan/Hawaiian Time Zone">
                                                           <option value="AK">Alaska</option>
                                                           <option value="HI">Hawaii</option>
                                                       </optgroup>
                                                       <optgroup label="Pacific Time Zone">
                                                           <option value="CA">California</option>
                                                           <option value="NV">Nevada</option>
                                                           <option value="OR">Oregon</option>
                                                           <option value="WA">Washington</option>
                                                       </optgroup>
                                                       <optgroup label="Mountain Time Zone">
                                                           <option value="AZ">Arizona</option>
                                                           <option value="CO">Colorado</option>
                                                           <option value="ID">Idaho</option>
                                                           <option value="MT">Montana</option><option value="NE">Nebraska</option>
                                                           <option value="NM">New Mexico</option>
                                                           <option value="ND">North Dakota</option>
                                                           <option value="UT">Utah</option>
                                                           <option value="WY">Wyoming</option>
                                                       </optgroup>
                                                       <optgroup label="Central Time Zone">
                                                           <option value="AL">Alabama</option>
                                                           <option value="AR">Arkansas</option>
                                                           <option value="IL">Illinois</option>
                                                           <option value="IA">Iowa</option>
                                                           <option value="KS">Kansas</option>
                                                           <option value="KY">Kentucky</option>
                                                           <option value="LA">Louisiana</option>
                                                           <option value="MN">Minnesota</option>
                                                           <option value="MS">Mississippi</option>
                                                           <option value="MO">Missouri</option>
                                                           <option value="OK">Oklahoma</option>
                                                           <option value="SD">South Dakota</option>
                                                           <option value="TX">Texas</option>
                                                           <option value="TN">Tennessee</option>
                                                           <option value="WI">Wisconsin</option>
                                                       </optgroup>
                                                       <optgroup label="Eastern Time Zone">
                                                           <option value="CT">Connecticut</option>
                                                           <option value="DE">Delaware</option>
                                                           <option value="FL">Florida</option>
                                                           <option value="GA">Georgia</option>
                                                           <option value="IN">Indiana</option>
                                                           <option value="ME">Maine</option>
                                                           <option value="MD">Maryland</option>
                                                           <option value="MA">Massachusetts</option>
                                                           <option value="MI">Michigan</option>
                                                           <option value="NH">New Hampshire</option><option value="NJ">New Jersey</option>
                                                           <option value="NY">New York</option>
                                                           <option value="NC">North Carolina</option>
                                                           <option value="OH">Ohio</option>
                                                           <option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>
                                                           <option value="VT">Vermont</option><option value="VA">Virginia</option>
                                                           <option value="WV">West Virginia</option>
                                                       </optgroup>
                                                    </select>
                                                     -->

                                                    <input type="text" class="form-control" name="state" id="state" placeholder="state">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="city">City</label>
                                                    <input type="text" class="form-control" name="city" id="city" placeholder="city">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                         <div class="col-md-12">
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="department">Department</label>
                                                    <input type="text" class="form-control" name="department" id="department" placeholder="IT">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="class_name">Class Name</label>
                                                    <input type="text" class="form-control" name="class_name" id="class_name" placeholder="IT1A">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="course">Course</label>
                                                    <input type="text" class="form-control" name="course" id="course" placeholder="B.TECH">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="current_semester">Current Semester</label>
                                                    <input type="text" class="form-control" name="current_semester" id="current_semester" placeholder="4">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        

                                        <div class="col-md-12">
                                            <div class="col-md-6 form-group">
                                                <label for="address">Address</label>
                                                <textarea class="form-control" name="address" id="address" style="height: 108px;"></textarea>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class="col-md-6 form-group">
                                                    <label for="pincode">Pincode</label>
                                                    <input type="text" class="form-control" name="pincode" id="pincode" placeholder="">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="nationality">Nationality</label>
                                                    <input type="text" class="form-control" id="nationality" placeholder="">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="secondary_mark">Secondary Mark</label>
                                                    <input type="text" class="form-control" name="secondary_mark" id="secondary_mark" placeholder="">
                                                </div>
                                                    
                                                <div class="col-md-6 form-group">
                                                    <label for="higher_secondary_mark">Higher Secondary Mark</label>
                                                    <input type="text" class="form-control" name="higher_secondary_mark" id="higher_secondary_mark" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        

                                        <div class="col-md-12 form-group">
                                            <div class="col-md-3">
                                                <label for="pincode">Quota</label>
                                                <select class="selectpicker quota" data-style="btn-default">
                                                  <option>Management</option>
                                                  <option>Merit</option>
                                                </select>
                                            </div>

                                            <div class="col-md-9">
                                                
                                                <label for="skills">Skills</label>
                                                <input type="text" class="form-control" name="skills" id="skills" placeholder="Java,php,python">
                                            </div>
                                        </div>
                                          
                                        <div class="checkbox">
                                          <label class="checkbox-custom">
                                              <i class="fa fa-fw fa-square-o accept_terms"></i>
                                              <input checked="checked" type="checkbox" name="agree" id="agree"> I agree to the Terms of Service.
                                          </label>
                                        </div>
                                       
                                        <button type="submit" class="btn btn-primary btn-block create-account">Create Account</button>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-app.col-unscrollable -->
            </div>
            <!-- // END col-table-row -->
        </div>
        <!-- // END col-table -->
    </div>
    <!-- // END col-separator.box -->
</div>
<!-- // END row-app -->


<style>
    #menu, #menu_kis, #menu-top, .navbar.hidden-print.navbar-primary.main{
        display: none;
    }

    .sidebar.sidebar-fusion #content {
      margin-left: 12px;
    }

    #menu_kis{
        display: none !important;
    }

    .sidebar.sidebar-kis #content {
      margin-right: 0px !important;
    }

    .error {
      padding-top: 10px !important;
    }

</style>