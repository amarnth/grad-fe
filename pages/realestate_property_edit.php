<div class="innerLR">
    <h4 class="innerAll margin-none bg-white">
        Edit Property &nbsp;<i class="fa fa-fw fa-edit"></i>
    </h4>
    <div class="col-separator-h"></div>
    <div class="row">
        <div class="col-sm-9">
            <div class="widget">
                <div class="widget-body innerAll inner-2x">
                    <form class="form-horizontal innerAll" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10 padding-top-none">
                                <input type="email" class="form-control" value="Longford Terrace, Monkstown, Ireland">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-xs-12 col-sm-10 padding-top-none">
                                <textarea class="form-control" rows="5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, inventore, alias, architecto, accusamus magni odit quasi fugiat dolore eaque deserunt quibusdam ad similique saepe fugit consectetur repellendus sapiente veritatis culpa.</textarea>
                                <div class="innerTB text-muted"> <i class="icon-note-pad fa fa-fw"></i> 2 Characters Left </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-3 padding-top-none">
                                <input type="text" class="form-control primary text-center" value="$90,000">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Beds</label>
                            <div class="col-sm-2 padding-top-none">
                                <input type="text" class="form-control" value="2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-2">
                                <select class="selectpicker">
                                    <option>Apartment</option>
                                    <option>House</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Upload</label>
                            <div class="col-sm-10 padding-top-none">
                                <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" class="margin-none" />
                                    </span>
                                    <span class="fileupload-preview"></span> <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>

                                </div>
                                <img data-src="holder.js/100%x150/dark" alt="holder"/>
                                <div class="clearfix"></div>
                                <div class="innerT">
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                    <a href="" class="pull-left innerR">
                                        <img data-src="holder.js/50x50/dark" class="img-clean" alt="holder" />
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Location 
                                <br/>
                                <span class="text-weight-normal">(Click / Drag)</span>
                            </label>
                            <div class="col-sm-10">
                                <div class="thumbnail">
                                    <div class="map_canvas" id="google-map-geocoding" style="height: 400px"></div>
                                </div>
                                <div id="modals"></div>
                            </div>
                        </div>
                        <div class="text-center">
<a href="" class="btn btn-primary">Save Changes</a>
</div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget">
                <h4 class="innerAll margin-bottom-none border-bottom bg-primary">
                    <i class="icon-phone fa fa-fw"></i> Contact
                </h4>
                <div class="widget-body bg-gray innerAll inner-2x">
                    <form role="form">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="" value="Adrian Demian">
                        </div>
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" value="contact@mosaicpro.biz">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="email" class="form-control" value="01 988 232189">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" rows="2">Longford Terrace, Monkstown, Ireland</textarea>
                        </div>
                        <div class="text-right">
                            <a href="" class="btn btn-inverse btn-sm"><i class="fa fa-refresh fa-fw"></i> Update Information</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>