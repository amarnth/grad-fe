<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator bg-none col-separator-first col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <!-- heading -->
                <h4 class="innerAll margin-none bg-white">
                    Calendar
                </h4>
                <p id="data"></p>
                <div class="col-separator-h"></div>
                <!-- // END heading -->
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app.col-unscrollable -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- row -->
                            <div class="row row-app">
                                <!-- col -->
                                <div class="col-md-9">
                                    <!-- col-separator -->
                                    <div class="col-separator bg-none">
                                        <!-- Widget -->
                                        <div class="widget">
                                            <div class="widget-body innerAll inner-2x">
                                                <div data-component>
                                                    <div id="mycalendar"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- // Widget END -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <!-- col-separator -->
                                    <div class="col-separator bg-none col-separator-last">
                                        <!-- Widget -->
                                        <div class="widget widget-body-gray">
                                            <!-- Widget Heading -->
                                            <div class="widget-head">
                                                <h4 class="heading glyphicons calendar">
                                                    <i></i>Mini Calendar
                                                </h4>
                                            </div>
                                            <!-- // Widget Heading END -->
                                            <div class="widget-body innerAll inner-2x">
                                                <div id="datepicker-inline"></div>
                                            </div>
                                        </div>
                                        <!-- // Widget END -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                            </div>
                            <!-- // END row -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->