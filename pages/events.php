<div class="row row-app">
   <!-- Tabs -->
        <div style="text-align: right;" class="innerB">
          <a href="#create-new-event" data-toggle="modal" class="btn btn-sm btn-default create-new-event"> <i class="fa fa-plus fa-fw"></i> CREATE NEW EVENT</a>
        </div>
        <div class="row seperator"></div>
    <div class="relativeWrap" >
        <div class="box-generic">
             <!-- Tabs Heading -->
            <div class="tabsbar tabsbar-2">
                <ul class="row row-merge">
                    <!-- <li class="col-md-2 glyphicons cargo active">
                        <a href="#tab-event-all" data-toggle="tab"><i></i> All</a>
                    </li> -->
                    <li class="col-md-2 glyphicons cargo active">
                         <?php if($_SESSION['user_role']=="admin"){ ?>
                           <a href="#tab-approved" data-toggle="tab"><i></i> <span>Approved</span></a>
                        <?php } ?>

                         <?php if($_SESSION['user_role']!="admin"){ ?>
                          <a href="#tab-invites" data-toggle="tab"><i></i> <span>Invites</span></a>
                        <?php } ?>
                        
                    </li>

                    <li class="col-md-2 glyphicons circle_info">
                        <a href="#tab-attending" data-toggle="tab"><i></i> <span>Attending</span></a>
                    </li>
                    <li class="col-md-2 glyphicons cart_in">
                        <a href="#tab-hosted" data-toggle="tab"><i></i> <span>Hosted</span></a>
                    </li>
                   
                    <li class="col-md-2 glyphicons pencil">
                        <a href="#tab-upcoming" data-toggle="tab"><i></i> <span>Upcoming</span></a>
                    </li>
                   
                   

                    <li class="col-md-2 glyphicons pencil">
                        <?php if($_SESSION['user_role']=="admin"){ ?>
                          <a href="#tab-approval" data-toggle="tab"><i></i> <span>Approval</span></a>
                        <?php } ?>

                         <?php if($_SESSION['user_role']!="admin"){ ?>
                          <a href="#tab-pending" data-toggle="tab"><i></i> <span>Pending</span></a>
                        <?php } ?>
                        
                    </li>

                </ul>
            </div>
            <!-- // Tabs Heading END -->
        </div>
    </div>


    <div id="get_events">
      <?php
          $events = new Events();
          $events->getEvents();
      ?>
    </div>

</div>

    <!-- Modal to view Answers -->
    <div class="modal fade" id="event-invite">
      <div class="modal-dialog">
          <div class="modal-content" id="preview-data">
             
          </div>
      </div>
    </div>

<!-- Modal -->
  <div class="modal fade" id="create-new-event" style="">
      <div class="modal-dialog">
          <div class="modal-content">
              <!-- Modal heading -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">
                      Create New Event
                  </h3>
              </div>
              <!-- // Modal heading END -->
              <!-- Modal body -->
              <div class="modal-body">
                  <div class="innerAll">
                        <input type="text" class="form-control event-name" placeholder="Event Name">

                        <div class="separator bottom"></div>

                        <textarea class="form-control event_description col-md-12" row="5" placeholder="Sample Description"></textarea>

                          <div class="col-md-6">
                              <label>Start Date</label>
                              <div class="input-group date" id="event-start-date">
                                  <input class="form-control" type="text" />
                                  <span class="input-group-addon"><i class="fa fa-th"></i></span>
                              </div>
                          </div>

                          <div class="col-md-6">
                              <label>Start Time</label>
                              <div class="input-group bootstrap-timepicker">
                                  <input id="event-start-time" type="text" class="form-control">
                                  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              </div>
                          </div>
                     

                        <div class="separator bottom"></div>

                      

                          <div class="col-md-6">
                              <label>End Date</label>
                              <div class="input-group date" id="event-end-date">
                                  <input class="form-control" type="text" />
                                  <span class="input-group-addon"><i class="fa fa-th"></i></span>
                              </div>
                          </div>

                          <div class="col-md-6">
                              <label>End Time</label>
                              <div class="input-group bootstrap-timepicker">
                                  <input id="event-end-time" type="text" class="form-control">
                                  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              </div>
                          </div>
                          
                      

                        <div class="separator bottom"></div>

                      

                          <div class="col-md-6">
                            <label>Event Type</label><br>
                            <select class="selectpicker event_type" data-style="btn-primary">
                              <option>Indoor Event</option>
                              <option>Outdoor Event</option>
                            </select> 
                          </div>

                          <div class="col-md-6">
                            <label>Invite To</label><br>
                            <select class="selectpicker event_pre_invite" data-style="btn-default">
                              <option>Public</option>
                              <option>Friends</option>
                              <option>Staff</option>
                            </select>
                          </div>

                       
                        <div class="separator bottom"></div>

                          <div class="center">
                              <button type="button" class="btn btn-primary new-event" data-loading-text="Proccesing...">Create</button>
                          </div>
                  </div>
              </div>
              <!-- // Modal body END -->
          </div>
      </div>
  </div>
<!-- // Modal END -->



<style>
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top{
        z-index: 10002 !important;
    }
</style>