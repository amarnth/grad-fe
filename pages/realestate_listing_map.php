<div id="google-fs-realestate" class="maps-google-fs"></div>
<!-- col-separator -->
<div id="map-fs-sidebar" class="applyNiceScroll">
    <div class="innerAll inner-2x text-center border-bottom bg-white">
        <a href="" class="btn btn-lg btn-inverse btn-circle"><i class="fa fa-fw fa-4x icon-home-1"></i></a>
    </div>
    <div class="innerAll border-bottom bg-gray text-center">
        <div class="btn-group">
<a href="index.php?page=realestate_listing_grid" class="btn btn-default">Listing</a>
<a href="index.php?page=realestate_listing_map" class="btn btn-primary">Map</a>
</div>
    </div>
    <div class="widget widget-tabs border-none margin-none">
        <div class="widget-head bg-gray">
            <ul>
                <li class="active">
                    <a href="#buy" data-toggle="tab">Buy <span class="badge badge-primary badge-stroke">30</span></a>
                </li>
                <li>
                    <a href="#rent" data-toggle="tab">Rent <span class="badge badge-default badge-stroke">7</span></a>
                </li>
            </ul>
        </div>
        <div class="widget-body padding-none">
            <div class="innerAll border-bottom">
                <div class="input-group">
                    <input type="text" placeholder="Location" class="form-control">
                    <span class="input-group-addon">
<i class="fa fa-map-marker"></i>
</span>
                </div>
            </div>
            <div class="innerAll border-bottom text-center">
                <div class="btn-group btn-group-block row margin-none" data-toggle="buttons">
                    <label class="btn btn-default col-md-6 active">
                        <input type="radio">
                        <i class="fa fa-fw icon-home-fill-1"></i> Agency

                    </label>
                    <label class="btn btn-default col-md-6">
                        <input type="radio">
                        <i class="fa fa-fw icon-user-1"></i> Individual

                    </label>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="buy">
                    <div class="innerAll border-bottom">
                        <label class="strong">Budget &euro;</label>
                        <!-- Slider -->
                        <div class="range-slider">
                            <div class="slider slider-primary" data-values="75,300"></div>
                            <div class="separator"></div>
                            <input type="text" class="amount form-control" />
                        </div>
                        <!-- // Slider END -->
                    </div>
                    <div class="innerAll border-bottom">
                        <label class="strong">
                            Area m
                            <sup>2</sup>
                        </label>
                        <!-- Slider -->
                        <div class="range-slider">
                            <div class="slider slider-primary" data-max="120"></div>
                            <div class="separator"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="17" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="100" />
                                </div>
                            </div>
                        </div>
                        <!-- // Slider END -->
                    </div>
                    <div class="innerAll border-bottom">
                        <label class="strong">Rooms</label>
                        <!-- Slider -->
                        <div class="range-slider">
                            <div class="slider slider-primary" data-max="30"></div>
                            <div class="separator"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="5" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="20" />
                                </div>
                            </div>
                        </div>
                        <!-- // Slider END -->
                    </div>
                    <div class="innerAll text-center border-bottom">
                        <select name="" data-container="body" data-style="btn-default" class="selectpicker form-control dropup" data-dropup-auto="false">
                            <option value="">Price</option>
                            <option value="">&euro;100k - &euro;300k</option>
                            <option value="">&euro;300k - &euro;500k</option>
                            <option value="">&euro;500k - &euro;1M</option>
                        </select>
                    </div>
                </div>
                <div class="tab-pane" id="rent">
                    <div class="innerAll">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, enim, quis, eius voluptatem ducimus recusandae omnis distinctio quae quaerat facilis atque natus sint doloribus voluptatum minus quisquam reiciendis quos. Eveniet!
</div>
                </div>
            </div>
            <div class="innerAll text-center">
                <button class="btn btn-block btn-success">Search <i class="fa fa-fw fa-search"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- // END col-separator -->