<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-12">
        <!-- col-separator -->
        <div class="col-separator">
            <!-- row-app -->
            <div class="row row-app">
                <!-- col -->
                <div class="col-lg-4">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-first box">
                        <div class="heading-buttons border-bottom innerLR">
                            <h4 class="margin-none innerTB pull-left">
                                Memos
                            </h4>
                            <button class="btn btn-xs btn-inverse pull-right"><i class="fa fa-plus"></i> Add memo <i class="fa fa-file-text-o fa-fw"></i></button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="bg-gray border-bottom innerAll">
                            <div class="input-group input-group-sm">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Name <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Name</a></li>
                                        <li><a href="#">Phone Number</a></li>
                                        <li><a href="#">Group</a></li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" placeholder="What are you searching for?">
                            </div>
                        </div>
                        <div class="innerAll">
                            <!-- MEMO START -->
                            <div class="box-generic padding-none">
                                <a href="">
                                    <h5 class="strong margin-none innerAll border-bottom">
                                        Dolor voluptates earum dignissimos
                                    </h5>
                                </a>
                                <div class="innerAll half border-bottom"> <i class="fa fa-calendar fa-fw text-primary"></i> 23 aug 2013 &nbsp; <i class="fa fa-tag fa-fw text-primary"></i> <span class="label label-primary">tagged</span> </div>
                                <div class="innerAll">
                                    <p class="margin-none innerAll"><i class="fa fa-quote-left fa-2x pull-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, aut, blanditiis pariatur ad enim ex dolorem. Rerum, fuga, praesentium nulla sed at numquam cupiditate dolores quos provident deleniti ab eius.</p>
                                </div>
                                <div class="innerLR innerT half bg-primary-light border-top">
                                    <button class="btn btn-primary pull-right"><i class="fa fa-sign-in"></i></button>
                                    <div class="media inline-block margin-none">
                                        <div class="innerLR">
                                            <i class="fa fa-paperclip pull-left text-primary fa-2x"></i> 
                                            <div class="media-body">
                                                <div><a href="" class="strong text-regular">Patients</a></div>
                                                <span>15 MB</span> 
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="media inline-block margin-none">
                                        <div class="innerLR border-left">
                                            <i class="fa fa-bar-chart-o pull-left text-primary fa-2x"></i> 
                                            <div class="media-body">
                                                <div><a href="" class="strong text-regular">Report</a></div>
                                                <span>244 KB</span> 
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // END MEMO -->
                            <!-- MEMO START -->
                            <div class="box-generic padding-none">
                                <a href="">
                                    <h5 class="strong margin-none innerAll border-bottom">
                                        Quibusdam expedita voluptates tenetur
                                    </h5>
                                </a>
                                <div class="innerAll half border-bottom"> <i class="fa fa-calendar fa-fw text-primary"></i> 21 aug 2013 &nbsp; <i class="fa fa-tag fa-fw text-primary"></i> <span class="label label-primary">important</span> </div>
                                <div class="innerAll">
                                    <p class="margin-none innerAll"><i class="fa fa-quote-left fa-2x pull-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, voluptates itaque similique excepturi porro suscipit iste assumenda fugiat accusantium explicabo incidunt a ad odio blanditiis dolores consequatur sequi quisquam mollitia.</p>
                                </div>
                                <div class="innerLR innerT half bg-primary-light border-top">
                                    <button class="btn btn-primary pull-right"><i class="fa fa-sign-in"></i></button>
                                    <div class="media inline-block margin-none">
                                        <div class="innerLR">
                                            <i class="fa fa-stethoscope pull-left text-primary fa-2x"></i> 
                                            <div class="media-body">
                                                <div><a href="" class="strong text-regular">Lab Results</a></div>
                                                <span>1 MB</span> 
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // END MEMO -->
                        </div>
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
                <!-- col -->
                <div class="col-lg-4">
                    <!-- col-separator.box -->
                    <div class="col-separator box">
                        <!-- MEMO START -->
                        <h4 class="innerAll margin-none border-bottom">
                            <i class="fa fa-star pull-right text-primary"></i>Bookmarks
                        </h4>
                        <div class="innerAll">
                            <p class="margin-none innerAll"><i class="fa fa-quote-left fa-4x pull-left"></i> Aliquam rutrum, sem at scelerisque tempor, nulla diam pulvinar tortor, id pulvinar massa velit eu purus. Curabitur eu fringilla diam, sed suscipit lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras iaculis enim vel odio imperdiet faucibus. Aliquam erat volutpat.</p>
                        </div>
                        <div class="innerLR innerT half bg-gray border-top">
                            <button class="btn btn-primary pull-right"><i class="fa fa-sign-in"></i></button>
                            <div class="media inline-block margin-none">
                                <div class="innerLR">
                                    <i class="fa fa-hospital pull-left fa-2x text-primary"></i> 
                                    <div class="media-body">
                                        <div><a href="" class="strong text-regular">Plans</a></div>
                                        <span>120 KB</span> 
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="media inline-block margin-none">
                                <div class="innerLR border-left">
                                    <i class="fa fa-euro pull-left fa-2x text-primary"></i> 
                                    <div class="media-body">
                                        <div><a href="" class="strong text-regular">Income</a></div>
                                        <span>24 KB</span> 
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- // END MEMO -->
                        <div class="col-separator-h box"></div>
                        <!-- START MEMO-->
                        <div class="innerAll">
                            <p class="margin-none innerAll"><i class="fa fa-quote-left fa-4x pull-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, labore unde tempora natus. Repellat, voluptatibus, omnis accusantium eveniet recusandae maiores laboriosam beatae rerum perferendis nostrum enim iure esse sunt non.</p>
                        </div>
                        <div class="innerAll half bg-gray border-top">
                            <button class="btn btn-inverse btn-xs pull-right"><i class="fa fa-sign-in"></i></button>
                            <span class="innerAll half inline-block"> <i class="fa fa-calendar text-primary fa-fw"></i> 22 nov 2013 &nbsp; <i class="fa fa-tag text-primary fa-fw"></i> <span class="label label-primary">tagged</span> &nbsp; </span>
                            <div class="clearfix"></div>
                        </div>
                        <!-- // END MEMO-->
                        <div class="col-separator-h box"></div>
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
                <!-- col -->
                <div class="col-lg-4">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-last box bg-gray">
                        <div class="innerAll"></div>
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
            </div>
            <!-- // END row-app -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->