<div class="row row-app email">
	
	
	<!-- col -->
	<div class="col-md-12">

		<!-- col-separator.box -->
		<div class="col-separator col-unscrollable box col-separator-first">

			<div class="row row-app">
				<div class="col-lg-4 col-md-6" id="email_list">
					<div class="col-separator col-unscrollable box">
						<div class="col-table">

							<div class="input-group input-group-lg border-bottom">
								<span class="input-group-btn">
									<a href="" class="btn"><i class="fa fa-search text-muted"></i></a>
								</span>
								<input type="text" class="form-control border-none" placeholder="Search">
							</div>

							<div class="col-table-row">
								<div class="col-app col-unscrollable">
									<div class="col-app">
                                        <!-- CONTACTS LIST START -->

<div class=" list-group email-item-list">

    
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/1.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">30 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Perpetua Inger</strong>
                                <strong class="display-block text-regular">You Won't Believe This ..</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR display-block half">
                                                    <i class="fa fa-fw fa-heart text-primary"></i>
                        
                        
                        <i class="fa fa-circle-o text-info display-block innerT half"></i>
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/2.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">29 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Zoticus Axel</strong>
                                <strong class="display-block text-regular">Nobody needs a new TV anymore</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item active">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                                                    <span class="display-block">
                            <span class="strong text-muted"><i class="fa fa-paperclip"></i> 2</span>
                        </span>
                        
                        
                                                    <span class="display-block innerT">
                            <i class="fa fa-circle-o text-success"></i>
                        </span>
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/3.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">28 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Yun Ragna &amp; Me &nbsp; <span class="text-weight-regular">(2)</span></strong>
                                <strong class="display-block text-regular">Web Application Inquiry</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        <i class="fa fa-circle-o text-primary display-block innerT half"></i>
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/4.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">27 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Victor Tacitus</strong>
                                <strong class="display-block text-regular">Introduction to animation in Maya</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/5.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">26 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Arden Catharine</strong>
                                <strong class="display-block text-regular">Don't Fly A Plane Through Ash</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/6.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">25 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Mihovil Govinda</strong>
                                <strong class="display-block text-regular">Trending on Twitter this week ..</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/7.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">24 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Mariya Hadya</strong>
                                <strong class="display-block text-regular">Are you a manager or ..</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/8.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">23 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Tahir Benedikt</strong>
                                <strong class="display-block text-regular">This Week's Featured Item</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/9.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">22 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Olayinka Kristin</strong>
                                <strong class="display-block text-regular">Please confirm the meeting tomorrow</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/10.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">21 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Danko Nikodim</strong>
                                <strong class="display-block text-regular">On sale this week</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/11.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">20 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Zoja Aileas</strong>
                                <strong class="display-block text-regular">Attend Thursday's Free Webinar</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/12.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">19 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Alphonsus Braidy</strong>
                                <strong class="display-block text-regular">New Edge Locations Added</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/13.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">18 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Helene Liana</strong>
                                <strong class="display-block text-regular">Google Engage Program</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/14.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">17 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Sebastian Niklas</strong>
                                <strong class="display-block text-regular">Daily proofing digest email</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/15.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">16 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Elvire Maya</strong>
                                <strong class="display-block text-regular">Bootcamp Sessions Deal</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
            <a href="#" class="list-group-item">
            <span class="innerTB display-block">

                <span class="media">
                    <span class="pull-left text-center innerLR innerT display-block half">
                                                    <i class="fa fa-fw fa-heart text-muted"></i>
                        
                        
                        
                        
                        
                    </span>
                    <span class="media-body display-block innerR">

                        <span class="media display-block innerB">
                            <img src="../assets/images/people/80/16.jpg" alt="" width="40" class="pull-left" />
                            <span class="media-body display-block">

                                <small class="text-muted pull-right">15 Oct</small>

                                <strong class="text-muted-dark text-weight-normal">Kerman Otakar</strong>
                                <strong class="display-block text-regular">Your payment for your ..</strong>

                                <!-- <i class="fa fa-flag text-primary"></i> -->
                            </span>
                            <span class="clearfix"></span>
                        </span>

                        <span class="display-block text-larger text-muted-darker">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</span>

                    </span>
                </span>
            </span>
        </a>
    
</div>

<!-- // END CONTACTS LIST -->




										<!-- // END list-group -->
									</div>
									<!-- // END col-app -->
								</div>
								<!-- // END col-app -->
							</div>
							<!-- // END col-table-row -->
						</div>
						<!-- // END col-table -->
					</div>
					<!-- // END col-separator -->
				</div> 
				<!-- // END col -->

				<div class="col-lg-8 col-md-6 hidden-sm hidden-xs" id="email_details">
					<div class="col-separator col-unscrollable col-separator-last">

						<div class="col-table">

							<div class="innerAll border-bottom bg-gray text-center">
								<div class="btn-group btn-group-sm">
									<a href="#" class="btn btn-default"><i class="fa fa-reply"></i></a><a href="#" class="btn btn-default"><i class="fa fa-forward"></i></a><a href="#" class="btn btn-primary"><i class="fa fa-trash-o"></i></a>
								</div>
								<button class="btn btn-sm btn-inverse" id="close-email-details"><i class="fa fa-fw fa-times"></i></button>
							</div>

							<div class="col-table-row">
								<div class="col-app col-unscrollable">
									<div class="col-app">

                                        <!-- CONTENT START -->
<div class="innerAll border-bottom inner-2x">
    <div class="media innerB">
        <img src="../assets/images/people/80/8.jpg" alt="" width="50" class="pull-left" />
        <div class="media-body innerT half">

            <small class="text-muted pull-right">28 October 2013</small>

            <h5 class="text-muted-darker">MosaicPro <span class="text-weight-regular">(Me)</span></h5>
            <h5 class="text-muted-dark text-weight-normal">To: Yun Ragna</h5>
        </div>
        <div class="clearfix"></div>
    </div>

    <h4 class="innerT margin-none"><i class="fa fa-fw fa-circle-o text-success"></i> RE: Web Application Inquiry</h4>
</div>
<div class="innerAll">
    <div class="innerAll">
        <p>Hi Yun,</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero corporis ipsam voluptatibus suscipit eos expedita sapiente omnis voluptatum ea! Culpa, vitae eaque quis modi voluptatum quisquam ullam. Modi, tempora!</p>
        <p class="text-muted margin-none">Regards,<br/>mosaicpro <br/>Director @ mosaicpro.biz<br/>www.mosaicpro.biz</p>
    </div>
</div>
<div class="innerAll border-top">
    <p class="margin-none innerAll text-muted-dark"><i class="fa fa-quote-left fa-4x pull-left"></i> Aliquam rutrum, sem at scelerisque tempor, nulla diam pulvinar tortor, id pulvinar massa velit eu purus. Curabitur eu fringilla diam, sed suscipit lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras iaculis enim vel odio imperdiet faucibus. Aliquam erat volutpat.</p>
</div>
<div class="innerAll bg-gray border-top">
    <div class="media inline-block margin-none">
        <div class="innerLR">
            <i class="fa fa-paperclip pull-left fa-2x"></i>
            <div class="media-body">
                <div><a href="" class="strong text-regular">Project.zip</a></div>
                <span>15 MB</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="media inline-block margin-none">
        <div class="innerLR border-left">
            <i class="fa fa-file pull-left fa-2x"></i>
            <div class="media-body">
                <div><a href="" class="strong text-regular">Contract.pdf</a></div>
                <span>244 KB</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- // END CONTENT -->
										<div class="col-separator-h box"></div>
                                        <!-- CONTENT START -->
<div class="innerAll border-bottom inner-2x">
    <div class="media innerB">
        <img src="../assets/images/people/80/3.jpg" alt="" width="50" class="pull-left" />
        <div class="media-body innerT half">

            <small class="text-muted pull-right">28 October 2013</small>

            <h5 class="text-muted-darker">Yun Ragna <span class="text-weight-regular">(yun@mail.com)</span></h5>
            <h5 class="text-muted-dark text-weight-normal">To: MosaicPro</h5>
        </div>
        <div class="clearfix"></div>
    </div>

    <h4 class="innerT margin-none"><i class="fa fa-fw fa-circle-o text-success"></i> Web Application Inquiry</h4>
</div>
<div class="innerAll">
    <div class="innerAll">
        <p>Hi Adrian,</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero corporis ipsam voluptatibus suscipit eos expedita sapiente omnis voluptatum ea! Culpa, vitae eaque quis modi voluptatum quisquam ullam. Modi, tempora!</p>
        <p class="text-muted margin-none">Yun Ragna <br/>Marketing Manager @ company.biz<br/>www.company.biz</p>
    </div>
</div>
<!-- // END CONTENT -->


									</div>
								</div>
							</div>
						</div>

					</div>
				</div> <!-- end col 2 -->

			</div>

		</div>

	</div>
</div>





<!-- Modal -->
<div class="modal fade" id="modal-compose">
	
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal heading -->
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title">Login</h3>
			</div> -->
			<!-- // Modal heading END -->

			<div class="innerAll border-bottom">
				<div class="pull-left">
					<a href="" class=" btn btn-default btn-sm"><i class="fa fa-fw fa-arrow-left"></i> back</a> 
				</div>
				<div class="pull-left innerL">
					<a href="" class=" btn btn-success btn-sm strong"><i class="fa fa-fw icon-paperclip"></i> Save Draft</a>
				</div>
				<a href="" class="pull-right btn btn-primary btn-sm strong"><i class="fa fa-fw icon-outbox-fill"></i> Send Email</a>
				<div class="clearfix"></div>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body padding-none">
				
				<form class="form-horizontal" role="form">
					<div class="bg-gray innerAll border-bottom">
						<div class="innerLR">
							<div class="form-group">
								<label for="to" class="col-sm-2 control-label">To:</label>
								<div class="col-sm-10">
									<div class="input-group">
										<input type="text" class="form-control" id="to">
										<div class="input-group-btn">
											<button type="button" data-toggle="collapse" data-target="#cc" class="btn btn-default">CC/BCC <span class="caret"></span></button>
										</div>
									</div>
								</div>
							</div>
							<div id="cc" class="collapse">
								<div class="form-group">
									<label for="Cc" class="col-sm-2 control-label">Cc:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="Cc">
									</div>
								</div>
								<div class="form-group">
									<label for="Bcc" class="col-sm-2 control-label">Bcc:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="Bcc">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="Bcc" class="col-sm-2 control-label">From:</label>
								<div class="col-sm-6">
									<select class="selectpicker">
										<option>contact@mosaicpro.biz</option>
									</select>
								</div>
							
							</div>

							<div class="form-group">
								<label for="Bcc" class="col-sm-2 control-label">Signature:</label>
								<div class="col-sm-6">
								<select class="selectpicker">
									<option>Select Signature</option>
								</select>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<div class="innerAll inner-2x">
						<textarea class="notebook border-none form-control padding-none" rows="4" placeholder="Write your content here..."></textarea>
						<div class="clearfix"></div>
					</div>
				</form>

			</div>
			<!-- // Modal body END -->

			<div class="innerAll text-center border-top">
				<a href="" class="btn btn-default"><i class="fa fa-fw icon-crossing"></i> Cancel</a>
				<a href="" class="btn btn-primary"><i class="fa fa-fw icon-outbox-fill"></i> Send email</a>
			</div>
	
		</div>
	</div>
	
</div>
<!-- // Modal END -->


