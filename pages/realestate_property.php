<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app">
                        <!-- row-app -->
                        <div class="row row-app margin-none">
                            <!-- col -->
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <!-- col-separator -->
                                <div class="col-separator col-unscrollable bg-none">
                                    <!-- col-table -->
                                    <div class="col-table">
                                        <h3 class="margin-none innerTB">
                                            Property for sale in Bruges, Belgium <i class="fa fa-fw icon-home-1"></i>
                                        </h3>
                                        <div class="btn-group innerB">
<a href="#details" class="btn btn-default" data-toggle="scrollTo">Property Details</a>
<a href="#map_view" class="btn btn-default" data-toggle="scrollTo">Map View</a>
<a href="#comments" class="btn btn-default" data-toggle="scrollTo">Comments</a>
</div>
                                        <div class="col-separator-h"></div>
                                        <!-- col-table-row -->
                                        <div class="col-table-row">
                                            <!-- col-app -->
                                            <div class="col-app col-unscrollable">
                                                <!-- col-app -->
                                                <div class="col-app">
                                                    <div class="row" id="details">
                                                        <div class="col-md-4">
                                                            <a href="">
                                                                <img src="../assets/images/realestate/photodune-195203-houses-xs.jpg" alt="" class="img-responsive">
                                                            </a>
                                                            <div class="col-separator-h"></div>
                                                            <div class="box-generic padding-none">
                                                                <h5 class="innerAll border-bottom margin-none">
                                                                    Gallery
                                                                </h5>
                                                                <div class="innerAll">
                                                                    <div id="owl-property" class="owl-carousel owl-theme">
                                                                        <div class="item">
                                                                            <a href="">
                                                                                <img src="../assets/images/realestate/photodune-195203-houses-xs.jpg" alt="photo" class="thumb img-responsive" />
                                                                            </a>
                                                                        </div>
                                                                        <div class="item">
                                                                            <a href="">
                                                                                <img src="../assets/images/realestate/photodune-196089-house-xs.jpg" alt="photo" class="thumb img-responsive" />
                                                                            </a>
                                                                        </div>
                                                                        <div class="item">
                                                                            <a href="">
                                                                                <img src="../assets/images/realestate/photodune-197173-residential-home-xs.jpg" alt="photo" class="thumb img-responsive" />
                                                                            </a>
                                                                        </div>
                                                                        <div class="item">
                                                                            <a href="">
                                                                                <img src="../assets/images/realestate/photodune-3979102-superb-backyard-xs.jpg" alt="photo" class="thumb img-responsive" />
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <h5 class="text-faded text-uppercase">
                                                                Featured Property
                                                            </h5>
                                                            <div class="box-generic bg-gray padding-none overflow-hidden">
                                                                <a href="">
                                                                    <img src="../assets/images/realestate/photodune-1112879-modern-kitchen-in-luxury-mansion-xs.jpg" class="img-responsive" alt="property" />
                                                                </a>
                                                                <div class="innerAll inner-2x">
                                                                    <h4 class="strong">
                                                                        Kilmacanogue, Co. Wicklow
                                                                    </h4>
                                                                    <p class="text-medium text-condensed text-primary strong margin-none">&dollar; 79,000</p>
                                                                </div>
                                                            </div>
                                                            <div class="separator"></div>
                                                            <h5 class="text-faded text-uppercase">
                                                                Browse properties
                                                            </h5>
                                                            <ul class="list-group list-group-1 margin-none">
                                                                <li class="list-group-item active">
                                                                    <a href="#"><span class="badge badge-primary pull-right hidden-md">13</span><i class="fa fa-user"></i> For Rent</a>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <a href="#"><span class="badge pull-right badge-primary hidden-md">13</span><i class="fa fa-book"></i> For Sale</a>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <a href="#"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-envelope"></i> Comercial</a>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <a href="#"><i class="fa fa-cogs"></i> Luxury</a>
                                                                </li>
                                                                <li class="list-group-item ">
                                                                    <a href="#"><i class="fa fa-lock"></i>Contact us</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-8 unscrollable">
                                                            <div class="box-generic padding-none margin-none">
                                                                <h5 class="innerAll border-bottom margin-none">
                                                                    Property description
                                                                </h5>
                                                                <p class="innerAll margin-none inner-2x">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, maiores, reiciendis voluptas natus quia provident asperiores dolorum assumenda aspernatur! Expedita, praesentium repellendus nobis velit aliquid accusantium culpa tempora nostrum? Ipsam!</p>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <div class="box-generic padding-none margin-none">
                                                                <h5 class="innerAll border-bottom margin-none">
                                                                    Property characteristics
                                                                </h5>
                                                                <div class="innerAll">
                                                                    <div class="row row-merge">
                                                                        <div class="col-sm-6">
                                                                            <div class="innerAll">
                                                                                <table class="table margin-none">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                                                            <td class="border-top-none">3</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Area</td>
                                                                                            <td>1,000 Sq.Yd</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Furnished</td>
                                                                                            <td>Furnished</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Facing</td>
                                                                                            <td>North West</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="innerAll">
                                                                                <table class="table margin-none">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-muted border-top-none">Bedrooms</td>
                                                                                            <td class="border-top-none">3</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Possession</td>
                                                                                            <td>12 months</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Ownership</td>
                                                                                            <td>Freehold</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-muted">Resale</td>
                                                                                            <td>New Property</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <h5 class="innerAll bg-gray margin-none">
                                                                Nearby facilities
                                                            </h5>
                                                            <div class="col-separator-h"></div>
                                                            <div class="media bg-white margin-none">
                                                                <a class="pull-left bg-primary innerAll text-center" href="#"><i class="icon-hamburger fa-4x fa fa-fw"></i></a>
                                                                <div class="media-body innerAll half">
                                                                    <h4 class="media-heading innerT">
                                                                        <a href="" class="text-inverse">Restaurant</a> 
                                                                        <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5 minutes walk</span>
                                                                    </h4>
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                                </div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <div class="media bg-white margin-none">
                                                                <a class="pull-left bg-info innerAll text-center" href="#"><i class="icon-bus fa-4x fa fa-fw"></i></a>
                                                                <div class="media-body innerAll half">
                                                                    <h4 class="media-heading innerT">
                                                                        <a href="" class="text-inverse">Bus Station</a> 
                                                                        <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 2 minutes walk</span>
                                                                    </h4>
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                                </div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <div class="media bg-white margin-none">
                                                                <a class="pull-left bg-purple innerAll text-center" href="#"><i class="icon-gas-pump fa-4x fa fa-fw"></i></a>
                                                                <div class="media-body innerAll half">
                                                                    <h4 class="media-heading innerT">
                                                                        <a href="" class="text-inverse">Gas Pump</a> 
                                                                        <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 6 minutes drive</span>
                                                                    </h4>
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                                </div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <h5 class="innerAll bg-gray margin-none" id="map_view">
                                                                Map View
                                                            </h5>
                                                            <div class="col-separator-h"></div>
                                                            <div class="thumbnail">
                                                                <div class="map_canvas" id="google-map-json" style="height: 400px"></div>
                                                            </div>
                                                            <div class="col-separator-h"></div>
                                                            <div class="innerAll bg-gray border-bottom" id="comments">
                                                                <a href="" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-circle"></i></a>
                                                                <h4 class="margin-none padding-none">
                                                                    Discussions
                                                                </h4>
                                                            </div>
                                                            <div class="media bg-white innerAll border-bottom margin-none">
                                                                <a href="" class="pull-left">
                                                                    <img src="../assets/images/people/35/16.jpg" alt="people" class="img-responsive img-circle" />
                                                                </a>
                                                                <div class="media-body">
                                                                    <small class="strong pull-right">3 hours ago</small>
                                                                    <h5>
                                                                        Reinald Lothair
                                                                    </h5>
                                                                    <p class="innerB border-bottom">Seller</p>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, dolorem, ad, aut amet quis sapiente architecto ut corrupti molestiae.</p>
                                                                </div>
                                                            </div>
                                                            <div class="media innerAll bg-gray border-bottom margin-none">
                                                                <a href="" class="pull-left">
                                                                    <img src="../assets/images/people/35/15.jpg" alt="people" class="img-responsive img-circle" />
                                                                </a>
                                                                <div class="media-body">
                                                                    <small class="strong pull-right">5 hours ago</small>
                                                                    <h5>
                                                                        Janele Sara
                                                                    </h5>
                                                                    <p class="innerB border-bottom">Inquiry</p>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, dolorem, ad, aut amet quis sapiente architecto ut corrupti molestiae.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- // END col-app -->
                                            </div>
                                            <!-- // END col-app -->
                                        </div>
                                        <!-- // END col-table-row -->
                                    </div>
                                    <!-- // END col-table -->
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                            <!-- col -->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <!-- col-separator -->
                                <div class="col-separator col-separator-last bg-none">
                                    <div class="box-generic overflow-hidden padding-none text-center margin-none">
                                        <div class="innerAll inner-2x bg-primary text-medium">
&euro;173,809.00</div>
                                        <div class="innerAll bg-gray-dark border-bottom">
<i class="fa fa-fw fa-circle-o"></i> Negotiable
</div>
                                        <div class="innerAll bg-gray border-bottom">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                    </div>
                                    <div class="col-separator-h"></div>
                                    <div class="box-generic overflow-hidden padding-none">
                                        <h4 class="innerAll border-bottom margin-none">
                                            Contact seller
                                        </h4>
                                        <div class="innerAll inner-2x">
                                            <form>
                                                <div class="form-group">
                                                    <label>Your name</label>
                                                    <input type="text" class="form-control" placeholder="Enter your full name" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Your email</label>
                                                    <input type="text" class="form-control" placeholder="Enter your email" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Phone no.</label>
                                                    <input type="text" class="form-control" placeholder="Phone" />
                                                </div>
                                                <div class="text-right">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Send message</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                        </div>
                        <!-- // END row -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->