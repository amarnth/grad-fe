<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator box col-separator-first col-unscrollable">
            <div class="col-table">
                <!-- <h4 class="innerAll margin-none border-bottom">
                    Timelines
                </h4> -->
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <h5 class="strong innerAll margin-none">
                                <!-- Mirror --> Timeline
                            </h5>


                            <div class="relativeWrap overflow-hidden">
                                <div class="row row-merge border-bottom border-top layout-timeline layout-timeline-mirror">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="innerAll">
                                            <ul class="timeline">
                                               <?php
                                                    $timeline = new Timeline();
                                                    $timeline->get();
                                               ?>
                                               <!--  <li>
                                                    <div class="separator">
                                                        <span class="date box-generic"><strong>27</strong>may</span>
                                                        <span class="type glyphicons picture">Photo <i></i><span class="time">08:00</span></span>
                                                    </div>
                                                    <div class="widget widget-heading-simple widget-body-white margin-none">
                                                        <div class="widget-body">
                                                            <div class="media">
                                                                <div class="media-object pull-left thumb">
                                                                    <img src="../assets//images/avatar-51x51.jpg" alt="Image" />
                                                                </div>
                                                                <div class="media-body">
                                                                    <a class="author">Adrian Demian</a>
                                                                    <br/>
                                                                    <span class="muted">adrian@ennovation.ie</span> 
                                                                </div>
                                                            </div>
                                                            <div class="alert alert-gray">
                                                                <p class="glyphicons circle_info"><i></i> Added photos to the album <a href="">MosaicPro</a></p>
                                                                 Gallery Layout -->
                                                                <!-- <div class="gallery gallery-2 separator top">
                                                                    <ul class="row" data-toggle="modal-gallery" data-target="#modal-gallery" id="gallery-5" data-delegate="#gallery-5">
                                                                        <li class="col-md-4 hidden-phone">
                                                                            <a class="thumb" href="../assets/images/gallery-2/6.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/6.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                        <li class="col-md-4 hidden-phone">
                                                                            <a class="thumb" href="../assets/images/gallery-2/5.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/5.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                        <li class="col-md-4 hidden-phone">
                                                                            <a class="thumb" href="../assets/images/gallery-2/4.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/4.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                        <li class="col-md-4">
                                                                            <a class="thumb" href="../assets/images/gallery-2/3.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/3.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                        <li class="col-md-4">
                                                                            <a class="thumb" href="../assets/images/gallery-2/2.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/2.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                        <li class="col-md-4">
                                                                            <a class="thumb" href="../assets/images/gallery-2/1.jpg" data-gallery="gallery">
                                                                                <img src="../assets/images/gallery-2/1.jpg" alt="photo" class="img-responsive" />
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div> -->
                                                                <!-- // Gallery Layout END -->
                                                      <!--       </div>
                                                            <a class="glyphicons single pencil"><i></i></a>
                                                        </div>
                                                    </div>
                                                </li>  -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>