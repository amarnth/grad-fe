<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator box col-separator-first col-unscrollable">
            <div class="col-table">
                <div class="heading-buttons innerLR border-bottom">
                    <h4 class="margin-none innerTB pull-left">
                        Finances
                    </h4>
                    <div class="btn-group btn-group-xs pull-right">
                        <a href="" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Add record</a>
                        <a href="" class="btn btn-default"><i class="fa fa-fw fa-clock-o"></i> History</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="innerAll bg-gray">
                                <h5 class="strong innerB margin-none">
                                    Summary
                                </h5>
                                <!-- Filters -->
                                <div class="filter-bar">
                                    <form class="margin-none form-inline">
                                        <!-- From -->
                                        <div class="form-group col-md-2 padding-none">
                                            <label>From:</label>
                                            <div class="input-group">
                                                <input type="text" name="from" id="dateRangeFrom" class="form-control" value="08/05/13" />
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                        <!-- // From END -->
                                        <!-- To -->
                                        <div class="form-group col-md-2 padding-none">
                                            <label>To:</label>
                                            <div class="input-group">
                                                <input type="text" name="to" id="dateRangeTo" class="form-control" value="08/18/13" />
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                        <!-- // To END -->
                                        <!-- Min -->
                                        <div class="form-group col-md-2 padding-none">
                                            <label>Min:</label>
                                            <div class="input-group">
                                                <input type="text" name="from" class="form-control" value="100" />
                                                <span class="input-group-addon"><i class="fa fa-euro"></i></span>
                                            </div>
                                        </div>
                                        <!-- // Min END -->
                                        <!-- Max -->
                                        <div class="form-group col-md-2 padding-none">
                                            <label>Max:</label>
                                            <div class="input-group">
                                                <input type="text" name="from" class="form-control" value="500" />
                                                <span class="input-group-addon"><i class="fa fa-euro"></i></span>
                                            </div>
                                        </div>
                                        <!-- // Max END -->
                                        <!-- Select -->
                                        <div class="form-group col-md-3 padding-none">
                                            <label class="label-control">Select:</label>
                                            <div class="col-md-8 padding-none">
                                                <select name="from" class="form-control">
                                                    <option>Some option</option>
                                                    <option>Other option</option>
                                                    <option>Some other option</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- // Select END -->
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                                <!-- // Filters END -->
                            </div>
                            <div class="col-separator-h box"></div>
                            <div class="innerAll finances_summary">
                                <!-- Row -->
                                <div class="row">
                                    <!-- Column -->
                                    <div class="col-md-4">
                                        <div class="innerAll">
                                            <div class="well margin-none"> Total expenses<strong>&euro;32,156.00</strong> </div>
                                            <div class="innerAll center">
                                                <span class="glyphicons flash standard"><i></i></span>
                                            </div>
                                            <div class="well"> Total income<strong>&euro;122,134.00</strong> </div>
                                        </div>
                                    </div>
                                    <!-- // Column END -->
                                    <!-- Column -->
                                    <div class="col-md-8">
                                        <!-- Simple Chart -->
                                        <div id="chart_simple" class="flotchart-holder"></div>
                                    </div>
                                    <!-- // Column END -->
                                </div>
                                <!-- // Row END -->
                            </div>
                            <div class="col-separator-h box"></div>
                            <div class="innerAll bg-gray">
                                <h5 class="strong border-bottom innerB margin-none">
                                    Transactions
                                </h5>
                                <!-- Table -->
                                <table class="table table-vertical-center table-thead-simple margin-none">
                                    <thead>
                                        <tr>
                                            <th class="center" style="width: 1%">No.</th>
                                            <th>Transaction</th>
                                            <th class="center">Date</th>
                                            <th class="center">Amount</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">1</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-up text-success"></i> Supplier Abc Xyz</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;153.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">2</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-down text-danger"></i> Amazon Web Services</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;430.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">3</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-down text-danger"></i> Bank of Ireland</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;326.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">4</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-down text-danger"></i> Amazon Web Services</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;571.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">5</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-down text-danger"></i> ThemeForest</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;560.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                        <!-- Item -->
                                        <tr class="selectable">
                                            <td class="center">6</td>
                                            <td class="strong"><i class="fa fa-fw fa-arrow-down text-danger"></i> Amazon Web Services</td>
                                            <td class="center"><span class="label label-primary">23 Jan 2013</span></td>
                                            <td class="center">&euro;716.00</td>
                                            <td class="text-right actions">
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- // Item END -->
                                    </tbody>
                                </table>
                                <!-- // Table END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>