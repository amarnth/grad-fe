<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator bg-none col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none bg-white">
                    File Managers
                </h4>
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- Widget -->
                            <div class="widget">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading glyphicons file_import">
                                        <i></i>Dropzone File Upload
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- Dropzone -->
                                    <div id="dropzone">
                                        <form action="../assets/assets/upload.php" class="dropzone" id="demo-upload">
                                            <div class="fallback">
                                                <input name="file" type="file" multiple />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="separator bottom"></div>
                                    <p>
                                        This is just a demo dropzone. Uploaded files are <strong>not</strong> processed, BUT there is a Simple PHP Upload Example available in 
                                        <code>components/forms/file_manager/dropzone/assets/lib/dropzone-upload-sample.php</code>
                                    </p>
                                    <!-- // Dropzone END -->
                                </div>
                            </div>
                            <!-- // Widget END -->
                            <!-- Widget -->
                            <div class="widget">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading glyphicons file_import">
                                        <i></i>Plupload File Upload
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body">
                                    <!-- Plupload -->
                                    <form id="pluploadForm">
                                        <div id="pluploadUploader">
                                            <p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
                                        </div>
                                    </form>
                                    <!-- // Plupload END -->
                                </div>
                            </div>
                            <!-- // Widget END -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->