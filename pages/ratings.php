<div class="row-app">
    <div class="col-sm-12">
        <div class="col-separator col-separator-first">
            <div class="col-table">
                <h4 class="bg-white innerAll margin-none">
                    Rating <span>Components</span>
                </h4>
                <div class="col-separator-h"></div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="row bg-container">
                                <div class="col-lg-3 col-md-4">
                                    <div class="widget widget-heading-simple widget-body-white">
                                        <div class="widget-body padding-none">
                                            <div class="row row-merge">
                                                <div class="col-md-6">
                                                    <h5 class="innerAll margin-none bg-gray border-bottom text-center strong muted text-uppercase">
                                                        <i class="fa fa-fw fa-coffee text-faded"></i> Guest
                                                    </h5>
                                                    <div class="innerAll center inner-2x">
<span class="text-xxlarge strong text-primary">4</span>
</div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 class="innerAll margin-none bg-gray border-bottom text-center strong muted text-uppercase">
                                                        <i class="fa fa-fw fa-beer text-faded"></i> Host
                                                    </h5>
                                                    <div class="innerAll center inner-2x muted">
<span class="text-xxlarge strong">1</span>
</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4">
                                    <div class="widget widget-body-white">
                                        <div class="widget-head">
                                            <h4 class="heading text-uppercase">
                                                Icon ratings
                                            </h4>
                                        </div>
                                        <div class="widget-body center innerAll inner-2x" data-height="111px">
                                            <span class="text-medium text-primary"><i class="fa fa-trophy"></i></span>
                                            <span class="text-medium text-primary"><i class="fa fa-trophy"></i></span>
                                            <span class="text-medium text-primary"><i class="fa fa-trophy"></i></span>
                                            <span class="text-medium text-faded"><i class="fa fa-trophy"></i></span>
                                            <span class="text-medium text-faded"><i class="fa fa-trophy"></i></span>
                                            <div class="separator bottom"></div>
                                            <p class="lead">Total rating: <strong>3.4</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4">
                                    <div class="widget widget-body-white">
                                        <div class="widget-head">
                                            <h4 class="heading text-uppercase">
                                                Users
                                            </h4>
                                        </div>
                                        <div class="widget-body padding-none">
                                            <table class="table table-striped table-vertical-center margin-none">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-faded center strong border-none">4</td>
                                                        <td class="text-primary border-none">mosaicpro</td>
                                                        <td class="text-right">
                                                            <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                            <a href=""><i class="fa fa-thumbs-down"></i></a>
                                                        </td>
                                                    </tr>
                                                    <!-- <tr>
                                                    <td class="text-faded center strong">3</td>
                                                    <td class="text-primary">some dude</td>
                                                    <td class="text-right">
                                                        <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                        <a href=""><i class="fa fa-thumbs-down"></i></a>
                                                    </td>
                                                </tr>
                                                -->

                                                <tr>
                                                    <td class="text-faded center strong">2</td>
                                                    <td class="text-primary">themeyard</td>
                                                    <td class="text-right">
                                                        <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                        <a href=""><i class="fa fa-thumbs-down"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-faded center strong">1</td>
                                                    <td class="text-primary">gonzales</td>
                                                    <td class="text-right">
                                                        <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                        <a href=""><i class="fa fa-thumbs-down"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-12">
                                <div class="widget">
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            CSS Star rating
                                        </h4>
                                    </div>
                                    <div class="widget-body center innerAll inner-2x" data-height="111px">
                                        <div class="rating text-medium text-faded margin-top-none"> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star active"></span> <span class="star"></span> </div>
                                        <div class="separator bottom"></div>
                                        <p class="lead">Current rating: <strong>2/5</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget border-none">
                            <div class="widget-body innerAll inner-2x overflow-x">
                                <table class="table table-white table-vertical-center margin-none table-striped footable">
                                    <thead>
                                        <tr>
                                            <th data-class="expand" class="center">Photo</th>
                                            <th data-hide="phone" class="center">Full name</th>
                                            <th data-hide="phone" class="center">Location</th>
                                            <th data-hide="phone,tablet" class="center">Rating</th>
                                            <th data-hide="phone" class="center">Bonus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center">
                                                <img data-src="holder.js/50x50/dark" alt="Image" />
                                            </td>
                                            <td class="strong center">Lorem Ipsum Dolor</td>
                                            <td class="center">
                                                Romania 
                                                <a href="" class="innerL text-underline">view on map <i class="fa fa-map-marker"></i></a>
                                            </td>
                                            <td class="center">
                                                <div class="rating text-medium text-faded read-only"> <span class="star active"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> </div>
                                            </td>
                                            <td class="center">
                                                <span class="strong text-medium">5 <i class="fa fa-gift"></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                <img data-src="holder.js/50x50/dark" alt="Image" />
                                            </td>
                                            <td class="strong center">John Doe</td>
                                            <td class="center">
                                                United States 
                                                <a href="" class="innerL text-underline">view on map <i class="fa fa-map-marker"></i></a>
                                            </td>
                                            <td class="center">
                                                <div class="rating text-medium read-only"> <span class="star"></span> <span class="star"></span> <span class="star active"></span> <span class="star"></span> <span class="star"></span> </div>
                                            </td>
                                            <td class="center">
                                                <span class="strong text-medium">4 <i class="fa fa-gift"></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                <img data-src="holder.js/50x50/dark" alt="Image" />
                                            </td>
                                            <td class="strong center">Jane Doe</td>
                                            <td class="center">
                                                Europe 
                                                <a href="" class="innerL text-underline">view on map <i class="fa fa-map-marker"></i></a>
                                            </td>
                                            <td class="center">
                                                <div class="rating text-medium text-faded read-only"> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> </div>
                                            </td>
                                            <td class="center">
                                                <span class="strong text-medium text-faded">2 <i class="fa fa-gift"></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center">
                                                <img data-src="holder.js/50x50/dark" alt="Image" />
                                            </td>
                                            <td class="strong center">Hulky Bean</td>
                                            <td class="center">
                                                On the Moon 
                                                <a href="" class="innerL text-underline">view on map <i class="fa fa-map-marker"></i></a>
                                            </td>
                                            <td class="center">
                                                <div class="rating text-medium text-faded read-only"> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> </div>
                                            </td>
                                            <td class="center">
                                                <span class="strong text-medium text-faded">1 <i class="fa fa-gift"></i></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>