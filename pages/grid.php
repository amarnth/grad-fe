<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator bg-none col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none bg-white">
                    12 Column Fluid Grid
                </h4>
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- 2 Column / Half -->
                            <div class="widget widget-body-white">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading">
                                        2 Column / Half
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- 2 Column Grid / Half -->
                                    <div class="row">
                                        <!-- Half Column -->
                                        <div class="col-md-6">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // Half Column END -->
                                        <!-- Half Column -->
                                        <div class="col-md-6">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // Half Column END -->
                                    </div>
                                    <!-- // 2 Column Grid / Half END -->
                                </div>
                            </div>
                            <!-- // 2 Column / Half END -->
                            <!-- 3 Column / One Third -->
                            <div class="widget widget-body-gray">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading">
                                        3 Column / One Third
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- 3 Column Grid / One Third -->
                                    <div class="row">
                                        <!-- One Third Column -->
                                        <div class="col-md-4">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Third Column END -->
                                        <!-- One Third Column -->
                                        <div class="col-md-4">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Third Column END -->
                                        <!-- One Third Column -->
                                        <div class="col-md-4">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Third Column END -->
                                    </div>
                                    <!-- // 3 Column Grid / One Third END -->
                                </div>
                            </div>
                            <!-- // 3 Column / One Third END -->
                            <!-- 2 Column / One Third & Two Third -->
                            <div class="widget widget-body-white">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading">
                                        2 Column / One Third &amp; Two Third
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- 2 Column Grid / One Third & Two Third -->
                                    <div class="row">
                                        <!-- One Third Column -->
                                        <div class="col-md-4">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Third Column END -->
                                        <!-- Two Third Column -->
                                        <div class="col-md-8">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // Two Third Column END -->
                                    </div>
                                    <!-- // 2 Column Grid / One Third & Two Third END -->
                                </div>
                            </div>
                            <!-- // 2 Column / One Third & Two Third END -->
                            <!-- 4 Column / One Fourth -->
                            <div class="widget widget-body-gray">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading">
                                        4 Column / One Fourth
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- 4 Column Grid / One Fourth -->
                                    <div class="row">
                                        <!-- One Fourth Column -->
                                        <div class="col-md-3">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Fourth Column END -->
                                        <!-- One Fourth Column -->
                                        <div class="col-md-3">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Fourth Column END -->
                                        <!-- One Fourth Column -->
                                        <div class="col-md-3">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Fourth Column END -->
                                        <!-- One Fourth Column -->
                                        <div class="col-md-3">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Fourth Column END -->
                                    </div>
                                    <!-- // 4 Column Grid / One Fourth END -->
                                </div>
                            </div>
                            <!-- // 4 Column / One Fourth END -->
                            <!-- 2 Column / One Fourth & Three Fourth -->
                            <div class="widget widget-body-white">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <h4 class="heading">
                                        2 Column / One Fourth &amp; Three Fourth
                                    </h4>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body innerAll inner-2x">
                                    <!-- 2 Column Grid / One Fourth & Three Fourth -->
                                    <div class="row">
                                        <!-- One Fourth Column -->
                                        <div class="col-md-3">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // One Fourth Column END -->
                                        <!-- Three Fourth Column -->
                                        <div class="col-md-9">
                                            <h4>
                                                Column
                                            </h4>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>
                                        <!-- // Three Fourth Column END -->
                                    </div>
                                    <!-- // 2 Column Grid / One Fourth & Three Fourth -->
                                </div>
                            </div>
                            <!-- // 2 Column / One Fourth & Three Fourth END -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->