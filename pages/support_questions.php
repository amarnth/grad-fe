<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-3">
        <!-- col-separator -->
        <div class="col-separator col-separator-first">

            <!-- Collapsible Widget -->
            <div class="widget " data-toggle="collapse-widget" data-collapse-closed="true">
                <div class="widget-head bg-inverse margin-none innerAll">
                    <h4 class="heading bg-inverse">
                        Categories
                    </h4>
                </div>
                <div class="widget-body">

                    <div class="form-group">
                        <input type="text" class="form-control new-category-name" placeholder="Category Name">
                    </div>

                    <div class="text-center innerAll">
                        <a href="" class="btn btn-primary invite-mentor">Add</a>
                    </div>

                </div>
            </div>
            <!-- // Collapsible Widget END -->
           
            <ul class="list-group list-group-1 margin-none borders-none">
                <li class="list-group-item animated fadeInUp">
                    <a href="#"><span class="badge pull-right badge-default hidden-md">15</span> Data Science</a>
                </li>
                <li class="list-group-item animated fadeInUp">
                    <a href="#"><span class="badge pull-right badge-default hidden-md">10</span> Machine Learning</a>
                </li>
                <li class="list-group-item animated fadeInUp">
                    <a href="#"><span class="badge pull-right badge-default hidden-md">10</span> Artificial Intelligence</a>
                </li>
                <li class="list-group-item border-bottom-none animated fadeInUp">
                    <a href="#"><span class="badge pull-right badge-default hidden-md">10</span> Probability and Statistics</a>
                </li>
            </ul>

            <div class="col-separator-h"></div>
            <h5 class="innerAll border-bottom margin-bottom-none">
                Filter by
            </h5>
            <form role="form">
                <div class="innerAll inner-2x">
                   <!--  <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question-circle text-faded"></i></span>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search for a topic">
                        </div>
                    </div> -->
                    <div class="form-group margin-none">
                        <div class="checkbox">
                            <label class="checkbox-custom">
                                <i class="fa fa-fw fa-square-o"></i>

                                <input type="checkbox" id="inlineCheckbox1" class="checkbox" value="option1" checked="checked">
                                Unanswered 

                            </label>
                        </div>
                        <div class="checkbox margin-none">
                            <label class="checkbox-custom">
                                <i class="fa fa-fw fa-square-o"></i>

                                <input type="checkbox" id="inlineCheckbox2" value="option2" class="checkbox">
                                Answered

                            </label>
                        </div>
                    </div>
                </div>
                <!-- <div class="border-top border-bottom text-center innerAll bg-gray">
                    <a href="#write" data-toggle="collapse" class="btn btn-primary strong btn-sm">Write an Question <i class="icon-compose fa fa-fw"></i></a>
                </div> -->
                <!-- <div class="text-center border-top innerTB">
                    <a href="index.php?page=support_questions" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Search</a>
                </div> -->
            </form>
            <div class="col-separator-h"></div>

            <div class="widget">
                <div class="text-center innerAll inner-2x">
                    <h2 class="innerT">
                        Question <i class="icon-compose fa fa-fw"></i>
                    </h2>

                    <div class="form-group">
                        <input type="text" class="form-control new-category-name" placeholder="have any doubt?">
                    </div>

                    

                    <div class="text-center innerAll">
                        <a href="" class="btn btn-primary invite-mentor">Ask</a>
                    </div>

                </div>
            </div>


        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-md-9">
        <!-- col-separator -->
        <div class="col-separator">
            <div class="col-table">
                <div class="row innerAll border-bottom bg-gray">
                    <div class="col-sm-6">
                        <h3 class="margin-none">
                            Questions and Answers
                        </h3>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Type in a question...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/1.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Finding the right team?</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Suzanne Marie</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/2.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">What makes Coral such an outstanding Template?</a> 
                                                    <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Adrian Demian</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <span class="empty-photo"> <i class="icon-blood-sample fa-2x"></i> </span>
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">What else will you guys come up with?</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Mary Dawson</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/4.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">How many pages does this have?</a> 
                                                    <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">John Carsten</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <span class="empty-photo"> <i class="icon-blood-sample fa-2x"></i> </span>
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Getting to know Coral</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Suzanne Marie</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <span class="empty-photo"> <i class="icon-blood-sample fa-2x"></i> </span>
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Is it right for my project?</a> 
                                                    <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Bogdan Laza</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/7.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">About the new Email page</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Stephanie</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/8.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Social Page customization</a> 
                                                    <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">John Carsten</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <span class="empty-photo"> <i class="icon-blood-sample fa-2x"></i> </span>
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Personal Assistant</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">girl381</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/10.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Developer</a> 
                                                    <label class="label label-success"><i class="icon-checkmark-thick fa fa-fw"></i> Answered</label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Bogdan Laza</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerAll border-bottom tickets">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left" href="index.php?page=support_answers">
                                                    <img class="media-object" src="assets/images/people/50/11.jpg" alt="..." >
                                                </a>
                                                <div class="media-body innerT half">
                                                    <a href="index.php?page=support_answers" class="media-heading">Project Manager</a> 
                                                    <label class="label label-default"><a href="" class="user-ticket-action">Unread</a></label>
                                                    <div class="clearfix"></div>
                                                    <small>posted by <a href="">Suzanne Marie</a> <i class="fa fa-fw icon-time-clock"></i> 12:51 AM</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="innerT pull-right">
                                            <a href="" class="btn btn-info btn-xs strong"><i class=" icon-comment-fill-1"></i> 3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->