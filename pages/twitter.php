<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Twitter API
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <p>Retrieving from Twitter a user's timeline with Twitter OAuth API 1.1 (PHP), getting the feed from PHP to HTML widgets via AJAX and using the awesome Grid-A-Licious plugin to create a responsive grid similar to Pinterest. <span class="strong">Important:</span> For Twitter API to work, you need to create an Application on Twitter and edit twitter.php with your credentials. Enjoy!</p>
                                <div class="jstwitter row" data-gridalicious="true" data-gridalicious-width="250">
                                    <span class="innerLR"><span class="label label-primary">Loading .. </span></span>
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->