<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first box">
            <h4 class="margin-none innerAll border-bottom margin-none">
                Friends List
            </h4>
            <div class="innerAll">
                <div class="widget widget-heading-simple widget-body-simple text-right margin-none">
                    <form class="input-group">
                        <input type="text" class="form-control" placeholder="Filter by Name " />
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-inverse">Search</button>
                        </span>
                    </form>
                </div>
            </div>
            <div class="col-separator-h box"></div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/1.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/2.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/3.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/4.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/5.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/6.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/7.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/8.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/9.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/10.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/11.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/12.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/13.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/14.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/15.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/16.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/17.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/18.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/19.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/20.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/21.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/22.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/23.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll pull-left">
                <div class="media innerAll margin-none box">
                    <a class="pull-left" href="#">
                        <img src="../assets/images/people/100/24.jpg" alt="photo" class="media-object" width="50">
                    </a>
                    <div class="media-body">
                        <a href="" class="strong">Full Name</a> <a href="" class="clearfix text-primary strong">www.domain.com</a> 
                        <div class="media-icons">
                            <a href=""><i class="fa text-faded fa-fw fa-clock-o"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-calendar"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-user"></i></a>
                            <a href=""><i class="fa text-faded fa-fw fa-star"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->