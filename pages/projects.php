<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first col-unscrollable">
            <div class="col-table">
                <div class="innerTB bg-container">
                    <h3 class="pull-left margin-none innerR">
                        Projects &nbsp;<i class="fa fa-fw text-primary icon-top-hat"></i>
                    </h3>
                    <!-- <a href="" class="btn btn-sm btn-primary pull-left">Join Today</a> -->
                    <div style="text-align: right;" class="innerB">
          
        </div>
        <!-- <a href="#create-new-project" data-toggle="modal" class="btn btn-sm btn-info pull-right"> <i class="fa fa-plus fa-fw"></i> Add Project</a> -->
                    <div class="clearfix"></div>
                </div>
                <div class="col-separator-h"></div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="row row-app" id="get-projects">
                            
                                <?php
                                    $project = new Project();
                                    $project->getProjects();
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal -->
  <div class="modal fade" id="create-new-project">
      <div class="modal-dialog">
          <div class="modal-content">
              <!-- Modal heading -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">
                      Create New Project
                  </h3>
              </div>
              <!-- // Modal heading END -->
              <!-- Modal body -->
              <div class="modal-body">
                  <div class="innerAll">
                      <form class="margin-none innerLR inner-2x">
                          <div class="form-group">
                              <input type="text" class="form-control project-name" placeholder="Project Name">
                          </div>

                          <div class="form-group">
                            <textarea class="form-control project-desc" placeholder="Project Description"></textarea>
                          </div>
                       
                            <div class="form-group get-student">
                                <label class="control-label">Select Students</label>
                                <select multiple="multiple" style="width: 100%;" id="select-students">
                                    
                                </select>
                            </div>

                            <div class="form-group get-staffs">
                                <label class="control-label">Select Staff</label>
                                <select multiple="multiple" style="width: 100%;" id="select-staffs">
                                </select>
                            </div>

                            <div class="form-group get-student">
                                <label class="control-label">Select Icon</label>
                                <select style="width: 100%;" id="select-icon">
                                    
                                </select>
                            </div>


                            <div class="form-group get-student">
                                <label class="control-label">Select Color</label>
                                <input type="text" id="colorpickerColor" class="form-control" value="#5a6a87" />
                                <div class="separator bottom"></div>
                                <div id="colorpicker"></div>
                            </div>

                          <div class="text-center innerAll">
                              <a href="" class="btn btn-primary create_project" data-dismiss="modal" aria-hidden="true">Create</a>
                          </div>

                      </form>
                  </div>
              </div>
              <!-- // Modal body END -->
          </div>
      </div>
  </div>
<!-- // Modal END -->
