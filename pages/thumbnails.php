<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Thumbnails
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <div class="widget-body">
                                        <p class="lead center margin-none">Highly customizable. <span>Add any kind of HTML content like headings or paragraphs into thumbnails.</span></p>
                                    </div>
                                </div>
                                <!-- Row -->
                                <div class="row">
                                    <!-- Column -->
                                    <div class="col-md-4">
                                        <!-- Thumbnail -->
                                        <div class="thumbnail widget-thumbnail">
                                            <img data-src="holder.js/100%x200" alt="100%x200 Image Holder" />
                                            <div class="caption">
                                                <h4>
                                                    Thumbnail heading
                                                </h4>
                                                <p>Cras justo odio, dapibus ac facidivsis in, egestas eget quam. Donec id edivt non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id edivt.</p>
                                                <a href="#" class="btn btn-primary">Action</a>
<a href="#" class="btn btn-inverse">Action</a>

                                            </div>
                                        </div>
                                        <!-- // Thumbnail END -->
                                    </div>
                                    <!-- // Column END -->
                                    <!-- Column -->
                                    <div class="col-md-4">
                                        <!-- Thumbnail -->
                                        <div class="thumbnail widget-thumbnail">
                                            <img data-src="holder.js/100%x200" alt="100%x200 Image Holder" />
                                            <div class="caption">
                                                <h4>
                                                    Thumbnail heading
                                                </h4>
                                                <p>Cras justo odio, dapibus ac facidivsis in, egestas eget quam. Donec id edivt non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id edivt.</p>
                                                <a href="#" class="btn btn-primary">Action</a>
<a href="#" class="btn btn-inverse">Action</a>

                                            </div>
                                        </div>
                                        <!-- // Thumbnail END -->
                                    </div>
                                    <!-- // Column END -->
                                    <!-- Column -->
                                    <div class="col-md-4">
                                        <!-- Thumbnail -->
                                        <div class="thumbnail widget-thumbnail">
                                            <img data-src="holder.js/100%x200" alt="100%x200 Image Holder" />
                                            <div class="caption">
                                                <h4>
                                                    Thumbnail heading
                                                </h4>
                                                <p>Cras justo odio, dapibus ac facidivsis in, egestas eget quam. Donec id edivt non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id edivt.</p>
                                                <a href="#" class="btn btn-primary">Action</a>
<a href="#" class="btn btn-inverse">Action</a>

                                            </div>
                                        </div>
                                        <!-- // Thumbnail END -->
                                    </div>
                                    <!-- // Thumbnail END -->
                                </div>
                                <!-- // Row END -->
                                <div class="separator bottom"></div>
                                <h3>
                                    Thumbnails links <span>default</span>
                                </h3>
                                <!-- Row -->
                                <div class="row separator bottom">
                                    <!-- Thumbnail -->
                                    <div class="col-md-3">
                                        <a href="#" class="thumbnail">
                                            <img data-src="holder.js/100%x180" alt="100%x180 Holder Image" />
                                        </a>
                                    </div>
                                    <!-- // Thumbnail END -->
                                    <!-- Thumbnail -->
                                    <div class="col-md-3">
                                        <a href="#" class="thumbnail">
                                            <img data-src="holder.js/100%x180" alt="100%x180 Holder Image" />
                                        </a>
                                    </div>
                                    <!-- // Thumbnail END -->
                                    <!-- Thumbnail -->
                                    <div class="col-md-3">
                                        <a href="#" class="thumbnail">
                                            <img data-src="holder.js/100%x180" alt="100%x180 Holder Image" />
                                        </a>
                                    </div>
                                    <!-- // Thumbnail END -->
                                    <!-- Thumbnail -->
                                    <div class="col-md-3">
                                        <a href="#" class="thumbnail">
                                            <img data-src="holder.js/100%x180" alt="100%x180 Holder Image" />
                                        </a>
                                    </div>
                                    <!-- // Thumbnail END -->
                                </div>
                                <!-- // Row END -->
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->