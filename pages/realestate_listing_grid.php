<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first border-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app">
                        <!-- row-app -->
                        <div class="row row-app margin-none">
                            <!-- col -->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <!-- col-separator -->
                                <div class="col-separator">
                                    <div class="innerAll inner-2x text-center border-bottom bg-white">
                                        <a href="" class="btn btn-lg btn-inverse btn-circle"><i class="fa fa-fw fa-4x icon-home-1"></i></a>
                                    </div>
                                    <div class="innerAll border-bottom bg-gray text-center">
                                        <div class="btn-group">
<a href="index.php?page=realestate_listing_grid" class="btn btn-primary">Listing</a>
<a href="index.php?page=realestate_listing_map" class="btn btn-default">Map</a>
</div>
                                    </div>
                                    <div class="widget widget-tabs border-none margin-none">
                                        <div class="widget-head bg-gray">
                                            <ul>
                                                <li class="active">
                                                    <a href="#buy" data-toggle="tab">Buy <span class="badge badge-primary badge-stroke">30</span></a>
                                                </li>
                                                <li>
                                                    <a href="#rent" data-toggle="tab">Rent <span class="badge badge-default badge-stroke">7</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="widget-body padding-none">
                                            <div class="innerAll border-bottom">
                                                <div class="input-group">
                                                    <input type="text" placeholder="Location" class="form-control">
                                                    <span class="input-group-addon">
<i class="fa fa-map-marker"></i>
</span>
                                                </div>
                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group btn-group-block row margin-none" data-toggle="buttons">
                                                    <label class="btn btn-default col-md-6 active">
                                                        <input type="radio">
                                                        <i class="fa fa-fw icon-home-fill-1"></i> Agency

                                                    </label>
                                                    <label class="btn btn-default col-md-6">
                                                        <input type="radio">
                                                        <i class="fa fa-fw icon-user-1"></i> Individual

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="buy">
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">Budget &euro;</label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-values="75,300"></div>
                                                            <div class="separator"></div>
                                                            <input type="text" class="amount form-control" />
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">
                                                            Area m
                                                            <sup>2</sup>
                                                        </label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-max="120"></div>
                                                            <div class="separator"></div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="17" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="100" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">Rooms</label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-max="30"></div>
                                                            <div class="separator"></div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="5" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="20" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll text-center border-bottom">
                                                        <select name="" data-container="body" data-style="btn-default" class="selectpicker form-control dropup" data-dropup-auto="false">
                                                            <option value="">Price</option>
                                                            <option value="">&euro;100k - &euro;300k</option>
                                                            <option value="">&euro;300k - &euro;500k</option>
                                                            <option value="">&euro;500k - &euro;1M</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="rent">
                                                    <div class="innerAll">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, enim, quis, eius voluptatem ducimus recusandae omnis distinctio quae quaerat facilis atque natus sint doloribus voluptatum minus quisquam reiciendis quos. Eveniet!
</div>
                                                </div>
                                            </div>
                                            <div class="innerAll text-center">
                                                <button class="btn btn-block btn-success">Search <i class="fa fa-fw fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                            <!-- col -->
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <!-- col-separator -->
                                <div class="col-separator col-unscrollable col-separator-last bg-none">
                                    <!-- col-table -->
                                    <div class="col-table">
                                        <div class="innerAll bg-white">
                                            <h4 class="margin-none innerT half pull-left">
                                                Property listing
                                            </h4>
                                            <div class="btn-group btn-group-sm pull-right">
                                                <a href="index.php?page=realestate_listing_grid" class="btn btn-primary"><i class="fa fa-th"></i></a>
                                                <a href="index.php?page=realestate_listing_list" class="btn btn-default"><i class="fa fa-list"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- col-table-row -->
                                        <div class="col-table-row">
                                            <!-- col-app -->
                                            <div class="col-app col-unscrollable">
                                                <!-- col-app -->
                                                <div class="col-app">
                                                    <div class="innerAll2">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="0">
                                                                    <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-378874-real-estate-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Property for sale
                                                                            </h5>
                                                                            <span class="text-primary">&euro;256,669.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="200">
                                                                    <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-195203-houses-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Property for sale
                                                                            </h5>
                                                                            <span class="text-primary">&euro;135,407.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="400">
                                                                    <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-196089-house-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Property for sale
                                                                            </h5>
                                                                            <span class="text-primary">&euro;88,915.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="600">
                                                                    <div class="mark">
<span class="caret caret-primary"></span>
<span class="text">Rent</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-197173-residential-home-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Residential Home
                                                                            </h5>
                                                                            <span class="text-primary">&euro;252,603.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="800">
                                                                    <div class="mark">
<span class="caret caret-primary"></span>
<span class="text">Rent</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-3979102-superb-backyard-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Superb Backyard
                                                                            </h5>
                                                                            <span class="text-primary">&euro;91,597.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="1000">
                                                                    <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-2238345-apartments-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Appartments
                                                                            </h5>
                                                                            <span class="text-primary">&euro;75,806.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="widget widget-pinterest fixed-height border-none" data-animate="bounceIn" data-delay="1200">
                                                                    <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                                                    <div class="widget-body padding-none">
                                                                        <a href="index.php?page=realestate_property" class="thumb" data-height="150px">
                                                                            <img src="../assets/images/realestate/photodune-1112879-modern-kitchen-in-luxury-mansion-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="description border-bottom">
                                                                            <a href="index.php?page=realestate_property" class="btn btn-primary btn-circle pull-right"><i class="fa fa-shopping-cart"></i></a>
                                                                            <h5 class="text-uppercase">
                                                                                Luxury Mansion
                                                                            </h5>
                                                                            <span class="text-primary">&euro;68,832.00</span>

                                                                        </div>
                                                                        <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- // END col-app -->
                                            </div>
                                            <!-- // END col-app -->
                                        </div>
                                        <!-- // END col-table-row -->
                                    </div>
                                    <!-- // END col-table -->
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                        </div>
                        <!-- // END row -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->