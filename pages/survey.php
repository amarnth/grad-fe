<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first box col-unscrollable col-fs">
            <div class="col-table">
                <div class="col-table-row">
                    <div class="col-app col-unscrollable tab-content">
                        <div class="col-app survey-wrapper survey-bg-1 tab-pane active animated fadeIn" id="survey-1-1">
                            <div class="survey-container">
                                <div class="survey-heading bg-gray border-bottom">
                                    <a href="index.php?page=index" class="btn btn-default btn-lg pull-right no-ajaxify"><i class="fa fa-times"></i></a>
                                    <h4 class="margin-none">
                                        Question 1 of 5
                                    </h4>
                                </div>
                                <div class="survey-body">
                                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, laudantium, fuga, repudiandae sunt aliquam nisi quidem asperiores autem tempora cupiditate?</p>
                                    <div class="survey-answer text-center">
                                        <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                                            <label class="btn btn-primary">
                                                <input type="radio" name="opt" id="survey-opt-1-1">
                                                Excellent 
                                            </label>
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="opt" id="survey-opt-1-2" checked="checked">
                                                Good 
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="opt" id="survey-opt-1-3">
                                                Okay 
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="opt" id="survey-opt-1-4">
                                                Bad 
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="opt" id="survey-opt-1-5">
                                                Terrible 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="survey-heading survey-footer bg-gray border-bottom"> <a href="#survey-1-2" data-toggle="tab" class="btn btn-primary btn-lg pull-right">next</a> </div>
                            </div>
                        </div>
                        <div class="col-app survey-wrapper survey-bg-2 tab-pane animated fadeIn" id="survey-1-2">
                            <div class="survey-container">
                                <div class="survey-heading bg-gray border-bottom">
                                    <a href="index.php?page=index" class="btn btn-default btn-lg pull-right no-ajaxify"><i class="fa fa-times"></i></a>
                                    <h4 class="margin-none">
                                        Question 2 of 5
                                    </h4>
                                </div>
                                <div class="survey-body">
                                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, accusantium, modi doloremque suscipit cupiditate officia esse sequi necessitatibus libero quasi corporis commodi molestias repudiandae error quos velit dolor totam perferendis.</p>
                                    <div class="survey-answer text-center">
                                        <div class="btn-group btn-group-lg" data-toggle="buttons">
                                            <label class="btn active btn-primary">
                                                <input type="checkbox" checked="checked">
                                                Option 1 
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="checkbox">
                                                Option 2 
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="checkbox">
                                                Option 3 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="survey-heading survey-footer bg-gray border-bottom"> <a href="#survey-1-3" data-toggle="tab" class="btn btn-primary btn-lg pull-right">next</a> <a href="#survey-1-1" data-toggle="tab" class="btn btn-success btn-lg pull-right">back</a> </div>
                            </div>
                        </div>
                        <div class="col-app survey-wrapper survey-bg-3 tab-pane animated fadeIn" id="survey-1-3">
                            <div class="survey-container">
                                <div class="survey-heading bg-gray border-bottom">
                                    <a href="index.php?page=index" class="btn btn-default btn-lg pull-right no-ajaxify"><i class="fa fa-times"></i></a>
                                    <h4 class="margin-none">
                                        Question 3 of 5
                                    </h4>
                                </div>
                                <div class="survey-body">
                                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, debitis, saepe blanditiis tempore quod quibusdam tenetur?</p>
                                    <div class="survey-answer text-center">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="active display-block bg-gray text-center">
                                                    <span class="display-block innerAll border-bottom text-regular"> <i class="fa fa-5x fa-heart"></i> </span>
                                                    <span class="display-block bg-inverse btn-lg">Yes</span> 
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center">
                                                    <span class="display-block innerAll border-bottom text-regular"> <i class="fa fa-5x fa-times"></i> </span>
                                                    <span class="display-block bg-inverse btn-lg">No</span> 
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center">
                                                    <span class="display-block innerAll border-bottom text-regular"> <i class="fa fa-5x fa-clock-o"></i> </span>
                                                    <span class="display-block bg-inverse btn-lg">Maybe</span> 
                                                </a>
                                            </div>
                                        </div>
                                        <div class="separator bottom"></div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center"> <span class="display-block btn-lg bg-inverse">Option 4</span> </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center"> <span class="display-block btn-lg bg-inverse">Option 5</span> </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center"> <span class="display-block btn-lg bg-inverse">Option 6</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="survey-heading survey-footer bg-gray border-bottom"> <a href="#survey-1-4" data-toggle="tab" class="btn btn-primary btn-lg pull-right">next</a> <a href="#survey-1-2" data-toggle="tab" class="btn btn-success btn-lg pull-right">back</a> </div>
                            </div>
                        </div>
                        <div class="col-app survey-wrapper survey-bg-1 tab-pane animated fadeIn" id="survey-1-4">
                            <div class="survey-container">
                                <div class="survey-heading bg-gray border-bottom">
                                    <a href="index.php?page=index" class="btn btn-default btn-lg pull-right no-ajaxify"><i class="fa fa-times"></i></a>
                                    <h4 class="margin-none">
                                        Question 4 of 5
                                    </h4>
                                </div>
                                <div class="survey-body">
                                    <div class="media innerTB border-bottom">
                                        <i class="fa fa-fw pull-left fa-5x fa-file-text-o"></i> 
                                        <div class="media-body">
                                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, odio consequuntur delectus vitae deserunt voluptatibus?</p>
                                        </div>
                                    </div>
                                    <div class="media innerTB border-bottom">
                                        <i class="fa fa-fw pull-right fa-5x fa-folder-o"></i> 
                                        <div class="media-body">
                                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, odio consequuntur delectus vitae deserunt voluptatibus?</p>
                                        </div>
                                    </div>
                                    <div class="survey-answer text-center">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="active display-block bg-gray text-center"> <span class="display-block btn-lg bg-inverse">Yes</span> </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a data-toggle="survey-answer" href="" class="display-block bg-gray text-center"> <span class="display-block btn-lg bg-inverse">No</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="survey-heading survey-footer bg-gray border-bottom"> <a href="#survey-1-5" data-toggle="tab" class="btn btn-primary btn-lg pull-right">next</a> <a href="#survey-1-3" data-toggle="tab" class="btn btn-success btn-lg pull-right">back</a> </div>
                            </div>
                        </div>
                        <div class="col-app survey-wrapper survey-bg-2 tab-pane animated fadeIn" id="survey-1-5">
                            <div class="survey-container">
                                <div class="survey-heading bg-gray border-bottom">
                                    <a href="index.php?page=index" class="btn btn-default btn-lg pull-right no-ajaxify"><i class="fa fa-times"></i></a>
                                    <h4 class="margin-none">
                                        Question 5 of 5
                                    </h4>
                                </div>
                                <div class="survey-body">
                                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, odio consequuntur delectus vitae deserunt voluptatibus?</p>
                                    <div class="survey-answer text-center">
                                        <!-- Slider -->
                                        <div class="slider-single slider-primary"></div>
                                    </div>
                                </div>
                                <div class="survey-heading survey-footer bg-gray border-bottom"> <a href="index.php?page=index" class="btn btn-inverse btn-lg pull-right no-ajaxify">close</a> <a href="#survey-1-4" data-toggle="tab" class="btn btn-success btn-lg pull-right">back</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>