<div class="bg-white innerAll inner-2x">
    <div class="container text-center">
        <h1>
            Need an App or Website Template? <em class="text-primary">We covered everything!</em>
        </h1>
    </div>
</div>
<div class="bg-primary">
    <div class="container">
        <div id="owl-landing">
            <a href="index.php?page=dashboard_analytics" target="_blank" class="item item1">
                <img src="../assets/images/landing/landing_slide_1_ratio.png" alt="" class="img-responsive slide1-ratio" />
                <img src="../assets/images/landing/landing_slide_1_mbp.png" alt="" class="img-responsive" id="slide1-mbp" data-animate="bounceIn" data-delay="800" />
                <img src="../assets/images/landing/landing_slide_1_ipad.png" alt="" class="img-responsive" id="slide1-ipad" data-animate="bounceIn" data-delay="400" />
                <img src="../assets/images/landing/landing_slide_1_iphone.png" alt="" class="img-responsive" id="slide1-iphone" data-animate="bounceIn" data-delay="600" />
                <img src="../assets/images/landing/landing_slide_1_responsive.png" alt="" class="img-responsive" id="slide1-iphone2" data-animate="slideInRight" data-delay="500" />
            </a>
            <a href="index.php?page=realestate_listing_grid" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_realestate.jpg" alt="" class="img-responsive" />
            </a>
            <a href="index.php?page=medical_overview" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_medical.jpg" alt="" class="img-responsive" />
            </a>
            <a href="index.php?page=index" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_social.jpg" alt="" class="img-responsive" />
            </a>
            <a href="index.php?page=finances" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_financial.jpg" alt="" class="img-responsive" />
            </a>
            <a href="?module=front&amp;page=index" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_front.jpg" alt="" class="img-responsive" />
            </a>
            <a href="?module=shop&amp;page=index" target="_blank" class="item">
                <img src="../assets/images/landing/landing_slide_shop.jpg" alt="" class="img-responsive" />
            </a>
        </div>
    </div>
</div>