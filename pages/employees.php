<!-- row-app -->
<div class="row row-app widget-employees">
    <!-- col -->
    <div class="col-md-3 col-sm-4">
        <!-- col-separator -->
        <div class="col-separator col-separator-first box col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <!-- SEARCH START -->
                <h4 class="innerAll margin-none border-bottom">
                    Employees
                </h4>
                <div class="innerAll bg-gray border-bottom">
                    <form autocomplete="off">
                        <div class="widget-search separator bottom">
                            <div class="input-group">
                                <input class="form-control" type="text" value="" placeholder="Find someone ..">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-inverse"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <select style="width: 100%">
                            <optgroup label="Department">
                                <option value="design">Design</option>
                                <option value="development">Development</option>
                            </optgroup>
                        </select>
                    </form>
                </div>
                <div class="innerAll half">
                    <span class="label label-primary">1490 Employees Found <i class="fa-circle-arrow-down"></i></span>
                </div>
                <!-- // END SEARCH -->
                <div class="col-separator-h box"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- EMPLOYEES LIST START -->
                            <ul class="list-group list-group-1 borders-none">
                                <li class="list-group-item active">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/2.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/3.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/4.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/5.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/6.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/7.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/8.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/9.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/10.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item border-bottom-none">
                                    <div class="innerAll media">
                                        <a href="" class="pull-left padding-none">
                                            <img src="../assets/images/people/100/11.jpg" class="thumb" alt="Image" width="50"/>
                                        </a>
                                        <div class="media-body innerT half">
                                            <p class="strong margin-none"><a class="padding-none" href="">Adrian Demian</a></p>
                                            <p class="text-muted margin-none">contact@mosaicpro.biz</p>
                                            <i class="fa fa-envelope"></i> <i class="fa fa-phone"></i> <i class="fa fa-skype"></i> 
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <!-- // EMPLOYEES LIST END -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-md-9 col-sm-8">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box">
            <!-- col-table -->
            <div class="col-table">
                <div class="innerAll border-bottom">
                    <h4 class="margin-none pull-left">
                        Adrian Demian
                    </h4>
                    <span class="text-muted pull-right">4 projects &nbsp;<i class="fa fa-suitcase text-primary"></i></span>
                    <div class="clearfix"></div>
                </div>
                <div class="innerAll">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="margin-none innerTB half"><i class="fa fa-certificate text-primary"></i>&nbsp; Senior Designer at MosaicPro, Inc.</p>
                        </div>
                        <div class="col-md-4 text-right">
                            <button class="btn btn-default innerLR btn-xs"><i class="fa fa-code-fork fa-rotate-270 fa-fw text-primary"></i> Assign task</button>
                        </div>
                    </div>
                </div>
                <div class="col-separator-h box"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <!-- CONTENT START -->
                                <div class="body">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            <div class="animated bounceInDown">
                                                <a href="" class="inline-block">
                                                    <img src="../assets/images/people/250/2.jpg" alt="Profile" width="380" class="img-responsive img-rounded thumb" />
                                                </a>
                                                <div class="separator bottom"></div>
                                                <h5 class="strong">
                                                    Reports
                                                </h5>
                                                <div class="btn-group btn-group-vertical btn-group-block">
                                                    <a href="" class="btn btn-primary btn-block"><i class="fa fa-download"></i> July</a>
                                                    <a href="" class="btn btn-primary btn-stroke btn-block"><i class="fa fa-download"></i> June</a>
                                                    <a href="" class="btn btn-primary btn-stroke btn-block"><i class="fa fa-download"></i> May</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="innerLR">
                                                <h5 class="innerTB strong border-bottom">
                                                    About
                                                </h5>
                                                <div class="separator bottom"></div>
                                                <p><i class="fa fa-male fa-4x text-faded pull-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, earum, quod nulla quis fugiat voluptatum quae aut ullam quisquam sint a consectetur placeat nam aspernatur delectus laudantium reiciendis! <span class="clearfix"></span></p>
                                                <h5 class="innerTB strong border-bottom">
                                                    Latest Work
                                                </h5>
                                                <div class="separator bottom"></div>
                                                <p><i class="fa fa-suitcase fa-4x text-faded pull-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, veritatis, asperiores, tempora, neque quasi similique placeat eos illo fuga praesentium laboriosam dolores earum veniam iste blanditiis ut excepturi aliquid molestias? <span class="clearfix"></span></p>
                                                <h5 class="innerTB strong border-bottom">
                                                    Lorem ipsum dolor sit
                                                </h5>
                                                <div class="separator bottom"></div>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, reprehenderit, rerum odit nesciunt id quaerat iusto est et harum vitae ex similique nisi dolorum nihil mollitia voluptatem animi voluptatum non.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="bg-primary-light box-generic padding-none">
                                                <h5 class="strong border-bottom innerAll">
                                                    <i class="fa fa-certificate text-primary pull-right"></i> Skills
                                                </h5>
                                                <div class="innerAll">
                                                    <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                                                        <div class="count">100%</div>
                                                        <div class="progress-bar progress-bar-primary" style="width: 100%;"></div>
                                                        <div class="add">HTML</div>
                                                    </div>
                                                    <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                                                        <div class="count">90%</div>
                                                        <div class="progress-bar progress-bar-primary" style="width: 90%;"></div>
                                                        <div class="add">CSS</div>
                                                    </div>
                                                    <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                                                        <div class="count">93%</div>
                                                        <div class="progress-bar progress-bar-primary" style="width: 93%;"></div>
                                                        <div class="add">jQuery</div>
                                                    </div>
                                                    <div class="progress bg-white progress-mini progress-primary count-outside add-outside">
                                                        <div class="count">79%</div>
                                                        <div class="progress-bar progress-bar-primary" style="width: 79%;"></div>
                                                        <div class="add">PHP</div>
                                                    </div>
                                                    <div class="progress bg-white progress-mini count-outside add-outside">
                                                        <div class="count">20%</div>
                                                        <div class="progress-bar progress-bar-primary" style="width: 20%;"></div>
                                                        <div class="add">WP</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bg-primary-light box-generic padding-none">
                                                <h5 class="strong border-bottom innerAll">
                                                    <i class="fa fa-user text-primary pull-right"></i> Get in touch
                                                </h5>
                                                <div class="innerAll border-bottom">
                                                    <ul class="list-unstyled fa-ul">
                                                        <li><i class="fa fa-fw fa-envelope"></i> contact@mosaicpro.biz</li>
                                                        <li><i class="fa fa-fw fa-phone"></i> 00353 9191238101</li>
                                                        <li><i class="fa fa-fw fa-skype"></i> mosaicpro</li>
                                                    </ul>
                                                </div>
                                                <div class="innerAll text-right">
                                                    <button class="btn btn-inverse btn-sm innerLR"><i class="fa fa-envelope fa-fw"></i> Send message</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- // END CONTENT -->
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
                <div class="col-separator-h box"></div>
                <div class="innerAll">
                    <div class="innerAll">
                        <h5 class="strong">
                            MosaicPro Team
                        </h5>
                        <ul class="team">
                            <li class="bg-primary-light"><span class="crt bg-primary">1</span><span class="strong">Adrian Demian</span><span class="muted">Senior Designer</span></li>
                            <li class="bg-white"><span class="crt">2</span><span class="strong">Laza Bogdan</span><span class="muted">Senior Developer</span></li>
                            <li class="bg-white"><span class="crt">3</span><span class="strong">Iuhas Daniel</span><span class="muted">Senior Developer</span></li>
                            <li class="bg-white"><span class="crt">4</span><span class="strong">Dobrit Radu</span><span class="muted">Junior Developer</span></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->