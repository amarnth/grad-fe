<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <div class="col-table">
                <div class="innerTB">
                    <h3 class="margin-none pull-left">
                        <!-- Dashboard Analytics &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> -->
                        Dashboard Overview &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i>
                    </h3>
                    <div class="btn-group pull-right">
                        <a href="index.php?page=dashboard" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Overview</a>
                        <a href="index.php?page=stats_class" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Class</a>
                        <a href="index.php?page=stats_student" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Students</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-separator-h"></div>

                    <?php
                        $stats = new Stats();
                        $stats = $stats->getClass();
                    ?>

                <div class="col-separator-h"></div>
            </div>
        </div>
    </div>
</div>