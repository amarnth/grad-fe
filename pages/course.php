<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <!-- breadcrumb -->
                <ul class="breadcrumb">
                    <li>
                        <a href="index.php?page=index"> <i class="icon-home-1 text-inverse"></i> Courses</a>
                    </li>
                    <li class="divider"></li>
                    <li><a>CORAL</a></li>
                    <li class="divider"></li>
                    <li>How to setup your local host?</li>
                </ul>
                <!-- // END breadcrumb -->
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app.col-unscrollable -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- row -->
                            <div class="row row-app">
                                <!-- col -->
                                <div class="col-md-9">
                                    <!-- col-separator -->
                                    <div class="col-separator">
                                        <div class="media innerB">
                                            <div class="innerAll">
                                                <a class="pull-left bg-success innerAll text-center" href="#"><i class="icon-desktop-play fa-5x fa fa-fw"></i></a>
                                            </div>
                                            <div class="media-body innerLR">
                                                <div class="innerLR">
                                                    <h2 class="media-heading margin-none">
                                                        <a href="" class="text-inverse">How to setup your local development web server</a>
                                                    </h2>
                                                    <p class="lead">by Adrian Demian</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <div class="innerAll inner-2x">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, earum dolor distinctio unde vel numquam provident deserunt eos doloribus ad. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, quod, tempora tenetur doloremque perspiciatis quam reprehenderit doloribus laboriosam consequuntur fuga fugit commodi odit et labore aperiam unde deserunt recusandae nemo!</p>
                                            <div class="separator"></div>
                                            <h4 class="innerB">
                                                1. Install Local Server
                                            </h4>
                                            <p>For a fast development you need to create your own local server.</p>
                                            <p>
                                                <a href="hhttp://www.wampserver.com/en/"> <i class="fa fa-fw icon-scale-2"></i> Installing WAMP</a>
                                            </p>
                                            <p>
                                                <a href="http://documentation.mamp.info/en/mamp"><i class="fa fa-fw icon-scale-2 "></i> Installing MAMP</a>
                                            </p>
                                            <div class="separator"></div>
                                            <pre class="prettyprint">
                                                &lt;?php for ($i=1;$i
                                                <=4;$i++): ?&gt;
&lt;a href="#" class="display-block media margin-none innerAll border-bottom"&gt;
&lt;img class="pull-left" data-src="holder.js/50x50" alt="..."/&gt;
&lt;span class="display-block media-body innerB"&gt;
&lt;h5 class="innerT half"&gt;Video Tutorial&lt;/h5&gt;
&lt;small class="text-center text-inverse muted"&gt;
&lt;i class="fa fa-fw fa-clock-o"&gt;&lt;/i&gt; 5:15
&lt;/small&gt;
&lt;/span&gt;
&lt;/a&gt;
&lt;?php endfor; ?&gt;
</pre>
                                                    <h4 class="innerT">
                                                        2. Setting Up your Project
                                                    </h4>
                                                    <p class="innerT">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, earum dolor distinctio unde vel numquam provident deserunt eos doloribus ad.</p>
                                                    <h4 class="innerT">
                                                        3. Creating a new Page
                                                    </h4>
                                                    <p class="innerT">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, in, tenetur, corporis, accusantium fugit esse laborum illo iste consectetur cumque aliquid repellat adipisci libero asperiores harum sapiente eius voluptas quisquam saepe eligendi aperiam voluptatum maiores necessitatibus dolorum officia? Deserunt, beatae, consectetur accusamus dolor qui iure at rem est fugiat sunt doloremque eum id nostrum fugit amet officia velit optio eveniet quibusdam tenetur. Amet, accusantium, sint nemo temporibus cum et nostrum quod culpa odit necessitatibus repellat assumenda laborum ab dignissimos sit possimus esse blanditiis nesciunt deserunt fugit at numquam aliquam suscipit ducimus unde harum rerum enim placeat consequatur illum quasi vero.</p>
                                                </div>
                                            </div>
                                            <!-- // END col-separator -->
                                        </div>
                                        <!-- // END col -->
                                        <!-- col -->
                                        <div class="col-md-3">
                                            <!-- col-separator -->
                                            <div class="col-separator col-separator-last">
                                                <h5 class="innerAll bg-primary margin-bottom-none">
                                                    Choose Category:
                                                </h5>
                                                <ul class="list-group list-group-1 margin-none borders-none">
                                                    <li class="list-group-item animated fadeInUp">
                                                        <a href="index.php?page=courses_listing"><span class="badge pull-right badge-primary hidden-md">30</span><i class="fa fa-exclamation-circle"></i> HTML 5</a>
                                                    </li>
                                                    <li class="list-group-item animated fadeInUp">
                                                        <a href="index.php?page=courses_listing"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-ticket"></i> CSS/LESS</a>
                                                    </li>
                                                    <li class="list-group-item animated fadeInUp">
                                                        <a href="index.php?page=courses_listing"><i class="fa fa-spinner"></i> Adobe Photoshop</a>
                                                    </li>
                                                    <li class="list-group-item border-bottom-none animated fadeInUp">
                                                        <a href="index.php?page=courses_listing"><i class="fa fa-pencil"></i> PHP</a>
                                                    </li>
                                                    <li class="list-group-item border-bottom-none animated fadeInUp">
                                                        <a href="index.php?page=courses_listing"><i class="icon-folder-fill"></i> MYSQL</a>
                                                    </li>
                                                </ul>
                                                <div class="col-separator-h"></div>
                                                <h5 class="innerAll margin-none border-bottom text-primary">
                                                    Popular Videos
                                                </h5>
                                                <a href="#" class="display-block media margin-none innerAll border-bottom">
                                                    <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                                    <span class="display-block media-body innerB">
                                                        <h5 class="innerT half">
                                                            Video Tutorial
                                                        </h5>
                                                        <div class="clearfix"></div>
                                                        <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                                    </span>
                                                </a>
                                                <a href="#" class="display-block media margin-none innerAll border-bottom">
                                                    <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                                    <span class="display-block media-body innerB">
                                                        <h5 class="innerT half">
                                                            Video Tutorial
                                                        </h5>
                                                        <div class="clearfix"></div>
                                                        <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                                    </span>
                                                </a>
                                                <a href="#" class="display-block media margin-none innerAll border-bottom">
                                                    <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                                    <span class="display-block media-body innerB">
                                                        <h5 class="innerT half">
                                                            Video Tutorial
                                                        </h5>
                                                        <div class="clearfix"></div>
                                                        <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                                    </span>
                                                </a>
                                                <a href="#" class="display-block media margin-none innerAll border-bottom">
                                                    <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                                    <span class="display-block media-body innerB">
                                                        <h5 class="innerT half">
                                                            Video Tutorial
                                                        </h5>
                                                        <div class="clearfix"></div>
                                                        <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                                    </span>
                                                </a>
                                            </div>
                                            <!-- // END col-separator -->
                                        </div>
                                        <!-- // END col -->
                                    </div>
                                    <!-- // END row -->
                                </div>
                                <!-- // END col-app -->
                            </div>
                            <!-- // END col-app.col-unscrollable -->
                        </div>
                        <!-- // END col-table-row -->
                    </div>
                    <!-- // END col-table -->
                </div>
                <!-- // END col-separator -->
            </div>
            <!-- // END col -->
        </div>
        <!-- // END row -->