<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Image Cropping
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-md-6 center">
                                                <img src="../assets/images/gallery-2/10.jpg" id="jcrop-target-1" alt="[Jcrop Example]" class="img-responsive" />
                                                <div class="separator"></div>
                                                <p class="lead">Simple cropping with auto-selection</p>
                                            </div>
                                            <div class="col-md-6 center">
                                                <img class="img-responsive" src="../assets/images/gallery-2/11.jpg" id="jcrop-target-2" alt="[Jcrop Example]" />
                                                <div id="preview-pane">
                                                    <div class="preview-container">
                                                        <img src="../assets/images/gallery-2/11.jpg" class="jcrop-preview img-responsive" alt="[Jcrop Example]" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <p class="lead">Image cropping with preview</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->