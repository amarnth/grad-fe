<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-12 col-md-7">
        <!-- col-separator -->
        <div class="col-separator col-separator-first box col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <div class="heading-buttons innerLR border-bottom">
                    <h4 class="margin-none innerTB pull-left">
                        Courses
                    </h4>
                    <div class="btn-group btn-group-xs pull-right">
                        <button class="btn btn-default active">All</button>
                        <button class="btn btn-default btn-stroke">My Courses</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll bg-white">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="box-generic bg-primary-light text-center margin-none">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-heart fa-4x text-primary"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <div class="innerAll">
                                                    <div class="progress bg-white progress-mini">
                                                        <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-sm btn-primary">Lesson 3/10 <i class="fa fa-fw fa-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box-generic bg-primary-light text-center margin-none">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-camera fa-4x text-primary"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <div class="innerAll">
                                                    <div class="progress bg-white progress-mini">
                                                        <div class="progress-bar progress-bar-primary" style="width: 50%"></div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-sm btn-primary">Lesson 5/10 <i class="fa fa-fw fa-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-separator-h box"></div>
                            <div class="heading-buttons innerLR border-bottom bg-white">
                                <h4 class="margin-none innerTB pull-left">
                                    Available Courses
                                </h4>
                                <div class="btn-group btn-group-xs pull-right">
                                    <button class="btn btn-default active">All</button>
                                    <button class="btn btn-default btn-stroke">Beginners</button>
                                    <button class="btn btn-default btn-stroke">Advanced</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="innerAll">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="box-generic text-center">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-map-marker fa-4x text-faded"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <button class="btn btn-sm btn-inverse">Start course <i class="fa fa-fw fa-play-circle"></i></button>
                                            </div>
                                            <!-- Ribbon -->
                                            <div class="ribbon-wrapper">
                                                <div class="ribbon">50% Off</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box-generic text-center">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-globe fa-4x text-faded"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <button class="btn btn-sm btn-inverse">Start course <i class="fa fa-fw fa-play-circle"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box-generic text-center">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-code-fork fa-4x text-faded"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <button class="btn btn-sm btn-inverse">Start course <i class="fa fa-fw fa-play-circle"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box-generic text-center">
                                            <div class="innerTB">
                                                <div class="innerAll"><i class="fa fa-twitter fa-4x text-faded"></i></div>
                                                <h5 class="strong">
                                                    Lorem ipsum dolor sit amet, consectetur.
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto.</p>
                                                <button class="btn btn-sm btn-inverse">Start course <i class="fa fa-fw fa-play-circle"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
                <div class="text-center innerAll border-top">
                    <ul class="pagination margin-none">
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-md-5 col-sm-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    My Profile
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="bg-gray innerAll">
                                <div class="media">
                                    <img src="../assets/images/people/250/10.jpg" class="pull-left img-circle thumb hidden-sm" alt="people" width="100" />
                                    <div class="media-body">
                                        <h3 class="margin-none">
                                            Mr. Awesome
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, ducimus, quidem odit officia minus optio eligendi explicabo non facilis laborum ea iure voluptatem magnam repudiandae dolores natus vero in reiciendis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-separator-h box"></div>
                            <!-- Row -->
                            <div class="row row-merge margin-none">
                                <div class="col-lg-6">
                                    <a href="" class="widget-stats widget-stats-1 widget-stats-inverse margin-none">
                                        <span class="glyphicons certificate"><i></i><span class="txt">Completed courses</span></span>
                                        <div class="clearfix"></div>
                                        <span class="count">25</span> 
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    <a href="" class="widget-stats widget-stats-primary widget-stats-1 margin-none">
                                        <span class="glyphicons book"><i></i><span class="txt">Avg. Score (%)</span></span>
                                        <div class="clearfix"></div>
                                        <span class="count">85</span> 
                                    </a>
                                </div>
                            </div>
                            <!-- // Row END -->
                            <div class="border-top">
                                <a href="" class="widget-stats widget-stats-2 text-regular margin-none">
                                    <span class="count">3/5</span> 
                                    <span class="txt"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> Stars</span>
                                </a>
                            </div>
                            <div class="col-separator-h box"></div>
                            <div class="innerAll text-center">
                                <div class="innerAll">
                                    <h1>
                                        Education Rocks!
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, explicabo, totam excepturi maxime ab sint facilis expedita laboriosam possimus non facere quidem quibusdam dolor commodi necessitatibus fugit laborum recusandae numquam.</p>
                                    <button class="btn btn-primary btn-lg">Sign me up!</button>
                                </div>
                            </div>
                            <div class="col-separator-h box"></div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->