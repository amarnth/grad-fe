<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator">
            <!-- row-app -->
            <div class="row row-app">
                <!-- col -->
                <div class="col-md-3">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-first box col-unscrollable">
                        <!-- col-table -->
                        <div class="col-table">
                            <h4 class="innerAll margin-none border-bottom">
                                <i class="fa fa-fw fa-bar-chart-o text-primary pull-right"></i> Metrics
                            </h4>
                            <!-- col-table-row -->
                            <div class="col-table-row">
                                <!-- col-app.col-unscrollable -->
                                <div class="col-app col-unscrollable">
                                    <!-- col-app -->
                                    <div class="col-app">
                                        <div id="metrics"></div>
                                        <div class="col-separator-h box"></div>
                                        <div class="innerAll bg-gray">
                                            <div class="innerAll text-center">
                                                <p class="lead margin-none"> <span class="text-large text-regular">540,120</span> </p>
                                                <p class="lead"> <span class="text-primary">transactions</span> this month</p>
                                                <div class="progress progress-mini">
                                                    <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                                </div>
                                                <p class="margin-none">November, 2013</p>
                                            </div>
                                        </div>
                                        <div class="col-separator-h box"></div>
                                    </div>
                                    <!-- // END col-app -->
                                </div>
                                <!-- // END col-app.col-unscrollable -->
                            </div>
                            <!-- // END col-table-row -->
                        </div>
                        <!-- // END col-table -->
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
                <!-- col -->
                <div class="col-md-9">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-last box col-unscrollable">
                        <!-- col-table -->
                        <div class="col-table">
                            <div class="heading-buttons innerLR border-bottom">
                                <div class="btn-group pull-right btn-group-sm" data-toggle="charts-metrics-changedata">
                                    <button class="btn btn-primary">Oct</button>
                                    <button class="btn btn-primary active">Nov</button>
                                    <button class="btn btn-primary">Dec</button>
                                </div>
                                <h4 class="margin-none innerTB">
                                    <i class="fa fa-fw fa-shield text-primary"></i> Transactions
                                </h4>
                            </div>
                            <!-- col-table-row -->
                            <div class="col-table-row">
                                <!-- col-app.col-unscrollable -->
                                <div class="col-app col-unscrollable">
                                    <!-- col-app -->
                                    <div class="col-app">
                                        <div class="innerAll">
                                            <div id="chart_metrics"></div>
                                        </div>
                                    </div>
                                    <!-- // END col-app -->
                                </div>
                                <!-- // END col-app.col-unscrollable -->
                            </div>
                            <!-- // END col-table-row -->
                        </div>
                        <!-- // END col-table -->
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
            </div>
            <!-- // END row-app -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->