<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-12">
        <!-- col-separator -->
        <div class="col-separator">
            <!-- row-app -->
            <div class="row row-app">
                <!-- col -->
                <div class="col-md-8 col-lg-9">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-first box col-unscrollable">
                        <!-- col-table -->
                        <div class="col-table">
                            <div class="innerAll">
                                <input type="text" class="form-control" placeholder="Search appointments ...">
                            </div>
                            <div class="innerAll bg-primary">
                                <div class="pull-left">
                                    <a href="" class="text-white strong"><i class="fa fa-fw fa-plus"></i> Add appointment</a>
                                </div>
                                <div class="pull-right">
                                    <a href="" class="text-white strong"><i class="fa fa-arrow-left"></i></a>
                                    <span class="strong innerLR">Friday, Nov 15, 2013</span> 
                                    <a href="" class="text-white strong"><i class="fa fa-arrow-right"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!-- col-table-row -->
                            <div class="col-table-row">
                                <!-- col-app.col-unscrollable -->
                                <div class="col-app col-unscrollable">
                                    <!-- col-app -->
                                    <div class="col-app">
                                        <!-- APPOINTMENTS START -->
                                        <div class="widget-body padding-none">
                                            <ul class="list-unstyled timeline-appointments border-bottom bg-primary-light">
                                                <li>
                                                    <div class="time">8:00 <span class="text-primary">am</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <div class="apt">
                                                                    <div class="btn-group btn-group-xs pull-right">
                                                                        <a href="" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                                        <a href="" class="btn btn-default"><i class="fa fa-times"></i></a>
                                                                    </div>
                                                                    <a href="" class="text-regular strong"><i class="fa fa-fw fa-user"></i> Saul Jehonathan</a>
                                                                    <i class="text-faded fa fa-ellipsis-h"></i> Ultrasonographer <i class="text-faded fa fa-tag"></i> <span class="label label-inverse">Upcoming</span> 
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="apt">
                                                                    <div class="btn-group btn-group-xs pull-right">
                                                                        <a href="" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                                        <a href="" class="btn btn-default"><i class="fa fa-times"></i></a>
                                                                    </div>
                                                                    <a href="" class="text-regular strong"><i class="fa fa-fw fa-user"></i> Gautam Alain</a>
                                                                    <i class="text-faded fa fa-ellipsis-h"></i> Gynaecologist 
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">9:00 <span class="text-primary">am</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                                <li>
                                                    <div class="time">10:00 <span class="text-primary">am</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <div class="apt">
                                                                    <div class="btn-group btn-group-xs pull-right">
                                                                        <a href="" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                                        <a href="" class="btn btn-default"><i class="fa fa-times"></i></a>
                                                                    </div>
                                                                    <a href="" class="text-regular strong"><i class="fa fa-fw fa-female"></i> Fima Brennus</a>
                                                                    <i class="text-faded fa fa-stethoscope"></i> Consultation 
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">11:00 <span class="text-primary">am</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <div class="apt">
                                                                    <div class="btn-group btn-group-xs pull-right">
                                                                        <a href="" class="btn btn-inverse"><i class="fa fa-plus"></i></a>
                                                                    </div>
                                                                    <span class="text-faded">Empty time slot ...</span> 
                                                                </div>
                                                            </li>
                                                            <li class="bg-primary-light text-primary"> <i class="fa fa-fw fa-clock-o"></i> Half an hour break </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">12:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                                <li>
                                                    <div class="time">1:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments">
                                                        <ul class="list-unstyled">
                                                            <li class="text">
                                                                <div class="strong text-regular"><i class="fa fa-fw fa-file-text-o"></i> Just a note</div>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, enim culpa adipisci atque consequatur sequi eligendi dignissimos minus asperiores esse. Numquam, voluptates, explicabo doloremque obcaecati laboriosam nulla ab fugiat hic. 
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="time">2:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                                <li>
                                                    <div class="time">3:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                                <li>
                                                    <div class="time">4:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                                <li>
                                                    <div class="time">5:00 <span class="text-primary">pm</span></div>
                                                    <i class="fa fa-dot-circle-o text-primary dot"></i> 
                                                    <div class="appointments"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // END APPOINTMENTS -->
                                    </div>
                                    <!-- // END col-app -->
                                </div>
                                <!-- // END col-app.col-unscrollable -->
                            </div>
                            <!-- // END col-table-row -->
                        </div>
                        <!-- // END col-table -->
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
                <!-- col -->
                <div class="col-md-4 col-lg-3">
                    <!-- col-separator.box -->
                    <div class="col-separator col-separator-last box bg-gray">
                        <!-- NEXT APPOINTMENT START -->
                        <div class="bg-gray border-bottom text-center innerAll">
                            <div class="innerAll">
                                <div class="strong">Next appointment</div>
                                <div class="text-large">2h 15min</div>
                                <div class="innerT"> <a href="" class="btn btn-inverse btn-lg">Cancel</a> </div>
                            </div>
                        </div>
                        <div class="bg-white innerAll half text-center"> <span class="strong">Saul Jehonathan</span> Ultrasonographer
</div>
                        <!-- // END NEXT APPOINTMENT -->
                        <div class="col-separator-h box"></div>
                        <div class="innerAll bg-white">
                            <div id="datepicker-inline"></div>
                        </div>
                        <div class="col-separator-h box"></div>
                    </div>
                    <!-- // END col-separator.box -->
                </div>
                <!-- // END col -->
            </div>
            <!-- // END row-app -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->