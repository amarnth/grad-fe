<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first border-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app">
                        <!-- row-app -->
                        <div class="row row-app margin-none">

                        <?php
                            $info = new Groups();
                            // $info->getInfo($_SESSION['group_id']);
                            $info->getInfo($_GET['gid']);
                        ?>
                            

</div>
<!-- // END box -->
<div class="col-separator-h"></div>
<!-- col-table-row -->
<div class="col-table-row">
<!-- box -->
<div class="col-app box col-unscrollable overflow-hidden">
<!-- col-table -->
<div class="col-table">
<div class="innerLR heading-buttons border-bottom">
<h4 class="innerTB margin-none pull-left">
Group Activity
</h4>
<div class="btn-group btn-group-xs pull-right">
<button class="btn btn-default filled"><i class="fa fa-refresh group-wall"></i></button>
<button class="btn btn-default "><i class="fa fa-cog "></i></button>
</div>
<div class="clearfix"></div>
</div>
<!-- col-table-row -->
<div class="col-table-row">
<!-- col-app -->
<div class="col-app col-unscrollable">
<!-- col-app -->
<div class="col-app">
    <ul class="timeline-activity list-unstyled" id="wallPosts">
       <?php
       
       
        // echo ASSETS_PATH."/grad_assets/classes/wallclass.php";
        $wall = new Groupwall();
        // $_SESSION['group_id'];
        // $wall->getWall($_SESSION['group_id']);
        $wall->getWall($_GET['gid']);
        // var_dump($grad->run());
        
        ?>

    </ul>
</div>
<!-- // END col-app -->
</div>
<!-- // END col-app -->
</div>
<!-- // END col-table-row -->
</div>
<!-- // END col-table -->
</div>
<!-- // END col-app.box -->
</div>
<!-- // END col-table-row -->
</div>
<!-- // END col-table -->
</div>
<!-- // END col-separator -->
</div>
<!-- // END col -->
<!-- col -->
<div class="col-sm-12 col-md-3 col-lg-2">
    
    <!-- <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head border-bottom bg-primary" >
            <h4 class="heading" style="color: #FFF;">
                Add User 
            </h4>
        <span class="collapse-toggle"></span></div>
        <div class="widget-body collapse">

            <div class="form-group get-members">
                <select multiple="multiple" style="width: 100%;" id="invite-members">
                </select>
            </div>

            <div class="text-center innerAll">
                <a class="btn btn-primary invite-new-member" data-dismiss="modal" aria-hidden="true">Invite</a>
            </div>

        </div>
    </div> -->
    
<!-- col-separator -->
<div class="col-separator col-unscrollable box col-separator-last reset-components">
<!-- col-table -->
<div class="col-table">
<div class="input-group friends-search">
<input class="form-control col-md-6 innerAll" id="appendedInputButtons" type="text" placeholder="Search">
<div class="input-group-btn">
<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
</div>
<div class="clearfix"></div>
</div>



<!-- col-table-row -->
<div class="col-table-row">
<!-- col-app -->
<div class="col-app col-unscrollable">




<!-- col-app -->
<div class="col-app">
<ul class="list-unstyled friends-list">
<?php
    // echo ASSETS_PATH."/grad_assets/classes/wallclass.php";
    $users = new Util();
    $users->getOnline();
    // var_dump($grad->run());
?>
</ul>
</div>
<!-- // END col-app -->
</div>
<!-- // END col-app -->
</div>
<!-- // END col-table-row -->



<div class="col-separator-h box"></div>



<!-- <a href="#" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add user</a> -->

<div class="col-separator-h box"></div>
<!-- Stats Widget -->
<!-- <a href="" class="widget-stats widget-stats-gray widget-stats-2">
<span class="count">14</span>
<span class="txt">New Members</span>
</a> -->
<!-- // Stats Widget END -->
</div>
<!-- // END col-table -->
</div>
<!-- // END col-separator -->
</div>
<!-- // END col -->
</div>
<!-- // END row -->
</div>
<!-- // END col-app -->
</div>
<!-- // END col-table-row -->
</div>
<!-- // END col-table -->
</div>
<!-- // END col-separator -->
</div>
<!-- // END col -->
</div>
<!-- // END row-app -->





<style>




.collapse-toggle:before {
  font-family: "Glyphicons Regular";
  font-size: 17px;
  display: block;
  width: 100%;
  height: 35px;
  line-height: 34px;
  text-align: center;
  color: #FFFFFF !important;
  content: "\e192";
}




.ajax-file-upload-statusbar {
margin-top: 10px;
width: 420px;
margin-right: 10px;
margin: 5px;
padding: 5px 5px 5px 5px;
}
.ajax-file-upload-filename {
display: inline-block;
height: auto;
margin: 0 5px 5px 10px;
color: #807579
}

.ajax-file-upload-progress {
    //display: none !important;
}
.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}
.ajax-file-upload-green {
    //display: none !important;;
}

.ajax-file-upload-bar {
background-color: #0ba1b5;
width: 0;
height: 20px;
border-radius: 3px;
color:#FFFFFF;
}
.ajax-file-upload-percent {
position: absolute;
display: inline-block;
top: 3px;
left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  width: 14%;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 15%;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px 10px 0px 10px;
}


 textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}

</style>