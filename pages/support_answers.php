<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-3">
        <!-- col-separator -->
        <div class="col-separator col-separator-first">
            <h5 class="innerAll border-bottom margin-bottom-none">
                Filter by
            </h5>
            <form role="form">
                <div class="innerAll inner-2x">
                    <div class="form-group margin-none">
                        <div class="checkbox">
                            <label class="checkbox-custom">
                                <i class="fa fa-fw fa-square-o"></i>

                                <input type="checkbox" id="inlineCheckbox1" class="checkbox" value="option1" checked="checked">
                                Unanswered

                            </label>
                        </div>
                        <div class="checkbox margin-none">
                            <label class="checkbox-custom">
                                <i class="fa fa-fw fa-square-o"></i>

                                <input type="checkbox" id="inlineCheckbox2" value="option2" class="checkbox">
                                Answered

                            </label>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-separator-h"></div>

                <div class="widget">
                    <div class="text-center innerAll inner-2x">
                        <h2 class="innerT">
                            Question <i class="icon-compose fa fa-fw"></i>
                        </h2>

                        <div class="form-group">
                            <input type="text" class="form-control new-category-name" placeholder="have any doubt?">
                        </div>

                        <!-- <div class = "form-group">
                            <label class = "control-label">Category</label>
                            <select multiple = "multiple" style = "width: 100%;" id = "select-category">
                            </select>
                        </div> -->

                        <div class="text-center innerAll">
                            <a href="" class="btn btn-primary invite-mentor">Ask</a>
                        </div>

                    </div>
                </div>
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->


    <!-- col -->
    <div class="col-md-9">
        <!-- col-separator -->
        <div class="col-separator">
            
        <?php
            $answers = new Answers();
            $answers->get();
        ?>

        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->