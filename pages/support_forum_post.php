<!-- row -->
<div class="row row-app">
    <div class="col-sm-12">
        <div class="col-separator col-separator-first bg-none">
            <div class="col-table">
                <!-- Main Heading -->
                <h1>
                    How do I change the skin of Coral?
                </h1>
                <div class="col-separator-h"></div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="row row-app">
                                <div class="col-md-9">
                                    <div class="col-separator">
                                        <!-- Post List -->
                                        <div class="media innerAll inner-2x border-bottom margin-none">
                                            <div class="pull-left media-object" style="width:100px">
                                                <div class="text-center">
                                                    <a href="#" class="clearfix">
                                                        <img src="../assets//images/people/50/1.jpg" class="rounded-none"/>
                                                    </a>
                                                    <small class="text-small">Junior Member</small>
                                                    <div class="innerT half">
                                                        <a href="" class="text-muted"><i class="icon-user-1"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-star-fill"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <small class="pull-right label label-default">3 days ago</small>
                                                <h5>
                                                    <a href="" class="text-inverse">joanne</a> <span>wrote:</span>
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
                                                <small class="display-block text-muted">
                                                    wrote by <a href="">by Adrian</a> | posted in <a href="">Category</a> |

                                                    <span class="text-success"><i class="fa fa-check"></i> Answered</span>
                                                </small>
                                            </div>
                                        </div>
                                        <div class="media innerAll inner-2x border-bottom margin-none">
                                            <div class="pull-left media-object" style="width:100px">
                                                <div class="text-center">
                                                    <a href="#" class="clearfix">
                                                        <img src="../assets//images/people/50/2.jpg" class="rounded-none"/>
                                                    </a>
                                                    <small class="text-small">Junior Member</small>
                                                    <div class="innerT half">
                                                        <a href="" class="text-muted"><i class="icon-user-1"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-star-fill"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <small class="pull-right label label-default">3 days ago</small>
                                                <h5>
                                                    <a href="" class="text-inverse">bill14</a> <span>wrote:</span>
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
                                                <small class="display-block text-muted">
                                                    wrote by <a href="">by Adrian</a> | posted in <a href="">Category</a> |

                                                    <span class="text-success"><i class="fa fa-check"></i> Answered</span>
                                                </small>
                                            </div>
                                        </div>
                                        <div class="media innerAll inner-2x border-bottom margin-none">
                                            <div class="pull-left media-object" style="width:100px">
                                                <div class="text-center">
                                                    <a href="#" class="clearfix">
                                                        <img src="../assets//images/people/50/3.jpg" class="rounded-none"/>
                                                    </a>
                                                    <small class="text-small">Junior Member</small>
                                                    <div class="innerT half">
                                                        <a href="" class="text-muted"><i class="icon-user-1"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-star-fill"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <small class="pull-right label label-default">3 days ago</small>
                                                <h5>
                                                    <a href="" class="text-inverse">mary</a> <span>wrote:</span>
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
                                                <small class="display-block text-muted">
                                                    wrote by <a href="">by Adrian</a> | posted in <a href="">Category</a> |

                                                    <span class="text-success"><i class="fa fa-check"></i> Answered</span>
                                                </small>
                                            </div>
                                        </div>
                                        <div class="media innerAll inner-2x border-bottom margin-none">
                                            <div class="pull-left media-object" style="width:100px">
                                                <div class="text-center">
                                                    <a href="#" class="clearfix">
                                                        <img src="../assets//images/people/50/4.jpg" class="rounded-none"/>
                                                    </a>
                                                    <small class="text-small">Junior Member</small>
                                                    <div class="innerT half">
                                                        <a href="" class="text-muted"><i class="icon-user-1"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
                                                        <a href="" class="text-muted"><i class="icon-star-fill"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <small class="pull-right label label-default">3 days ago</small>
                                                <h5>
                                                    <a href="" class="text-inverse">mosaicpro</a> <span>wrote:</span>
                                                </h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
                                                <small class="display-block text-muted">
                                                    wrote by <a href="">by Adrian</a> | posted in <a href="">Category</a> |

                                                    <span class="text-success"><i class="fa fa-check"></i> Answered</span>
                                                </small>
                                            </div>
                                        </div>
                                        <!-- // END Category Listing -->
                                        <div class="innerAll text-right">
                                            <ul class="pagination margin-none">
                                                <li class="disabled"><a href="#">&laquo;</a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">&raquo;</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <h4 class="innerAll border-bottom bg-gray margin-none">
                                            Reply to Post
                                        </h4>
                                        <form class="text-right innerAll">
                                            <div class="innerB">
                                                <textarea name="" id="" cols="30" rows="3" class="form-control rounded-none margin-bottom notebook border-none padding-none" placeholder="Type here..."></textarea>
                                            </div>
                                            <a href="" class="btn btn-primary btn-sm strong">Submit <i class="fa fa-arrow-right fa-fw"></i></a>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-separator col-separator-last">
                                        <div class="innerAll border-bottom bg-gray">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search posts ...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <h4 class="innerAll bg-primary margin-none">
                                            Latest Posts
                                        </h4>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by joanne</a>

                                        </div>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by bill14</a>

                                        </div>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by mary</a>

                                        </div>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by mosaicpro</a>

                                        </div>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by bogdan</a>

                                        </div>
                                        <div class="bg-gray-hover border-bottom innerAll">
                                            <a href="" class="text-primary strong">Post title goes here</a>

                                            <div class="clearfix"></div>
                                            <span class="text-small">1 hour ago</span> <a href="" class="text-small">by andrew</a>

                                        </div>
                                        <div class="col-separator-h"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>