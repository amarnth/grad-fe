<div class="row row-app">
  <div class="innerAll">
       <!-- Tabs -->
        <div style="text-align: right;" class="innerB">
          <a href="#create-new-group" data-toggle="modal" class="btn btn-sm btn-default create-new-group"> <i class="fa fa-plus fa-fw"></i> CREATE NEW GROUP</a>
        </div>
        <div class="row seperator"></div>
      <div class="relativeWrap" >
          <div class="box-generic" style="border: 0px solid #FFFFFF; padding: 0px; margin: 0px;">
              <!-- Tabs Heading -->
              <div class="tabsbar tabsbar-2" style="margin: 0px;">
                  <ul class="row row-merge">
                      <li class="col-md-3 glyphicons cargo active">
                          <a href="#tab1-4" data-toggle="tab"><i></i> My Group</a>
                      </li>
                      <li class="col-md-3 glyphicons circle_info">
                          <a href="#tab2-4" data-toggle="tab"><i></i> <span>Friends Group</span></a>
                      </li>
                      <li class="col-md-3 glyphicons cart_in">
                          <a href="#tab3-4" data-toggle="tab"><i></i> <span>Suggested Group</span></a>
                      </li>
                      <li class="col-md-3 glyphicons pencil">
                          <a href="#tab4-4" data-toggle="tab"><i></i> <span>Pending</span></a>
                      </li>
                  </ul>
              </div>
              <!-- // Tabs Heading END -->
          </div>
      </div>
     
        <div id="groupsInfo" style="background: #fff;">
          <?php
              $groups = new Groups();
              $groups->getGroups();
          ?>
        </div>

      <!-- // Tabs END -->
      <!-- Tabs -->
  </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="create-new-group">
      <div class="modal-dialog">
          <div class="modal-content">
              <!-- Modal heading -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">
                      Create New Group
                  </h3>
              </div>
              <!-- // Modal heading END -->
              <!-- Modal body -->
              <div class="modal-body">
                  <div class="innerAll">
                      <form class="margin-none innerLR inner-2x">
                          <div class="form-group">
                              <!-- <label>Group Name</label> -->
                              <input type="text" class="form-control group-name" placeholder="Group Name">
                          </div>
                          
                          <div class="form-group">
                              <input type="textarea" class="form-control group-description" placeholder="Group Description">
                          </div>
                          

                          <div class="form-group">
                            <!-- <div class="col-md-4"> -->
                              <label>Group Image</label>
                              <div class="btn btn-default btn-file" id="mulitplefileuploader">Select</div>
                              <div id="newPostPreview"></div>
                              <div id="mulitplefileuploadStatus"></div>
                            <!-- </div> -->
                          </div>
                          


                          <!-- <div class="form-group">
                            <h4 class="separator bottom">Group Image</h4>
                            <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                <span class="btn btn-default btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                  <input type="file" name="files" class="margin-none group-image" />
                                </span>
                                <span class="fileupload-preview"></span>
                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                            </div>
                          </div> -->

                          <div class="form-group">
                              <label class="control-label">Select Members</label>
                              <select multiple="multiple" style="width: 100%;" id="select2_members">
                                 
                              </select>
                          </div>

                          
                          <div class="form-group">
                              <div class="row">
                                  <div class="col-md-3">
                                    <label class="control-label">Group type</label> <br/>
                                    <div class="radio">
                                        <label class="radio-custom group-type-open">
                                            <input type="radio" name="radio" checked="checked"> 
                                            <i class="fa fa-circle-o checked"></i> Open
                                        </label> 
                                    </div> 
                                    <div class="radio"> 
                                        <label class="radio-custom group-type-close"> 
                                            <input type="radio" name="radio"> 
                                            <i class="fa fa-circle-o"></i> Close
                                        </label> 
                                    </div> 
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">Admin Approval</label><br/>
                                    <div class="make-switch" data-on="default" data-off="default"><input type="checkbox" checked></div>
                                </div>
                              </div>
                          </div>

                          <div class="text-center innerAll">
                              <a href="" class="btn btn-primary create_a_group" data-dismiss="modal" aria-hidden="true">Create</a>
                          </div>

                      </form>
                  </div>
              </div>
              <!-- // Modal body END -->
          </div>
      </div>
  </div>
<!-- // Modal END -->






<style>

h4.group_name{
	cursor: pointer;
}

.ajax-file-upload-error{
  color: red;
}

.ajax-file-upload-statusbar {
margin-top: 10px;
width: 420px;
margin-right: 10px;
margin: 5px;
padding: 5px 5px 5px 5px
}
.ajax-file-upload-filename {
display: inline-block;
height: auto;
margin: 0 5px 5px 10px;
color: #807579
}

.ajax-file-upload-progress {
    display: none !important;
}
.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}
.ajax-file-upload-green {
    display: none !important;;
}

.ajax-file-upload-bar {
background-color: #0ba1b5;
width: 0;
height: 20px;
border-radius: 3px;
color:#FFFFFF;
}
.ajax-file-upload-percent {
position: absolute;
display: inline-block;
top: 3px;
left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  width: 15%;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 15%;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px;
}


 textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}

</style>