<!-- row -->
<div class="row row-app">
    <div class="col-sm-12">
        <div class="col-separator bg-none col-separator-first">
            <div class="col-table">
                <!-- Top -->
                <div class="row">
                    <div class="col-sm-8">
                        <!-- Main Heading -->
                        <h1 class="pull-left">
                            Knowledge Base
                        </h1>
                    </div>
                    <div class="col-sm-4">
                        <!-- Search Bar -->
                        <div class="input-group innerT half pull-right">
                            <input type="text" class="form-control" placeholder="Search the Knowledge Base">
                            <span class="input-group-btn">
                                <button class="btn btn-primary rounded-none" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-separator-h"></div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="row row-app">
                                <div class="col-md-9">
                                    <div class="col-separator">
                                        <div class="row row-merge">
                                            <div class="col-sm-4 bg-gray-hover">
                                                <div class="innerAll text-center">
                                                    <i class="icon-question fa fa-5x innerTB text-primary "></i>

                                                    <div class="clearfix"></div>
                                                    <a class="lead innerTB strong text-inverse">Community Q&amp;A</a>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, deserunt.</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 bg-gray-hover">
                                                <div class="innerAll text-center">
                                                    <i class="icon-ticket fa fa-5x innerTB text-primary"></i>

                                                    <div class="clearfix"></div>
                                                    <a class="lead innerTB strong text-inverse ">Support Tickets</a>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, deserunt.</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 bg-gray-hover">
                                                <div class="innerAll text-center">
                                                    <i class=" icon-comment-heart-fill fa fa-5x innerTB text-primary"></i>

                                                    <div class="clearfix"></div>
                                                    <a class="lead innerTB strong text-inverse ">Feedback</a>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, deserunt.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- Category Heading -->
                                        <h4 class="innerAll bg-gray border-bottom margin-bottom-none ">
                                            General Information
                                        </h4>
                                        <!-- Category Links -->
                                        <div class="innerAll inner-2x">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- Category Heading -->
                                        <h4 class="innerAll bg-gray border-bottom margin-bottom-none">
                                            Documentation
                                        </h4>
                                        <!-- Category Links -->
                                        <div class="innerAll inner-2x">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                    <div class="separator"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                    <div class="separator"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6">
                                                    <h4 class="border-bottom innerB half text-weight-normal text-muted-darker">
                                                        Category
                                                    </h4>
                                                    <div class="separator bottom"></div>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Getting Started</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Requirements</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Common Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Pro and Cons</a>
                                                        </li>
                                                        <li>
                                                            <a href=""><i class="icon-paper-document fa fa-fw text-muted"></i> Best Practices</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <!-- col-separator -->
                                    <div class="col-separator col-separator-last">
                                        <!-- Heading -->
                                        <a href="" class="btn btn-success display-block innerTB strong margin-none rounded-none">Ask a question <i class="fa fa-question-circle"></i></a>
                                        <div class="col-separator-h"></div>
                                        <!-- Category -->
                                        <ul class="list-group list-group-1 margin-none borders-none">
                                            <li class="list-group-item active animated fadeInUp">
                                                <a href="#"><span class="badge pull-right badge-primary hidden-md">30</span><i class="fa fa-exclamation-circle"></i> Unread Tickets</a>
                                            </li>
                                            <li class="list-group-item animated fadeInUp">
                                                <a href="#"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-ticket"></i> New Tickets</a>
                                            </li>
                                            <li class="list-group-item animated fadeInUp">
                                                <a href="#"><i class="fa fa-spinner"></i> Pending Tickets</a>
                                            </li>
                                            <li class="list-group-item border-bottom-none animated fadeInUp">
                                                <a href="#"><i class="fa fa-pencil"></i> Drafts</a>
                                            </li>
                                            <li class="list-group-item border-bottom-none animated fadeInUp">
                                                <a href="#"><i class="icon-folder-fill"></i> Archive</a>
                                            </li>
                                        </ul>
                                        <div class="col-separator-h"></div>
                                        <!-- Heading -->
                                        <h5 class="innerAll margin-bottom-none bg-inverse border-bottom">
                                            <i class="fa fa-refresh fa-fw"></i> Latest Questions
                                        </h5>
                                        <!-- Listing -->
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <a href="#" class="innerB half margin-none text-primary strong">How do you guys change navbar into white?</a> 
                                            <div class="clearfix"></div>
                                            <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <a href="#" class="innerB half margin-none text-primary strong">How do you guys change navbar into white?</a> 
                                            <div class="clearfix"></div>
                                            <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <a href="#" class="innerB half margin-none text-primary strong">How do you guys change navbar into white?</a> 
                                            <div class="clearfix"></div>
                                            <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <a href="#" class="innerB half margin-none text-primary strong">How do you guys change navbar into white?</a> 
                                            <div class="clearfix"></div>
                                            <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                        </div>
                                        <!-- // END Listing -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // END row -->