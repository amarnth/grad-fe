<div class="row row-app">
  <div class="col-lg-12" id="get-noticeboard" style="padding: 0 10px;">
    <?php
    	$noticeboard = new Noticeboard();
    	$noticeboard->get();
    ?>
  </div>
</div>


<!-- Modal -->
  <div class="modal fade" id="create-new-noticeboard" style="">
      <div class="modal-dialog">
          <div class="modal-content">
              <!-- Modal heading -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 class="modal-title">
                      Create New
                  </h3>
              </div>
              <!-- // Modal heading END -->
              <!-- Modal body -->
              <div class="modal-body">
                  <div class="innerAll">
                        <form class="margin-none innerLR inner-2x">
							<input type="text" placeholder="Title" class="form-control new_noticeboard_title" />

							<div class="separator bottom"></div>

							<textarea id="textDescription_1" class="wysihtml5 form-control new_noticeboard_description" rows="7"></textarea>

              <div class="form-group">
                <!-- <div class="col-md-4"> -->
                  <label>Attachment</label>
                  <div class="btn btn-default btn-file" id="mulitplefileuploader">Select</div>
                  <div id="newPostPreview"></div>
                  <div id="mulitplefileuploadStatus"></div>
                <!-- </div> -->
              </div>

              <div class="separator bottom"></div>

              <div class="widget-body">
                  <div class="form-group">
                      <label class="">To: </label>
                      <div class="radio ">
                          <label class="radio-custom select-courses col-md-3">
                              <input type="radio" name="radio" checked="checked"> 
                              <i class="fa fa-circle-o checked"></i> Department
                          </label> 
                          <label class="radio-custom select-classes col-md-2"> 
                              <input type="radio" name="radio"> 
                              <i class="fa fa-circle-o"></i> Class
                          </label> 
                      </div> 
                  </div>


                  <div class="form-group get-courses">
                      <h5 class="strong text-uppercase">Department</h5>
                      <select id="multiselect-optgroup" multiple="multiple">
                        <!-- append data via ajax -->
                         <?php 
                          $util = new Util();
                          $util->getCoursesGroup();
                        ?>
                      </select>
                  </div>

                  <div class="form-group get-classes">
                      <h5 class="strong text-uppercase">Classes</h5>
                      <!-- <label class="control-label">Classes</label> -->
                      <select multiple="multiple" style="width: 100%;" id="select-classes">
                        <!-- append data via ajax -->
                      </select>
                  </div>
              </div>

							<div class="separator bottom"></div>

                            <div class="center innerAll">
                                <button type="button" class="btn btn-primary new-noticeboard" data-loading-text="Proccesing...">Create</button>
                            </div>

                        </form>
                  </div>
              </div>
              <!-- // Modal body END -->
          </div>
      </div>
  </div>
<!-- // Modal END -->

<!-- <section id="blog-landing">
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/07/Blog-Post-Imagery5.png" alt="ALT">
<h1><a href="#">Putting users first with flat design</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit.</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2014/04/food-drink-expo.png" alt="ALT">
<h1><a href="#">Is your website converting visits into sales? </a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2014/03/tile-app-2.jpg" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2014/03/blog-webdesign.gif" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2014/01/sky2.jpg" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2014/03/blog-ecommerce.gif" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/12/Blog-Post-Imagery.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/10/Foursys3.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/10/Blog-Post-Imagery4.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/10/Blog-Post-Imagery3.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/09/Kiddiwinks-blog.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec,</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/08/shutterstock_120872494.jpg" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/08/Blog-Post-Imagery2.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/07/tech-blog.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/06/how-to-use-google-glass.jpg" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consevamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/06/DOG-SEO3.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. Vivamus ullamcorper aliquam purus, et euismod justo. Nulla</p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/05/responsive.gif" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non sollicitudin elit. Curabitur magna ligula, condimentum sed lacus nec, vulputate cursus sem. Sed a semper felis. Curabitur ligula enim, auctor eget rutrum a, convallis non diam. </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/04/web-design-2013.png" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
</article>
<article class="white-panel"> <img src="http://www.mediademon.com/wp-content/uploads/2013/04/Sales.jpg" alt="ALT">
<h1><a href="#">Fantasy football and Raphaël: the vector graphic JS library</a></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
</article>
</section> -->

<style>
 #blog-landing {
margin-top: 81px;
position: relative;
max-width: 100%;
width: 100%;
}
img {
width: 100%;
max-width: 100%;
height: auto;
}
.white-panel {
position: absolute;
background: white;
box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
padding: 10px;
}
.white-panel h1 {
font-size: 1em;
}
.white-panel h1 a {
color: #A92733;
}
.white-panel:hover {
box-shadow: 1px 1px 10px rgba(0,0,0,0.5);
margin-top: -5px;
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}




    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top{
        z-index: 10002 !important;
    }




.pp_social{
    display: none;
}





.ajax-file-upload-statusbar {
margin-top: 10px;
width: 420px;
margin-right: 10px;
margin: 5px;
padding: 5px 5px 5px 5px;
}
.ajax-file-upload-filename {
display: inline-block;
height: auto;
margin: 0 5px 5px 10px;
color: #807579
}

.ajax-file-upload-progress {
    //display: none !important;
}
.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}
.ajax-file-upload-green {
    //display: none !important;;
}

.ajax-file-upload-bar {
background-color: #0ba1b5;
width: 0;
height: 20px;
border-radius: 3px;
color:#FFFFFF;
}
.ajax-file-upload-percent {
position: absolute;
display: inline-block;
top: 3px;
left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  width: 14%;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 15%;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px 10px 0px 10px;
}


 textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}

</style>






