
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first box">
            <!-- Tabs -->
            <div class="widget widget-tabs widget-tabs-social-account widget-tabs-responsive">
                <!-- Tabs Heading -->
                <div class="widget-head">
                    <ul>
                        <li class="active">
                            <a class="glyphicons user" href="#tabAccount" data-toggle="tab"><i></i><span>Account</span></a>
                        </li>
<!--                         <li>
                            <a class="glyphicons credit_card" href="#tabPayments" data-toggle="tab"><i></i><span>Payment</span></a>
                        </li>
                        <li>
                            <a class="glyphicons envelope" href="#tabInbox" data-toggle="tab"><i></i><span>Inbox</span></a>
                        </li> -->
                        <li>
                            <a class="glyphicons lock" href="#tabPassword" data-toggle="tab"><i></i><span>Password</span></a>
                        </li>
                    </ul>
                </div>
                <!-- // Tabs Heading END -->
                <div class="col-separator-h box"></div>
                <div class="widget-body">
                    <div class="tab-content">
                        <!-- Tab content -->
                        <div id="tabAccount" class="tab-pane active widget-body-regular padding-none border-none reset-components">
                            <!-- Widget -->
                            <div class="widget widget-tabs border-bottom-none">
                                <!-- Widget heading -->
                                <div class="widget-head">
                                    <ul>
                                        <li class="active">
                                            <a class="glyphicons edit" href="#account-details" data-toggle="tab"><i></i>Account details</a>
                                        </li>
                                        <li>
                                            <a class="glyphicons settings" href="#account-settings" data-toggle="tab"><i></i>Account settings</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- // Widget heading END -->
                                <div class="widget-body">
                                    <?php
                                        $account = new SocialAccount();
                                        $account->get();
                                    ?>
                                    
                                </div>
                            </div>
                            <!-- // Widget END -->
                        </div>
                        <!-- // Tab content END -->
                        <!-- Tab content -->
                     <!--    <div id="tabPayments" class="tab-pane innerAll">
                            <h4 class="innerTB">
                                Payment &amp; Billing Information
                            </h4>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="box-generic bg-primary padding-none">
                                        <div class="innerAll half pull-right">
                                            <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times "></i></a>
                                        </div>
                                        <h4 class="text-white strong margin-none innerAll pull-left">
                                            <i class="fa fa-credit-card innerR"></i> XXXX XXXX XXXX XXXX 1209
                                        </h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="box-generic padding-none">
                                        <div class="pull-right btn-group btn-group-sm innerAll half">
                                            <a href="#" class="btn btn-default"><i class="fa fa-check "></i> Make Primary</a>
                                            <a href="" class="btn btn-default"><i class="fa fa-times"></i></a>
                                        </div>
                                        <h4 class="strong margin-none innerAll pull-left">
                                            <i class="fa fa-credit-card innerR"></i> XXXX XXXX XXXX XXXX 1209
                                        </h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="box-generic padding-none">
                                        <h5 class="strong innerAll border-bottom margin-none bg-gray">
                                            Add New Card
                                        </h5>
                                        <div class="innerAll inner-2x">
                                            <form action="?module=shop&amp;page=checkout_confirmation">
                                                <div class="form-group">
                                                    <label>Card Number</label>
                                                    <input type="email" class="form-control" placeholder="Enter Card Number">
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Expiry Date</label>
                                                            <div class="row margin-none">
                                                                <div class="col-xs-6 padding-none">
                                                                    <select class="form-control selectpicker">
                                                                        <option>1</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                        <option>6</option>
                                                                        <option>7</option>
                                                                        <option>8</option>
                                                                        <option>9</option>
                                                                        <option>10</option>
                                                                        <option>11</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-6 padding-none">
                                                                    <select class="form-control selectpicker">
                                                                        <option>2013</option>
                                                                        <option>2014</option>
                                                                        <option>2015</option>
                                                                        <option>2016</option>
                                                                        <option>2017</option>
                                                                        <option>2018</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <label>Security Code <i class="fa fa-question-circle"></i></label>
                                                            <input type="password" class="form-control" placeholder="CVV / CVV2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Cardholder Name</label>
                                                    <input type="text" class="form-control" placeholder="Name on Card">
                                                </div>
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary">Make a Payment </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box-generic strong">
                                        <i class="fa fa-user fa-3x pull-left text-faded"></i> Adrian Demian 
                                        <br/>
                                        <small class="text-faded">12 Nov 2013</small>
                                    </div>
                                    <div class="box-generic padding-none">
                                        <div class="innerAll border-bottom">
                                            <a href="" class="pull-right text-primary"><i class="fa fa-pencil"></i></a>
                                            <h4 class="panel-title strong">
                                                Billing Address 
                                            </h4>
                                        </div>
                                        <div class="innerAll"> <i class="fa fa-building pull-left fa-3x"></i> <span>129 Longford Terrace, Dublin, Ireland</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- // Tab content END -->
                        <!-- Tab content -->
                       <!--  <div id="tabInbox" class="tab-pane">
                            <div class="row margin-none email bg-white">
                                <div class="col-md-2 col-sm-12 padding-none">
                                    <ul class="list-group list-group-1 margin-none borders-none">
                                        <li class="list-group-item active">
                                            <a href="#"><span class="badge pull-right badge-primary hidden-md">30</span><i class="fa fa-inbox"></i> Inbox</a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-book"></i> Drafts</a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"><i class="fa fa-envelope"></i> Sent</a>
                                        </li>
                                        <li class="list-group-item border-bottom-none">
                                            <a href="#"><i class="fa fa-cogs"></i> Settings</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-sm-3 padding-none">
                                    <div class="widget">
                                        <div class="innerAll border-bottom">
                                            <div class="label label-default">INBOX</div>
                                            You have <strong class="text-primary">5 new</strong> emails

                                        </div>
                                        <div class="widget-body padding-none">
                                            <div class="list-group email-item-list">
                                                <a href="#" class="list-group-item active">
                                                    <span class="label label-inverse pull-right">27 Oct</span> 
                                                    <h4>
                                                        MosaicPRO <i class="fa fa-flag text-primary"></i>
                                                    </h4>
                                                    <p class='text-regular margin-none'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</p>
                                                </a>
                                                <a href="#" class="list-group-item">
                                                    <span class="label label-inverse pull-right">16 Sep</span> 
                                                    <h4 class="list-group-item-heading">
                                                        Adrian <i class="fa fa-flag text-primary"></i>
                                                    </h4>
                                                    <p class="text-regular margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</p>
                                                </a>
                                                <a href="#" class="list-group-item ">
                                                    <span class="label label-inverse pull-right">19 Aug</span> 
                                                    <h4 class="list-group-item-heading">
                                                        Mr.Awesome <i class="fa fa-bookmark-empty text-primary"></i> <i class="fa fa-briefcase text-regular"></i>
                                                    </h4>
                                                    <p class="text-regular margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</p>
                                                </a>
                                                <a href="#" class="list-group-item ">
                                                    <span class="label label-inverse pull-right">5 Aug</span> 
                                                    <h4 class="list-group-item-heading">
                                                        MosaicPRO <i class="fa fa-flag text-primary"></i>
                                                    </h4>
                                                    <p class="text-regular margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</p>
                                                </a>
                                                <a href="#" class="list-group-item ">
                                                    <span class="label label-inverse pull-right">1 Jul</span> 
                                                    <h4 class="list-group-item-heading">
                                                        Adrian
                                                    </h4>
                                                    <p class="text-regular margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-9">
                                    <div class="action border-bottom innerAll">
                                        <div class="btn-group btn-group-sm">
                                            <a href="#" class="btn btn-default"><i class="fa fa-reply"></i></a>
                                            <a href="#" class="btn btn-default"><i class="fa fa-forward"></i></a>
                                            <a href="#" class="btn btn-primary"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="innerAll">
                                        <div class="email-content innerB">
                                            <div class="from">
                                                <a href="#">Adrian Demian</a> <span>(contact@mosaicpro.biz)</span>

                                                <div class="clearfix"></div>
                                                <span>To: your@email.com</span>

                                            </div>
                                            <strong>Subject Line Goes Here</strong>

                                        </div>
                                        <p>Hi Adrian,</p>
                                        <p class='text-regular'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, libero corporis ipsam voluptatibus suscipit eos expedita sapiente omnis voluptatum ea! Culpa, vitae eaque quis modi voluptatum quisquam ullam. Modi, tempora!</p>
                                        <p>
                                            Regards,
                                            <br/>
                                            mosaicpro 
                                            <br/>
                                            Director @ mosaicpro.biz
                                            <br/>
                                            www.mosaicpro.biz
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- // Tab content END -->
                        <!-- Tab content -->
                        <div id="tabPassword" class="tab-pane innerAll">
                            <h4 class="innerTB">
                                Change your Password
                            </h4>
                            <form class="form-horizontal innerT " role="form">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Current Password</label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Type here">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control" id="inputPassword3" placeholder="Type here">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">Save Changes <i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- // Tab content END -->
                    </div>
                </div>
            </div>
            <!-- // Tabs END -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app