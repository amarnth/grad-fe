<div class="row row-app">
	<div class="widget show-assignments">
		<div class="widget-body innerAll inner-2x">

			<!-- <a href="index.php?page=assignments" class="btn btn-primary"> <i class="fa fa-fw fa-arrow-left"></i> Back</a> -->
			<!-- <h1>Assignment View </h1> -->
			<div class="innerAll bg-white">
    			<div class="row">

					<div id="assignment_view">
						<?php
					    	$assignment = new Assignments();
					    	if('staff' == $_SESSION['user_role']){
					    		$assignment->getAsStaff($_GET['aid']);	
					    	}else{
					    		$assignment->getAsStudent($_GET['aid']);
					    	}
					    ?>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>



<!-- Modal to view Answers -->
<div class="modal fade" id="open-attachment">
	<div class="modal-dialog">
	  <div class="modal-content" id="preview-data">
	     
	  </div>
	</div>
</div>


<!-- Modal to upload Answers -->
<div class="modal fade" id="open-modal-answer">
	<div class="modal-dialog">
	  <div class="modal-content" id="upload-student-data">
	     
	  </div>
	</div>
</div>




<style>



.ajax-file-upload-error{
  color: red;
}

.ajax-file-upload-statusbar {
margin-top: 10px;
width: 420px;
margin-right: 10px;
margin: 5px;
padding: 5px 5px 5px 5px
}
.ajax-file-upload-filename {
display: inline-block;
height: auto;
margin: 0 5px 5px 10px;
color: #807579
}

.ajax-file-upload-progress {
    display: none !important;
}
.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}
.ajax-file-upload-green {
    display: none !important;;
}

.ajax-file-upload-bar {
background-color: #0ba1b5;
width: 0;
height: 20px;
border-radius: 3px;
color:#FFFFFF;
}
.ajax-file-upload-percent {
position: absolute;
display: inline-block;
top: 3px;
left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
  text-align: center;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 100% !important;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px;
}


 textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}

</style>