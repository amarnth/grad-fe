<!-- row -->
<div class="row row-app">
    <div class="col-sm-12">
        <div class="col-separator col-separator-first bg-none">
            <div class="col-table">
                <!-- Main Heading -->
                <h1>
                    Forum
                </h1>
                <div class="col-separator-h"></div>
                <!-- Search Bar -->
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search this forum...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary rounded-none" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
                <div class="col-separator-h"></div>
                <!-- End Search Bar -->
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="row-app">
                                <div class="col-md-9">
                                    <div class="col-separator">
                                        <!-- Category Heading -->
                                        <div class="heading-buttons bg-gray border-bottom innerR half">
                                            <a href="#" class="btn btn-sm btn-inverse pull-right"><i class="fa fa-plus fa-fw"></i> New Topic</a>
                                            <h4 class="innerTB margin-bottom-none">
                                                General Discussion
                                            </h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- End Category Heading -->
                                        <!-- Category Listing -->
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Getting Started</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">101</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/2.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Requirements</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">212</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/3.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Common Questions</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">323</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/4.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- // END Category Listing -->
                                        <div class="col-separator-h"></div>
                                        <!-- Category Heading -->
                                        <div class="heading-buttons bg-gray border-bottom innerR half">
                                            <a href="#" class="btn btn-sm btn-inverse pull-right"><i class="fa fa-plus fa-fw"></i> New Topic</a>
                                            <h4 class="innerTB margin-bottom-none">
                                                Customization
                                            </h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- End Category Heading -->
                                        <!-- Category Listing -->
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Changing Skin of Coral</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">101</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/2.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Sidebar Type</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">212</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/3.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Common</a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">323</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/4.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row innerAll half border-bottom bg-gray-hover">
                                            <div class="col-sm-6 col-xs-8">
                                                <ul class="media-list margin-none">
                                                    <li class="media">
                                                        <a class="pull-left innerAll" href="index.php?page=support_forum_post">
                                                            <span class="empty-photo"><i class="fa fa-folder fa-2x text-muted"></i></span>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="innerAll">
                                                                <h4 class="margin-none">
                                                                    <a href="index.php?page=support_forum_post" class="media-heading strong text-primary">Including New Elements </a>
                                                                </h4>
                                                                <div class="clearfix"></div>
                                                                <small class="margin-none">Topic information goes here</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="text-center">
                                                    <p class="lead strong margin-bottom-none">434</p>
                                                    <span class="text-muted">POSTS</span>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-hidden">
                                                <div class="innerAll">
                                                    <div class="media">
                                                        <a href="" class="pull-left">
                                                            <img src="../assets//images/people/35/5.jpg" class="media-object"/>
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="" class="text-small">mosaicpro</a>

                                                            <div class="clearfix"></div>
                                                            <small>last replied</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- // END Category Listing -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <!-- col-separator -->
                                    <div class="col-separator col-separator-last">
                                        <!-- Heading -->
                                        <h5 class="innerAll margin-none bg-primary">
                                            <i class="fa fa-fw fa-user"></i> Recent Members
                                        </h5>
                                        <!-- Listing -->
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="media">
                                                <a href="" class="pull-left innerAll half">
<i class="fa fa-user fa-2x text-muted"></i>
</a>
                                                <div class="media-body">
                                                    <a href="" class="text-small strong text-primary">mosaicpro</a>

                                                    <small class="clearfix">on 16 Jan 2014</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="media">
                                                <a href="" class="pull-left innerAll half">
<i class="fa fa-user fa-2x text-muted"></i>
</a>
                                                <div class="media-body">
                                                    <a href="" class="text-small strong text-primary">mosaicpro</a>

                                                    <small class="clearfix">on 16 Jan 2014</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="media">
                                                <a href="" class="pull-left innerAll half">
<i class="fa fa-user fa-2x text-muted"></i>
</a>
                                                <div class="media-body">
                                                    <a href="" class="text-small strong text-primary">mosaicpro</a>

                                                    <small class="clearfix">on 16 Jan 2014</small>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Listing -->
                                        <div class="col-separator-h"></div>
                                        <!-- Heading -->
                                        <h5 class="innerAll margin-none bg-primary">
                                            <i class="fa fa-fw fa-refresh"></i> Latest Posts
                                        </h5>
                                        <!-- Listing -->
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="innerAll half">
                                                <h5 class="innerB half margin-none">
                                                    <a href="#" class="text-inverse">How do you guys change navbar into white?</a>
                                                </h5>
                                                <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                            </div>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="innerAll half">
                                                <h5 class="innerB half margin-none">
                                                    <a href="#" class="text-inverse">How do you guys change navbar into white?</a>
                                                </h5>
                                                <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                            </div>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="innerAll half">
                                                <h5 class="innerB half margin-none">
                                                    <a href="#" class="text-inverse">How do you guys change navbar into white?</a>
                                                </h5>
                                                <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                            </div>
                                        </div>
                                        <div class="bg-gray-hover innerAll border-bottom">
                                            <div class="innerAll half">
                                                <h5 class="innerB half margin-none">
                                                    <a href="#" class="text-inverse">How do you guys change navbar into white?</a>
                                                </h5>
                                                <small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> by Adrian | <i class="fa fa-fw fa-calendar "></i> 17/01/2014</small>
                                            </div>
                                        </div>
                                        <!-- // END Listing -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // END row -->