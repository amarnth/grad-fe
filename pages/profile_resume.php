<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first border-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app">
                        <!-- row-app -->
                        <div class="row row-app margin-none">
                            <!-- col -->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <!-- col-separator -->
                                <div class="col-separator col-unscrollable box reset-components">
                                    <!-- col-table -->
                                    <div class="col-table">
                                        <!-- Profile Photo -->
                                        <div class="border-bottom">
                                            <a href="">
                                                <img src="../assets//images/people/250/22.jpg" class="img-responsive img-clean"/>
                                            </a>
                                        </div>
                                        <div class="innerAll inner-2x text-center">
                                            <p class="lead strong margin-none">Bill Jonson</p>
                                            <p class="lead">Senior UI Designer</p>
                                            <div class="btn-group">
                                                <a href="" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Add to list</a>
                                                <a href="" class="btn btn-primary btn-stroke"><i class="fa fa-envelope"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- col-table-row -->
                                        <div class="col-table-row">
                                            <!-- col-app -->
                                            <div class="col-app col-unscrollable">
                                                <!-- col-app -->
                                                <div class="col-app">
                                                    <div class="innerAll border-bottom bg-gray">
                                                        <h4 class="text-primary">
                                                            <i class=""></i> Skills
                                                        </h4>
                                                        <ul class="list-unstyled">
                                                            <li>Photoshop</li>
                                                            <li>HTML/CSS</li>
                                                            <li>User Experience</li>
                                                        </ul>
                                                    </div>
                                                    <div class="innerAll border-bottom">
                                                        <h4 class="text-primary">
                                                            <i class=""></i> Experience
                                                        </h4>
                                                        <div class="innerAll half box-generic">
                                                            <p class="margin-none strong">Coral UI Design</p>
                                                            <span >User Interface Design</span>

                                                        </div>
                                                        <div class="innerAll half box-generic">
                                                            <p class="margin-none strong">Coral UI Design</p>
                                                            <span >User Interface Design</span>

                                                        </div>
                                                        <div class="innerAll half box-generic margin-none">
                                                            <p class="margin-none strong">Coral UI Design</p>
                                                            <span >User Interface Design</span>

                                                        </div>
                                                    </div>
                                                    <div class="innerAll">
                                                        <h4 class="text-primary">
                                                            <i class=""></i> Education
                                                        </h4>
                                                        <div class="bg-gray innerAll half box-generic">
                                                            <p class="margin-none">Adobe Photoshop Cerficate</p>
                                                            <span class="label label-default ">on 16 Dec 2013</span>

                                                        </div>
                                                        <div class="bg-gray innerAll half box-generic">
                                                            <p class="margin-none">Bachelor Degree in Web Graphics</p>
                                                            <span class="label label-default ">on 16 Dec 2013</span>

                                                        </div>
                                                        <a href="#" class="btn btn-primary btn-xs">View all</a>

                                                    </div>
                                                </div>
                                                <!-- // END col-app -->
                                            </div>
                                            <!-- // END col-app -->
                                        </div>
                                        <!-- // END col-table-row -->
                                    </div>
                                    <!-- // END col-table -->
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                            <!-- col -->
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <!-- col-separator -->
                                <div class="col-separator col-unscrollable col-separator-last">
                                    <!-- col-table -->
                                    <div class="col-table">
                                        <div class="innerAll border-bottom">
                                            <h4 class="pull-left margin-none">
                                                Short Bio
                                            </h4>
                                            <div class="dropdown pull-right dropdown-icons dropdown-icons-xs">
                                                <a data-toggle="dropdown" href="#" class="btn btn-default btn-stroke btn-circle dropdown-toggle"><i class="fa fa-download"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li data-toggle="tooltip" data-title="Resume" data-placement="left" data-container="body">
                                                        <a href="#"><i class="fa fa-file"></i></a>
                                                    </li>
                                                    <li data-toggle="tooltip" data-title="vCard" data-placement="left" data-container="body">
                                                        <a href="#"><i class="fa fa-link"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="innerAll inner-2x">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, voluptatibus laboriosam debitis quod consectetur ratione accusamus quibusdam alias magni in cum distinctio dolorum quam quo repellendus possimus dignissimos! Enim, reprehenderit rem explicabo illum saepe similique architecto adipisci eius consequuntur libero?</p>
                                            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-download fa-fw"></i> Resume</a>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- col-table-row -->
                                        <div class="col-table-row">
                                            <!-- box -->
                                            <div class="col-app box col-unscrollable overflow-hidden">
                                                <!-- col-table -->
                                                <div class="col-table">
                                                    <div class="innerLR heading-buttons border-bottom">
                                                        <h4 class="innerTB margin-none pull-left">
                                                            Recommendations
                                                        </h4>
                                                        <button class="btn btn-default btn-xs pull-right"><i class="fa fa-cog "></i></button>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="innerAll">
                                                        <ul class="list-unstyled resume-documents">
                                                            <li>
                                                                <a href=""><i class="fa fa-file-o"></i> mosaicpro <span>17/12/2013</span></a>
                                                            </li>
                                                            <li>
                                                                <a href=""><i class="fa fa-file-o"></i> Company LTD <span>17/12/2013</span></a>
                                                            </li>
                                                            <li>
                                                                <a href=""><i class="fa fa-file-o"></i> Adrian <span>17/12/2013</span></a>
                                                            </li>
                                                            <li>
                                                                <a href=""><i class="fa fa-file-o"></i> Bogdan <span>17/12/2013</span></a>
                                                            </li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                        <div class="innerLR innerB">
                                                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil fa-fw"></i> Write New </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-separator-h"></div>
                                                    <div class="innerLR heading-buttons border-bottom">
                                                        <h4 class="innerTB margin-none pull-left">
                                                            Work History
                                                        </h4>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <!-- col-table-row -->
                                                    <div class="col-table-row">
                                                        <!-- col-app -->
                                                        <div class="col-app col-unscrollable">
                                                            <!-- col-app -->
                                                            <div class="col-app">
                                                                <ul class="timeline-activity list-unstyled">
                                                                    <li class="active">
                                                                        <i class="list-icon fa fa-share"></i>

                                                                        <div class="block block-inline">
                                                                            <div class="caret"></div>
                                                                            <div class="box-generic">
                                                                                <div class="timeline-top-info content-filled border-bottom">
                                                                                    <i class="fa fa-user"></i> <a href="">Bill</a> got a review for <a href="" class="text-primary">Coral UI Interface Design</a> from 
                                                                                    <a href="#">
                                                                                        <img src="../assets/images/people/80/8.jpg" alt="photo" width="20">
                                                                                    </a>
                                                                                    <a href="">Andrew M.</a>

                                                                                    <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                </div>
                                                                                <div class="media innerAll margin-none">
                                                                                    <a class="pull-left" href="#">
                                                                                        <img src="../assets/images/people/80/8.jpg" alt="photo" class="media-object" width="35">
                                                                                    </a>
                                                                                    <div class="media-body">
                                                                                        <a href="" class="strong">Andrew</a> Good Job. Congrats and hope to see more admin templates like this in the future. 
                                                                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <i class="list-icon fa fa-share"></i>

                                                                        <div class="block block-inline">
                                                                            <div class="caret"></div>
                                                                            <div class="box-generic">
                                                                                <div class="timeline-top-info content-filled border-bottom">
                                                                                    <i class="fa fa-user"></i> <a href="">Bill</a> got a review for <a href="" class="text-primary">Support &amp; Ticket System</a> from 
                                                                                    <a href="#">
                                                                                        <img src="../assets/images/people/80/20.jpg" alt="photo" width="20">
                                                                                    </a>
                                                                                    <a href="">Andrew M.</a>

                                                                                    <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                </div>
                                                                                <div class="media innerAll margin-none">
                                                                                    <a class="pull-left" href="#">
                                                                                        <img src="../assets/images/people/80/20.jpg" alt="photo" class="media-object" width="35">
                                                                                    </a>
                                                                                    <div class="media-body">
                                                                                        <a href="" class="strong">Bogdan</a> Good Job. Congrats and hope to see more admin templates like this in the future. 
                                                                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <i class="list-icon fa fa-share"></i>

                                                                        <div class="block block-inline">
                                                                            <div class="caret"></div>
                                                                            <div class="box-generic">
                                                                                <div class="timeline-top-info content-filled border-bottom">
                                                                                    <i class="fa fa-user"></i> <a href="">Bill</a> got a review for <a href="" class="text-primary">Project Management</a> from 
                                                                                    <a href="#">
                                                                                        <img src="../assets/images/people/80/12.jpg" alt="photo" width="20">
                                                                                    </a>
                                                                                    <a href="">Andrew M.</a>

                                                                                    <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                </div>
                                                                                <div class="media innerAll margin-none">
                                                                                    <a class="pull-left" href="#">
                                                                                        <img src="../assets/images/people/80/12.jpg" alt="photo" class="media-object" width="35">
                                                                                    </a>
                                                                                    <div class="media-body">
                                                                                        <a href="" class="strong">John </a> Good Job. Congrats and hope to see more admin templates like this in the future. 
                                                                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <i class="list-icon fa fa-share"></i>

                                                                        <div class="block block-inline">
                                                                            <div class="caret"></div>
                                                                            <div class="box-generic">
                                                                                <div class="timeline-top-info content-filled border-bottom">
                                                                                    <i class="fa fa-user"></i> <a href="">Bill</a> got a review for <a href="" class="text-primary">Coral UI Interface Design</a> from 
                                                                                    <a href="#">
                                                                                        <img src="../assets/images/people/80/10.jpg" alt="photo" width="20">
                                                                                    </a>
                                                                                    <a href="">Andrew M.</a>

                                                                                    <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                </div>
                                                                                <div class="media innerAll margin-none">
                                                                                    <a class="pull-left" href="#">
                                                                                        <img src="../assets/images/people/80/10.jpg" alt="photo" class="media-object" width="35">
                                                                                    </a>
                                                                                    <div class="media-body">
                                                                                        <a href="" class="strong">Andrew</a> Good Job. Congrats and hope to see more admin templates like this in the future. 
                                                                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 2 days ago </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <!-- // END col-app -->
                                                        </div>
                                                        <!-- // END col-app -->
                                                    </div>
                                                    <!-- // END col-table-row -->
                                                </div>
                                                <!-- // END col-table -->
                                            </div>
                                            <!-- // END col-app.box -->
                                        </div>
                                        <!-- // END col-table-row -->
                                    </div>
                                    <!-- // END col-table -->
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                        </div>
                        <!-- // END row -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->