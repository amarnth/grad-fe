<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-6 col-md-offset-3">
        <!-- col-separator -->
        <div class="col-separator box col-separator-first col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll border-bottom margin-none center">
                    Survey
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="border-bottom innerAll">
                                <div class="media margin-none innerAll">
                                    <div class="pull-left">
                                        <img data-src="holder.js/50x50/dark" alt="" class="img-responsive img-circle" />
                                    </div>
                                    <div class="media-body">
                                        <div class="pull-right innerAll half">
                                            <div class="btn-group-vertical btn-group-sm">
                                                <button class="btn btn-inverse">About</button>
                                                <button class="btn btn-default">Contact</button>
                                            </div>
                                        </div>
                                        <div class="innerAll half media margin-none">
                                            <div class="pull-left innerLR half">
                                                <p class="margin-none">Survey by</p>
                                                <p class="strong">Company, Inc</p>
                                            </div>
                                            <div class="media-body border-left innerLR"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, dicta, odio id cumque voluptates debitis asperiores. Dolorum, quod, cupiditate. </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-separator-h"></div>
                            <div class="innerAll text-center">
                                <ul class="nav nav-pills strong display-block-inline">
                                    <li class="active"><a href="#feedback" data-toggle="tab">1. Feedback</a></li>
                                    <li><a href="#website" data-toggle="tab">2. Website</a></li>
                                    <li><a href="#thankyou" data-toggle="tab">3. Thank you</a></li>
                                </ul>
                            </div>
                            <div class="col-separator-h"></div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="feedback">
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4 class="innerTB">
                                            1. What do you think?
                                        </h4>
                                        <textarea class="form-control" placeholder="Type in here"></textarea>
                                    </div>
                                    <div class="innerAll inner-2x border-bottom">
                                        <h4 class="innerTB">
                                            2. Have you seen our website?
                                        </h4>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    Option one is this and that&mdash;be sure to include why it's great 
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                    Option one is this and that&mdash;be sure to include why it's great 
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                                    Option two can be something else and selecting it will deselect option one 
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerAll inner-2x">
                                        <h4 class="innerTB">
                                            3. Multiple Columns?
                                        </h4>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h5>
                                                    Do you like A?
                                                </h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <h5>
                                                    Do you like B?
                                                </h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Maybe 
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane innerAll inner-2x" id="website">
                                    <h4 class="innerTB">
                                        More Options
                                    </h4>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, reiciendis maiores earum cupiditate aspernatur ducimus dolorem temporibus corporis! Modi, maiores, distinctio doloremque eos ipsam libero suscipit sequi dignissimos alias eaque. 
                                </div>
                                <div class="tab-pane innerAll inner-2x" id="thankyou">
                                    <h4 class="innerTB">
                                        Thank you message
                                    </h4>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, sequi, quia explicabo sapiente impedit nesciunt quis quisquam laborum earum magni. Qui, iusto, modi hic repellat provident magnam architecto vero ipsam. 
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
                <div class="innerAll border-top text-center"> <a href="" class="btn btn-primary">Continue</a> </div>
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->