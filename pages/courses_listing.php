<!-- row -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <!-- heading -->
                <div class="innerTB bg-container">
                    <h3 class="pull-left margin-none innerR">
                        Start Learning &nbsp;<i class="fa fa-fw text-primary icon-top-hat"></i>
                    </h3>
                    <a href="" class="btn btn-sm btn-primary pull-left">Join Today</a>
<a href="" class="btn btn-sm btn-info pull-right">Ask a question</a>

                    <div class="clearfix"></div>
                </div>
                <div class="col-separator-h"></div>
                <!-- // END heading -->
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app.col-unscrollable -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <!-- row -->
                            <div class="row row-app">
                                <!-- col -->
                                <div class="col-md-9">
                                    <!-- col-separator -->
                                    <div class="col-separator">
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-success innerAll text-center" href="#"><i class="fa fa-fw icon-desktop-play fa-5x"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                        <div class="col-separator-h"></div>
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-primary innerAll text-center" href="#"><i class="icon-scale-2 fa-5x fa fa-fw"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                        <div class="col-separator-h"></div>
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-purple innerAll text-center" href="#"><i class="icon-race-flag-fill fa-5x fa fa-fw"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT ">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                        <div class="col-separator-h"></div>
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-info innerAll text-center" href="#"><i class="icon-store-front fa-5x fa fa-fw"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                        <div class="col-separator-h"></div>
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-inverse innerAll text-center" href="#"><i class="icon-desktop-play fa-5x fa fa-fw"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                        <div class="col-separator-h"></div>
                                        <!-- course -->
                                        <div class="media margin-none">
                                            <a class="pull-left bg-mustard innerAll text-center" href="#"><i class="icon-ambulance fa-5x fa fa-fw"></i></a>
                                            <div class="media-body innerAll">
                                                <h4 class="media-heading innerT">
                                                    <a href="" class="text-inverse">Computer 1 on 1</a> 
                                                    <span class="text-small"><i class="fa fa-fw fa-clock-o"></i> 5:15</span>
                                                </h4>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, sed. 
                                            </div>
                                        </div>
                                        <!-- // END course -->
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <!-- col-separator -->
                                    <div class="col-separator col-separator-last">
                                        <h5 class="innerAll bg-primary margin-bottom-none">
                                            Choose Category:
                                        </h5>
                                        <ul class="list-group list-group-1 margin-none borders-none">
                                            <li class="list-group-item animated fadeInUp">
                                                <a href="index.php?page=courses_listing"><span class="badge pull-right badge-primary hidden-md">30</span><i class="fa fa-exclamation-circle"></i> HTML 5</a>
                                            </li>
                                            <li class="list-group-item animated fadeInUp">
                                                <a href="index.php?page=courses_listing"><span class="badge pull-right badge-primary hidden-md">2</span><i class="fa fa-ticket"></i> CSS/LESS</a>
                                            </li>
                                            <li class="list-group-item animated fadeInUp">
                                                <a href="index.php?page=courses_listing"><i class="fa fa-spinner"></i> Adobe Photoshop</a>
                                            </li>
                                            <li class="list-group-item border-bottom-none animated fadeInUp">
                                                <a href="index.php?page=courses_listing"><i class="fa fa-pencil"></i> PHP</a>
                                            </li>
                                            <li class="list-group-item border-bottom-none animated fadeInUp">
                                                <a href="index.php?page=courses_listing"><i class="icon-folder-fill"></i> MYSQL</a>
                                            </li>
                                        </ul>
                                        <div class="col-separator-h"></div>
                                        <h5 class="innerAll margin-none border-bottom text-primary">
                                            Popular Videos
                                        </h5>
                                        <a href="#" class="display-block media margin-none innerAll border-bottom">
                                            <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                            <span class="display-block media-body innerB">
                                                <h5 class="innerT half">
                                                    Video Tutorial
                                                </h5>
                                                <div class="clearfix"></div>
                                                <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                            </span>
                                        </a>
                                        <a href="#" class="display-block media margin-none innerAll border-bottom">
                                            <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                            <span class="display-block media-body innerB">
                                                <h5 class="innerT half">
                                                    Video Tutorial
                                                </h5>
                                                <div class="clearfix"></div>
                                                <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                            </span>
                                        </a>
                                        <a href="#" class="display-block media margin-none innerAll border-bottom">
                                            <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                            <span class="display-block media-body innerB">
                                                <h5 class="innerT half">
                                                    Video Tutorial
                                                </h5>
                                                <div class="clearfix"></div>
                                                <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                            </span>
                                        </a>
                                        <a href="#" class="display-block media margin-none innerAll border-bottom">
                                            <img class="pull-left" data-src="holder.js/50x50" alt="..."/>
                                            <span class="display-block media-body innerB">
                                                <h5 class="innerT half">
                                                    Video Tutorial
                                                </h5>
                                                <div class="clearfix"></div>
                                                <small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 5:15</small>
                                            </span>
                                        </a>
                                    </div>
                                    <!-- // END col-separator -->
                                </div>
                                <!-- // END col -->
                            </div>
                            <!-- // END row -->
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->