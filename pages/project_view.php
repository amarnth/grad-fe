<div class="row row-app" id="view-project">

    <?php

        $project = new ViewProject();
        $project->view($_GET['pid']);
    ?>
</div>

<style>
    .widget {
        margin: 0 auto;
    }

    .widget .widget-tabs .collapse-toggle{
        display: none !important;
    }







.ajax-file-upload-statusbar {
    margin-top: 10px;
    width: 420px;
    margin-right: 10px;
    margin: 5px;
    padding: 5px 5px 5px 5px;
}

.ajax-file-upload-filename {
    display: inline-block;
    height: auto;
    margin: 0 5px 5px 10px;
    color: #807579
}

.ajax-file-upload-progress {
    //display: none !important;
}


.ajax-file-upload-red {
    cursor: pointer;
    display: inline-block;
}


.ajax-file-upload-green {
    //display: none !important;;
}

.ajax-file-upload-bar {
    background-color: #0ba1b5;
    width: 0;
    height: 20px;
    border-radius: 3px;
    color:#FFFFFF;
}


.ajax-file-upload-percent {
    position: absolute;
    display: inline-block;
    top: 3px;
    left: 48%
}

.ajax-file-upload {
  /*background: #eb6a5a;*/
  background: #4193d0;
  box-shadow: 1px 1px #fff;
  color: #FFF;
  vertical-align: middle;
  width: 14%;
  padding: 6px 13px;
  border-radius: 3px;
  cursor: pointer;
}
  

.ajax-upload-dragdrop
{
    border: none;
    width: 15%;
    display: inline-block;
    padding: 0;
    vertical-align:middle;
    padding:10px 10px 0px 10px;
}


textarea{
      min-height: 63px;
      height: 63px;
}

.ajax-upload-dragdrop span{
    display: none;
} 

#mulitplefileuploadStatus{
    padding-left: 20px;
}
</style>