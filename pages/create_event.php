<div class="row row-app">
  <form class="margin-none innerLR inner-2x">
      <div class="form-group">
          <!-- <label>Group Name</label> -->
          <input type="text" class="form-control group-name" placeholder="Group Name">
      </div>

      <div class="form-group">

        <div class="col-md-6">
            <label>Start Date</label>
            <div class="input-group date" id="event-start-date">
                <input class="form-control" type="text" value="14 February 2013" />
                <span class="input-group-addon"><i class="fa fa-th"></i></span>
            </div>
        </div>

        <div class="col-md-6">
            <label>Start Time</label>
            <div class="input-group bootstrap-timepicker">
                <input id="event-start-time" type="text" class="form-control">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
            </div>
        </div>

      </div>


      <div class="form-group">

        <div class="col-md-6">
            <label>End Date</label>
            <div class="input-group date" id="event-end-date">
                <input class="form-control" type="text" value="14 February 2013" />
                <span class="input-group-addon"><i class="fa fa-th"></i></span>
            </div>
        </div>

        <div class="col-md-6">
            <label>End Time</label>
            <div class="input-group bootstrap-timepicker">
                <input id="event-end-time" type="text" class="form-control">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
            </div>
        </div>
        
      </div>

    

        <div class="col-md-6">
            <label>Event Type</label>
            <select class="selectpicker col-md-6" data-style="btn-primary">
                <option>Indoor Event</option>
                <option>Outdoor Event</option>
            </select> 
        </div>

        <div class="col-md-6">
            <label>Invite To</label>
            <select class="selectpicker col-md-6" data-style="btn-primary">
                <option>Public</option>
                <option>Friends</option>
                <option>Staff</option>
            </select> 
        </div>
     


      <div class="text-center innerAll">
          <a href="" class="btn btn-primary create_a_group" data-dismiss="modal" aria-hidden="true">Create</a>
      </div>

  </form>
</div>

 