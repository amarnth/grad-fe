<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <!-- col-separator.box -->
    <div class="col-separator col-unscrollable box">
        <!-- col-table -->
        <div class="col-table">
            <h4 class="innerAll margin-none border-bottom text-center">
                <i class="fa fa-lock"></i> Login to your Account
            </h4>
            <!-- col-table-row -->
            <div class="col-table-row">
                <!-- col-app -->
                <div class="col-app col-unscrollable">
                    <!-- col-app -->
                    <div class="col-app">
                        <div class="login">
                            <div class="placeholder text-center">
				<img src="assets/grad_assets/img/signup_logo.png" alt="" style="border: none;">
                            </div>
                            
                            <div class="panel panel-default col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4">
                                <div class="panel-body">
                                    <?php

                                    // if(!isset($_COOKIE["grad_uname"])) {
                                    //     echo "Cookie named '" . $_COOKIE['grad_uname'] . "' is not set!";
                                    // } else {
                                    //     var_dump($_COOKIE);
                                    //     echo "Cookie '" . $_COOKIE["grad_uname"] . "' is set!<br>";
                                    // }

                                    if(!isset($_COOKIE["grad_uname"])){

                                    ?>
                                    <form role="form" action="index.php?page=index">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <!-- <input type="email" class="form-control email" placeholder="Enter email"> -->
                                            <input type="text" class="form-control email" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control password" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block" id="login">Login</button>

                                        <div class="checkbox">
                                            <label class="checkbox-custom">
                                                <i class="fa fa-fw fa-square-o remember_me"></i>
                                                <input type="checkbox" class="checkbox"> Remember my details
                                            </label>
                                        </div>

                                    </form>

                                    <?php }else{ ?>

                                     <form role="form" action="index.php?page=index">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control email" placeholder="Enter email" value='<?php echo $_COOKIE["grad_uname"]; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control password" placeholder="Password" value='<?php echo $_COOKIE["grad_pwd"]; ?>'>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block" id="login">Login</button>

                                        <div class="checkbox">
                                            <label class="checkbox-custom">
                                                <i class="fa fa-fw fa-square-o remember_me checked"></i>
                                                <input type="checkbox" class="checkbox" checked="checked"> Remember my details
                                            </label>
                                        </div>

                                    </form>

                                    <?php } ?>
                                </div>
                            </div>
                            
                         
                            
                            <div class="col-sm-4 col-sm-offset-4 text-center">
                                <div class="innerAll">
                                    <a href="index.php?page=signup_student" class="btn btn-info">Create a new account? <i class="fa fa-pencil"></i> </a>
                                    <div class="separator"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-app.col-unscrollable -->
            </div>
            <!-- // END col-table-row -->
        </div>
        <!-- // END col-table -->
    </div>
    <!-- // END col-separator.box -->
</div>
<!-- // END row-app -->



<style>
    #menu, #menu_kis, #menu-top, .navbar.hidden-print.navbar-primary.main{
        display: none;
    }

    .sidebar.sidebar-fusion #content {
      margin-left: 12px;
    }

    #menu_kis{
        display: none !important;
    }

    .sidebar.sidebar-kis #content {
      margin-right: 0px !important;
    }

</style>