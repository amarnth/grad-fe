<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Notifications
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <!-- Top Notyfy Options & Styles Live Demo -->
                                <div class="widget widget-heading-simple widget-body-gray">
                                    <!-- Widget heading -->
                                    <div class="widget-head">
                                        <h4 class="heading">
                                            Top notifications
                                        </h4>
                                    </div>
                                    <!-- // Widget heading END -->
                                    <div class="widget-body center"> <span class="btn btn-default" data-layout="top" data-type="alert" data-toggle="notyfy">Alert</span>
<span class="btn btn-success" data-layout="top" data-type="success" data-toggle="notyfy">Success</span>
<span class="btn btn-danger" data-layout="top" data-type="error" data-toggle="notyfy">Danger</span>
<span class="btn btn-warning" data-layout="top" data-type="warning" data-toggle="notyfy">Warning</span>
<span class="btn btn-info" data-layout="top" data-type="information" data-toggle="notyfy">Information</span>
<span class="btn btn-inverse" data-layout="top" data-type="confirm" data-toggle="notyfy">Confirm</span> </div>
                                </div>
                                <!-- // Top Notyfy Options & Styles Live Demo END -->
                                <div class="row">
                                    <!-- Left Notyfy Options Live Demo -->
                                    <div class="col-md-4">
                                        <div class="widget widget-heading-simple widget-body-gray">
                                            <!-- Widget heading -->
                                            <div class="widget-head">
                                                <h4 class="heading">
                                                    Left
                                                </h4>
                                            </div>
                                            <!-- // Widget heading END -->
                                            <div class="widget-body center"> <span class="btn btn-default" data-layout="topLeft" data-type="primary" data-toggle="notyfy">Top</span>
<span class="btn btn-default" data-layout="centerLeft" data-type="primary" data-toggle="notyfy">Center</span>
<span class="btn btn-default" data-layout="bottomLeft" data-type="primary" data-toggle="notyfy">Bottom</span> </div>
                                        </div>
                                    </div>
                                    <!-- // Left Notyfy Options Live Demo END -->
                                    <!-- Center Notyfy Options Live Demo -->
                                    <div class="col-md-4">
                                        <div class="widget widget-heading-simple widget-body-gray">
                                            <!-- Widget heading -->
                                            <div class="widget-head">
                                                <h4 class="heading">
                                                    Center
                                                </h4>
                                            </div>
                                            <!-- // Widget heading END -->
                                            <div class="widget-body center"> <span class="btn btn-default btn-block" data-layout="center" data-type="primary" data-toggle="notyfy">Center notification</span> </div>
                                        </div>
                                    </div>
                                    <!-- // Center Notyfy Options Live Demo END -->
                                    <!-- Right Notyfy Options Live Demo -->
                                    <div class="col-md-4">
                                        <div class="widget widget-heading-simple widget-body-gray">
                                            <!-- Widget heading -->
                                            <div class="widget-head">
                                                <h4 class="heading">
                                                    Right
                                                </h4>
                                            </div>
                                            <!-- // Widget heading END -->
                                            <div class="widget-body center"> <span class="btn btn-default" data-layout="topRight" data-type="primary" data-toggle="notyfy">Top</span>
<span class="btn btn-default" data-layout="centerRight" data-type="primary" data-toggle="notyfy">Center</span>
<span class="btn btn-default" data-layout="bottomRight" data-type="primary" data-toggle="notyfy">Bottom</span> </div>
                                        </div>
                                    </div>
                                    <!-- // Right Notyfy Options Live Demo END -->
                                </div>
                                <p class="separator text-center"><i class="icon-ellipsis-horizontal icon-3x"></i></p>
                                <h3>
                                    Gritter <span>Notifications plugin</span>
                                </h3>
                                <div class="row">
                                    <!-- Color Options Column -->
                                    <div class="col-md-4">
                                        <div class="widget widget-heading-simple widget-body-gray">
                                            <!-- Widget heading -->
                                            <div class="widget-head">
                                                <h4 class="heading">
                                                    Color options
                                                </h4>
                                            </div>
                                            <!-- // Widget heading END -->
                                            <div class="widget-body center"> <span class="gritter-add-white btn btn-default btn-block">Light</span>
<span class="gritter-add-regular btn btn-inverse btn-block">Dark</span>
<span class="gritter-add-primary btn btn-primary btn-block">Primary</span> </div>
                                        </div>
                                    </div>
                                    <!-- // Color Options Column END -->
                                    <!-- Other Examples Column -->
                                    <div class="col-md-8">
                                        <div class="widget widget-heading-simple widget-body-gray">
                                            <!-- Widget heading -->
                                            <div class="widget-head">
                                                <h4 class="heading">
                                                    Other examples
                                                </h4>
                                            </div>
                                            <!-- // Widget heading END -->
                                            <div class="widget-body center">
                                                <span class="gritter-add-sticky btn btn-default btn-block btn-icon-stacked glyphicons right_arrow"><i></i><strong>Add sticky notification</strong><span>Has to be manually removed</span></span>
                                                <span class="gritter-add-without-image btn btn-default btn-block btn-icon glyphicons right_arrow"><i></i>Add notification without image</span>
                                                <span class="gritter-add-white btn btn-default btn-block btn-icon-stacked glyphicons right_arrow"><i></i><strong>Add white notification</strong><span>Has a "gritter-light" class applied</span></span>
                                                <span class="gritter-add-max btn btn-default btn-block btn-icon-stacked glyphicons right_arrow"><i></i><strong>Add notification with max. of 3</strong><span>Only if there are &lt;= 3 messages already on screen</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- // Other Examples Column END -->
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->