<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first col-unscrollable">
            <div class="col-table">
                <div class="heading-buttons innerLR box">
                    <h4 class="margin-none innerTB pull-left">
                        Bookings
                    </h4>
                    <a href="" class="btn-xs pull-right btn btn-primary"><i class="fa fa-fw fa-plus"></i> Add record</a>
                    <div class="clearfix"></div>
                </div>
                <div class="col-separator-h"></div>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="col-table">
                                <div class="innerAll bg-gray">
                                    <!-- Filters -->
                                    <div class="filter-bar">
                                        <form class="margin-none form-inline">
                                            <!-- From -->
                                            <div class="form-group col-md-2 padding-none">
                                                <label>From:</label>
                                                <div class="input-group">
                                                    <input type="text" name="from" id="dateRangeFrom" class="form-control" value="08/05/13" />
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <!-- // From END -->
                                            <!-- To -->
                                            <div class="form-group col-md-2 padding-none">
                                                <label>To:</label>
                                                <div class="input-group">
                                                    <input type="text" name="to" id="dateRangeTo" class="form-control" value="08/18/13" />
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <!-- // To END -->
                                            <!-- Min -->
                                            <div class="form-group col-md-2 padding-none">
                                                <label>Min:</label>
                                                <div class="input-group">
                                                    <input type="text" name="from" class="form-control" value="100" />
                                                    <span class="input-group-addon"><i class="fa fa-euro"></i></span>
                                                </div>
                                            </div>
                                            <!-- // Min END -->
                                            <!-- Max -->
                                            <div class="form-group col-md-2 padding-none">
                                                <label>Max:</label>
                                                <div class="input-group">
                                                    <input type="text" name="from" class="form-control" value="500" />
                                                    <span class="input-group-addon"><i class="fa fa-euro"></i></span>
                                                </div>
                                            </div>
                                            <!-- // Max END -->
                                            <!-- Select -->
                                            <div class="form-group col-md-3 padding-none">
                                                <label class="label-control">Select:</label>
                                                <div class="col-md-8 padding-none">
                                                    <select name="from" class="form-control">
                                                        <option>Some option</option>
                                                        <option>Other option</option>
                                                        <option>Some other option</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- // Select END -->
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                    <!-- // Filters END -->
                                </div>
                                <div class="col-separator-h box"></div>
                                <div class="col-table-row">
                                    <div class="row-app">
                                        <div class="col-md-8">
                                            <div class="col-separator box bg-gray">
                                                <h5 class="innerAll margin-none bg-white strong border-bottom">
                                                    <i class="fa fa-fw fa-calendar"></i> Monday, 10 June 2013
                                                </h5>
                                                <div class="innerAll bg-white">
                                                    <!-- Total bookings & sort by options -->
                                                    <div class="separator bottom">
                                                        Total bookings: 26 
                                                        <span class="pull-right">
                                                            <label class="strong">Sort by:</label>
                                                            <select class="selectpicker margin-none" data-style="btn-default btn-xs">
                                                                <option>Option</option>
                                                                <option>Option</option>
                                                                <option>Option</option>
                                                            </select>
                                                        </span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <!-- // Total bookings & sort by options END -->
                                                    <!-- Table -->
                                                    <table class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 1%;" class="uniformjs">
                                                                    <input type="checkbox" />
                                                                </th>
                                                                <th class="center">No.</th>
                                                                <th class="center">Time</th>
                                                                <th>Client</th>
                                                                <th class="center">Phone</th>
                                                                <th class="center">Amount</th>
                                                                <th class="center">Heard from</th>
                                                                <th class="center" style="width: 150px;">Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- Item -->
                                                            <tr class="selectable">
                                                                <td class="center uniformjs">
                                                                    <input type="checkbox" />
                                                                </td>
                                                                <td class="center">1</td>
                                                                <td class="center">11:00</td>
                                                                <td>
                                                                    <strong>Lorem Ipsum</strong>
                                                                    <br/>
                                                                    <small>Location: Hollywood</small>
                                                                </td>
                                                                <td class="center">0740000000</td>
                                                                <td class="center">&euro;44</td>
                                                                <td class="center">Flyer</td>
                                                                <td class="center">
                                                                    <div class="btn-group btn-group-sm">
                                                                        <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                                        <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- // Item END -->
                                                            <!-- Item -->
                                                            <tr class="selectable selected">
                                                                <td class="center uniformjs">
                                                                    <input type="checkbox" checked="checked" />
                                                                </td>
                                                                <td class="center">2</td>
                                                                <td class="center">12:00</td>
                                                                <td>
                                                                    <strong>Lorem Ipsum</strong>
                                                                    <br/>
                                                                    <small>Location: New York</small>
                                                                </td>
                                                                <td class="center">0740000000</td>
                                                                <td class="center">&euro;37</td>
                                                                <td class="center">Friend</td>
                                                                <td class="center">
                                                                    <div class="btn-group btn-group-sm">
                                                                        <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                                        <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- // Item END -->
                                                            <!-- Item -->
                                                            <tr class="selectable">
                                                                <td class="center uniformjs">
                                                                    <input type="checkbox" />
                                                                </td>
                                                                <td class="center">3</td>
                                                                <td class="center">13:00</td>
                                                                <td>
                                                                    <strong>Lorem Ipsum</strong>
                                                                    <br/>
                                                                    <small>Location: Miami</small>
                                                                </td>
                                                                <td class="center">0740000000</td>
                                                                <td class="center">&euro;23</td>
                                                                <td class="center">Google Search</td>
                                                                <td class="center">
                                                                    <div class="btn-group btn-group-sm">
                                                                        <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                                        <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- // Item END -->
                                                            <!-- Item -->
                                                            <tr class="selectable">
                                                                <td class="center uniformjs">
                                                                    <input type="checkbox" />
                                                                </td>
                                                                <td class="center">4</td>
                                                                <td class="center">14:00</td>
                                                                <td>
                                                                    <strong>Lorem Ipsum</strong>
                                                                    <br/>
                                                                    <small>Location: New York</small>
                                                                </td>
                                                                <td class="center">0740000000</td>
                                                                <td class="center">&euro;24</td>
                                                                <td class="center">Friend</td>
                                                                <td class="center">
                                                                    <div class="btn-group btn-group-sm">
                                                                        <a href="#" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                                        <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- // Item END -->
                                                        </tbody>
                                                    </table>
                                                    <!-- // Table END -->
                                                    <!-- With selected actions -->
                                                    <div class="pull-left checkboxs_actions hide-2">
                                                        <label class="strong">
                                                            With selected: 
                                                            <select class="selectpicker margin-none" data-style="btn-default btn-small">
                                                                <option>Action</option>
                                                                <option>Action</option>
                                                                <option>Action</option>
                                                            </select>
                                                        </label>
                                                    </div>
                                                    <!-- // With selected actions END -->
                                                    <!-- Pagination -->
                                                    <ul class="pagination pull-right margin-none">
                                                        <li class="disabled"><a href="#">&laquo;</a></li>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                        <li><a href="#">&raquo;</a></li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                    <!-- // Pagination END -->
                                                </div>
                                                <div class="col-separator-h box"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-separator col-separator-last box">
                                                <!-- Widget -->
                                                <div class="widget widget-3">
                                                    <!-- Widget heading -->
                                                    <div class="widget-head">
                                                        <h4 class="heading text-faded">
                                                            Total amount
                                                        </h4>
                                                    </div>
                                                    <!-- // Widget heading END -->
                                                    <div class="widget-body large"> &euro;15,368.50 </div>
                                                    <!-- Widget footer -->
                                                    <div class="widget-footer align-right">
                                                        <a href="#" class="glyphicons print"><i></i> Print</a>
                                                        <a href="#" class="glyphicons list"><i></i> View</a>
                                                    </div>
                                                    <!-- // Widget footer END -->
                                                </div>
                                                <!-- // Widget END -->
                                                <div class="col-separator-h box"></div>
                                                <!-- Widget -->
                                                <div class="widget widget-3">
                                                    <!-- Widget heading -->
                                                    <div class="widget-head">
                                                        <h4 class="heading text-faded">
                                                            New clients
                                                        </h4>
                                                    </div>
                                                    <!-- // Widget heading END -->
                                                    <div class="widget-body large"> 21 </div>
                                                    <!-- Widget footer -->
                                                    <div class="widget-footer">
                                                        <a href="#" class="glyphicons print"><i></i> Print</a>
                                                        <a href="#" class="glyphicons list"><i></i> View</a>
                                                    </div>
                                                    <!-- // Widget footer END -->
                                                </div>
                                                <!-- // Widget END -->
                                                <div class="col-separator-h box"></div>
                                                <!-- Widget -->
                                                <div class="widget widget-3">
                                                    <!-- Widget heading -->
                                                    <div class="widget-head">
                                                        <h4 class="heading text-faded">
                                                            Cancellations
                                                        </h4>
                                                    </div>
                                                    <!-- // Widget heading END -->
                                                    <div class="widget-body large cancellations">
                                                        <div class="inline-block text-xlarge">4</div>
                                                        <span> <span>Lost</span> <span>&euro;89.00</span> </span>
                                                    </div>
                                                    <!-- Widget footer -->
                                                    <div class="widget-footer align-center">
                                                        <a href="#" class="glyphicons print"><i></i> Print</a>
                                                        <a href="#" class="glyphicons list"><i></i> View</a>
                                                    </div>
                                                    <!-- // Widget footer END -->
                                                </div>
                                                <!-- // Widget END -->
                                                <div class="col-separator-h box"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>