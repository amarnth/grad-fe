<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <div class="col-table">
                <div class="innerTB">
                    <h3 class="margin-none pull-left">
                        Dashboard Overview &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i>
                    </h3>
                    <div class="btn-group pull-right">
                        <a href="index.php?page=dashboard" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Overview</a>
                        <a href="index.php?page=stats_class" class="btn btn-default"><i class="fa fa-fw fa-dashboard"></i> Class</a>
                        <a href="index.php?page=stats_student" class="btn btn-primary"><i class="fa fa-fw fa-user"></i> Students</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-separator-h"></div>


                <div class="row">

                    <div class="input-group ">
                        <input class="form-control col-md-6 innerAll student-search" id="appendedInputButtons" type="text" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>

                    <?php
                        $stats = new Stats();
                        $stats = $stats->getStudent();
                    ?>
                </div>

                <style>
                    .progress.progress-mini{
                        width: 65%;
                    }

                    .progress.progress-mini .add{
                        text-transform: uppercase;
                    }
                </style>



                <div class="col-separator-h"></div>
            </div>
        </div>
    </div>
</div>