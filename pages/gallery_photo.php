<div class="row row-app">
    <div class="col-md-12">
        <div class="col-separator box col-separator-first col-unscrollable">
            <div class="col-table">
                <h4 class="innerAll border-bottom margin-none">
                    Gallery
                </h4>
                <div class="col-table-row">
                    <div class="col-app col-unscrollable">
                        <div class="col-app">
                            <div class="innerAll">
                                <div data-toggle="gridalicious" data-gridalicious-width="280" data-gridalicious-gutter="0">
                                    <div class="innerAll inner-2x loading text-center text-medium">
<i class="fa fa-fw fa-spinner fa-spin"></i> Loading
</div>
                                    <div class="loaded">
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/1.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/1.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest active">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/2.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/2.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/3.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/3.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/4.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/4.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/5.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/5.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/6.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/6.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/7.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/7.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest active">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/8.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/8.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/9.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/9.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest active">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/10.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/10.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/11.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/11.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/12.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/12.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/13.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/13.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/14.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/14.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest active">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/15.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/15.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/16.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/16.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/17.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/17.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/18.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/18.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/19.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/19.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/20.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/20.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/21.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/21.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/22.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/22.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/23.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/23.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget widget-heading-simple widget-body-white widget-pinterest">
                                            <div class="widget-body padding-none">
                                                <a href="assets/images/people/250/24.jpg" class="thumb" data-gallery>
                                                    <img src="assets/images/people/250/24.jpg" alt="photo" />
                                                </a>
                                                <div class="description col-md-12">
                                                    <h5 class="text-uppercase">
                                                        Photo title
                                                    </h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, consequuntur iure commodi omnis at pariatur quis hic mollitia impedit error.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Blueimp Gallery -->
                                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                    <div class="slides"></div>
                                    <h3 class="title"></h3>
                                    <a class="prev no-ajaxify">‹</a> <a class="next no-ajaxify">›</a> <a class="close no-ajaxify">×</a> <a class="play-pause no-ajaxify"></a> 
                                    <ol class="indicator"></ol>
                                </div>
                                <!-- // Blueimp Gallery END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>