<!-- row -->
<div class="row row-app margin-none">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first border-none">
            <!-- col-table -->
            <div class="col-table">
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app">
                        <!-- row-app -->
                        <div class="row row-app margin-none">
                            <!-- col -->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <!-- col-separator -->
                                <div class="col-separator">
                                    <div class="innerAll inner-2x text-center border-bottom bg-white">
                                        <a href="" class="btn btn-lg btn-inverse btn-circle"><i class="fa fa-fw fa-4x icon-home-1"></i></a>
                                    </div>
                                    <div class="innerAll border-bottom bg-gray text-center">
                                        <div class="btn-group">
<a href="index.php?page=realestate_listing_grid" class="btn btn-primary">Listing</a>
<a href="index.php?page=realestate_listing_map" class="btn btn-default">Map</a>
</div>
                                    </div>
                                    <div class="widget widget-tabs border-none margin-none">
                                        <div class="widget-head bg-gray">
                                            <ul>
                                                <li class="active">
                                                    <a href="#buy" data-toggle="tab">Buy <span class="badge badge-primary badge-stroke">30</span></a>
                                                </li>
                                                <li>
                                                    <a href="#rent" data-toggle="tab">Rent <span class="badge badge-default badge-stroke">7</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="widget-body padding-none">
                                            <div class="innerAll border-bottom">
                                                <div class="input-group">
                                                    <input type="text" placeholder="Location" class="form-control">
                                                    <span class="input-group-addon">
<i class="fa fa-map-marker"></i>
</span>
                                                </div>
                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group btn-group-block row margin-none" data-toggle="buttons">
                                                    <label class="btn btn-default col-md-6 active">
                                                        <input type="radio">
                                                        <i class="fa fa-fw icon-home-fill-1"></i> Agency

                                                    </label>
                                                    <label class="btn btn-default col-md-6">
                                                        <input type="radio">
                                                        <i class="fa fa-fw icon-user-1"></i> Individual

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="buy">
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">Budget &euro;</label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-values="75,300"></div>
                                                            <div class="separator"></div>
                                                            <input type="text" class="amount form-control" />
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">
                                                            Area m
                                                            <sup>2</sup>
                                                        </label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-max="120"></div>
                                                            <div class="separator"></div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="17" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="100" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll border-bottom">
                                                        <label class="strong">Rooms</label>
                                                        <!-- Slider -->
                                                        <div class="range-slider">
                                                            <div class="slider slider-primary" data-max="30"></div>
                                                            <div class="separator"></div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="5" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="20" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Slider END -->
                                                    </div>
                                                    <div class="innerAll text-center border-bottom">
                                                        <select name="" data-container="body" data-style="btn-default" class="selectpicker form-control dropup" data-dropup-auto="false">
                                                            <option value="">Price</option>
                                                            <option value="">&euro;100k - &euro;300k</option>
                                                            <option value="">&euro;300k - &euro;500k</option>
                                                            <option value="">&euro;500k - &euro;1M</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="rent">
                                                    <div class="innerAll">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, enim, quis, eius voluptatem ducimus recusandae omnis distinctio quae quaerat facilis atque natus sint doloribus voluptatum minus quisquam reiciendis quos. Eveniet!
</div>
                                                </div>
                                            </div>
                                            <div class="innerAll text-center">
                                                <button class="btn btn-block btn-success">Search <i class="fa fa-fw fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                            <!-- col -->
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <!-- col-separator -->
                                <div class="col-separator col-unscrollable col-separator-last bg-none">
                                    <!-- col-table -->
                                    <div class="col-table">
                                        <div class="innerAll bg-white">
                                            <h4 class="margin-none innerT half pull-left">
                                                Property listing
                                            </h4>
                                            <div class="btn-group btn-group-sm pull-right">
                                                <a href="index.php?page=realestate_listing_grid" class="btn btn-default"><i class="fa fa-th"></i></a>
                                                <a href="index.php?page=realestate_listing_list" class="btn btn-primary"><i class="fa fa-list"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-separator-h"></div>
                                        <!-- col-table-row -->
                                        <div class="col-table-row">
                                            <!-- col-app -->
                                            <div class="col-app col-unscrollable">
                                                <!-- col-app -->
                                                <div class="col-app">
                                                    <div class="innerAll2">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="0">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-378874-real-estate-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Property for sale
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;52,452.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency &nbsp; <span class="label label-default">Sale</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="200">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-195203-houses-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Property for sale
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;151,229.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency &nbsp; <span class="label label-default">Sale</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="400">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-196089-house-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Property for sale
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;149,685.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual &nbsp; <span class="label label-default">Sale</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="600">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-197173-residential-home-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Residential Home
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;244,267.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual &nbsp; <span class="label label-primary">Rent</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="800">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-3979102-superb-backyard-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Superb Backyard
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;236,135.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency &nbsp; <span class="label label-primary">Rent</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="1000">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-2238345-apartments-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Appartments
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;212,536.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-user-1"></i> Individual &nbsp; <span class="label label-default">Sale</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="bg-white innerAll" data-animate="bounceIn" data-delay="1200">
                                                                    <div class="media margin-none">
                                                                        <a href="index.php?page=realestate_property" class="pull-left" style="width: 200px;">
                                                                            <img src="../assets/images/realestate/photodune-1112879-modern-kitchen-in-luxury-mansion-xs.jpg" alt="" class="img-responsive">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <div class="pull-right">
                                                                                <a href="index.php?page=realestate_property" class="btn btn-default btn-xs"><i class="fa fa-info fa-fw"></i></a>
                                                                                <a href="index.php?page=realestate_property" class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></a>
                                                                            </div>
                                                                            <h3 class="margin-none">
                                                                                Luxury Mansion
                                                                            </h3>
                                                                            <div class="innerB">
<span class="text-primary strong">&euro;44,546.00</span>
</div>
                                                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency &nbsp; <span class="label label-default">Sale</span> &nbsp; <i class="fa fa-fw fa-map-marker"></i> London, UK <a href="index.php?page=realestate_listing_map" class="text-primary text-underline text-small">map</a>
</div>
                                                                            <div class="separator"></div>
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, fugiat, sequi, mollitia, commodi rerum at debitis suscipit corrupti officia rem perferendis alias quo odit quia inventore sint aliquam molestiae.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="separator"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- // END col-app -->
                                            </div>
                                            <!-- // END col-app -->
                                        </div>
                                        <!-- // END col-table-row -->
                                    </div>
                                    <!-- // END col-table -->
                                </div>
                                <!-- // END col-separator -->
                            </div>
                            <!-- // END col -->
                        </div>
                        <!-- // END row -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->