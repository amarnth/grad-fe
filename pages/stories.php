<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-lg-4 col-sm-6 col-md-6">
        <!-- col-separator.box -->
        <div class="col-separator col-separator-first box">
            <div class="row row-merge bg-gray">
                <div class="col-md-6 bg-white text-regular">
                    <div class="innerAll inner-2x">
                        <h4 class="margin-none">
                            No More Traffic
                        </h4>
                        <p class="text-muted text-small"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, recusandae rem consectetur laudantium quasi natus ducimus necessitatibus magni eveniet odio vel doloribus eligendi accusantium.</p>
                        <a href="" class="text-primary">read more</a> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center innerAll inner-2x">
                        <img src="../assets/images/photodune-4729139-urban-landscape-xs.jpg" width="140" alt="" class="display-inline-block" />
                        <div class="separator"></div>
                        <h5>
                            Lorem ipsum dolor
                        </h5>
                        <div class="separator bottom"></div>
                        <div class="btn-group">
                            <a href="" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-primary"><i class="fa fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-separator-h box"></div>
            <div style="height: 150px; overflow: hidden;">
                <img src="../assets/images/photodune-4012760-art-xs.jpg" alt="" class="img-responsive border-none padding-none"/>
            </div>
            <div class="innerAll inner-2x border-bottom text-center">
                <h4>
                    The Modern Art
                </h4>
                <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 1 hour ago &nbsp; <i class="fa fa-fw fa-map-marker"></i> near <a href="">New York</a></p>
            </div>
            <div class="innerAll text-center">
                <div class="media margin-none display-block-inline">
                    <a href="" class="pull-left">
                        <img src="../assets/images/people/35/6.jpg" alt="" class="img-circle" />
                    </a>
                    <div class="media-body">
                        by <strong>Ewan Shelah</strong> 
                        <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                    </div>
                </div>
            </div>
            <div class="col-separator-h"></div>
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-lg-4 col-sm-6 col-md-6">
        <!-- col-separator.box -->
        <div class="col-separator col-separator-last-sm">
            <div class="relativeWrap">
                <img src="../assets/images/photodune-3998291-park-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                <div class="fixed-bottom bg-inverse-faded">
                    <div class="media margin-none innerAll">
                        <a href="" class="pull-left">
                            <img src="../assets/images/people/35/6.jpg" alt="" />
                        </a>
                        <div class="media-body text-white">
                            by <strong>Ewan Shelah</strong> 
                            <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerAll inner-2x">
                <h4>
                    A Walk in the Park
                </h4>
                <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, magnam necessitatibus libero eaque dolore.</p>
            </div>
            <div class="col-separator-h box"></div>
            <div class="row row-merge bg-gray">
                <div class="col-md-6 bg-white text-regular">
                    <div class="innerAll inner-2x">
                        <h4 class="margin-none">
                            Smile &amp; Move on
                        </h4>
                        <p class="text-muted text-small"><i class="fa fa-fw fa-clock-o"></i> 5 hours ago</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, assumenda, voluptatum at ipsa asperiores iure quisquam tempora temporibus fugit vitae ducimus ut.</p>
                        <a href="" class="text-primary">read more</a> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center innerAll inner-2x">
                        <img src="../assets/images/people/100/15.jpg" width="140" alt="" class="display-inline-block" />
                        <div class="separator"></div>
                        <h5>
                            Lorem ipsum dolor
                        </h5>
                        <div class="separator bottom"></div>
                        <div class="btn-group">
                            <a href="" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-primary"><i class="fa fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-separator-h box"></div>
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-lg-4 col-sm-12">
        <!-- col-separator.box -->
        <div class="col-separator">
            <div class="relativeWrap">
                <img src="../assets/images/photodune-1176874-modern-interior-design-of-apartment-xs.jpg" alt="" class="img-responsive padding-none border-none" />
                <div class="fixed-bottom bg-gray border-bottom">
                    <div class="innerAll inner-2x">
                        <a href="" class="pull-right btn btn-xs btn-primary"><i class="fa fa-fw fa-caret-right"></i></a>
                        <h4 class="margin-none">
                            The Modern Home
                        </h4>
                    </div>
                </div>
            </div>
            <div class="media margin-none innerAll">
                <a href="" class="pull-left">
                    <img src="../assets/images/people/35/3.jpg" alt="" />
                </a>
                <div class="media-body">
                    by <strong>Ananta Bjarte</strong> 
                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 13 hours ago</p>
                </div>
            </div>
            <div class="col-separator-h"></div>
            <div class="innerAll inner-2x bg-primary text-center">
                <div class="innerTB">
                    <h4 class="text-white">
                        Awesome Story
                    </h4>
                    <a href="" class="text-white"><i class="fa fa-fw fa-eye"></i> 1,540</a>
                    &nbsp; 
                    <a href="" class="text-white"><i class="fa fa-fw fa-share"></i> 9,800</a>
                </div>
            </div>
            <div class="media margin-none innerAll">
                <a href="" class="pull-left">
                    <img src="../assets/images/people/35/2.jpg" alt="" />
                </a>
                <div class="media-body">
                    by <strong>Erik Viggo</strong> 
                    <p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> 15 hours ago</p>
                </div>
            </div>
            <div class="col-separator-h"></div>
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->