<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="margin-none innerAll bg-white">
                    Contacts
                </h4>
                <div class="col-separator-h"></div>
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control contacts-search" placeholder="Search contacts">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="row row-merge">



                                <?php
                                    $contacts = new Contacts();
                                    $contacts->get();
                                ?>
                              
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->