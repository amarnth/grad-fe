<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-4">
        <!-- col-separator -->
        <div class="col-separator col-separator-first box col-unscrollable">
            <!-- col-table -->
            <div class="col-table">
                <div class="heading-buttons innerLR border-bottom">
                    <h4 class="margin-none innerTB">
                        Activity
                    </h4>
                    <div class="dropdown pull-right dropdown-icons dropdown-icons-xs">
                        <a data-toggle="dropdown" href="#" class="btn btn-default btn-stroke btn-circle dropdown-toggle"><i class="fa fa-eye"></i></a>
                        <ul class="dropdown-menu">
                            <li data-toggle="tooltip" data-title="Important" data-placement="left" data-container="body">
                                <a href="#"><i class="fa fa-circle"></i></a>
                            </li>
                            <li data-toggle="tooltip" data-title="View all" data-placement="left" data-container="body">
                                <a href="#"><i class="fa fa-circle-o"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app bg-primary-light">
                            <ul class="timeline-activity list-unstyled">
                                <li>
                                    <i class="list-icon fa fa-group"></i>

                                    <div class="block">
                                        <div class="caret"></div>
                                        <div class="box-generic">
                                            <div class="timeline-top-info">
<i class="fa fa-user"></i> <a href="">mosaicpro</a> assigned <a href="" class="text-primary">Company Presentation</a> to 10 members
</div>
                                            <div class="innerLR innerB">
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/10.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/11.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/12.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/13.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/14.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/15.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/16.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/17.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/18.jpg" alt="photo" width="20"/>
                                                </a>
                                                <a href="#" class="thumbnail pull-left border-none">
                                                    <img src="../assets/images/people/80/19.jpg" alt="photo" width="20"/>
                                                </a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 14:59 &nbsp;
<i class="fa fa-calendar"></i> 3 October 2013
</div>
                                        <div class="separator bottom"></div>
                                    </div>
                                </li>
                                <li>
                                    <i class="list-icon fa fa-plus"></i>

                                    <div class="block block-inline">
                                        <div class="caret"></div>
                                        <div class="box-generic">
                                            <div class="timeline-top-info">
<i class="fa fa-user"></i> <a href="">mosaicpro</a> added <a href="" class="text-primary">Company Presentation</a> task due on 6 Oct 2013
</div>
                                        </div>
                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 14:50 &nbsp;
<i class="fa fa-calendar"></i> 3 October 2013
</div>
                                    </div>
                                </li>
                                <li>
                                    <i class="list-icon fa fa-user"></i>

                                    <div class="block block-inline">
                                        <div class="caret"></div>
                                        <div class="box-generic">
                                            <div class="timeline-top-info">
                                                <i class="fa fa-user"></i> <a href="">mosaicpro</a> assigned <a href="" class="text-primary">Home Page Development</a> to 
                                                <a href="#">
                                                    <img src="../assets/images/people/80/10.jpg" alt="photo" width="20">
                                                </a>
                                                <a href="">Bogdan L.</a>

                                            </div>
                                        </div>
                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 11:00 &nbsp;
<i class="fa fa-calendar"></i> 2 October 2013
</div>
                                    </div>
                                </li>
                                <li>
                                    <i class="list-icon fa fa-plus"></i>

                                    <div class="block block-inline">
                                        <div class="caret"></div>
                                        <div class="box-generic">
                                            <div class="timeline-top-info">
<i class="fa fa-user"></i> <a href="">mosaicpro</a> added <a href="" class="text-primary">Home Page Development</a> task due on 5 Oct 2013
</div>
                                        </div>
                                        <div class="timeline-bottom">
<i class="fa fa-clock-o"></i> 10:00 &nbsp;
<i class="fa fa-calendar"></i> 1 October 2013
</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
                <div class="innerAll border-top">
                    <div class="innerAll alert alert-primary margin-none text-center animated flipInY"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, fugiat, aut voluptate error dolore! </div>
                </div>
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
    <!-- col -->
    <div class="col-sm-8">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box">
            <!-- col-table -->
            <div class="col-table">
                <div class="heading-buttons innerLR border-bottom">
                    <h4 class="margin-none pull-left innerTB">
                        Tasks
                    </h4>
                    <div class="btn-group btn-group-xs pull-right">
                        <button class="btn btn-default"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-primary"><i class="fa fa-refresh"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="innerAll">
                    <p class="text-uppercase margin-none"><span class="strong">Showing all tasks</span> <span class="text-faded">due by</span> 8 october 2013 <span class="text-faded">for</span> MosaicPro, Inc.</p>
                </div>
                <div class="col-separator-h box"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <label for="">Due by</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            <input type="text" class="form-control" placeholder="8 October 2013">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <label for="">Search for</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-search"></i> </span>
                                            <input type="text" class="form-control" placeholder="MosaicPro, Inc.">
                                            <span class="input-group-btn">
                                                <button class="btn btn-inverse"> <i class="fa fa-arrow-right"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group innerT">
                                    <button class="btn btn-inverse">All priorities <span class="badge badge-inverse">30</span></button>
                                    <button class="btn btn-primary">High <span class="badge badge-primary">15</span></button>
                                    <button class="btn btn-primary">Medium <span class="badge badge-primary">10</span></button>
                                    <button class="btn btn-primary">Low <span class="badge badge-primary">5</span></button>
                                </div>
                            </div>
                            <div class="col-separator-h box"></div>
                            <div class="innerAll">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Home Page Development
                                                </h4>
                                                <p class="text-muted margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 5 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 15 Hours</a>
                                                &nbsp; <span class="label label-primary">high</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-inverse"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 2 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Custom Email Form
                                                </h4>
                                                <p class="text-faded margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 6 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 3 Hours</a>
                                                &nbsp; <span class="label label-default">medium</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 3 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Fix Known Bugs
                                                </h4>
                                                <p class="text-faded margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 7 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 35 Hours</a>
                                                &nbsp; <span class="label label-primary">high</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 4 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Newsletter Design
                                                </h4>
                                                <p class="text-faded margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 9 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 3 Hours</a>
                                                &nbsp; <span class="label label-default label-stroke">low</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 6 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Application Wireframe
                                                </h4>
                                                <p class="text-faded margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 6 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 3 Hours</a>
                                                &nbsp; <span class="label label-default">medium</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 3 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-generic padding-none">
                                            <div class="innerAll border-bottom">
                                                <h4 class="strong">
                                                    Company Presentation
                                                </h4>
                                                <p class="text-faded margin-none">Corporate Website / MosaicPro, Inc.</p>
                                            </div>
                                            <div class="innerAll border-bottom"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cum autem alias est fugiat ratione nesciunt eum ullam! Esse, sed, sequi obcaecati pariatur non iste soluta optio necessitatibus expedita cum. </div>
                                            <div class="bg-white innerAll border-bottom">
                                                <a href=""><i class="fa fa-calendar"></i> 6 Oct 2013</a>
                                                &nbsp; 
                                                <a href=""><i class="fa fa-euro"></i> 10 Hours</a>
                                                &nbsp; <span class="label label-primary">high</span> 
                                            </div>
                                            <div class="bg-white box-generic-footer">
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-sun-o"></i></button>
                                                </div>
                                                <span class="pull-left innerAll"> <i class="fa fa-clock-o"></i> 3 days left </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
                <div class="innerAll border-top">
                    <ul class="pagination margin-none">
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->