<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-md-12">
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box col-separator-first">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom">
                    Attendance
                </h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="innerAll">

								<!-- Tabs -->
				                <div class="relativeWrap" >
				                    <div class="box-generic">
				                        <!-- Tabs Heading -->
				                        <div class="tabsbar">
				                            <ul>
				                                <li class="active">
				                                    <a href="#tab1-3" data-toggle="tab"><i></i> Mark Attendance</a>
				                                </li>
				                                <li class="">
				                                    <a href="#tab2-3" data-toggle="tab"><i></i> View</a>
				                                </li>
				                            </ul>
				                        </div>
				                        <!-- // Tabs Heading END -->
				                        <div class="tab-content">
				                            <!-- Tab content -->
				                            <div class="tab-pane active" id="tab1-3">

				                                <h4>
				                                    First tab
				                                </h4>

				                                <div class="innerAll bg-white margin-none border-bottom">
									
													<div class="col-md-2">
														<label>Date</label>
														<input class="form-control" type="text" id="attendance-date" value="<?php echo date("d M Y"); ?>" />
													</div>

													<div class="col-md-2">
														<label>Select Class</label>

														<select style="width: 100%;" id="select-class">
															<?php
																$get = new Util();
																$get->getDepartmentList();
															?>
															<!-- <option></option>
															<optgroup label="IT Department">
											                   <option value="AK">IT - A</option>
											                   <option value="HI">IT - B</option>
											                   <option value="HI">IT - C</option>
											                   <option value="HI">IT - D</option>
											               </optgroup>
											               <optgroup label="EEE Department">
											                   <option value="AK">MECH - A</option>
											                   <option value="HI">MECH - B</option>
											               </optgroup> -->
											        	</select>
											        </div>

											        <div class="col-md-2">
														<label>Select Subjects</label>

														<select style="width: 100%;" id="select-subject">
															<?php
																$get = new Util();
																$get->getSubjectsList();
															?>
											        	</select>
											        </div>

											        <div class="clearfix"></div>

											        <?php
												        $attendance = new Attendance();
												        $attendance->get();
												    ?>

												</div>
												
				                            </div>
				                            <!-- // Tab content END -->
				                            <!-- Tab content -->
				                            <div class="tab-pane" id="tab2-3">
				                                <h4>
				                                    Second tab
				                                </h4>

				                                <div class="innerAll bg-white margin-none border-bottom">
									
													<div class="col-md-2">
														<label>Date</label>
														<input class="form-control" type="text" id="view-attendance-date" value="<?php echo date("d M Y"); ?>" />
													</div>

													<div class="col-md-2">
														<label>Select Class</label>

														<select style="width: 100%;" id="view-select-class">
															<?php
																$get = new Util();
																$get->getDepartmentList();
															?>
											        	</select>
											        </div>

											        <div class="col-md-2">
														<label>Select Subjects</label>

														<select style="width: 100%;" id="view-select-subject">
															<?php
																$get = new Util();
																$get->getSubjectsList();
															?>
											        	</select>
											        </div>

											        <div class="clearfix"></div>

											        <?php
												        // $attendance = new Attendance();
												        // $attendance->get();
												    ?>

												</div>
												
				                            </div>
				                            <!-- // Tab content END -->
				                        </div>
				                    </div>
				                </div>
				                <!-- // Tabs END -->


								
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row-app -->





<style>
	.checkbox {
		margin: 0px;
	}
</style>
