<!-- row-app -->
<div class="row row-app">
    <!-- col -->
    <div class="col-sm-12">
        <!-- col-separator -->
        <div class="col-separator col-separator-first col-unscrollable bg-none">
            <!-- col-table -->
            <div class="col-table">
                <h1 class="margin-none innerAll">
                    Property <span>comparison</span> <i class="fa fa-fw icon-home-1"></i>
                </h1>
                <div class="col-separator-h"></div>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div id="realestate-carousel-compare" class="owl-carousel owl-theme">
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-378874-real-estate-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Property for sale
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;250,478.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-195203-houses-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Property for sale
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;159,193.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-196089-house-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Property for sale
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;104,308.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw fa-user"></i> Individual
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-primary"></span>
<span class="text">Rent</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-197173-residential-home-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Residential Home
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;1,819.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw fa-user"></i> Individual
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-primary"></span>
<span class="text">Rent</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-3979102-superb-backyard-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Superb Backyard
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;1,359.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-2238345-apartments-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Appartments
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;257,427.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw fa-user"></i> Individual
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="widget widget-pinterest">
                                        <div class="mark">
<span class="caret caret-default"></span>
<span class="text">Sale</span> </div>
                                        <div class="widget-body padding-none">
                                            <a href="index.php?page=realestate_property" class="thumb overflow-hidden" data-height="140px">
                                                <img src="../assets/images/realestate/photodune-1112879-modern-kitchen-in-luxury-mansion-xs.jpg" alt="photo" class="img-responsive" />
                                            </a>
                                            <div class="description border-bottom">
                                                <h5 class="text-uppercase">
                                                    Luxury Mansion
                                                </h5>
                                                <span class="text-primary text-medium text-condensed">&euro;234,128.00</span>

                                            </div>
                                            <div class="innerAll border-bottom text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-primary"><i class="fa fa-fw fa-envelope"></i> Contact seller</button>
                                                    <button class="btn btn-default"><i class="fa fa-fw fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="border-bottom bg-gray">
                                                <table class="table margin-none">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-muted border-top-none">Bedroom</td>
                                                            <td class="border-top-none">3</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Area</td>
                                                            <td>1,000 Sq.Yd</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Furnished</td>
                                                            <td>Furnished</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-muted">Facing</td>
                                                            <td>North West</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="innerAll border-bottom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, eum, repudiandae numquam vitae expedita.
</div>
                                            <div class="innerAll bg-gray">
<i class="fa fa-fw icon-home-fill-1"></i> Agency
</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator -->
    </div>
    <!-- // END col -->
</div>
<!-- // END row -->