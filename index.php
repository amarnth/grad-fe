<?php
/*
 * Minimal bootstrap file; 
 * acts like a router; 
 * loads the requested file;
 */
session_start();

$module = 'admin';

/* 
 * Load config.app;
 * Main configuration file
 */
require_once 'config/config.app.php';

/*
 * Load functions library
 */
require_once 'library/functions.php';

/* 
 * Load class.Menu.php;
 * Menu generator class
 */
require_once 'library/class.Menu.php';

/* 
 * Load config.colors;
 * Skins configuration file
 */
require_once 'config/config.colors.php';

/* 
 * Load config.menus;
 * Generate Sidebar Menu
 */
require_once 'config/config.menus.php';

/* 
 * Load config.scripts;
 * Dynamically load JavaScript files in the header and footer
 */
require_once 'config/config.scripts.php';

/*
 * Requested page;
 * Index by default
 */
$page = isset($_GET['page']) ? $_GET['page'] : 'index';
$section = isset($_GET['section']) ? $_GET['section'] : 'index';

/* Load header */
require_once 'header.php';

/*
 * Load page;
 */
 //session_start();
 
// var_dump($_SESSION);

		if($_SESSION==NULL){
			
			if(strpos($_SERVER['REQUEST_URI'],'signup_staff') !== false){
				require_once 'pages/signup_staff.php';	
			}elseif(strpos($_SERVER['REQUEST_URI'],'signup_student') !== false){
				require_once 'pages/signup_student.php';	
			}elseif(strpos($_SERVER['REQUEST_URI'],'signup') !== false){
				require_once 'pages/signup.php';	
			}else {
				require_once 'pages/login.php';
			}
			
			

		}else{

			require_once 'pages/' . $page . '.php';	
		}

//require_once 'pages/' . $page . '.php';

/* Load footer */
require_once 'footer.php';