<?php $scripts = array (
  'js.jquery.min' => 
  array (
    'header' => true,
    'file' => 'assets/library/jquery/jquery.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.jquery-migrate.min' => 
  array (
    'header' => true,
    'file' => 'assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.modernizr' => 
  array (
    'header' => true,
    'file' => 'assets/library/modernizr/modernizr.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.less.min' => 
  array (
    'header' => true,
    'file' => 'assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.excanvas' => 
  array (
    'header' => true,
    'file' => 'assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.ie.prototype.polyfill' => 
  array (
    'header' => true,
    'file' => 'assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.bootstrap.min' => 
  array (
    'header' => false,
    'file' => 'assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.jquery.nicescroll.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.breakpoints' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.pace.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.preload.pace.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.core.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/core/core.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'login',
      9 => 'signup',
      10 => 'ratings',
      11 => 'email',
      12 => 'email_compose',
      13 => 'events',
      14 => 'survey',
      15 => 'survey_multiple',
      16 => 'shop_products',
      17 => 'shop_edit_product',
      18 => 'gallery_photo',
      19 => 'gallery_video',
      20 => 'ui',
      21 => 'icons',
      22 => 'typography',
      23 => 'widgets',
      24 => 'calendar',
      25 => 'tabs',
      26 => 'tables',
      27 => 'tables_responsive',
      28 => 'pricing_tables',
      29 => 'form_wizards',
      30 => 'form_elements',
      31 => 'form_validator',
      32 => 'file_managers',
      33 => 'sliders',
      34 => 'charts',
      35 => 'grid',
      36 => 'notifications',
      37 => 'modals',
      38 => 'thumbnails',
      39 => 'carousels',
      40 => 'image_crop',
      41 => 'twitter',
      42 => 'infinite_scroll',
      43 => 'maps_vector',
      44 => 'maps_vector',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'maps_google',
      49 => 'maps_google',
      50 => 'maps_google',
      51 => 'maps_google',
      52 => 'tasks',
      53 => 'employees',
      54 => 'medical_overview',
      55 => 'medical_patients',
      56 => 'medical_memos',
      57 => 'medical_appointments',
      58 => 'medical_metrics',
      59 => 'courses',
      60 => 'projects',
      61 => 'courses_listing',
      62 => 'course',
      63 => 'news',
      64 => 'stories',
      65 => 'faq',
      66 => 'search',
      67 => 'support_tickets',
      68 => 'doubts',
      69 => 'support_answers',
      70 => 'support_forum_overview',
      71 => 'support_forum_post',
      72 => 'support_kb',
      73 => 'invoice',
      74 => 'finances',
      75 => 'bookings',
      76 => 'realestate_listing_map',
      77 => 'realestate_listing_grid',
      78 => 'realestate_listing_list',
      79 => 'realestate_property',
      80 => 'realestate_property_edit',
      81 => 'realestate_compare',
      82 => 'error',
      83 => 'profile_resume',
      84 => 'landing',
      85 => 'dashboard',
      86 => 'stats_class',
      87 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.blueimp-gallery.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_blueimp/js/blueimp-gallery.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'timeline',
      2 => 'gallery_photo',
      3 => 'tasks',
      4 => 'medical_patients',
      5 => 'search',
      6 => 'profile_resume',
    ),
  ),
  'js.jquery.blueimp-gallery.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_blueimp/js/jquery.blueimp-gallery.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'timeline',
      2 => 'gallery_photo',
      3 => 'tasks',
      4 => 'medical_patients',
      5 => 'search',
      6 => 'profile_resume',
    ),
  ),
  'js.image-preview' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_image-preview/image-preview.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
    ),
  ),
  'js.sidebar.main.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_menus/sidebar.main.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'ratings',
      9 => 'email',
      10 => 'email_compose',
      11 => 'events',
      12 => 'shop_products',
      13 => 'shop_edit_product',
      14 => 'gallery_photo',
      15 => 'gallery_video',
      16 => 'ui',
      17 => 'icons',
      18 => 'typography',
      19 => 'widgets',
      20 => 'calendar',
      21 => 'tabs',
      22 => 'tables',
      23 => 'tables_responsive',
      24 => 'pricing_tables',
      25 => 'form_wizards',
      26 => 'form_elements',
      27 => 'form_validator',
      28 => 'file_managers',
      29 => 'sliders',
      30 => 'charts',
      31 => 'grid',
      32 => 'notifications',
      33 => 'modals',
      34 => 'thumbnails',
      35 => 'carousels',
      36 => 'image_crop',
      37 => 'twitter',
      38 => 'infinite_scroll',
      39 => 'maps_vector',
      40 => 'maps_vector',
      41 => 'maps_google',
      42 => 'maps_google',
      43 => 'maps_google',
      44 => 'maps_google',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'tasks',
      49 => 'employees',
      50 => 'medical_overview',
      51 => 'medical_patients',
      52 => 'medical_memos',
      53 => 'medical_appointments',
      54 => 'medical_metrics',
      55 => 'courses',
      56 => 'projects',
      57 => 'courses_listing',
      58 => 'course',
      59 => 'news',
      60 => 'stories',
      61 => 'faq',
      62 => 'search',
      63 => 'support_tickets',
      64 => 'doubts',
      65 => 'support_answers',
      66 => 'support_forum_overview',
      67 => 'support_forum_post',
      68 => 'support_kb',
      69 => 'invoice',
      70 => 'finances',
      71 => 'bookings',
      72 => 'realestate_listing_map',
      73 => 'realestate_listing_grid',
      74 => 'realestate_listing_list',
      75 => 'realestate_property',
      76 => 'realestate_property_edit',
      77 => 'realestate_compare',
      78 => 'error',
      79 => 'profile_resume',
      80 => 'dashboard',
      81 => 'stats_class',
      82 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.sidebar.collapse.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_menus/sidebar.collapse.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'ratings',
      9 => 'email',
      10 => 'email_compose',
      11 => 'events',
      12 => 'shop_products',
      13 => 'shop_edit_product',
      14 => 'gallery_photo',
      15 => 'gallery_video',
      16 => 'ui',
      17 => 'icons',
      18 => 'typography',
      19 => 'widgets',
      20 => 'calendar',
      21 => 'tabs',
      22 => 'tables',
      23 => 'tables_responsive',
      24 => 'pricing_tables',
      25 => 'form_wizards',
      26 => 'form_elements',
      27 => 'form_validator',
      28 => 'file_managers',
      29 => 'sliders',
      30 => 'charts',
      31 => 'grid',
      32 => 'notifications',
      33 => 'modals',
      34 => 'thumbnails',
      35 => 'carousels',
      36 => 'image_crop',
      37 => 'twitter',
      38 => 'infinite_scroll',
      39 => 'maps_vector',
      40 => 'maps_vector',
      41 => 'maps_google',
      42 => 'maps_google',
      43 => 'maps_google',
      44 => 'maps_google',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'tasks',
      49 => 'employees',
      50 => 'medical_overview',
      51 => 'medical_patients',
      52 => 'medical_memos',
      53 => 'medical_appointments',
      54 => 'medical_metrics',
      55 => 'courses',
      56 => 'projects',
      57 => 'courses_listing',
      58 => 'course',
      59 => 'news',
      60 => 'stories',
      61 => 'faq',
      62 => 'search',
      63 => 'support_tickets',
      64 => 'doubts',
      65 => 'support_answers',
      66 => 'support_forum_overview',
      67 => 'support_forum_post',
      68 => 'support_kb',
      69 => 'invoice',
      70 => 'finances',
      71 => 'bookings',
      72 => 'realestate_listing_map',
      73 => 'realestate_listing_grid',
      74 => 'realestate_listing_list',
      75 => 'realestate_property',
      76 => 'realestate_property_edit',
      77 => 'realestate_compare',
      78 => 'error',
      79 => 'profile_resume',
      80 => 'dashboard',
      81 => 'stats_class',
      82 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'permissions',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.bootstrap-select' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_bootstrap-select/js/bootstrap-select.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'ratings',
      9 => 'email',
      10 => 'email_compose',
      11 => 'events',
      12 => 'shop_products',
      13 => 'shop_edit_product',
      14 => 'gallery_photo',
      15 => 'gallery_video',
      16 => 'ui',
      17 => 'icons',
      18 => 'typography',
      19 => 'widgets',
      20 => 'calendar',
      21 => 'tabs',
      22 => 'tables',
      23 => 'tables_responsive',
      24 => 'pricing_tables',
      25 => 'form_wizards',
      26 => 'form_elements',
      27 => 'form_validator',
      28 => 'file_managers',
      29 => 'sliders',
      30 => 'charts',
      31 => 'grid',
      32 => 'notifications',
      33 => 'modals',
      34 => 'thumbnails',
      35 => 'carousels',
      36 => 'image_crop',
      37 => 'twitter',
      38 => 'infinite_scroll',
      39 => 'maps_vector',
      40 => 'maps_vector',
      41 => 'maps_google',
      42 => 'maps_google',
      43 => 'maps_google',
      44 => 'maps_google',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'tasks',
      49 => 'employees',
      50 => 'medical_overview',
      51 => 'medical_patients',
      52 => 'medical_memos',
      53 => 'medical_appointments',
      54 => 'medical_metrics',
      55 => 'courses',
      56 => 'projects',
      57 => 'courses_listing',
      58 => 'course',
      59 => 'news',
      60 => 'stories',
      61 => 'faq',
      62 => 'search',
      63 => 'support_tickets',
      64 => 'doubts',
      65 => 'support_answers',
      66 => 'support_forum_overview',
      67 => 'support_forum_post',
      68 => 'support_kb',
      69 => 'invoice',
      70 => 'finances',
      71 => 'bookings',
      72 => 'realestate_listing_map',
      73 => 'realestate_listing_grid',
      74 => 'realestate_listing_list',
      75 => 'realestate_property',
      76 => 'realestate_property_edit',
      77 => 'realestate_compare',
      78 => 'error',
      79 => 'profile_resume',
      80 => 'dashboard',
      81 => 'stats_class',
      82 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'signup',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'permissions',
      99 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.bootstrap-select.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_bootstrap-select/bootstrap-select.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'social_account',
      2 => 'messages',
      3 => 'social_friends',
      4 => 'social',
      5 => 'timeline',
      6 => 'contacts_2',
      7 => 'contacts',
      8 => 'ratings',
      9 => 'email',
      10 => 'email_compose',
      11 => 'events',
      12 => 'shop_products',
      13 => 'shop_edit_product',
      14 => 'gallery_photo',
      15 => 'gallery_video',
      16 => 'ui',
      17 => 'icons',
      18 => 'typography',
      19 => 'widgets',
      20 => 'calendar',
      21 => 'tabs',
      22 => 'tables',
      23 => 'tables_responsive',
      24 => 'pricing_tables',
      25 => 'form_wizards',
      26 => 'form_elements',
      27 => 'form_validator',
      28 => 'file_managers',
      29 => 'sliders',
      30 => 'charts',
      31 => 'grid',
      32 => 'notifications',
      33 => 'modals',
      34 => 'thumbnails',
      35 => 'carousels',
      36 => 'image_crop',
      37 => 'twitter',
      38 => 'infinite_scroll',
      39 => 'maps_vector',
      40 => 'maps_vector',
      41 => 'maps_google',
      42 => 'maps_google',
      43 => 'maps_google',
      44 => 'maps_google',
      45 => 'maps_google',
      46 => 'maps_google',
      47 => 'maps_google',
      48 => 'tasks',
      49 => 'employees',
      50 => 'medical_overview',
      51 => 'medical_patients',
      52 => 'medical_memos',
      53 => 'medical_appointments',
      54 => 'medical_metrics',
      55 => 'courses',
      56 => 'projects',
      57 => 'courses_listing',
      58 => 'course',
      59 => 'news',
      60 => 'stories',
      61 => 'faq',
      62 => 'search',
      63 => 'support_tickets',
      64 => 'doubts',
      65 => 'support_answers',
      66 => 'support_forum_overview',
      67 => 'support_forum_post',
      68 => 'support_kb',
      69 => 'invoice',
      70 => 'finances',
      71 => 'bookings',
      72 => 'realestate_listing_map',
      73 => 'realestate_listing_grid',
      74 => 'realestate_listing_list',
      75 => 'realestate_property',
      76 => 'realestate_property_edit',
      77 => 'realestate_compare',
      78 => 'error',
      79 => 'profile_resume',
      80 => 'dashboard',
      81 => 'stats_class',
      82 => 'stats_student',
      88 => 'groups',
      89 => 'group_wall',
      90 => 'assignments',
      91 => 'timetable',
      92 => 'noticeboard',
      93 => 'project_view',
      94 => 'signup',
      95 => 'attendance',
      96 => 'signup_staff',
      97 => 'signup_student',
      98 => 'permissions',
      99 => 'assignment_view',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.sidebar.kis.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_menus/sidebar.kis.init.js',
    'pages' => 
    array (
      
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.jquery.easy-pie-chart' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_easy_pie/js/jquery.easy-pie-chart.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'widgets',
      2 => 'medical_overview',
      3 => 'dashboard',
      4 => 'stats_class',
      5 => 'stats_student',
    ),
  ),
  'js.easy-pie.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_easy_pie/easy-pie.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'index',
      1 => 'widgets',
      2 => 'medical_overview',
      3 => 'dashboard',
      4 => 'stats_class',
      5 => 'stats_student',
    ),
  ),
  'js.bootstrap-datepicker' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'social_account',
      1 => 'calendar',
      2 => 'form_elements',
      3 => 'medical_appointments',
      4 => 'search',
      5 => 'support_tickets',
      6 => 'index',
      7 => 'events',
      8 => 'assignments',
      9 => 'signup',
      10 => 'project_view',
      11 => 'attendance',
      12 => 'signup_staff',
      13 => 'signup_student',
      14 => 'assignment_view',
    ),
  ),
  'js.bootstrap-datepicker.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_bootstrap-datepicker/bootstrap-datepicker.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'social_account',
      1 => 'calendar',
      2 => 'form_elements',
      3 => 'medical_appointments',
      4 => 'search',
      5 => 'support_tickets',
      6 => 'index',
      7 => 'events',
      8 => 'assignments',
      9 => 'signup',
      10 => 'project_view',
      11 => 'attendance',
      12 => 'signup_staff',
      13 => 'signup_student',
      14 => 'assignment_view',
    ),
  ),
  'js.wysihtml5-0.3.0_rc2.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_editors_wysihtml5/js/wysihtml5-0.3.0_rc2.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'social_account',
      1 => 'shop_edit_product',
      2 => 'form_wizards',
      3 => 'form_elements',
      4 => 'modals',
    ),
  ),
  'js.bootstrap-wysihtml5-0.0.2' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_editors_wysihtml5/js/bootstrap-wysihtml5-0.0.2.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'social_account',
      1 => 'shop_edit_product',
      2 => 'form_wizards',
      3 => 'form_elements',
      4 => 'modals',
    ),
  ),
  'js.wysihtml5.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_editors_wysihtml5/wysihtml5.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'social_account',
      1 => 'shop_edit_product',
      2 => 'form_wizards',
      3 => 'form_elements',
      4 => 'modals',
    ),
  ),
  'js.holder' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_holder/holder.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'social',
      1 => 'contacts',
      2 => 'ratings',
      3 => 'events',
      4 => 'shop_products',
      5 => 'widgets',
      6 => 'thumbnails',
      7 => 'courses_listing',
      8 => 'course',
      9 => 'search',
      10 => 'realestate_property_edit',
      11 => 'index',
      12 => 'groups',
      13 => 'group_wall',
    ),
  ),
  'js.footable.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_responsive/js/footable.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'ratings',
      1 => 'tables_responsive',
    ),
  ),
  'js.tables-responsive-footable.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/tables_responsive/tables-responsive-footable.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'ratings',
      1 => 'tables_responsive',
    ),
  ),
  'js.email.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_email/email.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'email',
      1 => 'email_compose',
    ),
  ),
  'js.bootbox.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'email',
      1 => 'email_compose',
      2 => 'form_elements',
      3 => 'modals',
      4 => 'maps_google',
      5 => 'realestate_property_edit',
      6 => 'index',
      7 => 'groups',
    ),
    'sections' => 
    array (
      'maps_google' => 
      array (
        0 => 'geocoding',
      ),
    ),
  ),
  'js.modals.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/ui_modals/modals.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'email',
      1 => 'email_compose',
      2 => 'form_elements',
      3 => 'modals',
      4 => 'groups',
    ),
  ),
  'js.jquery.gritter.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/notifications_gritter/js/jquery.gritter.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'email',
      1 => 'email_compose',
      2 => 'form_elements',
      3 => 'notifications',
      4 => 'modals',
      5 => 'groups',
    ),
  ),
  'js.gritter.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_notifications_gritter/gritter.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'email',
      1 => 'email_compose',
      2 => 'form_elements',
      3 => 'notifications',
      4 => 'modals',
      5 => 'groups',
    ),
  ),
  'js.owl.carousel.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_owl-carousel/owl.carousel.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'events',
      1 => 'shop_products',
      2 => 'news',
      3 => 'realestate_property',
      4 => 'realestate_compare',
      5 => 'landing',
      6 => 'groups',
      7 => 'index',
      8 => 'project_view',
    ),
  ),
  'js.events-carousel.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_events/events-carousel.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'events',
      1 => 'groups',
      2 => 'project_view',
    ),
  ),
  'js.events-speakers.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_events/events-speakers.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'events',
      1 => 'groups',
      2 => 'project_view',
    ),
  ),
  'js.jquery-ui.min' => 
  array (
    'header' => true,
    'file' => 'assets/library/jquery-ui/js/jquery-ui.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'survey',
      1 => 'shop_products',
      2 => 'widgets',
      3 => 'calendar',
      4 => 'tables',
      5 => 'sliders',
      6 => 'finances',
      7 => 'bookings',
      8 => 'realestate_listing_map',
      9 => 'realestate_listing_grid',
      10 => 'realestate_listing_list',
      11 => 'assignments',
      12 => 'timetable',
      13 => 'project_view',
      14 => 'assignment_view',
    ),
  ),
  'js.jquery.ui.touch-punch.min' => 
  array (
    'header' => true,
    'file' => 'assets/plugins/core_jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'survey',
      1 => 'shop_products',
      2 => 'widgets',
      3 => 'calendar',
      4 => 'tables',
      5 => 'sliders',
      6 => 'finances',
      7 => 'bookings',
      8 => 'realestate_listing_map',
      9 => 'realestate_listing_grid',
      10 => 'realestate_listing_list',
      11 => 'assignments',
      12 => 'timetable',
      13 => 'project_view',
      14 => 'assignment_view',
    ),
  ),
  'js.jqueryui-sliders.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/ui_sliders_jqueryui/jqueryui-sliders.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'survey',
      1 => 'sliders',
      2 => 'realestate_listing_map',
      3 => 'realestate_listing_grid',
      4 => 'realestate_listing_list',
    ),
  ),
  'js.surveys.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_surveys/surveys.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'survey',
    ),
  ),
  'js.jquery.appear' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/other_appear/jquery.appear.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'survey',
      1 => 'landing',
    ),
  ),
  'js.jquery.uniform.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_uniform/js/jquery.uniform.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'shop_products',
      1 => 'search',
      2 => 'bookings',
    ),
  ),
  'js.uniform.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_uniform/uniform.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'shop_products',
      1 => 'search',
      2 => 'bookings',
    ),
  ),
  'js.tables-classic.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/tables/tables-classic.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'shop_products',
      1 => 'tables',
      2 => 'bookings',
      3 => 'assignment_view',
    ),
  ),
  'js.shop-featured-1.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_shop/shop-featured-1.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'shop_products',
    ),
  ),
  'js.jquery.gridalicious.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'gallery_photo',
      1 => 'gallery_video',
      2 => 'twitter',
      3 => 'events',
    ),
  ),
  'js.gridalicious.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'gallery_photo',
      1 => 'gallery_video',
      2 => 'twitter',
      3 => 'events',
    ),
  ),
  'js.jquery.prettyPhoto' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_prettyphoto/js/jquery.prettyPhoto.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'gallery_video',
      1 => 'index',
    ),
  ),
  'js.prettyphoto.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/media_prettyphoto/prettyphoto.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'gallery_video',
      1 => 'index',
    ),
  ),
  'js.jquery.bootpag' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/ui_pagination/jquery.bootpag.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'ui',
    ),
  ),
  'js.jquery.bootpag.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/ui_pagination/jquery.bootpag.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'ui',
    ),
  ),
  'js.widget-progress.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/widget_progress/widget-progress.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'widgets',
    ),
  ),
  'js.widget-collapsible.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/widget_collapsible/widget-collapsible.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'widgets',
      1 => 'project_view',
      2 => 'doubts',
      3 => 'group_wall',
      4 => 'dashboard',
    ),
  ),
  'js.jquery.slimscroll' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/core_slimscroll/jquery.slimscroll.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'widgets',
    ),
  ),
  'js.widget-scrollable.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/widget_scrollable/widget-scrollable.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'widgets',
    ),
  ),
  'js.fullcalendar.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/calendar_fullcalendar/js/fullcalendar.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'calendar',
      1 => 'timetable',
    ),
  ),
  'js.calendar.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/calendar/calendar.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'calendar',
      1 => 'timetable',
    ),
  ),
  'js.fuelux-checkbox.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_fuelux-checkbox/fuelux-checkbox.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'calendar',
      1 => 'tables',
      2 => 'form_elements',
      3 => 'form_validator',
      4 => 'modals',
      5 => 'support_tickets',
      6 => 'doubts',
      7 => 'support_answers',
      8 => 'assignment_view',
      9 => 'timetable',
      10 => 'login',
      11 => 'signup',
      12 => 'index',
      13 => 'groups',
      14 => 'group_wall',
      15 => 'events',
      16 => 'noticeboard',
      17 => 'social_account',
      18 => 'permissions',
      19 => 'attendance',
      20 => 'signup_staff',
      21 => 'signup_student',
    ),
  ),
  'js.jquery.dataTables.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_datatables/js/jquery.dataTables.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.TableTools.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_datatables/extras/TableTools/media/js/TableTools.min.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.ColVis.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_datatables/extras/ColVis/media/js/ColVis.min.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.DT_bootstrap' => 
  array (
    'header' => false,
    'file' => 'assets/components/tables_datatables/js/DT_bootstrap.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.datatables.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/tables_datatables/js/datatables.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.FixedHeader' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_datatables/extras/FixedHeader/FixedHeader.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.ColReorder.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/tables_datatables/extras/ColReorder/media/js/ColReorder.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'tables',
      1 => 'assignment_view',
    ),
  ),
  'js.jquery.bootstrap.wizard' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_wizards/jquery.bootstrap.wizard.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_wizards',
      1 => 'form_elements',
      2 => 'modals',
    ),
  ),
  'js.form-wizards.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_wizards/form-wizards.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_wizards',
      1 => 'form_elements',
      2 => 'modals',
    ),
  ),
  'js.bootstrap-switch' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'groups',
      2 => 'permissions',
    ),
  ),
  'js.bootstrap-switch.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'groups',
      2 => 'permissions',
    ),
  ),
  'js.fuelux-radio.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_fuelux-radio/fuelux-radio.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'groups',
      2 => 'assignments',
      3 => 'noticeboard',
      4 => 'events',
      5 => 'projects',
    ),
  ),
  'js.bootstrap-fileupload' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_jasny-fileupload/js/bootstrap-fileupload.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'realestate_property_edit',
      2 => 'index',
      3 => 'groups',
      4 => 'project_view',
      5 => 'group_wall',
      6 => 'noticeboard',
    ),
  ),
  'js.button-loading.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_button-states/button-loading.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'index',
      2 => 'groups',
    ),
  ),
  'js.select2' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_select2/js/select2.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'employees',
      2 => 'groups',
      3 => 'events',
      4 => 'assignments',
      5 => 'noticeboard',
      6 => 'signup',
      7 => 'projects',
      8 => 'project_view',
      9 => 'doubts',
      10 => 'group_wall',
      11 => 'attendance',
      12 => 'signup_staff',
      13 => 'signup_student',
      14 => 'assignment_view',
    ),
  ),
  'js.select2.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_select2/select2.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'employees',
      2 => 'groups',
      3 => 'events',
      4 => 'assignments',
      5 => 'noticeboard',
      6 => 'signup',
      7 => 'projects',
      8 => 'project_view',
      9 => 'doubts',
      10 => 'group_wall',
      11 => 'attendance',
      12 => 'signup_staff',
      13 => 'signup_student',
      14 => 'assignment_view',
    ),
  ),
  'js.jquery.multi-select' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_multiselect/js/jquery.multi-select.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'groups',
      2 => 'assignments',
      3 => 'noticeboard',
      4 => 'events',
      5 => 'projects',
      6 => 'project_view',
      7 => 'doubts',
      8 => 'group_wall',
      9 => 'attendance',
      10 => 'assignment_view',
    ),
  ),
  'js.multiselect.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_multiselect/multiselect.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'groups',
      2 => 'assignments',
      3 => 'noticeboard',
      4 => 'events',
      5 => 'projects',
      6 => 'project_view',
      7 => 'doubts',
      8 => 'group_wall',
      9 => 'attendance',
      10 => 'assignment_view',
    ),
  ),
  'js.jquery.inputmask.bundle.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_inputmask/jquery.inputmask.bundle.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
    ),
  ),
  'js.inputmask.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_inputmask/inputmask.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
    ),
  ),
  'js.bootstrap-timepicker' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_bootstrap-timepicker/js/bootstrap-timepicker.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'index',
      2 => 'events',
    ),
  ),
  'js.bootstrap-timepicker.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_bootstrap-timepicker/bootstrap-timepicker.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'index',
      2 => 'events',
    ),
  ),
  'js.farbtastic.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_elements_colorpicker-farbtastic/js/farbtastic.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'projects',
      2 => 'project_view',
    ),
  ),
  'js.colorpicker-farbtastic.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_elements_colorpicker-farbtastic/colorpicker-farbtastic.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_elements',
      1 => 'projects',
      2 => 'project_view',
    ),
  ),
  'js.jquery.validate.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_validator/jquery-validation/dist/jquery.validate.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_validator',
      1 => 'signup',
      2 => 'social_account',
      3 => 'signup_staff',
      4 => 'signup_student',
    ),
  ),
  'js.form-validator.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_validator/form-validator.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'form_validator',
    ),
  ),
  'js.dropzone.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_file_dropzone/js/dropzone.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'file_managers',
    ),
  ),
  'js.dropzone.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_file_dropzone/dropzone.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'file_managers',
    ),
  ),
  'js.plupload.full' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_file_plupload/plupload.full.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'file_managers',
    ),
  ),
  'js.jquery.plupload.queue' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/forms_file_plupload/jquery.plupload.queue/jquery.plupload.queue.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'file_managers',
    ),
  ),
  'js.plupload.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/forms_file_plupload/plupload.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'file_managers',
    ),
  ),
  'js.jquery.mousewheel.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/ui_sliders_range_mousewheel/jquery.mousewheel.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'sliders',
    ),
  ),
  'js.jQAllRangeSliders-withRuler-min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/ui_sliders_range_jqrangeslider/js/jQAllRangeSliders-withRuler-min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'sliders',
    ),
  ),
  'js.range-sliders.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/ui_sliders_range/range-sliders.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'sliders',
    ),
  ),
  'js.jquery.flot' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/jquery.flot.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'medical_overview',
      2 => 'medical_metrics',
      3 => 'finances',
      4 => 'dashboard',
      5 => 'stats_student',
    ),
  ),
  'js.jquery.flot.resize' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/jquery.flot.resize.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'medical_overview',
      2 => 'medical_metrics',
      3 => 'finances',
      4 => 'dashboard',
      5 => 'stats_student',
    ),
  ),
  'js.jquery.flot.tooltip.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/plugins/jquery.flot.tooltip.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'medical_metrics',
      2 => 'finances',
      3 => 'dashboard',
      4 => 'stats_student',
    ),
  ),
  'js.flotcharts.common' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotcharts.common.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'medical_overview',
      2 => 'medical_metrics',
      3 => 'finances',
      4 => 'dashboard',
      5 => 'stats_student',
    ),
  ),
  'js.flotchart-simple.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-simple.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'finances',
      2 => 'stats_student',
    ),
  ),
  'js.flotchart-line.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-line.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.jquery.flot.orderBars' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/plugins/jquery.flot.orderBars.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
      1 => 'medical_overview',
    ),
  ),
  'js.flotchart-bars-ordered.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-bars-ordered.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.jquery.flot.pie' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/jquery.flot.pie.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.flotchart-donut.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-donut.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.flotchart-bars-stacked.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-bars-stacked.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.flotchart-pie.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-pie.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.flotchart-bars-horizontal.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-bars-horizontal.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.flotchart-autoupdating.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-autoupdating.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.jquery.notyfy' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/notifications_notyfy/js/jquery.notyfy.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'notifications',
      1 => 'noticeboard',
    ),
  ),
  'js.notyfy.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_notifications_notyfy/notyfy.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'notifications',
      1 => 'noticeboard',
    ),
  ),
  'js.jquery.Jcrop' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/media_image-crop/js/jquery.Jcrop.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'image_crop',
    ),
  ),
  'js.image-crop.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/media_image-crop/image-crop.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'image_crop',
    ),
  ),
  'js.twitter.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/widget_twitter/twitter.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'twitter',
    ),
  ),
  'js.jquery.jscroll' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/other_infinite-scroll/jquery.jscroll.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'infinite_scroll',
    ),
  ),
  'js.infinite-scroll.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/other_infinite-scroll/infinite-scroll.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'infinite_scroll',
    ),
  ),
  'js.jquery-jvectormap-1.2.2.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/maps_vector/jquery-jvectormap-1.2.2.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'maps_vector',
      1 => 'dashboard',
      2 => 'stats_class',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
    ),
  ),
  'js.jquery-jvectormap-world-mill-en' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/maps_vector/maps/jquery-jvectormap-world-mill-en.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'maps_vector',
      1 => 'dashboard',
      2 => 'stats_class',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
    ),
  ),
  'js.maps-vector.world-map-markers.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/maps_vector/maps-vector.world-map-markers.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'maps_vector',
      1 => 'dashboard',
      2 => 'stats_class',
    ),
    'sections' => 
    array (
      'maps_vector' => 
      array (
        0 => 'fullscreen',
      ),
    ),
  ),
  'js.maps-google.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/maps_google/maps-google.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'maps_google',
      1 => 'maps_google',
      2 => 'maps_google',
      3 => 'maps_google',
      4 => 'maps_google',
      5 => 'maps_google',
      6 => 'realestate_listing_map',
      7 => 'realestate_property',
      8 => 'realestate_property_edit',
    ),
    'sections' => 
    array (
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.initGoogleMaps' => 
  array (
    'header' => false,
    'file' => 'http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initGoogleMaps',
    'pages' => 
    array (
      0 => 'maps_google',
      1 => 'maps_google',
      2 => 'maps_google',
      3 => 'maps_google',
      4 => 'maps_google',
      5 => 'maps_google',
      6 => 'realestate_listing_map',
      7 => 'realestate_property',
      8 => 'realestate_property_edit',
    ),
    'sections' => 
    array (
      'maps_google' => 
      array (
        0 => 'clustering',
        1 => 'extend-pagination',
        2 => 'filtering',
        3 => 'geocoding',
        4 => 'streetview',
        5 => 'fullscreen',
      ),
    ),
  ),
  'js.employees.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_employees/employees.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'employees',
    ),
  ),
  'js.medical.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_medical/medical.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'medical_overview',
      1 => 'medical_metrics',
    ),
  ),
  'js.jquery.flot.growraf' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_flot/plugins/jquery.flot.growraf.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'medical_metrics',
    ),
  ),
  'js.prettify' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/core_prettyprint/js/prettify.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'course',
    ),
  ),
  'js.news-featured-2.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_news/news-featured-2.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'news',
    ),
  ),
  'js.news-featured-1.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_news/news-featured-1.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'news',
    ),
  ),
  'js.news-featured-3.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_news/news-featured-3.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'news',
    ),
  ),
  'js.invoice.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_invoice/invoice.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'invoice',
    ),
  ),
  'js.realestate.carousel.property.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/realestate_carousel/realestate.carousel.property.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'realestate_property',
    ),
  ),
  'js.realestate.carousel.compare.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/realestate_carousel/realestate.carousel.compare.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'realestate_compare',
    ),
  ),
  'js.landing.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/admin_landing/landing.init.js?v=v2.0.0-rc1',
    'pages' => 
    array (
      0 => 'landing',
    ),
  ),
  'js.flotchart-line-2.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-line-2.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'js.jquery.sparkline.min' => 
  array (
    'header' => false,
    'file' => 'assets/plugins/charts_sparkline/jquery.sparkline.min.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'dashboard',
      1 => 'stats_class',
    ),
  ),
  'js.sparkline.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_sparkline/sparkline.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'dashboard',
      1 => 'stats_class',
    ),
  ),
  'js.flotchart-mixed-1.init' => 
  array (
    'header' => false,
    'file' => 'assets/components/charts_flot/flotchart-mixed-1.init.js?v=v2.0.0-rc1&amp;sv=v0.0.1.2',
    'pages' => 
    array (
      0 => 'charts',
    ),
  ),
  'wall'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/wall.js',
    'pages' => 
    array (
      0 => 'index',
    ),
  ),
  'todo'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/todo.js',
    'pages' => 
    array (
      0 => 'index',
    ),
  ),
  'groups'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/groups.js',
    'pages' => 
    array (
      0 => 'groups',
    ),
  ),
  'group_wall'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/group_wall.js',
    'pages' => 
    array (
      0 => 'group_wall',
    ),
  ),
  'Create Event'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/create_event.js',
    'pages' => 
    array (
      0 => 'events',
    ),
  ),
  'Events'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/events.js',
    'pages' => 
    array (
      0 => 'events',
    ),
  ),
  'Create Assignments'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/assignments.js',
    'pages' => 
    array (
      0 => 'assignments',
    ),
  ),
  'View Assignment'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/assignment_view.js',
    'pages' => 
    array (
      0 => 'assignment_view',
    ),
  ),
  'mycalendar'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/myCalendar.js',
    'pages' => 
    array (
      0 => 'calendar',
    ),
  ),
  'noticeboard'=> 
  array (
    'header' => true,
    'file' => 'assets/grad_assets/js/liveurl.js',
    'pages' => 
    array (
      0 => 'noticeboard',
    ),
  ),
  'noticeboard'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/noticeboard.js',
    'pages' => 
    array (
      0 => 'noticeboard',
    ),
  ),
  'signup_student'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/signup_student.js',
    'pages' => 
    array (
      0 => 'signup_student',
    ),
  ),
  'signup_staff'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/signup_staff.js',
    'pages' => 
    array (
      0 => 'signup_staff',
    ),
  ),
  'account'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/account.js',
    'pages' => 
    array (
      0 => 'social_account',
    ),
  ),
  'contacts'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/contacts.js',
    'pages' => 
    array (
      0 => 'contacts',
    ),
  ),
  'stats_filter_students'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/stats_filter_students.js',
    'pages' => 
    array (
      0 => 'stats_student',
    ),
  ),
  'projects'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/projects.js',
    'pages' => 
    array (
      0 => 'projects',
    ),
  ),
  'project_view'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/project_view.js',
    'pages' => 
    array (
      0 => 'project_view',
    ),
  ),
  'doubts'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/doubts.js',
    'pages' => 
    array (
      0 => 'doubts',
    ),
  ),
  'messages'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/messages.js',
    'pages' => 
    array (
      0 => 'messages',
    ),
  ),
  'attendance'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/attendance.js',
    'pages' => 
    array (
      0 => 'attendance',
    ),
  ),
  'pinterest_grid'=> 
  array (
    'header' => true,
    'file' => 'assets/grad_assets/js/lib/pinterest_grid.js',
    'pages' => 
    array (
      0 => 'noticeboard',
      1 => 'groups',
    ),
  ),
  'pinterest'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/pinterest-init.js',
    'pages' => 
    array (
      0 => 'noticeboard',
      1 => 'groups',
    ),
  ),
  'menu-hidden'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/menu-hidden.js',
    'pages' => 
    array (
      0 => 'dashboard',
      1 => 'stats_class',
      2 => 'stats_student',
      3 => 'events',
      4 => 'groups',
      5 => 'group_wall',
      6 => 'assignments',
      7 => 'noticeboard',
      8 => 'calendar',
      9 => 'projects',
      10 => 'project_view',
      11 => 'doubts',
      12 => 'timeline',
      13 => 'messages',
      14 => 'attendance',
      15 => 'assignment_view',
    ),
  ),
  'upload'=> 
  array (
    'header' => true,
    'file' => 'assets/grad_assets/js/lib/jquery.uploadfile.min.js',
    'pages' => 
    array (
      0 => 'index',
      1 => 'groups',
      2 => 'assignments',
      3 => 'project_view',
      4 => 'group_wall',
      5 => 'noticeboard',
      6 => 'assignment_view',
    ),
  ),
  'login'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/login.js',
    'pages' => 
    array (
      0 => 'login',
      1 => 'index',
      2 => 'groups',
      3 => 'group_wall',
      4 => 'events',
      5 => 'assignments',
      6 => 'noticeboard',
      7 => 'social_account',
      8 => 'timeline',
      9 => 'contacts',
      10 => 'messages',
      11 => 'projects',
      12 => 'project_view',
      13 => 'dashboard',
      14 => 'stats_student',
      15 => 'stats_class',
      16 => 'doubts',
      17 => 'attendance',
      18 => 'assignment_view',
    ),
  ),
  'grad_script'=> 
  array (
    'header' => false,
    'file' => 'assets/grad_assets/js/script.js',
    'pages' => 
    array (
      0 => 'index',
      1 => 'groups',
      2 => 'group_wall',
      3 => 'events',
      4 => 'assignments',
      5 => 'noticeboard',
      6 => 'projects',
      7 => 'project_view',
      8 => 'dashboard',
      9 => 'stats_student',
      10 => 'stats_class',
      11 => 'project_view',
      12 => 'projects',
      13 => 'doubts',
      14 => 'support_answers',
      15 => 'attendance',
      16 => 'assignment_view',
    ),
  ),
);
