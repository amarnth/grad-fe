<?php
$config['menu'] = [
    'admin' => [
        'components' => [
            array(
                'label' => 'UI Elements',
                'icon' => 'fa fa-circle-o',
                'page' => 'ui'
            ),
            array(
                'label' => 'Icons',
                'icon' => 'fa fa-circle-o',
                'page' => 'icons'
            ),
            array(
                'label' => 'Typography',
                'icon' => 'fa fa-circle-o',
                'page' => 'typography'
            ),
            array(
                'label' => 'Tabs',
                'icon' => 'fa fa-circle-o',
                'page' => 'tabs'
            ),
            array(
                'label' => 'Tables',
                'icon' => 'fa fa-circle-o',
                'badge' => [
                    'class' => 'badge-primary',
                    'label' => '3'
                ],
                'page' => array(
                    array(
                        'label' => 'Tables',
                        'page' => 'tables'
                    ),
                    array(
                        'label' => 'Responsive Tables',
                        'page' => 'tables_responsive'
                    ),
                )
            ),
            array(
                'label' => 'Forms',
                'icon' => 'fa fa-circle-o',
                'badge' => [
                    'class' => 'badge-primary',
                    'label' => '4'
                ],
                'page' => array(
                    array(
                        'label' => 'Form Wizards',
                        'page' => 'form_wizards'
                    ),
                    array(
                        'label' => 'Form Elements',
                        'page' => 'form_elements'
                    ),
                    array(
                        'label' => 'Form Validator',
                        'page' => 'form_validator'
                    ),
                    array(
                        'label' => 'File Managers',
                        'page' => 'file_managers'
                    ),
                )
            ),
            array(
                'label' => 'Sliders',
                'icon' => 'fa fa-circle-o',
                'page' => 'sliders'
            ),
            array(
                'label' => 'Charts',
                'icon' => 'fa fa-circle-o',
                'page' => 'charts'
            ),
            array(
                'label' => 'Grid',
                'icon' => 'fa fa-circle-o',
                'page' => 'grid'
            ),
            array(
                'label' => 'Notifications',
                'icon' => 'fa fa-circle-o',
                'page' => 'notifications'
            ),
            array(
                'label' => 'Modals',
                'icon' => 'fa fa-circle-o',
                'page' => 'modals'
            ),
            array(
                'label' => 'Thumbnails',
                'icon' => 'fa fa-circle-o',
                'page' => 'thumbnails'
            ),
            array(
                'label' => 'Carousels',
                'icon' => 'fa fa-circle-o',
                'page' => 'carousels'
            ),
            array(
                'label' => 'Image Cropping',
                'icon' => 'fa fa-circle-o',
                'page' => 'image_crop'
            ),
            array(
                'label' => 'Twitter API',
                'icon' => 'fa fa-circle-o',
                'page' => 'twitter'
            ),
            array(
                'label' => 'Infinite Scroll',
                'icon' => 'fa fa-circle-o',
                'page' => 'infinite_scroll'
            ),
        ],
        'content' => [
            array(
                'label' => 'News',
                'icon' => 'fa fa-file-text',
                'page' => 'news'
            ),
            array(
                'label' => 'Stories',
                'icon' => 'fa fa-file-text-o',
                'page' => 'stories'
            ),
            array(
                'label' => 'Search',
                'icon' => 'fa fa-search',
                'page' => 'search'
            ),
            array(
                'label' => 'faq',
                'icon' => 'fa fa-question-circle',
                'page' => 'faq'
            ),
            array(
                'label' => 'Followers',
                'class' => 'reset',
                'file' => 'partials/content_folowers.php'
            ),
            array(
                'label' => 'Friends',
                'class' => 'reset',
                'file' => 'partials/content_friends.php'
            ),
            array(
                'label' => 'Stories',
                'class' => 'reset',
                'file' => 'partials/content_stories.php'
            ),
            array(
                'label' => 'Publish',
                'class' => 'reset',
                'file' => 'partials/content_publish.php'
            ),


        ],
        'learning' => [
            array(
                'label' => 'Courses Home #1',
                'icon' => 'fa fa-book',
                'page' => 'courses_2'
            ),
            array(
                'label' => 'Courses Home #2',
                'icon' => 'fa fa-book',
                'page' => 'courses'
            ),
            array(
                'label' => 'Courses Listing',
                'icon' => 'fa fa-book',
                'page' => 'courses_listing'
            ),
            array(
                'label' => 'Course Page',
                'icon' => 'fa fa-book',
                'page' => 'course'
            ),
            array(
                'label' => 'Subscription',
                'class' => 'reset',
                'file' => 'partials/learning.php'
            ),
        ],
        'default' => [
            array(
                'label' => 'Dashboard',
                'icon' => 'fa fa-dashboard',
                'page' => [
                    array(
                        'label' => 'Dashboard Analytics',
                        'icon' => 'fa fa-bar-chart-o',
                        'page' => 'dashboard_analytics'
                    ),
                    array(
                        'label' => 'Dashboard Users',
                        'icon' => 'fa fa-user',
                        'page' => 'dashboard_users'
                    ),
                    array(
                        'label' => 'Dashboard Overview',
                        'icon' => 'fa fa-dashboard',
                        'page' => 'dashboard_overview'
                    ),
                ],
            ),
            array(
                'label' => 'Social App',
                'icon' => 'fa fa-user',
                'badge' => ['label' => '7', 'class' => 'badge-primary'],
                'page' => [
                    array(
                        'label' => 'Activity',
                        'icon' => 'fa fa-flash',
                        'page' => 'index'
                    ),
                    array(
                        'label' => 'Account',
                        'icon' => 'fa fa-male',
                        'page' => 'social_account'
                    ),
                    array(
                        'label' => 'Groups',
                        'icon' => 'fa fa-fw icon-group',
                        'page' => 'groups'
                    ),
                    array(
                        'label' => 'Messages',
                        'icon' => 'fa fa-comments-o',
                        'page' => 'social_messages'
                    ),
                    array(
                        'label' => 'Widgets',
                        'icon' => 'fa fa-folder',
                        'page' => 'social'
                    ),
                    array(
                        'label' => 'Timelines',
                        'icon' => 'fa fa-clock-o',
                        'page' => 'timeline'
                    ),
                    array(
                        'label' => 'Contacts',
                        'icon' => 'fa fa-phone',
                        'page' => 'contacts'
                    ),
                    array(
                        'label' => 'Ratings',
                        'icon' => 'fa fa-star',
                        'page' => 'ratings'
                    ),
                ],
            ),
            array(
                'label' => 'Email',
                'icon' => 'fa fa-envelope',
                'badge' => ['label' => '30', 'class' => 'badge-primary'],
                'page' => 'email'
            ),
            array(
                'label' => 'Real Estate',
                'icon' => 'fa fa-home',
                'page' => [
                    array(
                        'label' => 'Property Map',
                        'icon' => 'fa fa-map-marker',
                        'page' => 'realestate_listing_map'
                    ),
                    array(
                        'label' => 'Property Grid',
                        'icon' => 'fa fa-th',
                        'page' => 'realestate_listing_grid'
                    ),
                    array(
                        'label' => 'Property List',
                        'icon' => 'fa fa-list',
                        'page' => 'realestate_listing_list'
                    ),
                    array(
                        'label' => 'Property Details',
                        'icon' => 'fa fa-info',
                        'page' => 'realestate_property'
                    ),
                    array(
                        'label' => 'Property Compare',
                        'icon' => 'fa fa-hand-o-right',
                        'page' => 'realestate_compare'
                    ),
                    array(
                        'label' => 'Property Edit',
                        'icon' => 'fa fa-edit',
                        'page' => 'realestate_property_edit'
                    ),
                ],  
            ),
            array(
                'label' => 'Events',
                'icon' => 'fa fa-globe',
                'page' => 'events'
            ),
            array(
                'label' => 'Assignments',
                'icon' => 'fa fa-book',
                'page' => 'assignments'
            ),
            array(
                'label' => 'Multimedia',
                'icon' => 'fa fa-picture-o',
                'badge' => ['label' => '2', 'class' => 'badge-primary'],
                'page' => [
                    array(
                        'label' => 'Video Gallery',
                        'icon' => 'fa fa-video-camera',
                        'page' => 'gallery_video'
                    ),
                    array(
                        'label' => 'Photo Gallery',
                        'icon' => 'fa fa-camera',
                        'page' => 'gallery_photo'
                    ),
                ],
            ),
            array(
                'label' => 'Maps',
                'icon' => 'fa fa-map-marker',
                'page' => [
                    array(
                        'label' => 'Vector Maps',
                        'icon' => 'fa fa-map-marker',
                        'page' => 'maps_vector'
                    ),
                    array(
                        'label' => 'Google Maps',
                        'icon' => 'fa fa-map-marker',
                        'page' => 'maps_google'
                    ),
                    
                ],  
            ),
            array(
                'label' => 'Medical',
                'icon' => 'fa fa-circle-o',
                'page' => [
                    array(
                        'label' => 'Overview',
                        'icon' => 'fa fa-medkit',
                        'page' => 'medical_overview'
                    ),
                    array(
                        'label' => 'Patients',
                        'icon' => 'fa fa-user-md',
                        'badge' => ['label' => '2', 'class' => 'badge-inverse'],
                        'page' => 'medical_patients'
                    ),
                    array(
                        'label' => 'Appointments',
                        'icon' => 'fa fa-stethoscope',
                        'page' => 'medical_appointments'
                    ),
                    array(
                        'label' => 'Memos',
                        'icon' => 'fa fa-file-text-o',
                        'page' => 'medical_memos'
                    ),
                    array(
                        'label' => 'Metrics',
                        'icon' => 'fa fa-bar-chart-o',
                        'page' => 'medical_metrics'
                    ),
                ],
            ),
            array(
                'label' => 'Courses',
                'icon' => 'fa fa-book',
                'page' => [
                    array(
                        'label' => 'Courses Home #1',
                        'icon' => 'fa fa-book',
                        'page' => 'courses_2'
                    ),
                    array(
                        'label' => 'Courses Home #2',
                        'icon' => 'fa fa-book',
                        'page' => 'courses'
                    ),
                    array(
                        'label' => 'Courses Listing',
                        'icon' => 'fa fa-book',
                        'page' => 'courses_listing'
                    ),
                    array(
                        'label' => 'Course Page',
                        'icon' => 'fa fa-book',
                        'page' => 'course'
                    ),
                ],
            ),
            array(
                'label' => 'Content',
                'icon' => 'fa fa-bookmark-o',
                'page' => [
                    array(
                        'label' => 'News',
                        'icon' => 'fa fa-file-text',
                        'page' => 'news'
                    ),
                    array(
                        'label' => 'Stories',
                        'icon' => 'fa fa-file-text-o',
                        'page' => 'stories'
                    ),
                    array(
                        'label' => 'Search',
                        'icon' => 'fa fa-search',
                        'page' => 'search'
                    ),
                    array(
                        'label' => 'Faq',
                        'icon' => 'fa fa-question-circle',
                        'page' => 'faq'
                    ),

                ],
            ),
            array(
                'label' => 'Financial',
                'icon' => 'fa fa-bank',
                'page' => [
                    array(
                        'label' => 'Invoice',
                        'icon' => 'fa fa-file-text-o',
                        'page' => 'invoice'
                    ),
                    array(
                        'label' => 'Finances',
                        'icon' => 'fa fa-legal',
                        'page' => 'finances'
                    ),
                    array(
                        'label' => 'Bookings',
                        'icon' => 'fa fa-ticket',
                        'page' => 'bookings'
                    ),
                ],  
            ),
            array(
                'label' => 'Support Tickets',
                'icon' => 'fa fa-ticket',
                'page' => 'support_tickets'
            ),
            array(
                'label' => 'Shop',
                'icon' => 'fa fa-shopping-cart',
                'page' => 'shop_products'
            ),
            array(
                'label' => 'Elements',
                'icon' => 'fa fa-th',
                'page' => 'ui'
            ),
            array(
                'label' => 'Account',
                'icon' => 'fa fa-lock',
                'page' => [
                    array(
                        'label' => 'Login',
                        'icon' => 'fa fa-lock',
                        'page' => 'login'
                    ),
                    array(
                        'label' => 'Sign Up',
                        'icon' => 'fa fa-pencil',
                        'page' => 'signup'
                    ),
                ],  
            ),
            array(
                'label' => 'Surveys',
                'icon' => 'fa fa-file-text-o',
                'page' => 'survey'
            ),
            array(
                'label' => 'Error',
                'icon' => 'fa fa-warning',
                'page' => 'error'
            ),
        ],
        'email' => [
            array(
                'label' => 'Inbox',
                'icon' => 'fa fa-inbox',
                'badge' => ['label' => '30', 'class' => 'badge-inverse'],
                'page' => 'email'
            ),
            array(
                'label' => 'Sent',
                'icon' => 'fa fa-envelope',
                'badge' => ['label' => '7', 'class' => 'badge-inverse'],
                'href' => '#'
            ),
            array(
                'label' => 'Draft',
                'icon' => 'fa fa-book',
                'href' => '#'
            ),
            array(
                'label' => 'Settings',
                'icon' => 'fa fa-cog',
                'href' => '#'
            ),
            array(
                'label' => 'Email Compose',
                'class' => 'reset',
                'file' => 'partials/email_compose.php'
            ),
            array(
                'label' => 'Filter',
                'class' => 'reset',
                'file' => 'partials/filter.php'
            ),

        ],
        'events' => [
            array(
                'label' => 'Events',
                'icon' => 'fa fa-globe',
                'badge' => ['label' => '5', 'class' => 'badge-inverse'],
                'page' => 'events'
            ),
            array(
                'label' => 'Event 1',
                'class' => 'reset',
                'file' => 'partials/events_1.php'
            ),

            array(
                'label' => 'Event 2',
                'class' => 'reset',
                'file' => 'partials/events_2.php'
            ),

            array(
                'label' => 'Event 3',
                'class' => 'reset',
                'file' => 'partials/events_3.php'
            ),

            array(
                'label' => 'Event 4',
                'class' => 'reset',
                'file' => 'partials/events_4.php'
            ),
            array(
                'label' => 'Event 5',
                'class' => 'reset',
                'file' => 'partials/events_5.php'
            ),

        ],
        'financial' => [
            array(
                'label' => 'Finances',
                'icon' => 'fa fa-legal',
                'page' => 'finances'
            ),
            array(
                'label' => 'Invoice',
                'icon' => 'fa fa-file-text-o',
                'page' => 'invoice'
            ),
            array(
                'label' => 'Bookins',
                'icon' => 'fa fa-ticket',
                'page' => 'bookings'
            ),
        ],
        'media' => [
            array(
                'label' => 'Video Gallyer',
                'icon' => 'fa fa-video-camera',
                'page' => 'gallery_video'
            ),
            array(
                'label' => 'Photo Gallery',
                'icon' => 'fa fa-camera',
                'page' => 'gallery_photo'
            ),

        ],
        'medical' => [
            array(
                'label' => 'Overview',
                'icon' => 'fa fa-medkit',
                'page' => 'medical_overview'
            ),
            array(
                'label' => 'Patients',
                'icon' => 'fa fa-user-md',
                'badge' => [
                    'class' => 'badge-primary',
                    'label' => '2'
                ],
                'page' => 'medical_patients'
            ),
            array(
                'label' => 'Appointments',
                'icon' => 'fa fa-stethoscope',
                'page' => 'medical_appointments'
            ),
            array(
                'label' => 'Memos',
                'icon' => 'fa fa-file-text-o',
                'page' => 'medical_memos'
            ),
            array(
                'label' => 'Metrics',
                'icon' => 'fa fa-bar-chart-o',
                'page' => 'medical_metrics'
            ),
        ],
        'front_modules' => [
            array(
                'label' => 'Multi-Purpose',
                'icon' => 'fa fa-th',
                'page' => 'index',
                'module' => 'front',
                'link_class' => 'no-ajaxify',
                'link_target' => '_blank'
            ),
            array(
                'label' => 'Real Estate',
                'icon' => 'fa fa-home',
                'page' => 'index',
                'module' => 'realestate',
                'link_class' => 'no-ajaxify',
                'link_target' => '_blank'
            ),
            array(
                'label' => 'Online-Shop',
                'icon' => 'fa fa-shopping-cart',
                'page' => 'index',
                'module' => 'shop',
                'link_class' => 'no-ajaxify',
                'link_target' => '_blank'
            ),
            
        ],
        'other' => [
            array(
                'label' => 'Dashboard Analytics',
                'icon' => 'fa fa-bar-chart-o',
                'page' => 'dashboard_analytics'
            ),
            array(
                'label' => 'Dashboard Users',
                'icon' => 'fa fa-user',
                'page' => 'dashboard_users'
            ),
            array(
                'label' => 'Dashboard Overview',
                'icon' => 'fa fa-dashboard',
                'page' => 'dashboard_overview'
            ),
            array(
                'label' => 'Login',
                'icon' => 'fa fa-lock',
                'page' => 'login'
            ),
            array(
                'label' => 'Sign Up',
                'icon' => 'fa fa-pencil',
                'page' => 'signup'
            ),
            array(
                'label' => 'Profile/CV',
                'icon' => 'fa fa-download',
                'page' => 'profile_resume'
            ),
            array(
                'label' => 'Landing',
                'icon' => 'fa fa-share',
                'page' => 'landing'
            ),
            array(
                'label' => 'Employees',
                'icon' => 'fa fa-group',
                'page' => 'employees'
            ),
            array(
                'label' => 'Error',
                'icon' => 'fa fa-warning',
                'page' => 'error'
            ),
            array(
                'label' => 'Elements',
                'icon' => 'fa fa-circle-o',
                'page' => 'ui'
            ),
            

        ],
        'realestate' => [
            array(
                'label' => 'Property Map',
                'icon' => 'fa fa-map-marker',
                'page' => 'realestate_listing_map'
            ),
            array(
                'label' => 'Property Grid',
                'icon' => 'fa fa-th',
                'page' => 'realestate_listing_grid'
            ),
            array(
                'label' => 'Property List',
                'icon' => 'fa fa-list',
                'page' => 'realestate_listing_list'
            ),
            array(
                'label' => 'Property Details',
                'icon' => 'fa fa-info',
                'page' => 'realestate_property'
            ),
            array(
                'label' => 'Property Compare',
                'icon' => 'fa fa-hand-o-right',
                'page' => 'realestate_compare'
            ),
            array(
                'label' => 'Property Add',
                'class' => 'reset',
                'file' => 'partials/realestate.php'
            ),
            
        ],
        'shop' => [
            array(
                'label' => 'Products',
                'icon' => 'fa fa-shopping-cart',
                'page' => 'shop_products'
            ),
            array(
                'label' => 'Add Product',
                'icon' => 'fa fa-pencil',
                'page' => 'shop_edit_product'
            ),

        ],
        'social' => [
            array(
                'label' => 'Profile Avatar',
                'class' => 'reset profile-avatar',
                'file' => 'partials/social_avatar.php'
            ),
            array(
                'label' => 'Activity',
                'icon' => 'fa fa-flash',
                'page' => 'index'
            ),
            array(
                'label' => 'Account',
                'icon' => 'fa fa-male',
                'page' => 'social_account'
            ),
            array(
                'label' => 'Messages',
                'icon' => 'fa fa-comments-o',
                'page' => 'social_messages'
            ),
            array(
                'label' => 'Contacts',
                'icon' => 'fa fa-phone',
                'page' => [
                    array(
                        'label' => 'Friends',
                        'icon' => 'fa fa-group',
                        'page' => 'social_friends'
                    ),
                    array(
                        'label' => 'Contacts #2',
                        'icon' => 'fa fa-phone',
                        'page' => 'contacts_2'
                    ),
                    array(
                        'label' => 'Contacts #1',
                        'icon' => 'fa fa-phone',
                        'page' => 'contacts'
                    ),

                ],
            ),
            array(
                'label' => 'Social Components',
                'icon' => 'fa fa-list',
                'page' => [
                    array(
                        'label' => 'Ratings',
                        'icon' => 'fa fa-star',
                        'page' => 'ratings'
                    ),
                    array(
                        'label' => 'Widgets',
                        'icon' => 'fa fa-folder',
                        'page' => 'social'
                    ),
                    array(
                        'label' => 'Timelines',
                        'icon' => 'fa fa-clock-o',
                        'page' => 'timeline'
                    ),

                ],
            ),
            array(
                'label' => 'FilterAvatar',
                'class' => 'reset',
                'file' => 'partials/filter.php'
            ),
        ],
        'support' => [
            array(
                'label' => 'Support Tickets',
                'icon' => 'fa fa-ticket',
                'badge' => [
                    'class' => 'badge-inverse',
                    'label' => '45'
                ],
                'page' => 'support_tickets'
            ),
            array(
                'label' => 'Forums',
                'icon' => 'fa fa-folder',
                'page' => [
                    array(
                        'label' => 'Overview Page',
                        'icon' => 'fa fa-folder-o',
                        'page' => 'support_forum_overview'
                    ),
                    array(
                        'label' => 'Post Page',
                        'icon' => 'fa fa-folder-o',
                        'page' => 'support_forum_post'
                    ),

                ],
            ),
            array(
                'label' => 'Knowledge Base',
                'icon' => 'fa fa-file-text-o',
                'page' => 'support_kb'
            ),
            array(
                'label' => 'Questions',
                'icon' => 'fa fa-question',
                'badge' => [
                    'class' => 'badge-inverse',
                    'label' => '7'
                ],
                'page' => 'support_questions'
            ),
            array(
                'label' => 'Answers',
                'icon' => 'fa fa-info',
                'page' => 'support_answers'
            ),
            array(
                'label' => 'Filter Support',
                'class' => 'reset',
                'file' => 'partials/support_filter.php'
            ),
        ],
    ]
];